﻿1.How to use Makefile of directory osdrv:
The arm-himix200-linux toolchain for glibc. Specific compiling commands are as follows:
Note:
Due to the rectification of open source tools, some open-source tools do not
provide source code packages. Customers need to download the source code
packages when compiling OSDRV.
a:linux-4.9.37.tar.gz (https://www.kernel.org/pub/)
Save the downloaded linux-4.9.37.tar.gz file to the opensource/kernel
directory of the osdrv.

b:yaffs2utils-0.2.9.tar.gz (https://github.com/dorigom/yaffs2utils/releases)
Save the downloaded yaffs2utils-0.2.9.tar.gz to the tools/pc/mkyaffs2image/
directory of the osdrv.

c:gdb-7.9.1.tar.gz (http://ftp.gnu.org/gnu/gdb/)
Save the downloaded gdb-7.9.1.tar.gz to the tools/board/gdb/ directory of the
osdrv.

d:ncurses-6.0.tar.gz (http://ftp.gnu.org/gnu/ncurses/)
Save the downloaded ncurses-6.0.tar.gz to the tools/board/gdb/ directory of
the osdrv.

e:util-linux-2.31.tar.gz
(https://www.kernel.org/pub/linux/utils/util-linux/v2.31)

Save the downloaded util-linux-2.31.tar.gz to the tools/pc/cramfs_tool/
directory of the osdrv.
(1)Compile the entire osdrv directory:
	Notes: The linux_v4.9.37 source file is not released default. Please download it from www.kernel.org. 
        1) Go to the website: www.kernel.org
		2) Select HTTP protocol resources https://www.kernel.org/pub/ option, enter the sub-page
        3) Select linux/ menu item, enter the sub-page
        4) Select the kernel/ menu item, enter the sub-page
        5) Select v4.x/ menu item, enter sub-page
        6) Download linux-4.9.37.tar.gz (or linux-4.9.37.tar.bz2/linux-4.9.37.tar.xz) to osdrv/opensource/kernel
	       
	make all

	The TARGET_XLSM parameter is using for choosing the
	other reg info files. As below:
	make TARGET_XLSM=*.xlsm

(2)Clear all compiled files under osdrv directory:
	make OSDRV_CROSS=arm-himix200-linux clean
(3)Completely remove all compiled files under osdrv directory, and the generated images:
	make OSDRV_CROSS=arm-himix200-linux distclean
(4)Separately compile kernel:
	Notes: before separately compile kernel, please read the readme_en.txt at osdrv/opensource/kernel.

	Enter the top directory the kernel source code, do the following:
	cp arch/arm/configs/hi3516dv300_smp_defconfig  .config
	make ARCH=arm CROSS_COMPILE=arm-himix200-linux- menuconfig
	make ARCH=arm CROSS_COMPILE=arm-himix200-linux- uImage
(5)Separately compile modules:
	Enter the top directory the kernel source code, do the following:
	cp arch/arm/configs/hi3516dv300_smp_defconfig  .config
	make ARCH=arm CROSS_COMPILE=arm-himix200-linux- menuconfig
	make ARCH=arm CROSS_COMPILE=arm-himix200-linux- modules
(6)Separately compile uboot:
   (Note: The package is released on the DMEB board by default. If the customer's board is inconsistent with the DEMB board, you need to modify the uboot table ac    cording to the customer's own board environment. Otherwise, uboot may not start or other problems may occur.)
    Enter the top directory of boot source code, do the following:
    make ARCH=arm CROSS_COMPILE=arm-himix200-linux- hi3516dv300_defconfig
    (eMMC starts by doing the following: Make Arch=arm cross_compile=arm-himix200-linux-hi3516dv300_emmc_defconfig)

    make ARCH=arm CROSS_COMPILE=arm-himix200-linux-
    
    make -C ../../../tools/pc/hi_gzip

    cp ../../../tools/pc/hi_gzip/bin/gzip arch/arm/cpu/armv7/hi3516dv300/hw_compressed/ -rf 
    
    cp ../../../tools/pc/uboot_tools/reg_info.bin .reg
    
    make ARCH=arm CROSS_COMPILE=arm-himix200-linux- u-boot-z.bin


(7)Build file system image:
        A compiled file system has already been in osdrv/pub/, so no need to re-build file system. What you need to do is to build the right file system image according to the flash specification of the single-board. 
	Filesystem images of jffs2 format is available for spi flash. While making jffs2 image or squashfs image, you need to specify the spi flash block size. flash block size will be printed when uboot runs. run mkfs.jffs2 first to get the right parameters from it's printed information. Here the block size is 64KB, for example:

	jffs2 filesystem image:
	osdrv/pub/bin/pc/mkfs.jffs2 -d osdrv/pub/rootfs_uclibc -l -e 0x10000 -o osdrv/pub/rootfs_uclibc_64k.jffs2
	or:
	osdrv/pub/bin/pc/mkfs.jffs2 -d osdrv/pub/rootfs_glibc -l -e 0x10000 -o osdrv/pub/rootfs_glibc_64k.jffs2

    	Filesystem image of yaffs2 format is available for nand flash. While making yaffs2 image, you need to specify its page size and ECC. This information will be printed when uboot runs. run mkyaffs2image first to get the right parr ameters from it's printed information. Here to 2KB pagesize, 4bit ecc of parallel nand flash, for example:

    	osdrv/pub/bin/pc/mkyaffs2image610 osdrv/pub/rootfs_uclibc osdrv/pub/rootfs_uclibc_2k_4bit.yaffs2 1 2
    	or:
    	osdrv/pub/bin/pc/mkyaffs2image610 osdrv/pub/rootfs_glibc osdrv/pub/rootfs_glibc_2k_4bit.yaffs2 1 2
	
    	UBIFS is available for nand flash. The mkubiimg.sh tool is specialized for making UBIFS image at the path: osdrv/tools/pc/ubi_sh.
    	Here to 2KB pagesize,128KiB blocksize, and 32MB mtdpartition for example:
	
	mkubiimg.sh hi3516dv300 2k 128k osdrv/pub/rootfs 32M osdrv/pub/bin/pc
	
	osdrv/pub/rootfs is the route of rootfs;
	osdrv/pub/bin/pc is the tool path of making UBI iamge;
	rootfs_hi3516dv300_2k_128k_32M.ubifs is the UBIFS image for burning;	
	
2. Output directory description
All compiled images, rootfs are located in directory osdrv/pub/.
pub
├── bin
│   ├── board_glibc -------------------------------------------- tools used on board with himix200
│   │   ├── ethtool
│   │   ├── flashcp
│   │   ├── flash_erase
│   │   ├── flash_otp_dump
│   │   ├── flash_otp_info
│   │   ├── gdb-arm-himix200-linux
│   │   ├── mtd_debug
│   │   ├── mtdinfo
│   │   ├── nanddump
│   │   ├── nandtest
│   │   ├── nandwrite
│   │   ├── sumtool
│   │   ├── ubiattach
│   │   ├── ubicrc32
│   │   ├── ubidetach
│   │   ├── ubiformat
│   │   ├── ubimkvol
│   │   ├── ubinfo
│   │   ├── ubinize
│   │   ├── ubirename
│   │   ├── ubirmvol
│   │   ├── ubirsvol
│   │   └── ubiupdatevol
│   └── pc
│       ├── lzma
│       ├── mkfs.cramfs
│       ├── mkfs.jffs2
│       ├── mkfs.ubifs
│       ├── mkimage
│       ├── mksquashfs
│       └── ubinize
├─image_glibc ------------------------------------------------- Images compiled with himix200
│   ├── uImage_hi3516dv300 ----------------------------------------- kernel imag
│   ├── u-boot-hi3516dv300.bin ------------------------------------- u-boot image
│   ├── rootfs_hi3516dv300_64k.jffs2 ------------------------------- jffs2 rootfs image(SPI NOR flash blocksize = 64K)
│   ├── rootfs_hi3516dv300_128k.jffs2 ------------------------------ jffs2 rootfs image(SPI NOR flash blocksize = 128K)
│   ├── rootfs_hi3516dv300_256k.jffs2 ------------------------------ jffs2 rootfs image(SPI NOR flash blocksize = 256K)
│   ├── rootfs_hi3516dv300_2k_4bit.yaffs2 -------------------------- yaffs2 rootfs image(nand-flash pagesize=2K ecc=4bit)
│   ├── rootfs_hi3516dv300_2k_24bit.yaffs2 ------------------------- yaffs2 rootfs image(nand-flash pagesize=2K ecc=24bit)
│   ├── rootfs_hi3516dv300_4k_4bit.yaffs2 -------------------------- yaffs2 rootfs image(nand-flash pagesize=4K ecc=4bit)
│   ├── rootfs_hi3516dv300_4k_24bit.yaffs2 ------------------------- yaffs2 rootfs image(nand-flash pagesize=4K ecc=24bit)
│   ├── rootfs_hi3516dv300_2k_128k_32M.ubifs------------------------ ubifs rootfs image(SPI nand-flash pagesize=2K blocksize=128K)
│   └── rootfs_hi3516dv300_4k_256k_50M.ubifs------------------------ ubifs rootfs image(SPI nand-flash pagesize=4K blocksize=256K)
│
├─ rootfs.ubiimg ----------------------------------------------- UBIFS rootfs
├─ rootfs_glibc.tgz  ------------------------------------------- rootfs compiled with himix200

3.osdrv directory structure：
osdrv
├─Makefile ------------------------------ osdrv compile script
├─tools --------------------------------- directory of all tools
│  ├─board ------------------------------ A variety of single-board tools
│  │  ├─ethtools ------------------------ ethtools tools
│  │  ├─reg-tools-1.0.0 ----------------- tools for accessing memory space and io space
│  │  ├─mtd-utils ----------------------- tool to read and write flash nude
│  │  ├─udev-167 ------------------------ udev toolset
│  │  ├─gdb ----------------------------- gdb tools
│  │  └─e2fsprogs ----------------------- e2fsprogs tools
│  └─pc ------------------------------------- tools used on PC
│      ├─jffs2_tool-------------------------- tools for making jffs2 file system
│      ├─cramfs_tool ------------------------ tools for making cramfs file system
│      ├─squashfs4.3 ------------------------ tools for making mksquashfs file system
│      ├─nand_production -------------------- nand Production tools
│      ├─lzma_tool -------------------------- lzma compress tool
│      ├─zlib ------------------------------- zlib tool
│      ├─mkyaffs2image -- ------------------- tools for making yaffs2 file system
│      └─uboot_tools ------------------------ tools for creating uboot image,xls files,ddr initialization script and reg_info.bin making tool
├─pub --------------------------------------- output directory
│  ├─image_glibc ---------------------------- images compiled with himix200: uboot,uImage and images of filesystem
│  ├─bin ------------------------------------ tools not placed in the rootfs
│  │  ├─pc ---------------------------------- tools used on the PC
│  │  ├─board_glibc ------------------------- board tools compiled with himix200
│  └─rootfs_glibc.tgz ----------------------- rootfs compiled with himix200
├─opensource--------------------------------- A variety of opensource code
│  ├─busybox -------------------------------- busybox source code
│  ├─uboot ---------------------------------- uboot source code
│  └─kernel --------------------------------- kernel source code
└─rootfs_scripts ---------------------------- scripts to generate rootfs directory

4. Precautions

(1) When the source code package is copied on Windows, executable files on Linux may become non-executable. As a result, compilation cannot be performed. After U-Boot or the kernel is compiled, there are many symbolic link files. Duplicating these source code packages on Windows will increase the sizes of the source code packages because the symbolic link files on Linux become real files on Windows, therefore, the size of the source code package increases. Do not copy a source code package on Windows.
(2) If you need to replace the toolchain which has been used for compiling, clear the compilation file of the toolchain before replacing the toolchain.
(3) Compile the board software
    This chip provides the floating-point operation unit and NEON. The libraries in the file system are compiled with hard floating point and neon compatible with the soft floating point call interface. Therefore, you need to add the -mcpu=cortex-a7, -mfloat-abi=softfp, and -mfpu=neon-vfpv4 options to Makefile during compilation of all board codes.
    The following uses A7 as an example:

    CFLAGS += -mcpu=cortex-a7 -mfloat-abi=softfp -mfpu=neon-vfpv4 -fno-aggressive-loop-optimizations
    CXXFlAGS +=-mcpu=cortex-a7 -mfloat-abi=softfp -mfpu=neon-vfpv4 -fno-aggressive-loop-optimizations

    XX in CXXFlAGS is determined according to the specific name of the macro used in Makefile, for example, CPPFLAGS.