/*
* Copyright (c) 2018 HiSilicon Technologies Co., Ltd.
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
*/
#include "hi_type.h"
#include "hdmi_reg_aon.h"
#include "hdmi_product_define.h"

/* Define the struct pointor of the module tx_aon_reg */
volatile S_tx_aon_reg_REGS_TYPE *goptx_aon_regAllReg = NULL;

int HDMI_TX_S_tx_aon_reg_REGS_TYPE_Init(HI_CHAR *pcAddr)
{
#ifdef HDMI_BUILD_IN_BOOT
    pcAddr = (HI_CHAR *)HDMI_IO_MAP(HDMI_TX_BASE_ADDR, 4);
#else
    if (HI_NULL == pcAddr) {
        HDMI_ERR("pcAddr is NULL! \n");
        return HI_FAILURE;
    }
#endif

    goptx_aon_regAllReg = (volatile S_tx_aon_reg_REGS_TYPE *)(pcAddr + (HDMI_TX_BASE_ADDR_AON));

    return HI_SUCCESS;
}

int HDMI_TX_S_tx_aon_reg_REGS_TYPE_DeInit(void)
{
    if (goptx_aon_regAllReg) {
        goptx_aon_regAllReg = HI_NULL;
    }

    return HI_SUCCESS;
}



#ifndef HDMI_BUILD_IN_BOOT

/******************************************************************************
 * Function    : HDMI_TX_AON_INTR_MASK_aon_intr_mask0Set
 * Description : Set the value of the member TX_AON_INTR_MASK.aon_intr_mask0
 * Input       : unsigned int uaon_intr_mask0: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_TX_AON_INTR_MASK_aon_intr_mask0Set(unsigned int uaon_intr_mask0)
{
    HI_U32 *pu32RegAddr = NULL;

    U_TX_AON_INTR_MASK o_tx_aon_intr_mask;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->TX_AON_INTR_MASK.u32);
    o_tx_aon_intr_mask.u32 = HDMITXRegRead(pu32RegAddr);
    o_tx_aon_intr_mask.bits.aon_intr_mask0 = uaon_intr_mask0;
    HDMITXRegWrite(pu32RegAddr, o_tx_aon_intr_mask.u32);

    return HI_SUCCESS;
}

/******************************************************************************
  Function    : HDMI_TX_AON_INTR_STATE_aon_intr_stat1Set
 * Description : Set the value of the member TX_AON_INTR_STATE.aon_intr_stat1
 * Input       : unsigned int uaon_intr_stat1: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_TX_AON_INTR_STATE_aon_intr_stat1Set(unsigned int uaon_intr_stat1)
{
    HI_U32 *pu32RegAddr = NULL;

    U_TX_AON_INTR_STATE o_tx_aon_intr_state;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->TX_AON_INTR_STATE.u32);
    o_tx_aon_intr_state.u32 = 0;
    o_tx_aon_intr_state.bits.aon_intr_stat1 = uaon_intr_stat1;
    HDMITXRegWrite(pu32RegAddr, o_tx_aon_intr_state.u32);

    return HI_SUCCESS;
}

/******************************************************************************
 * Function    : HDMI_TX_AON_INTR_STATE_aon_intr_stat0Set
 * Description : Set the value of the member TX_AON_INTR_STATE.aon_intr_stat0
 * Input       : unsigned int uaon_intr_stat0: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_TX_AON_INTR_STATE_aon_intr_stat0Set(unsigned int uaon_intr_stat0)
{
    HI_U32 *pu32RegAddr = NULL;

    U_TX_AON_INTR_STATE o_tx_aon_intr_state;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->TX_AON_INTR_STATE.u32);
    o_tx_aon_intr_state.u32 = 0;
    o_tx_aon_intr_state.bits.aon_intr_stat0 = uaon_intr_stat0;
    HDMITXRegWrite(pu32RegAddr, o_tx_aon_intr_state.u32);

    return HI_SUCCESS;
}
#endif

/******************************************************************************
 * Function    : HDMI_DDC_MST_CTRL_dcc_man_enSet
 * Description : Set the value of the member DDC_MST_CTRL.dcc_man_en
 * Input       : unsigned int udcc_man_en: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_DDC_MST_CTRL_dcc_man_enSet(unsigned int udcc_man_en)
{
    HI_U32 *pu32RegAddr = NULL;

    U_DDC_MST_CTRL o_ddc_mst_ctrl;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->DDC_MST_CTRL.u32);
    o_ddc_mst_ctrl.u32 = HDMITXRegRead(pu32RegAddr);
    o_ddc_mst_ctrl.bits.dcc_man_en = udcc_man_en;
    HDMITXRegWrite(pu32RegAddr, o_ddc_mst_ctrl.u32);

    return HI_SUCCESS;
}

/******************************************************************************
 * Function    : HDMI_DDC_MAN_CTRL_ddc_sda_oenSet
 * Description : Set the value of the member DDC_MAN_CTRL.ddc_sda_oen
 * Input       : unsigned int uddc_sda_oen: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_DDC_MAN_CTRL_ddc_sda_oenSet(unsigned int uddc_sda_oen)
{
    HI_U32 *pu32RegAddr = NULL;

    U_DDC_MAN_CTRL o_ddc_man_ctrl;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->DDC_MAN_CTRL.u32);
    o_ddc_man_ctrl.u32 = HDMITXRegRead(pu32RegAddr);
    o_ddc_man_ctrl.bits.ddc_sda_oen = uddc_sda_oen;
    HDMITXRegWrite(pu32RegAddr, o_ddc_man_ctrl.u32);

    return HI_SUCCESS;
}

/******************************************************************************
 * Function    : HDMI_DDC_MAN_CTRL_ddc_scl_oenSet
 * Description : Set the value of the member DDC_MAN_CTRL.ddc_scl_oen
 * Input       : unsigned int uddc_scl_oen: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_DDC_MAN_CTRL_ddc_scl_oenSet(unsigned int uddc_scl_oen)
{
    HI_U32 *pu32RegAddr = NULL;

    U_DDC_MAN_CTRL o_ddc_man_ctrl;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->DDC_MAN_CTRL.u32);
    o_ddc_man_ctrl.u32 = HDMITXRegRead(pu32RegAddr);
    o_ddc_man_ctrl.bits.ddc_scl_oen = uddc_scl_oen;
    HDMITXRegWrite(pu32RegAddr, o_ddc_man_ctrl.u32);

    return HI_SUCCESS;
}

/******************************************************************************
 * Function    : HDMI_DDC_MST_STATE_ddc_i2c_no_ackGet
 * Description : Set the value of the member DDC_MST_STATE.ddc_i2c_no_ack
 * Input       : unsigned int uddc_i2c_no_ack: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_DDC_MST_STATE_ddc_i2c_no_ackGet(void)
{
    HI_U32 *pu32RegAddr = NULL;

    U_DDC_MST_STATE o_ddc_mst_state;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->DDC_MST_STATE.u32);
    o_ddc_mst_state.u32 = HDMITXRegRead(pu32RegAddr);
    return o_ddc_mst_state.bits.ddc_i2c_no_ack;
}

/******************************************************************************
 * Function    : HDMI_DDC_MST_STATE_ddc_i2c_bus_lowGet
 * Description : Set the value of the member DDC_MST_STATE.ddc_i2c_bus_low
 * Input       : unsigned int uddc_i2c_bus_low: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_DDC_MST_STATE_ddc_i2c_bus_lowGet(void)
{
    HI_U32 *pu32RegAddr = NULL;

    U_DDC_MST_STATE o_ddc_mst_state;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->DDC_MST_STATE.u32);
    o_ddc_mst_state.u32 = HDMITXRegRead(pu32RegAddr);
    return o_ddc_mst_state.bits.ddc_i2c_bus_low;
}

#ifndef HDMI_BUILD_IN_BOOT
/******************************************************************************
 * Function    : HDMI_HOTPLUG_ST_CFG_hpd_polarity_ctlGet
 * Description : Set the value of the member HOTPLUG_ST_CFG.hpd_polarity_ctl
 * Input       : unsigned int uhpd_polarity_ctl: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_HOTPLUG_ST_CFG_hpd_polarity_ctlGet(HI_VOID)
{
    HI_U32 *pu32RegAddr = NULL;

    U_HOTPLUG_ST_CFG o_hotplug_st_cfg;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->HOTPLUG_ST_CFG.u32);
    o_hotplug_st_cfg.u32 = HDMITXRegRead(pu32RegAddr);
    return o_hotplug_st_cfg.bits.hpd_polarity_ctl;
}

/******************************************************************************
 * Function    : HDMI_TX_AON_STATE_phy_rx_senseGet
 * Description : Set the value of the member TX_AON_STATE.phy_rx_sense
 * Input       : unsigned int uphy_rx_sense: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_TX_AON_STATE_phy_rx_senseGet(HI_VOID)
{
    HI_U32 *pu32RegAddr = NULL;

    U_TX_AON_STATE o_tx_aon_state;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->TX_AON_STATE.u32);
    o_tx_aon_state.u32 = HDMITXRegRead(pu32RegAddr);
    return o_tx_aon_state.bits.phy_rx_sense;
}

/******************************************************************************
 * Function    : HDMI_TX_AON_STATE_hotplug_stateGet
 * Description : Set the value of the member TX_AON_STATE.hotplug_state
 * Input       : unsigned int uhotplug_state: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_TX_AON_STATE_hotplug_stateGet(HI_VOID)
{
    HI_U32 *pu32RegAddr = NULL;

    U_TX_AON_STATE o_tx_aon_state;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->TX_AON_STATE.u32);
    o_tx_aon_state.u32 = HDMITXRegRead(pu32RegAddr);
    return o_tx_aon_state.bits.hotplug_state;
}

/******************************************************************************
 * Function    : HDMI_TX_AON_INTR_STATE_aon_intr_stat1Get
 * Description : Set the value of the member TX_AON_INTR_STATE.aon_intr_stat1
 * Input       : unsigned int uaon_intr_stat1: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_TX_AON_INTR_STATE_aon_intr_stat1Get(HI_VOID)
{
    HI_U32 *pu32RegAddr = NULL;

    U_TX_AON_INTR_STATE o_tx_aon_intr_state;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->TX_AON_INTR_STATE.u32);
    o_tx_aon_intr_state.u32 = HDMITXRegRead(pu32RegAddr);
    return o_tx_aon_intr_state.bits.aon_intr_stat1;
}

/******************************************************************************
 * Function    : HDMI_TX_AON_INTR_STATE_aon_intr_stat0Get
 * Description : Set the value of the member TX_AON_INTR_STATE.aon_intr_stat0
 * Input       : unsigned int uaon_intr_stat0: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_TX_AON_INTR_STATE_aon_intr_stat0Get(HI_VOID)
{
    HI_U32 *pu32RegAddr = NULL;

    U_TX_AON_INTR_STATE o_tx_aon_intr_state;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->TX_AON_INTR_STATE.u32);
    o_tx_aon_intr_state.u32 = HDMITXRegRead(pu32RegAddr);
    return o_tx_aon_intr_state.bits.aon_intr_stat0;
}
#endif

/******************************************************************************
 * Function    : HDMI_DDC_MAN_CTRL_ddc_sda_stGet
 * Description : Set the value of the member DDC_MAN_CTRL.ddc_sda_st
 * Input       : unsigned int uddc_sda_st: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_DDC_MAN_CTRL_ddc_sda_stGet(HI_VOID)
{
    HI_U32 *pu32RegAddr = NULL;

    U_DDC_MAN_CTRL o_ddc_man_ctrl;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->DDC_MAN_CTRL.u32);
    o_ddc_man_ctrl.u32 = HDMITXRegRead(pu32RegAddr);
    return o_ddc_man_ctrl.bits.ddc_sda_st;
}

/******************************************************************************
 * Function    : HDMI_DDC_MAN_CTRL_ddc_scl_stGet
 * Description : Set the value of the member DDC_MAN_CTRL.ddc_scl_st
 * Input       : unsigned int uddc_scl_st: 1 bits
 * Return      : int: 0-Error, 1-Success
******************************************************************************/
int HDMI_DDC_MAN_CTRL_ddc_scl_stGet(HI_VOID)
{
    HI_U32 *pu32RegAddr = NULL;

    U_DDC_MAN_CTRL o_ddc_man_ctrl;
    pu32RegAddr = (HI_U32*)&(goptx_aon_regAllReg->DDC_MAN_CTRL.u32);
    o_ddc_man_ctrl.u32 = HDMITXRegRead(pu32RegAddr);
    return o_ddc_man_ctrl.bits.ddc_scl_st;
}


