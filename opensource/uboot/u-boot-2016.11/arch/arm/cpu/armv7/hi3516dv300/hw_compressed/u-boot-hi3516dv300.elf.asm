
u-boot-hi3516dv300.elf:     file format elf32-littlearm


Disassembly of section .text:

80700000 <__image_copy_start>:
80700000:	ea000815 	b	8070205c <reset>
80700004:	eafffffe 	b	80700004 <__image_copy_start+0x4>
80700008:	eafffffe 	b	80700008 <__image_copy_start+0x8>
8070000c:	eafffffe 	b	8070000c <__image_copy_start+0xc>
80700010:	eafffffe 	b	80700010 <__image_copy_start+0x10>
80700014:	eafffffe 	b	80700014 <__image_copy_start+0x14>
80700018:	eafffffe 	b	80700018 <__image_copy_start+0x18>
8070001c:	eafffffe 	b	8070001c <__image_copy_start+0x1c>
80700020:	deadbeef 	.word	0xdeadbeef
80700024:	deadbeef 	.word	0xdeadbeef
80700028:	deadbeef 	.word	0xdeadbeef
8070002c:	deadbeef 	.word	0xdeadbeef
80700030:	deadbeef 	.word	0xdeadbeef
80700034:	deadbeef 	.word	0xdeadbeef
80700038:	deadbeef 	.word	0xdeadbeef
8070003c:	deadbeef 	.word	0xdeadbeef

80700040 <__blank_zone_start>:
	...

80702040 <_blank_zone_start>:
80702040:	80700040 	.word	0x80700040

80702044 <_blank_zone_end>:
80702044:	80702040 	.word	0x80702040
80702048:	deadbeef 	.word	0xdeadbeef
8070204c:	deadbeef 	.word	0xdeadbeef

80702050 <_TEXT_BASE>:
80702050:	80700000 	.word	0x80700000

80702054 <_clr_remap_fmc_entry>:
80702054:	14002154 	.word	0x14002154

80702058 <_start_armboot>:
80702058:	807050b4 	.word	0x807050b4

8070205c <reset>:
8070205c:	ea000087 	b	80702280 <save_boot_params>

80702060 <save_boot_params_ret>:
80702060:	e10f0000 	mrs	r0, CPSR
80702064:	e200101f 	and	r1, r0, #31
80702068:	e331001a 	teq	r1, #26
8070206c:	13c0001f 	bicne	r0, r0, #31
80702070:	13800013 	orrne	r0, r0, #19
80702074:	e38000c0 	orr	r0, r0, #192	; 0xc0
80702078:	e129f000 	msr	CPSR_fc, r0
8070207c:	ee110f10 	mrc	15, 0, r0, cr1, cr0, {0}
80702080:	e3c00a02 	bic	r0, r0, #8192	; 0x2000
80702084:	ee010f10 	mcr	15, 0, r0, cr1, cr0, {0}
80702088:	e24f0090 	sub	r0, pc, #144	; 0x90
8070208c:	e2400c20 	sub	r0, r0, #32, 24	; 0x2000
80702090:	ee0c0f10 	mcr	15, 0, r0, cr12, cr0, {0}
80702094:	e59f023c 	ldr	r0, [pc, #572]	; 807022d8 <cpu_init_cp15+0x54>
80702098:	e5901140 	ldr	r1, [r0, #320]	; 0x140
8070209c:	e59f2238 	ldr	r2, [pc, #568]	; 807022dc <cpu_init_cp15+0x58>
807020a0:	e1510002 	cmp	r1, r2
807020a4:	1a000019 	bne	80702110 <normal_start_flow>
807020a8:	e1a0100d 	mov	r1, sp
807020ac:	e5801140 	str	r1, [r0, #320]	; 0x140
807020b0:	e51f0078 	ldr	r0, [pc, #-120]	; 80702040 <_blank_zone_start>
807020b4:	e51f106c 	ldr	r1, [pc, #-108]	; 80702050 <_TEXT_BASE>
807020b8:	e0400001 	sub	r0, r0, r1
807020bc:	e59f121c 	ldr	r1, [pc, #540]	; 807022e0 <cpu_init_cp15+0x5c>
807020c0:	e59fd21c 	ldr	sp, [pc, #540]	; 807022e4 <cpu_init_cp15+0x60>
807020c4:	e0800001 	add	r0, r0, r1
807020c8:	e3a01000 	mov	r1, #0
807020cc:	eb00008a 	bl	807022fc <init_registers>
807020d0:	e59f0200 	ldr	r0, [pc, #512]	; 807022d8 <cpu_init_cp15+0x54>
807020d4:	eb0001d5 	bl	80702830 <start_ddr_training>
807020d8:	e59f01f8 	ldr	r0, [pc, #504]	; 807022d8 <cpu_init_cp15+0x54>
807020dc:	e5901140 	ldr	r1, [r0, #320]	; 0x140
807020e0:	e1a0d001 	mov	sp, r1
807020e4:	e5901144 	ldr	r1, [r0, #324]	; 0x144
807020e8:	e1a0f001 	mov	pc, r1
807020ec:	e320f000 	nop	{0}
807020f0:	e320f000 	nop	{0}
807020f4:	e320f000 	nop	{0}
807020f8:	e320f000 	nop	{0}
807020fc:	e320f000 	nop	{0}
80702100:	e320f000 	nop	{0}
80702104:	e320f000 	nop	{0}
80702108:	e320f000 	nop	{0}
8070210c:	eafffffe 	b	8070210c <save_boot_params_ret+0xac>

80702110 <normal_start_flow>:
80702110:	e59fd1cc 	ldr	sp, [pc, #460]	; 807022e4 <cpu_init_cp15+0x60>
80702114:	eb000a86 	bl	80704b34 <uart_early_init>
80702118:	eb00004a 	bl	80702248 <msg_main_cpu_startup>
8070211c:	e59f01c4 	ldr	r0, [pc, #452]	; 807022e8 <cpu_init_cp15+0x64>
80702120:	e59f31c4 	ldr	r3, [pc, #452]	; 807022ec <cpu_init_cp15+0x68>
80702124:	e5803020 	str	r3, [r0, #32]
80702128:	e3a03001 	mov	r3, #1
8070212c:	e5803000 	str	r3, [r0]
80702130:	e1a00c2f 	lsr	r0, pc, #24
80702134:	e3500000 	cmp	r0, #0
80702138:	1a000005 	bne	80702154 <do_clr_remap>

8070213c <check_boot_type>:
8070213c:	e59f0194 	ldr	r0, [pc, #404]	; 807022d8 <cpu_init_cp15+0x54>
80702140:	e590008c 	ldr	r0, [r0, #140]	; 0x8c
80702144:	e1a06220 	lsr	r6, r0, #4
80702148:	e2066001 	and	r6, r6, #1
8070214c:	e3560001 	cmp	r6, #1
80702150:	351ff104 	ldrcc	pc, [pc, #-260]	; 80702054 <_clr_remap_fmc_entry>

80702154 <do_clr_remap>:
80702154:	e59f417c 	ldr	r4, [pc, #380]	; 807022d8 <cpu_init_cp15+0x54>
80702158:	e5940000 	ldr	r0, [r4]
8070215c:	e3800c01 	orr	r0, r0, #256	; 0x100
80702160:	e5840000 	str	r0, [r4]
80702164:	ee110f10 	mrc	15, 0, r0, cr1, cr0, {0}
80702168:	e3800a01 	orr	r0, r0, #4096	; 0x1000
8070216c:	ee010f10 	mcr	15, 0, r0, cr1, cr0, {0}
80702170:	e1a00e2f 	lsr	r0, pc, #28
80702174:	e3500008 	cmp	r0, #8
80702178:	3a000002 	bcc	80702188 <ddr_init>

8070217c <no_ddr_init>:
8070217c:	e24f0084 	sub	r0, pc, #132	; 0x84
80702180:	e2400c21 	sub	r0, r0, #8448	; 0x2100
80702184:	ea000017 	b	807021e8 <copy_to_ddr>

80702188 <ddr_init>:
80702188:	e51f0150 	ldr	r0, [pc, #-336]	; 80702040 <_blank_zone_start>
8070218c:	e51f1144 	ldr	r1, [pc, #-324]	; 80702050 <_TEXT_BASE>
80702190:	e0400001 	sub	r0, r0, r1
80702194:	e24f109c 	sub	r1, pc, #156	; 0x9c
80702198:	e2411c21 	sub	r1, r1, #8448	; 0x2100
8070219c:	e0800001 	add	r0, r0, r1
807021a0:	e3a01000 	mov	r1, #0
807021a4:	eb000054 	bl	807022fc <init_registers>
807021a8:	e59fd134 	ldr	sp, [pc, #308]	; 807022e4 <cpu_init_cp15+0x60>
807021ac:	e59f0124 	ldr	r0, [pc, #292]	; 807022d8 <cpu_init_cp15+0x54>
807021b0:	eb00019e 	bl	80702830 <start_ddr_training>

807021b4 <check_boot_mode>:
807021b4:	e59f011c 	ldr	r0, [pc, #284]	; 807022d8 <cpu_init_cp15+0x54>
807021b8:	e590008c 	ldr	r0, [r0, #140]	; 0x8c
807021bc:	e1a06220 	lsr	r6, r0, #4
807021c0:	e2066001 	and	r6, r6, #1
807021c4:	e3560001 	cmp	r6, #1
807021c8:	1a000005 	bne	807021e4 <copy_flash_to_ddr>

807021cc <emmc_boot>:
807021cc:	e51f0184 	ldr	r0, [pc, #-388]	; 80702050 <_TEXT_BASE>
807021d0:	e59f1118 	ldr	r1, [pc, #280]	; 807022f0 <cpu_init_cp15+0x6c>
807021d4:	e59f2118 	ldr	r2, [pc, #280]	; 807022f4 <cpu_init_cp15+0x70>
807021d8:	e0421001 	sub	r1, r2, r1
807021dc:	eb000a87 	bl	80704c00 <emmc_boot_read>
807021e0:	ea000007 	b	80702204 <relocate>

807021e4 <copy_flash_to_ddr>:
807021e4:	e3a00305 	mov	r0, #335544320	; 0x14000000

807021e8 <copy_to_ddr>:
807021e8:	e59f1100 	ldr	r1, [pc, #256]	; 807022f0 <cpu_init_cp15+0x6c>
807021ec:	e1500001 	cmp	r0, r1
807021f0:	0a000baf 	beq	807050b4 <start_armboot>
807021f4:	e59f20f4 	ldr	r2, [pc, #244]	; 807022f0 <cpu_init_cp15+0x6c>
807021f8:	e59f30f4 	ldr	r3, [pc, #244]	; 807022f4 <cpu_init_cp15+0x70>
807021fc:	e0432002 	sub	r2, r3, r2
80702200:	eb00000a 	bl	80702230 <memcpy>

80702204 <relocate>:
80702204:	e59f00ec 	ldr	r0, [pc, #236]	; 807022f8 <cpu_init_cp15+0x74>
80702208:	e590f000 	ldr	pc, [r0]

8070220c <bug>:
8070220c:	e320f000 	nop	{0}
80702210:	e320f000 	nop	{0}
80702214:	e320f000 	nop	{0}
80702218:	e320f000 	nop	{0}
8070221c:	e320f000 	nop	{0}
80702220:	e320f000 	nop	{0}
80702224:	e320f000 	nop	{0}
80702228:	e320f000 	nop	{0}
8070222c:	eafffffe 	b	8070222c <bug+0x20>

80702230 <memcpy>:
80702230:	e0802002 	add	r2, r0, r2

80702234 <memcpy_loop>:
80702234:	e8b007f8 	ldm	r0!, {r3, r4, r5, r6, r7, r8, r9, sl}
80702238:	e8a107f8 	stmia	r1!, {r3, r4, r5, r6, r7, r8, r9, sl}
8070223c:	e1500002 	cmp	r0, r2
80702240:	dafffffb 	ble	80702234 <memcpy_loop>
80702244:	e1a0f00e 	mov	pc, lr

80702248 <msg_main_cpu_startup>:
80702248:	e1a0500e 	mov	r5, lr
8070224c:	e28f0004 	add	r0, pc, #4
80702250:	eb000a44 	bl	80704b68 <uart_early_puts>
80702254:	e1a0f005 	mov	pc, r5

80702258 <L10>:
80702258:	0a0d0a0d 	.word	0x0a0d0a0d
8070225c:	74737953 	.word	0x74737953
80702260:	73206d65 	.word	0x73206d65
80702264:	74726174 	.word	0x74726174
80702268:	0a0d7075 	.word	0x0a0d7075
8070226c:	00          	.byte	0x00
8070226d:	00          	.byte	0x00
	...

80702270 <c_runtime_cpu_setup>:
80702270:	ee070f15 	mcr	15, 0, r0, cr7, cr5, {0}
80702274:	ee070f9a 	mcr	15, 0, r0, cr7, cr10, {4}
80702278:	ee070f95 	mcr	15, 0, r0, cr7, cr5, {4}
8070227c:	e12fff1e 	bx	lr

80702280 <save_boot_params>:
80702280:	eaffff76 	b	80702060 <save_boot_params_ret>

80702284 <cpu_init_cp15>:
80702284:	e3a00000 	mov	r0, #0
80702288:	ee080f17 	mcr	15, 0, r0, cr8, cr7, {0}
8070228c:	ee070f15 	mcr	15, 0, r0, cr7, cr5, {0}
80702290:	ee070fd5 	mcr	15, 0, r0, cr7, cr5, {6}
80702294:	ee070f9a 	mcr	15, 0, r0, cr7, cr10, {4}
80702298:	ee070f95 	mcr	15, 0, r0, cr7, cr5, {4}
8070229c:	ee110f10 	mrc	15, 0, r0, cr1, cr0, {0}
807022a0:	e3c00a02 	bic	r0, r0, #8192	; 0x2000
807022a4:	e3c00007 	bic	r0, r0, #7
807022a8:	e3800002 	orr	r0, r0, #2
807022ac:	e3800b02 	orr	r0, r0, #2048	; 0x800
807022b0:	e3800a01 	orr	r0, r0, #4096	; 0x1000
807022b4:	ee010f10 	mcr	15, 0, r0, cr1, cr0, {0}
807022b8:	e1a0500e 	mov	r5, lr
807022bc:	ee101f10 	mrc	15, 0, r1, cr0, cr0, {0}
807022c0:	e1a03a21 	lsr	r3, r1, #20
807022c4:	e203300f 	and	r3, r3, #15
807022c8:	e201400f 	and	r4, r1, #15
807022cc:	e1a02203 	lsl	r2, r3, #4
807022d0:	e1842002 	orr	r2, r4, r2
807022d4:	e1a0f005 	mov	pc, r5
807022d8:	12020000 	.word	0x12020000
807022dc:	7a696a75 	.word	0x7a696a75
807022e0:	04010500 	.word	0x04010500
807022e4:	0401a000 	.word	0x0401a000
807022e8:	12040000 	.word	0x12040000
807022ec:	02faf080 	.word	0x02faf080
807022f0:	80700000 	.word	0x80700000
807022f4:	8074b85c 	.word	0x8074b85c
807022f8:	80702058 	.word	0x80702058

807022fc <init_registers>:
807022fc:	e92d40f0 	push	{r4, r5, r6, r7, lr}
80702300:	e3a04001 	mov	r4, #1
80702304:	e590e000 	ldr	lr, [r0]
80702308:	e5902008 	ldr	r2, [r0, #8]
8070230c:	e35e0000 	cmp	lr, #0
80702310:	e590300c 	ldr	r3, [r0, #12]
80702314:	1a000006 	bne	80702334 <init_registers+0x38>
80702318:	e590c004 	ldr	ip, [r0, #4]
8070231c:	e35c0000 	cmp	ip, #0
80702320:	1a000003 	bne	80702334 <init_registers+0x38>
80702324:	e3520000 	cmp	r2, #0
80702328:	1a000001 	bne	80702334 <init_registers+0x38>
8070232c:	e3530000 	cmp	r3, #0
80702330:	0a000052 	beq	80702480 <init_registers+0x184>
80702334:	e3510000 	cmp	r1, #0
80702338:	0a000028 	beq	807023e0 <init_registers+0xe4>
8070233c:	e3130002 	tst	r3, #2
80702340:	0a000010 	beq	80702388 <init_registers+0x8c>
80702344:	e7e4c1d3 	ubfx	ip, r3, #3, #5
80702348:	e5905004 	ldr	r5, [r0, #4]
8070234c:	e28cc001 	add	ip, ip, #1
80702350:	e59e6000 	ldr	r6, [lr]
80702354:	e35c0020 	cmp	ip, #32
80702358:	11a0cc14 	lslne	ip, r4, ip
8070235c:	17e435d3 	ubfxne	r3, r3, #11, #5
80702360:	124cc001 	subne	ip, ip, #1
80702364:	01a0c005 	moveq	ip, r5
80702368:	11c6c31c 	bicne	ip, r6, ip, lsl r3
8070236c:	118cc315 	orrne	ip, ip, r5, lsl r3
80702370:	e58ec000 	str	ip, [lr]
80702374:	e320f000 	nop	{0}
80702378:	e2522001 	subs	r2, r2, #1
8070237c:	2afffffc 	bcs	80702374 <init_registers+0x78>
80702380:	e2800010 	add	r0, r0, #16
80702384:	eaffffde 	b	80702304 <init_registers+0x8>
80702388:	e3130802 	tst	r3, #131072	; 0x20000
8070238c:	0a00000f 	beq	807023d0 <init_registers+0xd4>
80702390:	e7e4c9d3 	ubfx	ip, r3, #19, #5
80702394:	e5907004 	ldr	r7, [r0, #4]
80702398:	e28cc001 	add	ip, ip, #1
8070239c:	e1a03da3 	lsr	r3, r3, #27
807023a0:	e1a06c14 	lsl	r6, r4, ip
807023a4:	e2466001 	sub	r6, r6, #1
807023a8:	e59e5000 	ldr	r5, [lr]
807023ac:	e35c0020 	cmp	ip, #32
807023b0:	10065335 	andne	r5, r6, r5, lsr r3
807023b4:	e320f000 	nop	{0}
807023b8:	e1570005 	cmp	r7, r5
807023bc:	1afffff9 	bne	807023a8 <init_registers+0xac>
807023c0:	e320f000 	nop	{0}
807023c4:	e2522001 	subs	r2, r2, #1
807023c8:	2afffffc 	bcs	807023c0 <init_registers+0xc4>
807023cc:	eaffffeb 	b	80702380 <init_registers+0x84>
807023d0:	e320f000 	nop	{0}
807023d4:	e2522001 	subs	r2, r2, #1
807023d8:	2afffffc 	bcs	807023d0 <init_registers+0xd4>
807023dc:	eaffffe7 	b	80702380 <init_registers+0x84>
807023e0:	e3130004 	tst	r3, #4
807023e4:	0a00000f 	beq	80702428 <init_registers+0x12c>
807023e8:	e7e4c1d3 	ubfx	ip, r3, #3, #5
807023ec:	e5905004 	ldr	r5, [r0, #4]
807023f0:	e28cc001 	add	ip, ip, #1
807023f4:	e59e6000 	ldr	r6, [lr]
807023f8:	e35c0020 	cmp	ip, #32
807023fc:	11a0cc14 	lslne	ip, r4, ip
80702400:	17e435d3 	ubfxne	r3, r3, #11, #5
80702404:	124cc001 	subne	ip, ip, #1
80702408:	01a0c005 	moveq	ip, r5
8070240c:	11c6c31c 	bicne	ip, r6, ip, lsl r3
80702410:	118cc315 	orrne	ip, ip, r5, lsl r3
80702414:	e58ec000 	str	ip, [lr]
80702418:	e320f000 	nop	{0}
8070241c:	e2522001 	subs	r2, r2, #1
80702420:	2afffffc 	bcs	80702418 <init_registers+0x11c>
80702424:	eaffffd5 	b	80702380 <init_registers+0x84>
80702428:	e3130701 	tst	r3, #262144	; 0x40000
8070242c:	0a00000f 	beq	80702470 <init_registers+0x174>
80702430:	e7e4c9d3 	ubfx	ip, r3, #19, #5
80702434:	e5907004 	ldr	r7, [r0, #4]
80702438:	e28cc001 	add	ip, ip, #1
8070243c:	e1a03da3 	lsr	r3, r3, #27
80702440:	e1a06c14 	lsl	r6, r4, ip
80702444:	e2466001 	sub	r6, r6, #1
80702448:	e59e5000 	ldr	r5, [lr]
8070244c:	e35c0020 	cmp	ip, #32
80702450:	10065335 	andne	r5, r6, r5, lsr r3
80702454:	e320f000 	nop	{0}
80702458:	e1570005 	cmp	r7, r5
8070245c:	1afffff9 	bne	80702448 <init_registers+0x14c>
80702460:	e320f000 	nop	{0}
80702464:	e2522001 	subs	r2, r2, #1
80702468:	2afffffc 	bcs	80702460 <init_registers+0x164>
8070246c:	eaffffc3 	b	80702380 <init_registers+0x84>
80702470:	e320f000 	nop	{0}
80702474:	e2522001 	subs	r2, r2, #1
80702478:	2afffffc 	bcs	80702470 <init_registers+0x174>
8070247c:	eaffffbf 	b	80702380 <init_registers+0x84>
80702480:	e320f000 	nop	{0}
80702484:	e8bd80f0 	pop	{r4, r5, r6, r7, pc}

80702488 <get_random_num>:
80702488:	e59f2014 	ldr	r2, [pc, #20]	; 807024a4 <get_random_num+0x1c>
8070248c:	e5923208 	ldr	r3, [r2, #520]	; 0x208
80702490:	e7e73453 	ubfx	r3, r3, #8, #8
80702494:	e3530000 	cmp	r3, #0
80702498:	0afffffb 	beq	8070248c <get_random_num+0x4>
8070249c:	e5920204 	ldr	r0, [r2, #516]	; 0x204
807024a0:	e12fff1e 	bx	lr
807024a4:	10090000 	.word	0x10090000

807024a8 <trng_init>:
807024a8:	e59f2018 	ldr	r2, [pc, #24]	; 807024c8 <trng_init+0x20>
807024ac:	e59231a0 	ldr	r3, [r2, #416]	; 0x1a0
807024b0:	e3833008 	orr	r3, r3, #8
807024b4:	e58231a0 	str	r3, [r2, #416]	; 0x1a0
807024b8:	e59f300c 	ldr	r3, [pc, #12]	; 807024cc <trng_init+0x24>
807024bc:	e3a0200a 	mov	r2, #10
807024c0:	e5832200 	str	r2, [r3, #512]	; 0x200
807024c4:	e12fff1e 	bx	lr
807024c8:	12010000 	.word	0x12010000
807024cc:	10090000 	.word	0x10090000

807024d0 <trng_deinit>:
807024d0:	e59f200c 	ldr	r2, [pc, #12]	; 807024e4 <trng_deinit+0x14>
807024d4:	e59231a0 	ldr	r3, [r2, #416]	; 0x1a0
807024d8:	e3c33008 	bic	r3, r3, #8
807024dc:	e58231a0 	str	r3, [r2, #416]	; 0x1a0
807024e0:	e12fff1e 	bx	lr
807024e4:	12010000 	.word	0x12010000

807024e8 <start_svb>:
807024e8:	e59f12e8 	ldr	r1, [pc, #744]	; 807027d8 <start_svb+0x2f0>
807024ec:	e92d41f0 	push	{r4, r5, r6, r7, r8, lr}
807024f0:	e24dd018 	sub	sp, sp, #24
807024f4:	e5912ee0 	ldr	r2, [r1, #3808]	; 0xee0
807024f8:	e59f02dc 	ldr	r0, [pc, #732]	; 807027dc <start_svb+0x2f4>
807024fc:	e59130ac 	ldr	r3, [r1, #172]	; 0xac
80702500:	e1520000 	cmp	r2, r0
80702504:	1a000085 	bne	80702720 <start_svb+0x238>
80702508:	e3c338ff 	bic	r3, r3, #16711680	; 0xff0000
8070250c:	e3833601 	orr	r3, r3, #1048576	; 0x100000
80702510:	e59fe2c8 	ldr	lr, [pc, #712]	; 807027e0 <start_svb+0x2f8>
80702514:	e3a06004 	mov	r6, #4
80702518:	e58130ac 	str	r3, [r1, #172]	; 0xac
8070251c:	e302770f 	movw	r7, #9999	; 0x270f
80702520:	e59e20bc 	ldr	r2, [lr, #188]	; 0xbc
80702524:	e7e93052 	ubfx	r3, r2, #0, #10
80702528:	e30026a8 	movw	r2, #1704	; 0x6a8
8070252c:	e0020392 	mul	r2, r2, r3
80702530:	e3a03000 	mov	r3, #0
80702534:	e58d3008 	str	r3, [sp, #8]
80702538:	e58d300c 	str	r3, [sp, #12]
8070253c:	e2422be2 	sub	r2, r2, #231424	; 0x38800
80702540:	e58d3010 	str	r3, [sp, #16]
80702544:	e2422d05 	sub	r2, r2, #320	; 0x140
80702548:	e58d3014 	str	r3, [sp, #20]
8070254c:	e1a026a2 	lsr	r2, r2, #13
80702550:	e2422028 	sub	r2, r2, #40	; 0x28
80702554:	e352007d 	cmp	r2, #125	; 0x7d
80702558:	a3a0207d 	movge	r2, #125	; 0x7d
8070255c:	e58d3004 	str	r3, [sp, #4]
80702560:	e59d1004 	ldr	r1, [sp, #4]
80702564:	e1510007 	cmp	r1, r7
80702568:	9a00007a 	bls	80702758 <start_svb+0x270>
8070256c:	e59e10d8 	ldr	r1, [lr, #216]	; 0xd8
80702570:	e2566001 	subs	r6, r6, #1
80702574:	e59dc00c 	ldr	ip, [sp, #12]
80702578:	e7e95851 	ubfx	r5, r1, #16, #10
8070257c:	e7e91051 	ubfx	r1, r1, #0, #10
80702580:	e085500c 	add	r5, r5, ip
80702584:	e59dc008 	ldr	ip, [sp, #8]
80702588:	e58d500c 	str	r5, [sp, #12]
8070258c:	e081100c 	add	r1, r1, ip
80702590:	e58d1008 	str	r1, [sp, #8]
80702594:	e59ec0dc 	ldr	ip, [lr, #220]	; 0xdc
80702598:	e59d4014 	ldr	r4, [sp, #20]
8070259c:	e7e9885c 	ubfx	r8, ip, #16, #10
807025a0:	e7e9c05c 	ubfx	ip, ip, #0, #10
807025a4:	e0884004 	add	r4, r8, r4
807025a8:	e59d8010 	ldr	r8, [sp, #16]
807025ac:	e58d4014 	str	r4, [sp, #20]
807025b0:	e08cc008 	add	ip, ip, r8
807025b4:	e58dc010 	str	ip, [sp, #16]
807025b8:	1affffe7 	bne	8070255c <start_svb+0x74>
807025bc:	e1a01121 	lsr	r1, r1, #2
807025c0:	e0813125 	add	r3, r1, r5, lsr #2
807025c4:	e083312c 	add	r3, r3, ip, lsr #2
807025c8:	e0833124 	add	r3, r3, r4, lsr #2
807025cc:	e1a03123 	lsr	r3, r3, #2
807025d0:	e3530f46 	cmp	r3, #280	; 0x118
807025d4:	3a000064 	bcc	8070276c <start_svb+0x284>
807025d8:	e3520045 	cmp	r2, #69	; 0x45
807025dc:	9a000064 	bls	80702774 <start_svb+0x28c>
807025e0:	e3a010cd 	mov	r1, #205	; 0xcd
807025e4:	e0020291 	mul	r2, r1, r2
807025e8:	e2422b0e 	sub	r2, r2, #14336	; 0x3800
807025ec:	e242200e 	sub	r2, r2, #14
807025f0:	e0833522 	add	r3, r3, r2, lsr #10
807025f4:	e2833006 	add	r3, r3, #6
807025f8:	e59f11d8 	ldr	r1, [pc, #472]	; 807027d8 <start_svb+0x2f0>
807025fc:	e3530095 	cmp	r3, #149	; 0x95
80702600:	93a03096 	movls	r3, #150	; 0x96
80702604:	e5912098 	ldr	r2, [r1, #152]	; 0x98
80702608:	e7d8281f 	bfc	r2, #16, #9
8070260c:	93822301 	orrls	r2, r2, #67108864	; 0x4000000
80702610:	9a000004 	bls	80702628 <start_svb+0x140>
80702614:	e300c15e 	movw	ip, #350	; 0x15e
80702618:	e153000c 	cmp	r3, ip
8070261c:	97da2d1f 	bfcls	r2, #26, #1
80702620:	83822301 	orrhi	r2, r2, #67108864	; 0x4000000
80702624:	81a0300c 	movhi	r3, ip
80702628:	e7d82813 	bfi	r2, r3, #16, #9
8070262c:	e5812098 	str	r2, [r1, #152]	; 0x98
80702630:	e59f21ac 	ldr	r2, [pc, #428]	; 807027e4 <start_svb+0x2fc>
80702634:	e592e02c 	ldr	lr, [r2, #44]	; 0x2c
80702638:	e5912ee0 	ldr	r2, [r1, #3808]	; 0xee0
8070263c:	e1520000 	cmp	r2, r0
80702640:	0a00004e 	beq	80702780 <start_svb+0x298>
80702644:	e59f119c 	ldr	r1, [pc, #412]	; 807027e8 <start_svb+0x300>
80702648:	e1520001 	cmp	r2, r1
8070264c:	0a000050 	beq	80702794 <start_svb+0x2ac>
80702650:	e2811842 	add	r1, r1, #4325376	; 0x420000
80702654:	e2811c3d 	add	r1, r1, #15616	; 0x3d00
80702658:	e1520001 	cmp	r2, r1
8070265c:	0a000051 	beq	807027a8 <start_svb+0x2c0>
80702660:	e2411803 	sub	r1, r1, #196608	; 0x30000
80702664:	e59fc180 	ldr	ip, [pc, #384]	; 807027ec <start_svb+0x304>
80702668:	e1520001 	cmp	r2, r1
8070266c:	e30015dc 	movw	r1, #1500	; 0x5dc
80702670:	e3002662 	movw	r2, #1634	; 0x662
80702674:	e59f4174 	ldr	r4, [pc, #372]	; 807027f0 <start_svb+0x308>
80702678:	11a02001 	movne	r2, r1
8070267c:	e59f1170 	ldr	r1, [pc, #368]	; 807027f4 <start_svb+0x30c>
80702680:	e59f0170 	ldr	r0, [pc, #368]	; 807027f8 <start_svb+0x310>
80702684:	11a0c001 	movne	ip, r1
80702688:	e59f116c 	ldr	r1, [pc, #364]	; 807027fc <start_svb+0x314>
8070268c:	11a00001 	movne	r0, r1
80702690:	e59f1168 	ldr	r1, [pc, #360]	; 80702800 <start_svb+0x318>
80702694:	11a01004 	movne	r1, r4
80702698:	e0621293 	mls	r2, r3, r2, r1
8070269c:	e1a01fc0 	asr	r1, r0, #31
807026a0:	e3a03000 	mov	r3, #0
807026a4:	e1510003 	cmp	r1, r3
807026a8:	e1a0e82e 	lsr	lr, lr, #16
807026ac:	01500002 	cmpeq	r0, r2
807026b0:	31a01003 	movcc	r1, r3
807026b4:	e1a03fcc 	asr	r3, ip, #31
807026b8:	31a00002 	movcc	r0, r2
807026bc:	e1530001 	cmp	r3, r1
807026c0:	015c0000 	cmpeq	ip, r0
807026c4:	e1a0200c 	mov	r2, ip
807026c8:	e3a01ffa 	mov	r1, #1000	; 0x3e8
807026cc:	e59f310c 	ldr	r3, [pc, #268]	; 807027e0 <start_svb+0x2f8>
807026d0:	81a02000 	movhi	r2, r0
807026d4:	e101218e 	smlabb	r1, lr, r1, r2
807026d8:	e1a04281 	lsl	r4, r1, #5
807026dc:	e0512004 	subs	r2, r1, r4
807026e0:	e28f10e8 	add	r1, pc, #232	; 0xe8
807026e4:	e1c100d0 	ldrd	r0, [r1]
807026e8:	e0922000 	adds	r2, r2, r0
807026ec:	e59f0110 	ldr	r0, [pc, #272]	; 80702804 <start_svb+0x31c>
807026f0:	e0022000 	and	r2, r2, r0
807026f4:	e3000c75 	movw	r0, #3189	; 0xc75
807026f8:	e0922000 	adds	r2, r2, r0
807026fc:	e58320b0 	str	r2, [r3, #176]	; 0xb0
80702700:	e3042e1f 	movw	r2, #19999	; 0x4e1f
80702704:	e3a03000 	mov	r3, #0
80702708:	e58d3008 	str	r3, [sp, #8]
8070270c:	e59d3008 	ldr	r3, [sp, #8]
80702710:	e1530002 	cmp	r3, r2
80702714:	9a000028 	bls	807027bc <start_svb+0x2d4>
80702718:	e28dd018 	add	sp, sp, #24
8070271c:	e8bd81f0 	pop	{r4, r5, r6, r7, r8, pc}
80702720:	e59fc0c0 	ldr	ip, [pc, #192]	; 807027e8 <start_svb+0x300>
80702724:	e152000c 	cmp	r2, ip
80702728:	0affff76 	beq	80702508 <start_svb+0x20>
8070272c:	e59fc0d4 	ldr	ip, [pc, #212]	; 80702808 <start_svb+0x320>
80702730:	e152000c 	cmp	r2, ip
80702734:	0affff73 	beq	80702508 <start_svb+0x20>
80702738:	e59fc0cc 	ldr	ip, [pc, #204]	; 8070280c <start_svb+0x324>
8070273c:	e152000c 	cmp	r2, ip
80702740:	0affff70 	beq	80702508 <start_svb+0x20>
80702744:	e59fc0c4 	ldr	ip, [pc, #196]	; 80702810 <start_svb+0x328>
80702748:	e152000c 	cmp	r2, ip
8070274c:	03c338ff 	biceq	r3, r3, #16711680	; 0xff0000
80702750:	03833801 	orreq	r3, r3, #65536	; 0x10000
80702754:	eaffff6d 	b	80702510 <start_svb+0x28>
80702758:	e320f000 	nop	{0}
8070275c:	e59d1004 	ldr	r1, [sp, #4]
80702760:	e2811001 	add	r1, r1, #1
80702764:	e58d1004 	str	r1, [sp, #4]
80702768:	eaffff7c 	b	80702560 <start_svb+0x78>
8070276c:	e3530f41 	cmp	r3, #260	; 0x104
80702770:	3affffa0 	bcc	807025f8 <start_svb+0x110>
80702774:	e3520018 	cmp	r2, #24
80702778:	92433004 	subls	r3, r3, #4
8070277c:	eaffff9d 	b	807025f8 <start_svb+0x110>
80702780:	e30025dc 	movw	r2, #1500	; 0x5dc
80702784:	e59fc088 	ldr	ip, [pc, #136]	; 80702814 <start_svb+0x32c>
80702788:	e59f0088 	ldr	r0, [pc, #136]	; 80702818 <start_svb+0x330>
8070278c:	e59f1088 	ldr	r1, [pc, #136]	; 8070281c <start_svb+0x334>
80702790:	eaffffc0 	b	80702698 <start_svb+0x1b0>
80702794:	e30025dc 	movw	r2, #1500	; 0x5dc
80702798:	e59fc080 	ldr	ip, [pc, #128]	; 80702820 <start_svb+0x338>
8070279c:	e59f0080 	ldr	r0, [pc, #128]	; 80702824 <start_svb+0x33c>
807027a0:	e59f1080 	ldr	r1, [pc, #128]	; 80702828 <start_svb+0x340>
807027a4:	eaffffbb 	b	80702698 <start_svb+0x1b0>
807027a8:	e30025dc 	movw	r2, #1500	; 0x5dc
807027ac:	e59fc06c 	ldr	ip, [pc, #108]	; 80702820 <start_svb+0x338>
807027b0:	e59f006c 	ldr	r0, [pc, #108]	; 80702824 <start_svb+0x33c>
807027b4:	e59f1070 	ldr	r1, [pc, #112]	; 8070282c <start_svb+0x344>
807027b8:	eaffffb6 	b	80702698 <start_svb+0x1b0>
807027bc:	e320f000 	nop	{0}
807027c0:	e59d3008 	ldr	r3, [sp, #8]
807027c4:	e2833001 	add	r3, r3, #1
807027c8:	e58d3008 	str	r3, [sp, #8]
807027cc:	eaffffce 	b	8070270c <start_svb+0x224>
807027d0:	01f0ac70 	.word	0x01f0ac70
807027d4:	00000000 	.word	0x00000000
807027d8:	12020000 	.word	0x12020000
807027dc:	3516d300 	.word	0x3516d300
807027e0:	12030000 	.word	0x12030000
807027e4:	100a0000 	.word	0x100a0000
807027e8:	3516c500 	.word	0x3516c500
807027ec:	000f4240 	.word	0x000f4240
807027f0:	0013bf76 	.word	0x0013bf76
807027f4:	000f6950 	.word	0x000f6950
807027f8:	000c5c10 	.word	0x000c5c10
807027fc:	000cf850 	.word	0x000cf850
80702800:	0013f985 	.word	0x0013f985
80702804:	ffff0000 	.word	0xffff0000
80702808:	35590200 	.word	0x35590200
8070280c:	35560200 	.word	0x35560200
80702810:	3516a300 	.word	0x3516a300
80702814:	000f1b30 	.word	0x000f1b30
80702818:	000caa30 	.word	0x000caa30
8070281c:	00137156 	.word	0x00137156
80702820:	000ef420 	.word	0x000ef420
80702824:	000c8320 	.word	0x000c8320
80702828:	00135603 	.word	0x00135603
8070282c:	00134a46 	.word	0x00134a46

80702830 <start_ddr_training>:
80702830:	e92d4013 	push	{r0, r1, r4, lr}
80702834:	ebffff2b 	bl	807024e8 <start_svb>
80702838:	e3a00000 	mov	r0, #0
8070283c:	eb000884 	bl	80704a54 <ddr_hw_training_if>
80702840:	eb00088e 	bl	80704a80 <ddr_cmd_site_save>
80702844:	e3a00000 	mov	r0, #0
80702848:	eb000880 	bl	80704a50 <ddr_sw_training_if>
8070284c:	eb00089a 	bl	80704abc <ddr_cmd_site_restore>
80702850:	e59f30e4 	ldr	r3, [pc, #228]	; 8070293c <start_ddr_training+0x10c>
80702854:	e3002401 	movw	r2, #1025	; 0x401
80702858:	e593302c 	ldr	r3, [r3, #44]	; 0x2c
8070285c:	e203300f 	and	r3, r3, #15
80702860:	e3530006 	cmp	r3, #6
80702864:	e59f30d4 	ldr	r3, [pc, #212]	; 80702940 <start_ddr_training+0x110>
80702868:	059f10d4 	ldreq	r1, [pc, #212]	; 80702944 <start_ddr_training+0x114>
8070286c:	e5832028 	str	r2, [r3, #40]	; 0x28
80702870:	05812028 	streq	r2, [r1, #40]	; 0x28
80702874:	e5932050 	ldr	r2, [r3, #80]	; 0x50
80702878:	e212100f 	ands	r1, r2, #15
8070287c:	1a000022 	bne	8070290c <start_ddr_training+0xdc>
80702880:	ebffff08 	bl	807024a8 <trng_init>
80702884:	ebfffeff 	bl	80702488 <get_random_num>
80702888:	e1a04000 	mov	r4, r0
8070288c:	ebfffefd 	bl	80702488 <get_random_num>
80702890:	e59f30b0 	ldr	r3, [pc, #176]	; 80702948 <start_ddr_training+0x118>
80702894:	e3a02000 	mov	r2, #0
80702898:	e5834028 	str	r4, [r3, #40]	; 0x28
8070289c:	e583002c 	str	r0, [r3, #44]	; 0x2c
807028a0:	e3a00010 	mov	r0, #16
807028a4:	e5832030 	str	r2, [r3, #48]	; 0x30
807028a8:	e5830030 	str	r0, [r3, #48]	; 0x30
807028ac:	e58d2004 	str	r2, [sp, #4]
807028b0:	e59f2094 	ldr	r2, [pc, #148]	; 8070294c <start_ddr_training+0x11c>
807028b4:	e59d3004 	ldr	r3, [sp, #4]
807028b8:	e1530002 	cmp	r3, r2
807028bc:	9a000019 	bls	80702928 <start_ddr_training+0xf8>
807028c0:	e59f3080 	ldr	r3, [pc, #128]	; 80702948 <start_ddr_training+0x118>
807028c4:	e3a02000 	mov	r2, #0
807028c8:	e5832028 	str	r2, [r3, #40]	; 0x28
807028cc:	e583202c 	str	r2, [r3, #44]	; 0x2c
807028d0:	ebfffeec 	bl	80702488 <get_random_num>
807028d4:	ebfffeeb 	bl	80702488 <get_random_num>
807028d8:	ebfffeea 	bl	80702488 <get_random_num>
807028dc:	ebfffee9 	bl	80702488 <get_random_num>
807028e0:	ebfffefa 	bl	807024d0 <trng_deinit>
807028e4:	e3510000 	cmp	r1, #0
807028e8:	0a000005 	beq	80702904 <start_ddr_training+0xd4>
807028ec:	e59f304c 	ldr	r3, [pc, #76]	; 80702940 <start_ddr_training+0x110>
807028f0:	e3a02002 	mov	r2, #2
807028f4:	e5832000 	str	r2, [r3]
807028f8:	e5932294 	ldr	r2, [r3, #660]	; 0x294
807028fc:	e3120001 	tst	r2, #1
80702900:	1afffffc 	bne	807028f8 <start_ddr_training+0xc8>
80702904:	e28dd008 	add	sp, sp, #8
80702908:	e8bd8010 	pop	{r4, pc}
8070290c:	e3a02001 	mov	r2, #1
80702910:	e5832000 	str	r2, [r3]
80702914:	e59f2024 	ldr	r2, [pc, #36]	; 80702940 <start_ddr_training+0x110>
80702918:	e5923294 	ldr	r3, [r2, #660]	; 0x294
8070291c:	e3130001 	tst	r3, #1
80702920:	0afffffc 	beq	80702918 <start_ddr_training+0xe8>
80702924:	eaffffd5 	b	80702880 <start_ddr_training+0x50>
80702928:	e320f000 	nop	{0}
8070292c:	e59d3004 	ldr	r3, [sp, #4]
80702930:	e2833001 	add	r3, r3, #1
80702934:	e58d3004 	str	r3, [sp, #4]
80702938:	eaffffdd 	b	807028b4 <start_ddr_training+0x84>
8070293c:	1206c000 	.word	0x1206c000
80702940:	12068000 	.word	0x12068000
80702944:	12069000 	.word	0x12069000
80702948:	12030000 	.word	0x12030000
8070294c:	0001869f 	.word	0x0001869f

80702950 <ddr_adjust_get_average>:
80702950:	e5902060 	ldr	r2, [r0, #96]	; 0x60
80702954:	e5901040 	ldr	r1, [r0, #64]	; 0x40
80702958:	e5903054 	ldr	r3, [r0, #84]	; 0x54
8070295c:	e5900050 	ldr	r0, [r0, #80]	; 0x50
80702960:	e1a02502 	lsl	r2, r2, #10
80702964:	e3500002 	cmp	r0, #2
80702968:	1a000004 	bne	80702980 <ddr_adjust_get_average+0x30>
8070296c:	e2811f86 	add	r1, r1, #536	; 0x218
80702970:	e0822001 	add	r2, r2, r1
80702974:	e7920383 	ldr	r0, [r2, r3, lsl #7]
80702978:	e200007f 	and	r0, r0, #127	; 0x7f
8070297c:	e12fff1e 	bx	lr
80702980:	e0823383 	add	r3, r2, r3, lsl #7
80702984:	e0833001 	add	r3, r3, r1
80702988:	e593221c 	ldr	r2, [r3, #540]	; 0x21c
8070298c:	e5933220 	ldr	r3, [r3, #544]	; 0x220
80702990:	e202007f 	and	r0, r2, #127	; 0x7f
80702994:	e203107f 	and	r1, r3, #127	; 0x7f
80702998:	e0800001 	add	r0, r0, r1
8070299c:	e7e61452 	ubfx	r1, r2, #8, #7
807029a0:	e0800001 	add	r0, r0, r1
807029a4:	e7e61852 	ubfx	r1, r2, #16, #7
807029a8:	e0800001 	add	r0, r0, r1
807029ac:	e7e62c52 	ubfx	r2, r2, #24, #7
807029b0:	e0800002 	add	r0, r0, r2
807029b4:	e7e62453 	ubfx	r2, r3, #8, #7
807029b8:	e0800002 	add	r0, r0, r2
807029bc:	e7e62853 	ubfx	r2, r3, #16, #7
807029c0:	e0800002 	add	r0, r0, r2
807029c4:	e7e63c53 	ubfx	r3, r3, #24, #7
807029c8:	e0800003 	add	r0, r0, r3
807029cc:	e1a001a0 	lsr	r0, r0, #3
807029d0:	e12fff1e 	bx	lr

807029d4 <ddr_read>:
807029d4:	e5900000 	ldr	r0, [r0]
807029d8:	e12fff1e 	bx	lr

807029dc <ddr_write>:
807029dc:	e5810000 	str	r0, [r1]
807029e0:	e12fff1e 	bx	lr

807029e4 <ddrtr_memcpy>:
807029e4:	e2403001 	sub	r3, r0, #1
807029e8:	e0812002 	add	r2, r1, r2
807029ec:	e1510002 	cmp	r1, r2
807029f0:	1a000000 	bne	807029f8 <ddrtr_memcpy+0x14>
807029f4:	e12fff1e 	bx	lr
807029f8:	e4d1c001 	ldrb	ip, [r1], #1
807029fc:	e5e3c001 	strb	ip, [r3, #1]!
80702a00:	eafffff9 	b	807029ec <ddrtr_memcpy+0x8>

80702a04 <ddrtr_memset>:
80702a04:	e0802002 	add	r2, r0, r2
80702a08:	e1a03000 	mov	r3, r0
80702a0c:	e1530002 	cmp	r3, r2
80702a10:	1a000000 	bne	80702a18 <ddrtr_memset+0x14>
80702a14:	e12fff1e 	bx	lr
80702a18:	e4c31001 	strb	r1, [r3], #1
80702a1c:	eafffffa 	b	80702a0c <ddrtr_memset+0x8>

80702a20 <ddr_training_by_dmc>:
80702a20:	e5903070 	ldr	r3, [r0, #112]	; 0x70
80702a24:	e3530000 	cmp	r3, #0
80702a28:	1a000000 	bne	80702a30 <ddr_training_by_dmc+0x10>
80702a2c:	ea00079c 	b	807048a4 <ddr_training_boot_func>
80702a30:	e3a00000 	mov	r0, #0
80702a34:	e12fff1e 	bx	lr

80702a38 <ddr_training_by_rank>:
80702a38:	e92d41f0 	push	{r4, r5, r6, r7, r8, lr}
80702a3c:	e3a05000 	mov	r5, #0
80702a40:	e5902040 	ldr	r2, [r0, #64]	; 0x40
80702a44:	e1a04000 	mov	r4, r0
80702a48:	e5901060 	ldr	r1, [r0, #96]	; 0x60
80702a4c:	e1a06005 	mov	r6, r5
80702a50:	e3a0703c 	mov	r7, #60	; 0x3c
80702a54:	e3a0800c 	mov	r8, #12
80702a58:	e5923048 	ldr	r3, [r2, #72]	; 0x48
80702a5c:	e3c3300f 	bic	r3, r3, #15
80702a60:	e1833001 	orr	r3, r3, r1
80702a64:	e5823048 	str	r3, [r2, #72]	; 0x48
80702a68:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
80702a6c:	e0030397 	mul	r3, r7, r3
80702a70:	e0842003 	add	r2, r4, r3
80702a74:	e5922008 	ldr	r2, [r2, #8]
80702a78:	e1520005 	cmp	r2, r5
80702a7c:	8a000001 	bhi	80702a88 <ddr_training_by_rank+0x50>
80702a80:	e1a00006 	mov	r0, r6
80702a84:	e8bd81f0 	pop	{r4, r5, r6, r7, r8, pc}
80702a88:	e0233598 	mla	r3, r8, r5, r3
80702a8c:	e5845064 	str	r5, [r4, #100]	; 0x64
80702a90:	e1a00004 	mov	r0, r4
80702a94:	e2855001 	add	r5, r5, #1
80702a98:	e0843003 	add	r3, r4, r3
80702a9c:	e5932014 	ldr	r2, [r3, #20]
80702aa0:	e5842044 	str	r2, [r4, #68]	; 0x44
80702aa4:	e593301c 	ldr	r3, [r3, #28]
80702aa8:	e584304c 	str	r3, [r4, #76]	; 0x4c
80702aac:	ebffffdb 	bl	80702a20 <ddr_training_by_dmc>
80702ab0:	e0866000 	add	r6, r6, r0
80702ab4:	eaffffeb 	b	80702a68 <ddr_training_by_rank+0x30>

80702ab8 <ddr_training_by_phy>:
80702ab8:	e92d4df0 	push	{r4, r5, r6, r7, r8, sl, fp, lr}
80702abc:	e3a07001 	mov	r7, #1
80702ac0:	e590305c 	ldr	r3, [r0, #92]	; 0x5c
80702ac4:	e3a0603c 	mov	r6, #60	; 0x3c
80702ac8:	e3a05000 	mov	r5, #0
80702acc:	e1a04000 	mov	r4, r0
80702ad0:	e1a07317 	lsl	r7, r7, r3
80702ad4:	e1a08005 	mov	r8, r5
80702ad8:	e0230396 	mla	r3, r6, r3, r0
80702adc:	e593a00c 	ldr	sl, [r3, #12]
80702ae0:	e155000a 	cmp	r5, sl
80702ae4:	1a000001 	bne	80702af0 <ddr_training_by_phy+0x38>
80702ae8:	e1a00008 	mov	r0, r8
80702aec:	e8bd8df0 	pop	{r4, r5, r6, r7, r8, sl, fp, pc}
80702af0:	e594205c 	ldr	r2, [r4, #92]	; 0x5c
80702af4:	e2853005 	add	r3, r5, #5
80702af8:	e5845060 	str	r5, [r4, #96]	; 0x60
80702afc:	e0020296 	mul	r2, r6, r2
80702b00:	e0823183 	add	r3, r2, r3, lsl #3
80702b04:	e0843003 	add	r3, r4, r3
80702b08:	e5933004 	ldr	r3, [r3, #4]
80702b0c:	e1170003 	tst	r7, r3
80702b10:	e5843048 	str	r3, [r4, #72]	; 0x48
80702b14:	0a000001 	beq	80702b20 <ddr_training_by_phy+0x68>
80702b18:	e2855001 	add	r5, r5, #1
80702b1c:	eaffffef 	b	80702ae0 <ddr_training_by_phy+0x28>
80702b20:	e1a00004 	mov	r0, r4
80702b24:	ebffffc3 	bl	80702a38 <ddr_training_by_rank>
80702b28:	e0888000 	add	r8, r8, r0
80702b2c:	eafffff9 	b	80702b18 <ddr_training_by_phy+0x60>

80702b30 <ddr_training_all>:
80702b30:	e92d41f0 	push	{r4, r5, r6, r7, r8, lr}
80702b34:	e3a04000 	mov	r4, #0
80702b38:	e1a05000 	mov	r5, r0
80702b3c:	e1a06004 	mov	r6, r4
80702b40:	e3a0703c 	mov	r7, #60	; 0x3c
80702b44:	e595303c 	ldr	r3, [r5, #60]	; 0x3c
80702b48:	e1530004 	cmp	r3, r4
80702b4c:	8a000001 	bhi	80702b58 <ddr_training_all+0x28>
80702b50:	e1a00006 	mov	r0, r6
80702b54:	e8bd81f0 	pop	{r4, r5, r6, r7, r8, pc}
80702b58:	e0030497 	mul	r3, r7, r4
80702b5c:	e585405c 	str	r4, [r5, #92]	; 0x5c
80702b60:	e1a00005 	mov	r0, r5
80702b64:	e2844001 	add	r4, r4, #1
80702b68:	e7953003 	ldr	r3, [r5, r3]
80702b6c:	e5853040 	str	r3, [r5, #64]	; 0x40
80702b70:	ebffffd0 	bl	80702ab8 <ddr_training_by_phy>
80702b74:	e0866000 	add	r6, r6, r0
80702b78:	eafffff1 	b	80702b44 <ddr_training_all+0x14>

80702b7c <ddr_training_cfg_set_rank>:
80702b7c:	e3a03001 	mov	r3, #1
80702b80:	e580300c 	str	r3, [r0, #12]
80702b84:	e59f3030 	ldr	r3, [pc, #48]	; 80702bbc <ddr_training_cfg_set_rank+0x40>
80702b88:	e59320a0 	ldr	r2, [r3, #160]	; 0xa0
80702b8c:	e580202c 	str	r2, [r0, #44]	; 0x2c
80702b90:	e5932090 	ldr	r2, [r3, #144]	; 0x90
80702b94:	e5802030 	str	r2, [r0, #48]	; 0x30
80702b98:	e59320a4 	ldr	r2, [r3, #164]	; 0xa4
80702b9c:	e5802034 	str	r2, [r0, #52]	; 0x34
80702ba0:	e5932094 	ldr	r2, [r3, #148]	; 0x94
80702ba4:	e5802038 	str	r2, [r0, #56]	; 0x38
80702ba8:	e5933094 	ldr	r3, [r3, #148]	; 0x94
80702bac:	e3530000 	cmp	r3, #0
80702bb0:	13a03002 	movne	r3, #2
80702bb4:	1580300c 	strne	r3, [r0, #12]
80702bb8:	e12fff1e 	bx	lr
80702bbc:	12020000 	.word	0x12020000

80702bc0 <ddr_training_cfg_set_phy>:
80702bc0:	e3a03001 	mov	r3, #1
80702bc4:	e580303c 	str	r3, [r0, #60]	; 0x3c
80702bc8:	e59f3010 	ldr	r3, [pc, #16]	; 80702be0 <ddr_training_cfg_set_phy+0x20>
80702bcc:	e5803000 	str	r3, [r0]
80702bd0:	e593302c 	ldr	r3, [r3, #44]	; 0x2c
80702bd4:	e203300f 	and	r3, r3, #15
80702bd8:	e5803004 	str	r3, [r0, #4]
80702bdc:	e12fff1e 	bx	lr
80702be0:	1206c000 	.word	0x1206c000

80702be4 <ddr_training_set_timing>:
80702be4:	e30033e9 	movw	r3, #1001	; 0x3e9
80702be8:	e2533001 	subs	r3, r3, #1
80702bec:	1a000004 	bne	80702c04 <ddr_training_set_timing+0x20>
80702bf0:	e30033e9 	movw	r3, #1001	; 0x3e9
80702bf4:	e5801108 	str	r1, [r0, #264]	; 0x108
80702bf8:	e2533001 	subs	r3, r3, #1
80702bfc:	1a000002 	bne	80702c0c <ddr_training_set_timing+0x28>
80702c00:	e12fff1e 	bx	lr
80702c04:	e320f000 	nop	{0}
80702c08:	eafffff6 	b	80702be8 <ddr_training_set_timing+0x4>
80702c0c:	e320f000 	nop	{0}
80702c10:	eafffff8 	b	80702bf8 <ddr_training_set_timing+0x14>

80702c14 <ddr_vref_set>:
80702c14:	e5903050 	ldr	r3, [r0, #80]	; 0x50
80702c18:	e3530001 	cmp	r3, #1
80702c1c:	1a000010 	bne	80702c64 <ddr_vref_set+0x50>
80702c20:	e5903060 	ldr	r3, [r0, #96]	; 0x60
80702c24:	e5902040 	ldr	r2, [r0, #64]	; 0x40
80702c28:	e590c054 	ldr	ip, [r0, #84]	; 0x54
80702c2c:	e2822f9d 	add	r2, r2, #628	; 0x274
80702c30:	e0822503 	add	r2, r2, r3, lsl #10
80702c34:	e792338c 	ldr	r3, [r2, ip, lsl #7]
80702c38:	e3c3301f 	bic	r3, r3, #31
80702c3c:	e1831001 	orr	r1, r3, r1
80702c40:	e782138c 	str	r1, [r2, ip, lsl #7]
80702c44:	e5902060 	ldr	r2, [r0, #96]	; 0x60
80702c48:	e5903040 	ldr	r3, [r0, #64]	; 0x40
80702c4c:	e2833f9d 	add	r3, r3, #628	; 0x274
80702c50:	e0833502 	add	r3, r3, r2, lsl #10
80702c54:	e5902054 	ldr	r2, [r0, #84]	; 0x54
80702c58:	e2822001 	add	r2, r2, #1
80702c5c:	e7831382 	str	r1, [r3, r2, lsl #7]
80702c60:	e12fff1e 	bx	lr
80702c64:	e1a02000 	mov	r2, r0
80702c68:	e92d4070 	push	{r4, r5, r6, lr}
80702c6c:	e1a04001 	mov	r4, r1
80702c70:	e5900044 	ldr	r0, [r0, #68]	; 0x44
80702c74:	e5905108 	ldr	r5, [r0, #264]	; 0x108
80702c78:	e3c51eff 	bic	r1, r5, #4080	; 0xff0
80702c7c:	e3c1100f 	bic	r1, r1, #15
80702c80:	ebffffd7 	bl	80702be4 <ddr_training_set_timing>
80702c84:	e5921040 	ldr	r1, [r2, #64]	; 0x40
80702c88:	e59130c0 	ldr	r3, [r1, #192]	; 0xc0
80702c8c:	e3833102 	orr	r3, r3, #-2147483648	; 0x80000000
80702c90:	e58130c0 	str	r3, [r1, #192]	; 0xc0
80702c94:	e5920040 	ldr	r0, [r2, #64]	; 0x40
80702c98:	e592c054 	ldr	ip, [r2, #84]	; 0x54
80702c9c:	e2803e27 	add	r3, r0, #624	; 0x270
80702ca0:	e59010c4 	ldr	r1, [r0, #196]	; 0xc4
80702ca4:	e793338c 	ldr	r3, [r3, ip, lsl #7]
80702ca8:	e381c102 	orr	ip, r1, #-2147483648	; 0x80000000
80702cac:	e580c0c4 	str	ip, [r0, #196]	; 0xc4
80702cb0:	e5920040 	ldr	r0, [r2, #64]	; 0x40
80702cb4:	e3c3303f 	bic	r3, r3, #63	; 0x3f
80702cb8:	e592c054 	ldr	ip, [r2, #84]	; 0x54
80702cbc:	e1833004 	orr	r3, r3, r4
80702cc0:	e2800e27 	add	r0, r0, #624	; 0x270
80702cc4:	e780338c 	str	r3, [r0, ip, lsl #7]
80702cc8:	e59f00f8 	ldr	r0, [pc, #248]	; 80702dc8 <ddr_vref_set+0x1b4>
80702ccc:	e5923040 	ldr	r3, [r2, #64]	; 0x40
80702cd0:	e5830004 	str	r0, [r3, #4]
80702cd4:	e5923040 	ldr	r3, [r2, #64]	; 0x40
80702cd8:	e283c004 	add	ip, r3, #4
80702cdc:	e59c0000 	ldr	r0, [ip]
80702ce0:	e3100001 	tst	r0, #1
80702ce4:	1afffffc 	bne	80702cdc <ddr_vref_set+0xc8>
80702ce8:	e3c11102 	bic	r1, r1, #-2147483648	; 0x80000000
80702cec:	e58310c4 	str	r1, [r3, #196]	; 0xc4
80702cf0:	e5920040 	ldr	r0, [r2, #64]	; 0x40
80702cf4:	e592c054 	ldr	ip, [r2, #84]	; 0x54
80702cf8:	e2803e27 	add	r3, r0, #624	; 0x270
80702cfc:	e59010c4 	ldr	r1, [r0, #196]	; 0xc4
80702d00:	e793338c 	ldr	r3, [r3, ip, lsl #7]
80702d04:	e381c102 	orr	ip, r1, #-2147483648	; 0x80000000
80702d08:	e580c0c4 	str	ip, [r0, #196]	; 0xc4
80702d0c:	e5920040 	ldr	r0, [r2, #64]	; 0x40
80702d10:	e3c3303f 	bic	r3, r3, #63	; 0x3f
80702d14:	e592c054 	ldr	ip, [r2, #84]	; 0x54
80702d18:	e1833004 	orr	r3, r3, r4
80702d1c:	e2800e27 	add	r0, r0, #624	; 0x270
80702d20:	e780338c 	str	r3, [r0, ip, lsl #7]
80702d24:	e59f009c 	ldr	r0, [pc, #156]	; 80702dc8 <ddr_vref_set+0x1b4>
80702d28:	e5923040 	ldr	r3, [r2, #64]	; 0x40
80702d2c:	e5830004 	str	r0, [r3, #4]
80702d30:	e5923040 	ldr	r3, [r2, #64]	; 0x40
80702d34:	e283c004 	add	ip, r3, #4
80702d38:	e59c0000 	ldr	r0, [ip]
80702d3c:	e3100001 	tst	r0, #1
80702d40:	1afffffc 	bne	80702d38 <ddr_vref_set+0x124>
80702d44:	e3c11102 	bic	r1, r1, #-2147483648	; 0x80000000
80702d48:	e58310c4 	str	r1, [r3, #196]	; 0xc4
80702d4c:	e5921040 	ldr	r1, [r2, #64]	; 0x40
80702d50:	e59130c0 	ldr	r3, [r1, #192]	; 0xc0
80702d54:	e3c33102 	bic	r3, r3, #-2147483648	; 0x80000000
80702d58:	e58130c0 	str	r3, [r1, #192]	; 0xc0
80702d5c:	e5920040 	ldr	r0, [r2, #64]	; 0x40
80702d60:	e592c054 	ldr	ip, [r2, #84]	; 0x54
80702d64:	e2801e27 	add	r1, r0, #624	; 0x270
80702d68:	e59030c4 	ldr	r3, [r0, #196]	; 0xc4
80702d6c:	e791138c 	ldr	r1, [r1, ip, lsl #7]
80702d70:	e383c102 	orr	ip, r3, #-2147483648	; 0x80000000
80702d74:	e580c0c4 	str	ip, [r0, #196]	; 0xc4
80702d78:	e3c1103f 	bic	r1, r1, #63	; 0x3f
80702d7c:	e5920054 	ldr	r0, [r2, #84]	; 0x54
80702d80:	e1814004 	orr	r4, r1, r4
80702d84:	e5921040 	ldr	r1, [r2, #64]	; 0x40
80702d88:	e2811e27 	add	r1, r1, #624	; 0x270
80702d8c:	e7814380 	str	r4, [r1, r0, lsl #7]
80702d90:	e59f0030 	ldr	r0, [pc, #48]	; 80702dc8 <ddr_vref_set+0x1b4>
80702d94:	e5921040 	ldr	r1, [r2, #64]	; 0x40
80702d98:	e5810004 	str	r0, [r1, #4]
80702d9c:	e5921040 	ldr	r1, [r2, #64]	; 0x40
80702da0:	e281c004 	add	ip, r1, #4
80702da4:	e59c0000 	ldr	r0, [ip]
80702da8:	e3100001 	tst	r0, #1
80702dac:	1afffffc 	bne	80702da4 <ddr_vref_set+0x190>
80702db0:	e3c33102 	bic	r3, r3, #-2147483648	; 0x80000000
80702db4:	e58130c4 	str	r3, [r1, #196]	; 0xc4
80702db8:	e1a01005 	mov	r1, r5
80702dbc:	e5920044 	ldr	r0, [r2, #68]	; 0x44
80702dc0:	e8bd4070 	pop	{r4, r5, r6, lr}
80702dc4:	eaffff86 	b	80702be4 <ddr_training_set_timing>
80702dc8:	00040001 	.word	0x00040001

80702dcc <ddr_training_stat>:
80702dcc:	e92d41f0 	push	{r4, r5, r6, r7, r8, lr}
80702dd0:	e1a06003 	mov	r6, r3
80702dd4:	e1a04000 	mov	r4, r0
80702dd8:	e1a05001 	mov	r5, r1
80702ddc:	e1a07002 	mov	r7, r2
80702de0:	eb000722 	bl	80704a70 <ddr_training_error>
80702de4:	e59f3040 	ldr	r3, [pc, #64]	; 80702e2c <ddr_training_stat+0x60>
80702de8:	e593309c 	ldr	r3, [r3, #156]	; 0x9c
80702dec:	e3530000 	cmp	r3, #0
80702df0:	18bd81f0 	popne	{r4, r5, r6, r7, r8, pc}
80702df4:	e3550000 	cmp	r5, #0
80702df8:	0a000004 	beq	80702e10 <ddr_training_stat+0x44>
80702dfc:	e59f302c 	ldr	r3, [pc, #44]	; 80702e30 <ddr_training_stat+0x64>
80702e00:	e1550003 	cmp	r5, r3
80702e04:	03a05a01 	moveq	r5, #4096	; 0x1000
80702e08:	13a05a02 	movne	r5, #8192	; 0x2000
80702e0c:	e1844005 	orr	r4, r4, r5
80702e10:	e59f3014 	ldr	r3, [pc, #20]	; 80702e2c <ddr_training_stat+0x60>
80702e14:	e3770001 	cmn	r7, #1
80702e18:	11844c07 	orrne	r4, r4, r7, lsl #24
80702e1c:	e3760001 	cmn	r6, #1
80702e20:	11844a06 	orrne	r4, r4, r6, lsl #20
80702e24:	e583409c 	str	r4, [r3, #156]	; 0x9c
80702e28:	e8bd81f0 	pop	{r4, r5, r6, r7, r8, pc}
80702e2c:	12020000 	.word	0x12020000
80702e30:	1206c000 	.word	0x1206c000

80702e34 <ddr_hw_training_process>:
80702e34:	e92d4df3 	push	{r0, r1, r4, r5, r6, r7, r8, sl, fp, lr}
80702e38:	e2516000 	subs	r6, r1, #0
80702e3c:	e5905040 	ldr	r5, [r0, #64]	; 0x40
80702e40:	e2853004 	add	r3, r5, #4
80702e44:	e58d3004 	str	r3, [sp, #4]
80702e48:	e5952004 	ldr	r2, [r5, #4]
80702e4c:	1a000002 	bne	80702e5c <ddr_hw_training_process+0x28>
80702e50:	e3a00000 	mov	r0, #0
80702e54:	e28dd008 	add	sp, sp, #8
80702e58:	e8bd8df0 	pop	{r4, r5, r6, r7, r8, sl, fp, pc}
80702e5c:	e3160a01 	tst	r6, #4096	; 0x1000
80702e60:	e3863001 	orr	r3, r6, #1
80702e64:	e1833002 	orr	r3, r3, r2
80702e68:	e1a07000 	mov	r7, r0
80702e6c:	e5853004 	str	r3, [r5, #4]
80702e70:	0a000025 	beq	80702f0c <ddr_hw_training_process+0xd8>
80702e74:	e590305c 	ldr	r3, [r0, #92]	; 0x5c
80702e78:	e3a0203c 	mov	r2, #60	; 0x3c
80702e7c:	e3a08000 	mov	r8, #0
80702e80:	e1a0a008 	mov	sl, r8
80702e84:	e0030392 	mul	r3, r2, r3
80702e88:	e080b003 	add	fp, r0, r3
80702e8c:	e58d3000 	str	r3, [sp]
80702e90:	e59b3008 	ldr	r3, [fp, #8]
80702e94:	e1530008 	cmp	r3, r8
80702e98:	8a000003 	bhi	80702eac <ddr_hw_training_process+0x78>
80702e9c:	e35a0000 	cmp	sl, #0
80702ea0:	0a000019 	beq	80702f0c <ddr_hw_training_process+0xd8>
80702ea4:	e3e00000 	mvn	r0, #0
80702ea8:	eaffffe9 	b	80702e54 <ddr_hw_training_process+0x20>
80702eac:	e59d2000 	ldr	r2, [sp]
80702eb0:	e3a0300c 	mov	r3, #12
80702eb4:	e3a04000 	mov	r4, #0
80702eb8:	e0232893 	mla	r3, r3, r8, r2
80702ebc:	e0873003 	add	r3, r7, r3
80702ec0:	e5932014 	ldr	r2, [r3, #20]
80702ec4:	e3a03002 	mov	r3, #2
80702ec8:	e5823000 	str	r3, [r2]
80702ecc:	e5923294 	ldr	r3, [r2, #660]	; 0x294
80702ed0:	e2844001 	add	r4, r4, #1
80702ed4:	e2133001 	ands	r3, r3, #1
80702ed8:	0a000007 	beq	80702efc <ddr_hw_training_process+0xc8>
80702edc:	e3740001 	cmn	r4, #1
80702ee0:	1afffff9 	bne	80702ecc <ddr_hw_training_process+0x98>
80702ee4:	e1a03004 	mov	r3, r4
80702ee8:	e1a02004 	mov	r2, r4
80702eec:	e1a01004 	mov	r1, r4
80702ef0:	e3a00010 	mov	r0, #16
80702ef4:	ebffffb4 	bl	80702dcc <ddr_training_stat>
80702ef8:	ea000000 	b	80702f00 <ddr_hw_training_process+0xcc>
80702efc:	e1a04003 	mov	r4, r3
80702f00:	e08aa004 	add	sl, sl, r4
80702f04:	e2888001 	add	r8, r8, #1
80702f08:	eaffffe0 	b	80702e90 <ddr_hw_training_process+0x5c>
80702f0c:	e3a04000 	mov	r4, #0
80702f10:	e59d3004 	ldr	r3, [sp, #4]
80702f14:	e2844001 	add	r4, r4, #1
80702f18:	e5933000 	ldr	r3, [r3]
80702f1c:	e3130001 	tst	r3, #1
80702f20:	1a000003 	bne	80702f34 <ddr_hw_training_process+0x100>
80702f24:	e5953008 	ldr	r3, [r5, #8]
80702f28:	e3530000 	cmp	r3, #0
80702f2c:	0affffc7 	beq	80702e50 <ddr_hw_training_process+0x1c>
80702f30:	ea000001 	b	80702f3c <ddr_hw_training_process+0x108>
80702f34:	e3740001 	cmn	r4, #1
80702f38:	1afffff4 	bne	80702f10 <ddr_hw_training_process+0xdc>
80702f3c:	e5953008 	ldr	r3, [r5, #8]
80702f40:	e1a02006 	mov	r2, r6
80702f44:	e1a01005 	mov	r1, r5
80702f48:	e3a00010 	mov	r0, #16
80702f4c:	ebffff9e 	bl	80702dcc <ddr_training_stat>
80702f50:	eaffffd3 	b	80702ea4 <ddr_hw_training_process+0x70>

80702f54 <ddr_training_check_bypass>:
80702f54:	e5903048 	ldr	r3, [r0, #72]	; 0x48
80702f58:	e1110003 	tst	r1, r3
80702f5c:	13a00001 	movne	r0, #1
80702f60:	03a00000 	moveq	r0, #0
80702f64:	e12fff1e 	bx	lr

80702f68 <ddr_training_phy_disable>:
80702f68:	e3a00000 	mov	r0, #0
80702f6c:	e12fff1e 	bx	lr

80702f70 <ddr_training_switch_axi>:
80702f70:	e590105c 	ldr	r1, [r0, #92]	; 0x5c
80702f74:	e3a0303c 	mov	r3, #60	; 0x3c
80702f78:	e59fc054 	ldr	ip, [pc, #84]	; 80702fd4 <ddr_training_switch_axi+0x64>
80702f7c:	e0230193 	mla	r3, r3, r1, r0
80702f80:	e59c2104 	ldr	r2, [ip, #260]	; 0x104
80702f84:	e3c2200f 	bic	r2, r2, #15
80702f88:	e3822004 	orr	r2, r2, #4
80702f8c:	e5933004 	ldr	r3, [r3, #4]
80702f90:	e3530006 	cmp	r3, #6
80702f94:	05903064 	ldreq	r3, [r0, #100]	; 0x64
80702f98:	00831081 	addeq	r1, r3, r1, lsl #1
80702f9c:	e1822001 	orr	r2, r2, r1
80702fa0:	e58c2104 	str	r2, [ip, #260]	; 0x104
80702fa4:	e59c3114 	ldr	r3, [ip, #276]	; 0x114
80702fa8:	e3c3300f 	bic	r3, r3, #15
80702fac:	e3833004 	orr	r3, r3, #4
80702fb0:	e1833001 	orr	r3, r3, r1
80702fb4:	e58c3114 	str	r3, [ip, #276]	; 0x114
80702fb8:	e5903060 	ldr	r3, [r0, #96]	; 0x60
80702fbc:	e3530001 	cmp	r3, #1
80702fc0:	05902044 	ldreq	r2, [r0, #68]	; 0x44
80702fc4:	05923060 	ldreq	r3, [r2, #96]	; 0x60
80702fc8:	03c33007 	biceq	r3, r3, #7
80702fcc:	05823060 	streq	r3, [r2, #96]	; 0x60
80702fd0:	e12fff1e 	bx	lr
80702fd4:	12060000 	.word	0x12060000

80702fd8 <ddr_phy_cfg_update>:
80702fd8:	e5903070 	ldr	r3, [r0, #112]	; 0x70
80702fdc:	e3832702 	orr	r2, r3, #524288	; 0x80000
80702fe0:	e3c33702 	bic	r3, r3, #524288	; 0x80000
80702fe4:	e5802070 	str	r2, [r0, #112]	; 0x70
80702fe8:	e5803070 	str	r3, [r0, #112]	; 0x70
80702fec:	e5903004 	ldr	r3, [r0, #4]
80702ff0:	e3832902 	orr	r2, r3, #32768	; 0x8000
80702ff4:	e3c33902 	bic	r3, r3, #32768	; 0x8000
80702ff8:	e5802004 	str	r2, [r0, #4]
80702ffc:	e5803004 	str	r3, [r0, #4]
80703000:	f57ff04f 	dsb	sy
80703004:	e12fff1e 	bx	lr

80703008 <ddr_training_save_reg>:
80703008:	e92d41f0 	push	{r4, r5, r6, r7, r8, lr}
8070300c:	e1a04001 	mov	r4, r1
80703010:	e5906044 	ldr	r6, [r0, #68]	; 0x44
80703014:	e3017064 	movw	r7, #4196	; 0x1064
80703018:	e5905040 	ldr	r5, [r0, #64]	; 0x40
8070301c:	e3520c01 	cmp	r2, #256	; 0x100
80703020:	e5961108 	ldr	r1, [r6, #264]	; 0x108
80703024:	e5841000 	str	r1, [r4]
80703028:	e5963028 	ldr	r3, [r6, #40]	; 0x28
8070302c:	e5843004 	str	r3, [r4, #4]
80703030:	e5953070 	ldr	r3, [r5, #112]	; 0x70
80703034:	e5843010 	str	r3, [r4, #16]
80703038:	e7953007 	ldr	r3, [r5, r7]
8070303c:	e5843014 	str	r3, [r4, #20]
80703040:	e7953007 	ldr	r3, [r5, r7]
80703044:	e5843014 	str	r3, [r4, #20]
80703048:	0a000022 	beq	807030d8 <ddr_training_save_reg+0xd0>
8070304c:	8a00001d 	bhi	807030c8 <ddr_training_save_reg+0xc0>
80703050:	e3520010 	cmp	r2, #16
80703054:	1a000003 	bne	80703068 <ddr_training_save_reg+0x60>
80703058:	e3c11eff 	bic	r1, r1, #4080	; 0xff0
8070305c:	e1a00006 	mov	r0, r6
80703060:	e3c1100f 	bic	r1, r1, #15
80703064:	ebfffede 	bl	80702be4 <ddr_training_set_timing>
80703068:	e5943004 	ldr	r3, [r4, #4]
8070306c:	e1a00004 	mov	r0, r4
80703070:	e3c33001 	bic	r3, r3, #1
80703074:	e5863028 	str	r3, [r6, #40]	; 0x28
80703078:	e5943010 	ldr	r3, [r4, #16]
8070307c:	e3c33801 	bic	r3, r3, #65536	; 0x10000
80703080:	e5853070 	str	r3, [r5, #112]	; 0x70
80703084:	e5953084 	ldr	r3, [r5, #132]	; 0x84
80703088:	e5843018 	str	r3, [r4, #24]
8070308c:	e3c33407 	bic	r3, r3, #117440512	; 0x7000000
80703090:	e5853084 	str	r3, [r5, #132]	; 0x84
80703094:	e59f305c 	ldr	r3, [pc, #92]	; 807030f8 <ddr_training_save_reg+0xf0>
80703098:	e5931104 	ldr	r1, [r3, #260]	; 0x104
8070309c:	e5841024 	str	r1, [r4, #36]	; 0x24
807030a0:	e1a01002 	mov	r1, r2
807030a4:	e5933114 	ldr	r3, [r3, #276]	; 0x114
807030a8:	e5843028 	str	r3, [r4, #40]	; 0x28
807030ac:	e5963060 	ldr	r3, [r6, #96]	; 0x60
807030b0:	e584302c 	str	r3, [r4, #44]	; 0x2c
807030b4:	eb00068f 	bl	80704af8 <ddr_training_save_reg_custom>
807030b8:	e1a00005 	mov	r0, r5
807030bc:	ebffffc5 	bl	80702fd8 <ddr_phy_cfg_update>
807030c0:	f57ff04f 	dsb	sy
807030c4:	e8bd81f0 	pop	{r4, r5, r6, r7, r8, pc}
807030c8:	e3520601 	cmp	r2, #1048576	; 0x100000
807030cc:	0a000005 	beq	807030e8 <ddr_training_save_reg+0xe0>
807030d0:	e3520502 	cmp	r2, #8388608	; 0x800000
807030d4:	eaffffde 	b	80703054 <ddr_training_save_reg+0x4c>
807030d8:	e3c11eff 	bic	r1, r1, #4080	; 0xff0
807030dc:	e1a00006 	mov	r0, r6
807030e0:	e3c1100f 	bic	r1, r1, #15
807030e4:	ebfffebe 	bl	80702be4 <ddr_training_set_timing>
807030e8:	e595302c 	ldr	r3, [r5, #44]	; 0x2c
807030ec:	e2133020 	ands	r3, r3, #32
807030f0:	07853007 	streq	r3, [r5, r7]
807030f4:	eaffffdb 	b	80703068 <ddr_training_save_reg+0x60>
807030f8:	12060000 	.word	0x12060000

807030fc <ddr_training_restore_reg>:
807030fc:	e92d4070 	push	{r4, r5, r6, lr}
80703100:	e1a02001 	mov	r2, r1
80703104:	e5905044 	ldr	r5, [r0, #68]	; 0x44
80703108:	e5904040 	ldr	r4, [r0, #64]	; 0x40
8070310c:	e5911000 	ldr	r1, [r1]
80703110:	e1a00005 	mov	r0, r5
80703114:	ebfffeb2 	bl	80702be4 <ddr_training_set_timing>
80703118:	e5923004 	ldr	r3, [r2, #4]
8070311c:	e1a00002 	mov	r0, r2
80703120:	e5853028 	str	r3, [r5, #40]	; 0x28
80703124:	e5923010 	ldr	r3, [r2, #16]
80703128:	e5843070 	str	r3, [r4, #112]	; 0x70
8070312c:	e594302c 	ldr	r3, [r4, #44]	; 0x2c
80703130:	e3130020 	tst	r3, #32
80703134:	05921014 	ldreq	r1, [r2, #20]
80703138:	03013064 	movweq	r3, #4196	; 0x1064
8070313c:	07841003 	streq	r1, [r4, r3]
80703140:	e5923018 	ldr	r3, [r2, #24]
80703144:	e5843084 	str	r3, [r4, #132]	; 0x84
80703148:	e59f3028 	ldr	r3, [pc, #40]	; 80703178 <ddr_training_restore_reg+0x7c>
8070314c:	e5921024 	ldr	r1, [r2, #36]	; 0x24
80703150:	e5831104 	str	r1, [r3, #260]	; 0x104
80703154:	e5921028 	ldr	r1, [r2, #40]	; 0x28
80703158:	e5831114 	str	r1, [r3, #276]	; 0x114
8070315c:	e592302c 	ldr	r3, [r2, #44]	; 0x2c
80703160:	e5853060 	str	r3, [r5, #96]	; 0x60
80703164:	eb00066e 	bl	80704b24 <ddr_training_restore_reg_custom>
80703168:	e1a00004 	mov	r0, r4
8070316c:	ebffff99 	bl	80702fd8 <ddr_phy_cfg_update>
80703170:	f57ff04f 	dsb	sy
80703174:	e8bd8070 	pop	{r4, r5, r6, pc}
80703178:	12060000 	.word	0x12060000

8070317c <ddr_phy_set_dq_bdl>:
8070317c:	e92d4010 	push	{r4, lr}
80703180:	e1a0e000 	mov	lr, r0
80703184:	e59ec054 	ldr	ip, [lr, #84]	; 0x54
80703188:	e59e3060 	ldr	r3, [lr, #96]	; 0x60
8070318c:	e59e2058 	ldr	r2, [lr, #88]	; 0x58
80703190:	e59ee050 	ldr	lr, [lr, #80]	; 0x50
80703194:	e1a0c38c 	lsl	ip, ip, #7
80703198:	e1a03503 	lsl	r3, r3, #10
8070319c:	e5900040 	ldr	r0, [r0, #64]	; 0x40
807031a0:	e35e0002 	cmp	lr, #2
807031a4:	e2024007 	and	r4, r2, #7
807031a8:	e083300c 	add	r3, r3, ip
807031ac:	1a00000c 	bne	807031e4 <ddr_phy_set_dq_bdl+0x68>
807031b0:	e3540003 	cmp	r4, #3
807031b4:	92833e21 	addls	r3, r3, #528	; 0x210
807031b8:	82833f85 	addhi	r3, r3, #532	; 0x214
807031bc:	e793c000 	ldr	ip, [r3, r0]
807031c0:	e1a02182 	lsl	r2, r2, #3
807031c4:	e2022018 	and	r2, r2, #24
807031c8:	e3a0e0ff 	mov	lr, #255	; 0xff
807031cc:	e201107f 	and	r1, r1, #127	; 0x7f
807031d0:	e1ccc21e 	bic	ip, ip, lr, lsl r2
807031d4:	e18c2211 	orr	r2, ip, r1, lsl r2
807031d8:	e7832000 	str	r2, [r3, r0]
807031dc:	e8bd4010 	pop	{r4, lr}
807031e0:	eaffff7c 	b	80702fd8 <ddr_phy_cfg_update>
807031e4:	e3540003 	cmp	r4, #3
807031e8:	92833f87 	addls	r3, r3, #540	; 0x21c
807031ec:	82833e22 	addhi	r3, r3, #544	; 0x220
807031f0:	eafffff1 	b	807031bc <ddr_phy_set_dq_bdl+0x40>

807031f4 <ddr_phy_get_dq_bdl>:
807031f4:	e52de004 	push	{lr}		; (str lr, [sp, #-4]!)
807031f8:	e5901054 	ldr	r1, [r0, #84]	; 0x54
807031fc:	e5903060 	ldr	r3, [r0, #96]	; 0x60
80703200:	e590e050 	ldr	lr, [r0, #80]	; 0x50
80703204:	e5902058 	ldr	r2, [r0, #88]	; 0x58
80703208:	e1a01381 	lsl	r1, r1, #7
8070320c:	e35e0002 	cmp	lr, #2
80703210:	e1a03503 	lsl	r3, r3, #10
80703214:	e202c007 	and	ip, r2, #7
80703218:	e0833001 	add	r3, r3, r1
8070321c:	1a000009 	bne	80703248 <ddr_phy_get_dq_bdl+0x54>
80703220:	e35c0003 	cmp	ip, #3
80703224:	92833e21 	addls	r3, r3, #528	; 0x210
80703228:	82833f85 	addhi	r3, r3, #532	; 0x214
8070322c:	e5901040 	ldr	r1, [r0, #64]	; 0x40
80703230:	e1a02182 	lsl	r2, r2, #3
80703234:	e2022018 	and	r2, r2, #24
80703238:	e7930001 	ldr	r0, [r3, r1]
8070323c:	e1a00230 	lsr	r0, r0, r2
80703240:	e200007f 	and	r0, r0, #127	; 0x7f
80703244:	e49df004 	pop	{pc}		; (ldr pc, [sp], #4)
80703248:	e35c0003 	cmp	ip, #3
8070324c:	92833f87 	addls	r3, r3, #540	; 0x21c
80703250:	82833e22 	addhi	r3, r3, #544	; 0x220
80703254:	eafffff4 	b	8070322c <ddr_phy_get_dq_bdl+0x38>

80703258 <ddr_rdqs_sync_rank_rdq>:
80703258:	e92d4070 	push	{r4, r5, r6, lr}
8070325c:	e1a04000 	mov	r4, r0
80703260:	e5902060 	ldr	r2, [r0, #96]	; 0x60
80703264:	e1a05001 	mov	r5, r1
80703268:	e5903040 	ldr	r3, [r0, #64]	; 0x40
8070326c:	e3a06000 	mov	r6, #0
80703270:	e5900054 	ldr	r0, [r0, #84]	; 0x54
80703274:	e2833f89 	add	r3, r3, #548	; 0x224
80703278:	e0833502 	add	r3, r3, r2, lsl #10
8070327c:	e7932380 	ldr	r2, [r3, r0, lsl #7]
80703280:	e202107f 	and	r1, r2, #127	; 0x7f
80703284:	e3c2207f 	bic	r2, r2, #127	; 0x7f
80703288:	e0851001 	add	r1, r5, r1
8070328c:	e6e71011 	usat	r1, #7, r1
80703290:	e1822001 	orr	r2, r2, r1
80703294:	e7832380 	str	r2, [r3, r0, lsl #7]
80703298:	e5846058 	str	r6, [r4, #88]	; 0x58
8070329c:	e1a00004 	mov	r0, r4
807032a0:	ebffffd3 	bl	807031f4 <ddr_phy_get_dq_bdl>
807032a4:	e2866001 	add	r6, r6, #1
807032a8:	e0851000 	add	r1, r5, r0
807032ac:	e1a00004 	mov	r0, r4
807032b0:	e6e71011 	usat	r1, #7, r1
807032b4:	ebffffb0 	bl	8070317c <ddr_phy_set_dq_bdl>
807032b8:	e3560008 	cmp	r6, #8
807032bc:	1afffff5 	bne	80703298 <ddr_rdqs_sync_rank_rdq+0x40>
807032c0:	e8bd8070 	pop	{r4, r5, r6, pc}

807032c4 <ddr_adjust_set_val>:
807032c4:	e92d41f0 	push	{r4, r5, r6, r7, r8, lr}
807032c8:	e1a04000 	mov	r4, r0
807032cc:	e5903050 	ldr	r3, [r0, #80]	; 0x50
807032d0:	e1a07001 	mov	r7, r1
807032d4:	e5902054 	ldr	r2, [r0, #84]	; 0x54
807032d8:	e3530001 	cmp	r3, #1
807032dc:	e5908060 	ldr	r8, [r0, #96]	; 0x60
807032e0:	e5903040 	ldr	r3, [r0, #64]	; 0x40
807032e4:	1a00001c 	bne	8070335c <ddr_adjust_set_val+0x98>
807032e8:	e2833f8b 	add	r3, r3, #556	; 0x22c
807032ec:	e7935382 	ldr	r5, [r3, r2, lsl #7]
807032f0:	e7936382 	ldr	r6, [r3, r2, lsl #7]
807032f4:	e7e86056 	ubfx	r6, r6, #0, #9
807032f8:	e0416006 	sub	r6, r1, r6
807032fc:	e1a01006 	mov	r1, r6
80703300:	ebffffd4 	bl	80703258 <ddr_rdqs_sync_rank_rdq>
80703304:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
80703308:	e3a0203c 	mov	r2, #60	; 0x3c
8070330c:	e0234392 	mla	r3, r2, r3, r4
80703310:	e593300c 	ldr	r3, [r3, #12]
80703314:	e3530001 	cmp	r3, #1
80703318:	0a000005 	beq	80703334 <ddr_adjust_set_val+0x70>
8070331c:	e2683001 	rsb	r3, r8, #1
80703320:	e1a01006 	mov	r1, r6
80703324:	e5843060 	str	r3, [r4, #96]	; 0x60
80703328:	e1a00004 	mov	r0, r4
8070332c:	ebffffc9 	bl	80703258 <ddr_rdqs_sync_rank_rdq>
80703330:	e5848060 	str	r8, [r4, #96]	; 0x60
80703334:	e3c51f7f 	bic	r1, r5, #508	; 0x1fc
80703338:	e5943040 	ldr	r3, [r4, #64]	; 0x40
8070333c:	e3c11003 	bic	r1, r1, #3
80703340:	e5942054 	ldr	r2, [r4, #84]	; 0x54
80703344:	e1811007 	orr	r1, r1, r7
80703348:	e2833f8b 	add	r3, r3, #556	; 0x22c
8070334c:	e7831382 	str	r1, [r3, r2, lsl #7]
80703350:	e5940040 	ldr	r0, [r4, #64]	; 0x40
80703354:	e8bd41f0 	pop	{r4, r5, r6, r7, r8, lr}
80703358:	eaffff1e 	b	80702fd8 <ddr_phy_cfg_update>
8070335c:	e2833f8d 	add	r3, r3, #564	; 0x234
80703360:	e0833508 	add	r3, r3, r8, lsl #10
80703364:	e7931382 	ldr	r1, [r3, r2, lsl #7]
80703368:	e3c11c1f 	bic	r1, r1, #7936	; 0x1f00
8070336c:	e1811407 	orr	r1, r1, r7, lsl #8
80703370:	eafffff5 	b	8070334c <ddr_adjust_set_val+0x88>

80703374 <ddr_phy_get_byte_num>:
80703374:	e5900050 	ldr	r0, [r0, #80]	; 0x50
80703378:	e1a001a0 	lsr	r0, r0, #3
8070337c:	e2000006 	and	r0, r0, #6
80703380:	e3500004 	cmp	r0, #4
80703384:	23a00004 	movcs	r0, #4
80703388:	e12fff1e 	bx	lr

8070338c <ddr_training_cfg_set_dmc>:
8070338c:	e92d4010 	push	{r4, lr}
80703390:	e1a03000 	mov	r3, r0
80703394:	e5902004 	ldr	r2, [r0, #4]
80703398:	e59f0070 	ldr	r0, [pc, #112]	; 80703410 <ddr_training_cfg_set_dmc+0x84>
8070339c:	e3520006 	cmp	r2, #6
807033a0:	e59f206c 	ldr	r2, [pc, #108]	; 80703414 <ddr_training_cfg_set_dmc+0x88>
807033a4:	1a000011 	bne	807033f0 <ddr_training_cfg_set_dmc+0x64>
807033a8:	e3a01002 	mov	r1, #2
807033ac:	e5831008 	str	r1, [r3, #8]
807033b0:	e59220a8 	ldr	r2, [r2, #168]	; 0xa8
807033b4:	e5830014 	str	r0, [r3, #20]
807033b8:	e6ff1072 	uxth	r1, r2
807033bc:	e583101c 	str	r1, [r3, #28]
807033c0:	ebffffeb 	bl	80703374 <ddr_phy_get_byte_num>
807033c4:	e5830018 	str	r0, [r3, #24]
807033c8:	e1a02822 	lsr	r2, r2, #16
807033cc:	e59f0044 	ldr	r0, [pc, #68]	; 80703418 <ddr_training_cfg_set_dmc+0x8c>
807033d0:	e5832028 	str	r2, [r3, #40]	; 0x28
807033d4:	e5830020 	str	r0, [r3, #32]
807033d8:	ebffffe5 	bl	80703374 <ddr_phy_get_byte_num>
807033dc:	e5932018 	ldr	r2, [r3, #24]
807033e0:	e5830024 	str	r0, [r3, #36]	; 0x24
807033e4:	e0820000 	add	r0, r2, r0
807033e8:	e5830010 	str	r0, [r3, #16]
807033ec:	e8bd8010 	pop	{r4, pc}
807033f0:	e3a01001 	mov	r1, #1
807033f4:	e5830014 	str	r0, [r3, #20]
807033f8:	e5831008 	str	r1, [r3, #8]
807033fc:	e59220a8 	ldr	r2, [r2, #168]	; 0xa8
80703400:	e583201c 	str	r2, [r3, #28]
80703404:	ebffffda 	bl	80703374 <ddr_phy_get_byte_num>
80703408:	e5830018 	str	r0, [r3, #24]
8070340c:	eafffff5 	b	807033e8 <ddr_training_cfg_set_dmc+0x5c>
80703410:	12068000 	.word	0x12068000
80703414:	12020000 	.word	0x12020000
80703418:	12069000 	.word	0x12069000

8070341c <ddr_training_cfg_init>:
8070341c:	e92d4010 	push	{r4, lr}
80703420:	e3a02078 	mov	r2, #120	; 0x78
80703424:	e1a04000 	mov	r4, r0
80703428:	e3a01000 	mov	r1, #0
8070342c:	ebfffd74 	bl	80702a04 <ddrtr_memset>
80703430:	e1a00004 	mov	r0, r4
80703434:	ebfffde1 	bl	80702bc0 <ddr_training_cfg_set_phy>
80703438:	e1a00004 	mov	r0, r4
8070343c:	ebffffd2 	bl	8070338c <ddr_training_cfg_set_dmc>
80703440:	e1a00004 	mov	r0, r4
80703444:	e8bd4010 	pop	{r4, lr}
80703448:	eafffdcb 	b	80702b7c <ddr_training_cfg_set_rank>

8070344c <ddr_ddrt_init>:
8070344c:	e92d41f0 	push	{r4, r5, r6, r7, r8, lr}
80703450:	e1a06000 	mov	r6, r0
80703454:	e5905060 	ldr	r5, [r0, #96]	; 0x60
80703458:	e1a07001 	mov	r7, r1
8070345c:	e3550001 	cmp	r5, #1
80703460:	13a05000 	movne	r5, #0
80703464:	1a00000b 	bne	80703498 <ddr_ddrt_init+0x4c>
80703468:	e5903044 	ldr	r3, [r0, #68]	; 0x44
8070346c:	e5932050 	ldr	r2, [r3, #80]	; 0x50
80703470:	e5933060 	ldr	r3, [r3, #96]	; 0x60
80703474:	e7e12252 	ubfx	r2, r2, #4, #2
80703478:	e2030007 	and	r0, r3, #7
8070347c:	e2800015 	add	r0, r0, #21
80703480:	e0800002 	add	r0, r0, r2
80703484:	e7e12453 	ubfx	r2, r3, #8, #2
80703488:	e0800002 	add	r0, r0, r2
8070348c:	e7e23253 	ubfx	r3, r3, #4, #3
80703490:	e0800003 	add	r0, r0, r3
80703494:	e1a05015 	lsl	r5, r5, r0
80703498:	e5963044 	ldr	r3, [r6, #68]	; 0x44
8070349c:	e3002152 	movw	r2, #338	; 0x152
807034a0:	e59f40a4 	ldr	r4, [pc, #164]	; 8070354c <ddr_ddrt_init+0x100>
807034a4:	e5933050 	ldr	r3, [r3, #80]	; 0x50
807034a8:	e7e13253 	ubfx	r3, r3, #4, #2
807034ac:	e2433001 	sub	r3, r3, #1
807034b0:	e1823603 	orr	r3, r2, r3, lsl #12
807034b4:	e584300c 	str	r3, [r4, #12]
807034b8:	e3a03202 	mov	r3, #536870912	; 0x20000000
807034bc:	e584301c 	str	r3, [r4, #28]
807034c0:	eb000568 	bl	80704a68 <ddr_ddrt_get_test_addr>
807034c4:	e59f3084 	ldr	r3, [pc, #132]	; 80703550 <ddr_ddrt_init+0x104>
807034c8:	e3570001 	cmp	r7, #1
807034cc:	e0850000 	add	r0, r5, r0
807034d0:	e1a00120 	lsr	r0, r0, #2
807034d4:	e5840020 	str	r0, [r4, #32]
807034d8:	e5843038 	str	r3, [r4, #56]	; 0x38
807034dc:	1a000008 	bne	80703504 <ddr_ddrt_init+0xb8>
807034e0:	e3a03043 	mov	r3, #67	; 0x43
807034e4:	e5843008 	str	r3, [r4, #8]
807034e8:	e3a03000 	mov	r3, #0
807034ec:	e5843010 	str	r3, [r4, #16]
807034f0:	e5843014 	str	r3, [r4, #20]
807034f4:	e5843018 	str	r3, [r4, #24]
807034f8:	e59f3054 	ldr	r3, [pc, #84]	; 80703554 <ddr_ddrt_init+0x108>
807034fc:	e5843030 	str	r3, [r4, #48]	; 0x30
80703500:	e8bd81f0 	pop	{r4, r5, r6, r7, r8, pc}
80703504:	e3a0304f 	mov	r3, #79	; 0x4f
80703508:	e5843008 	str	r3, [r4, #8]
8070350c:	e596205c 	ldr	r2, [r6, #92]	; 0x5c
80703510:	e3a0303c 	mov	r3, #60	; 0x3c
80703514:	e5960064 	ldr	r0, [r6, #100]	; 0x64
80703518:	e0030293 	mul	r3, r3, r2
8070351c:	e3a0200c 	mov	r2, #12
80703520:	e0203092 	mla	r0, r2, r0, r3
80703524:	e0860000 	add	r0, r6, r0
80703528:	e590301c 	ldr	r3, [r0, #28]
8070352c:	e5843030 	str	r3, [r4, #48]	; 0x30
80703530:	e3a0307f 	mov	r3, #127	; 0x7f
80703534:	e5843010 	str	r3, [r4, #16]
80703538:	e3e03000 	mvn	r3, #0
8070353c:	e5843014 	str	r3, [r4, #20]
80703540:	e3a03000 	mov	r3, #0
80703544:	e5843018 	str	r3, [r4, #24]
80703548:	e8bd81f0 	pop	{r4, r5, r6, r7, r8, pc}
8070354c:	11250000 	.word	0x11250000
80703550:	6d6d6d6d 	.word	0x6d6d6d6d
80703554:	55aa55aa 	.word	0x55aa55aa

80703558 <ddr_ddrt_test>:
80703558:	e59f3108 	ldr	r3, [pc, #264]	; 80703668 <ddr_ddrt_test+0x110>
8070355c:	e380c001 	orr	ip, r0, #1
80703560:	e92d4010 	push	{r4, lr}
80703564:	e583c000 	str	ip, [r3]
80703568:	e3a0c000 	mov	ip, #0
8070356c:	e583c004 	str	ip, [r3, #4]
80703570:	f57ff04f 	dsb	sy
80703574:	e59f40f0 	ldr	r4, [pc, #240]	; 8070366c <ddr_ddrt_test+0x114>
80703578:	e593e004 	ldr	lr, [r3, #4]
8070357c:	e28cc001 	add	ip, ip, #1
80703580:	e31e0001 	tst	lr, #1
80703584:	1a000008 	bne	807035ac <ddr_ddrt_test+0x54>
80703588:	e15c0004 	cmp	ip, r4
8070358c:	1afffff9 	bne	80703578 <ddr_ddrt_test+0x20>
80703590:	e3e03000 	mvn	r3, #0
80703594:	e3a01000 	mov	r1, #0
80703598:	e1a02003 	mov	r2, r3
8070359c:	e3a00008 	mov	r0, #8
807035a0:	ebfffe09 	bl	80702dcc <ddr_training_stat>
807035a4:	e3e02000 	mvn	r2, #0
807035a8:	ea000019 	b	80703614 <ddr_ddrt_test+0xbc>
807035ac:	e15c0004 	cmp	ip, r4
807035b0:	0afffff6 	beq	80703590 <ddr_ddrt_test+0x38>
807035b4:	e2000c03 	and	r0, r0, #768	; 0x300
807035b8:	e3500c02 	cmp	r0, #512	; 0x200
807035bc:	0a000027 	beq	80703660 <ddr_ddrt_test+0x108>
807035c0:	e21ee002 	ands	lr, lr, #2
807035c4:	1a000025 	bne	80703660 <ddr_ddrt_test+0x108>
807035c8:	e3720001 	cmn	r2, #1
807035cc:	0a000012 	beq	8070361c <ddr_ddrt_test+0xc4>
807035d0:	e5930080 	ldr	r0, [r3, #128]	; 0x80
807035d4:	e3a0c001 	mov	ip, #1
807035d8:	e0823181 	add	r3, r2, r1, lsl #3
807035dc:	e010331c 	ands	r3, r0, ip, lsl r3
807035e0:	1affffef 	bne	807035a4 <ddr_ddrt_test+0x4c>
807035e4:	e3520003 	cmp	r2, #3
807035e8:	c2423004 	subgt	r3, r2, #4
807035ec:	d1a03182 	lslle	r3, r2, #3
807035f0:	e1a02122 	lsr	r2, r2, #2
807035f4:	e0821081 	add	r1, r2, r1, lsl #1
807035f8:	e59f2070 	ldr	r2, [pc, #112]	; 80703670 <ddr_ddrt_test+0x118>
807035fc:	c1a03183 	lslgt	r3, r3, #3
80703600:	e7922101 	ldr	r2, [r2, r1, lsl #2]
80703604:	e3a010ff 	mov	r1, #255	; 0xff
80703608:	e0123311 	ands	r3, r2, r1, lsl r3
8070360c:	13e02000 	mvnne	r2, #0
80703610:	03a02000 	moveq	r2, #0
80703614:	e1a00002 	mov	r0, r2
80703618:	e8bd8010 	pop	{r4, pc}
8070361c:	e3710001 	cmn	r1, #1
80703620:	01a0200e 	moveq	r2, lr
80703624:	0afffffa 	beq	80703614 <ddr_ddrt_test+0xbc>
80703628:	e5930080 	ldr	r0, [r3, #128]	; 0x80
8070362c:	e3a0c0ff 	mov	ip, #255	; 0xff
80703630:	e1a03181 	lsl	r3, r1, #3
80703634:	e010331c 	ands	r3, r0, ip, lsl r3
80703638:	1afffff5 	bne	80703614 <ddr_ddrt_test+0xbc>
8070363c:	e59f302c 	ldr	r3, [pc, #44]	; 80703670 <ddr_ddrt_test+0x118>
80703640:	e7932181 	ldr	r2, [r3, r1, lsl #3]
80703644:	e1a01081 	lsl	r1, r1, #1
80703648:	e2811001 	add	r1, r1, #1
8070364c:	e7933101 	ldr	r3, [r3, r1, lsl #2]
80703650:	e1720003 	cmn	r2, r3
80703654:	13e02000 	mvnne	r2, #0
80703658:	03a02000 	moveq	r2, #0
8070365c:	eaffffec 	b	80703614 <ddr_ddrt_test+0xbc>
80703660:	e3a02000 	mov	r2, #0
80703664:	eaffffea 	b	80703614 <ddr_ddrt_test+0xbc>
80703668:	11250000 	.word	0x11250000
8070366c:	000f4240 	.word	0x000f4240
80703670:	11250060 	.word	0x11250060

80703674 <ddr_dataeye_check_dq>:
80703674:	e590306c 	ldr	r3, [r0, #108]	; 0x6c
80703678:	e3530001 	cmp	r3, #1
8070367c:	1a000018 	bne	807036e4 <ddr_dataeye_check_dq+0x70>
80703680:	e92d4070 	push	{r4, r5, r6, lr}
80703684:	e1a04000 	mov	r4, r0
80703688:	e5903064 	ldr	r3, [r0, #100]	; 0x64
8070368c:	e5905054 	ldr	r5, [r0, #84]	; 0x54
80703690:	e3a00000 	mov	r0, #0
80703694:	e59f6058 	ldr	r6, [pc, #88]	; 807036f4 <ddr_dataeye_check_dq+0x80>
80703698:	e1a03083 	lsl	r3, r3, #1
8070369c:	e1550003 	cmp	r5, r3
807036a0:	20455003 	subcs	r5, r5, r3
807036a4:	e5860030 	str	r0, [r6, #48]	; 0x30
807036a8:	e5942058 	ldr	r2, [r4, #88]	; 0x58
807036ac:	e1a01005 	mov	r1, r5
807036b0:	ebffffa8 	bl	80703558 <ddr_ddrt_test>
807036b4:	e3500000 	cmp	r0, #0
807036b8:	1a00000b 	bne	807036ec <ddr_dataeye_check_dq+0x78>
807036bc:	e594304c 	ldr	r3, [r4, #76]	; 0x4c
807036c0:	e1a01005 	mov	r1, r5
807036c4:	e3a00a02 	mov	r0, #8192	; 0x2000
807036c8:	e5863030 	str	r3, [r6, #48]	; 0x30
807036cc:	e5942058 	ldr	r2, [r4, #88]	; 0x58
807036d0:	ebffffa0 	bl	80703558 <ddr_ddrt_test>
807036d4:	e2900000 	adds	r0, r0, #0
807036d8:	13a00001 	movne	r0, #1
807036dc:	e2600000 	rsb	r0, r0, #0
807036e0:	e8bd8070 	pop	{r4, r5, r6, pc}
807036e4:	e3a00000 	mov	r0, #0
807036e8:	e12fff1e 	bx	lr
807036ec:	e3e00000 	mvn	r0, #0
807036f0:	e8bd8070 	pop	{r4, r5, r6, pc}
807036f4:	11250000 	.word	0x11250000

807036f8 <ddr_dataeye_search_dq>:
807036f8:	e92d4df3 	push	{r0, r1, r4, r5, r6, r7, r8, sl, fp, lr}
807036fc:	e1a06000 	mov	r6, r0
80703700:	e59d5028 	ldr	r5, [sp, #40]	; 0x28
80703704:	e1a0a001 	mov	sl, r1
80703708:	e1a0b002 	mov	fp, r2
8070370c:	e1a08003 	mov	r8, r3
80703710:	e04a4006 	sub	r4, sl, r6
80703714:	e1a00005 	mov	r0, r5
80703718:	e08640a4 	add	r4, r6, r4, lsr #1
8070371c:	e1a01004 	mov	r1, r4
80703720:	ebfffe95 	bl	8070317c <ddr_phy_set_dq_bdl>
80703724:	e1a00005 	mov	r0, r5
80703728:	ebffffd1 	bl	80703674 <ddr_dataeye_check_dq>
8070372c:	e3580002 	cmp	r8, #2
80703730:	0a000009 	beq	8070375c <ddr_dataeye_search_dq+0x64>
80703734:	e3580004 	cmp	r8, #4
80703738:	0a00000f 	beq	8070377c <ddr_dataeye_search_dq+0x84>
8070373c:	e3580001 	cmp	r8, #1
80703740:	03a07006 	moveq	r7, #6
80703744:	13a07000 	movne	r7, #0
80703748:	e3500000 	cmp	r0, #0
8070374c:	1a00000f 	bne	80703790 <ddr_dataeye_search_dq+0x98>
80703750:	e58b4000 	str	r4, [fp]
80703754:	e28dd008 	add	sp, sp, #8
80703758:	e8bd8df0 	pop	{r4, r5, r6, r7, r8, sl, fp, pc}
8070375c:	e3500000 	cmp	r0, #0
80703760:	1a000009 	bne	8070378c <ddr_dataeye_search_dq+0x94>
80703764:	e1a01006 	mov	r1, r6
80703768:	e1a00005 	mov	r0, r5
8070376c:	ebfffe82 	bl	8070317c <ddr_phy_set_dq_bdl>
80703770:	e1a00005 	mov	r0, r5
80703774:	ebffffbe 	bl	80703674 <ddr_dataeye_check_dq>
80703778:	eafffff4 	b	80703750 <ddr_dataeye_search_dq+0x58>
8070377c:	e3500000 	cmp	r0, #0
80703780:	1a000011 	bne	807037cc <ddr_dataeye_search_dq+0xd4>
80703784:	e1a0100a 	mov	r1, sl
80703788:	eafffff6 	b	80703768 <ddr_dataeye_search_dq+0x70>
8070378c:	e3a07004 	mov	r7, #4
80703790:	e154000a 	cmp	r4, sl
80703794:	11560004 	cmpne	r6, r4
80703798:	0affffed 	beq	80703754 <ddr_dataeye_search_dq+0x5c>
8070379c:	e3170002 	tst	r7, #2
807037a0:	0a000005 	beq	807037bc <ddr_dataeye_search_dq+0xc4>
807037a4:	e58d5000 	str	r5, [sp]
807037a8:	e1a03008 	mov	r3, r8
807037ac:	e1a0200b 	mov	r2, fp
807037b0:	e1a01004 	mov	r1, r4
807037b4:	e1a00006 	mov	r0, r6
807037b8:	ebffffce 	bl	807036f8 <ddr_dataeye_search_dq>
807037bc:	e3170004 	tst	r7, #4
807037c0:	e1a06004 	mov	r6, r4
807037c4:	0affffe2 	beq	80703754 <ddr_dataeye_search_dq+0x5c>
807037c8:	eaffffd0 	b	80703710 <ddr_dataeye_search_dq+0x18>
807037cc:	e3a07002 	mov	r7, #2
807037d0:	eaffffee 	b	80703790 <ddr_dataeye_search_dq+0x98>

807037d4 <ddr_dataeye_deskew>:
807037d4:	e92d4df0 	push	{r4, r5, r6, r7, r8, sl, fp, lr}
807037d8:	e3a05000 	mov	r5, #0
807037dc:	e5903054 	ldr	r3, [r0, #84]	; 0x54
807037e0:	e24dd028 	sub	sp, sp, #40	; 0x28
807037e4:	e1a04000 	mov	r4, r0
807037e8:	e1a06001 	mov	r6, r1
807037ec:	e1a08005 	mov	r8, r5
807037f0:	e1a0a005 	mov	sl, r5
807037f4:	e58d3008 	str	r3, [sp, #8]
807037f8:	e1a03183 	lsl	r3, r3, #3
807037fc:	e5815100 	str	r5, [r1, #256]	; 0x100
80703800:	e58d300c 	str	r3, [sp, #12]
80703804:	e59d300c 	ldr	r3, [sp, #12]
80703808:	e1a00004 	mov	r0, r4
8070380c:	e5845058 	str	r5, [r4, #88]	; 0x58
80703810:	e0853003 	add	r3, r5, r3
80703814:	e58d3010 	str	r3, [sp, #16]
80703818:	ebfffe75 	bl	807031f4 <ddr_phy_get_dq_bdl>
8070381c:	e5943054 	ldr	r3, [r4, #84]	; 0x54
80703820:	e5947058 	ldr	r7, [r4, #88]	; 0x58
80703824:	e0877183 	add	r7, r7, r3, lsl #3
80703828:	e58d0014 	str	r0, [sp, #20]
8070382c:	e1a00004 	mov	r0, r4
80703830:	ebfffe6f 	bl	807031f4 <ddr_phy_get_dq_bdl>
80703834:	e58d001c 	str	r0, [sp, #28]
80703838:	e1a0b000 	mov	fp, r0
8070383c:	e1a00004 	mov	r0, r4
80703840:	ebffff8b 	bl	80703674 <ddr_dataeye_check_dq>
80703844:	e3500000 	cmp	r0, #0
80703848:	0a000037 	beq	8070392c <ddr_dataeye_deskew+0x158>
8070384c:	e28d2028 	add	r2, sp, #40	; 0x28
80703850:	e3e03000 	mvn	r3, #0
80703854:	e3a0107f 	mov	r1, #127	; 0x7f
80703858:	e3a00000 	mov	r0, #0
8070385c:	e522300c 	str	r3, [r2, #-12]!
80703860:	e3a03001 	mov	r3, #1
80703864:	e58d4000 	str	r4, [sp]
80703868:	ebffffa2 	bl	807036f8 <ddr_dataeye_search_dq>
8070386c:	e59d301c 	ldr	r3, [sp, #28]
80703870:	e3730001 	cmn	r3, #1
80703874:	1a00002c 	bne	8070392c <ddr_dataeye_deskew+0x158>
80703878:	e0862107 	add	r2, r6, r7, lsl #2
8070387c:	e3a03000 	mov	r3, #0
80703880:	e1a0100b 	mov	r1, fp
80703884:	e7863107 	str	r3, [r6, r7, lsl #2]
80703888:	e1a00004 	mov	r0, r4
8070388c:	e5823080 	str	r3, [r2, #128]	; 0x80
80703890:	ebfffe39 	bl	8070317c <ddr_phy_set_dq_bdl>
80703894:	e59d3010 	ldr	r3, [sp, #16]
80703898:	e2833020 	add	r3, r3, #32
8070389c:	e7967103 	ldr	r7, [r6, r3, lsl #2]
807038a0:	e1a0b827 	lsr	fp, r7, #16
807038a4:	e35b0007 	cmp	fp, #7
807038a8:	8a000057 	bhi	80703a0c <ddr_dataeye_deskew+0x238>
807038ac:	e35a0000 	cmp	sl, #0
807038b0:	02455001 	subeq	r5, r5, #1
807038b4:	03a0a001 	moveq	sl, #1
807038b8:	0a000009 	beq	807038e4 <ddr_dataeye_deskew+0x110>
807038bc:	e35b0000 	cmp	fp, #0
807038c0:	1a000051 	bne	80703a0c <ddr_dataeye_deskew+0x238>
807038c4:	e59d1014 	ldr	r1, [sp, #20]
807038c8:	e1a00004 	mov	r0, r4
807038cc:	ebfffe2a 	bl	8070317c <ddr_phy_set_dq_bdl>
807038d0:	e1a03005 	mov	r3, r5
807038d4:	e59d2008 	ldr	r2, [sp, #8]
807038d8:	e3a00040 	mov	r0, #64	; 0x40
807038dc:	e5941040 	ldr	r1, [r4, #64]	; 0x40
807038e0:	ebfffd39 	bl	80702dcc <ddr_training_stat>
807038e4:	e2855001 	add	r5, r5, #1
807038e8:	e3550007 	cmp	r5, #7
807038ec:	daffffc4 	ble	80703804 <ddr_dataeye_deskew+0x30>
807038f0:	e5943050 	ldr	r3, [r4, #80]	; 0x50
807038f4:	e3530002 	cmp	r3, #2
807038f8:	1a000006 	bne	80703918 <ddr_dataeye_deskew+0x144>
807038fc:	e5942060 	ldr	r2, [r4, #96]	; 0x60
80703900:	e7e681d8 	ubfx	r8, r8, #3, #7
80703904:	e5943040 	ldr	r3, [r4, #64]	; 0x40
80703908:	e2833f86 	add	r3, r3, #536	; 0x218
8070390c:	e0833502 	add	r3, r3, r2, lsl #10
80703910:	e59d2008 	ldr	r2, [sp, #8]
80703914:	e7838382 	str	r8, [r3, r2, lsl #7]
80703918:	e5940040 	ldr	r0, [r4, #64]	; 0x40
8070391c:	ebfffdad 	bl	80702fd8 <ddr_phy_cfg_update>
80703920:	e3a00000 	mov	r0, #0
80703924:	e28dd028 	add	sp, sp, #40	; 0x28
80703928:	e8bd8df0 	pop	{r4, r5, r6, r7, r8, sl, fp, pc}
8070392c:	e59d101c 	ldr	r1, [sp, #28]
80703930:	e3a03002 	mov	r3, #2
80703934:	e58d4000 	str	r4, [sp]
80703938:	e28d2020 	add	r2, sp, #32
8070393c:	e3a00000 	mov	r0, #0
80703940:	e58d1020 	str	r1, [sp, #32]
80703944:	ebffff6b 	bl	807036f8 <ddr_dataeye_search_dq>
80703948:	e59d1020 	ldr	r1, [sp, #32]
8070394c:	e3510000 	cmp	r1, #0
80703950:	da00000a 	ble	80703980 <ddr_dataeye_deskew+0x1ac>
80703954:	e2411001 	sub	r1, r1, #1
80703958:	e1a00004 	mov	r0, r4
8070395c:	e58d1020 	str	r1, [sp, #32]
80703960:	ebfffe05 	bl	8070317c <ddr_phy_set_dq_bdl>
80703964:	e1a00004 	mov	r0, r4
80703968:	ebffff41 	bl	80703674 <ddr_dataeye_check_dq>
8070396c:	e3500000 	cmp	r0, #0
80703970:	0afffff4 	beq	80703948 <ddr_dataeye_deskew+0x174>
80703974:	e59d3020 	ldr	r3, [sp, #32]
80703978:	e2833001 	add	r3, r3, #1
8070397c:	e58d3020 	str	r3, [sp, #32]
80703980:	e59d001c 	ldr	r0, [sp, #28]
80703984:	e28d2028 	add	r2, sp, #40	; 0x28
80703988:	e3a03004 	mov	r3, #4
8070398c:	e3a0107f 	mov	r1, #127	; 0x7f
80703990:	e5220004 	str	r0, [r2, #-4]!
80703994:	e58d4000 	str	r4, [sp]
80703998:	ebffff56 	bl	807036f8 <ddr_dataeye_search_dq>
8070399c:	e59d1024 	ldr	r1, [sp, #36]	; 0x24
807039a0:	e351007e 	cmp	r1, #126	; 0x7e
807039a4:	ca00000a 	bgt	807039d4 <ddr_dataeye_deskew+0x200>
807039a8:	e2811001 	add	r1, r1, #1
807039ac:	e1a00004 	mov	r0, r4
807039b0:	e58d1024 	str	r1, [sp, #36]	; 0x24
807039b4:	ebfffdf0 	bl	8070317c <ddr_phy_set_dq_bdl>
807039b8:	e1a00004 	mov	r0, r4
807039bc:	ebffff2c 	bl	80703674 <ddr_dataeye_check_dq>
807039c0:	e3500000 	cmp	r0, #0
807039c4:	0afffff4 	beq	8070399c <ddr_dataeye_deskew+0x1c8>
807039c8:	e59d3024 	ldr	r3, [sp, #36]	; 0x24
807039cc:	e2433001 	sub	r3, r3, #1
807039d0:	e58d3024 	str	r3, [sp, #36]	; 0x24
807039d4:	e1a0100b 	mov	r1, fp
807039d8:	e1a00004 	mov	r0, r4
807039dc:	ebfffde6 	bl	8070317c <ddr_phy_set_dq_bdl>
807039e0:	e59d3020 	ldr	r3, [sp, #32]
807039e4:	e59d1024 	ldr	r1, [sp, #36]	; 0x24
807039e8:	e0860107 	add	r0, r6, r7, lsl #2
807039ec:	e0412003 	sub	r2, r1, r3
807039f0:	e1811803 	orr	r1, r1, r3, lsl #16
807039f4:	e2822001 	add	r2, r2, #1
807039f8:	e7861107 	str	r1, [r6, r7, lsl #2]
807039fc:	e08330a2 	add	r3, r3, r2, lsr #1
80703a00:	e1833802 	orr	r3, r3, r2, lsl #16
80703a04:	e5803080 	str	r3, [r0, #128]	; 0x80
80703a08:	eaffffa1 	b	80703894 <ddr_dataeye_deskew+0xc0>
80703a0c:	e6ff7077 	uxth	r7, r7
80703a10:	e1a00004 	mov	r0, r4
80703a14:	e0888007 	add	r8, r8, r7
80703a18:	e3a0a000 	mov	sl, #0
80703a1c:	e1a01007 	mov	r1, r7
80703a20:	ebfffdd5 	bl	8070317c <ddr_phy_set_dq_bdl>
80703a24:	e5963100 	ldr	r3, [r6, #256]	; 0x100
80703a28:	e083b00b 	add	fp, r3, fp
80703a2c:	e586b100 	str	fp, [r6, #256]	; 0x100
80703a30:	eaffffab 	b	807038e4 <ddr_dataeye_deskew+0x110>

80703a34 <ddr_vref_get_win>:
80703a34:	e92d4070 	push	{r4, r5, r6, lr}
80703a38:	e3a03000 	mov	r3, #0
80703a3c:	e5813100 	str	r3, [r1, #256]	; 0x100
80703a40:	e1a05001 	mov	r5, r1
80703a44:	e5903050 	ldr	r3, [r0, #80]	; 0x50
80703a48:	e1a04000 	mov	r4, r0
80703a4c:	e3530001 	cmp	r3, #1
80703a50:	13a01032 	movne	r1, #50	; 0x32
80703a54:	1a000003 	bne	80703a68 <ddr_vref_get_win+0x34>
80703a58:	e5903060 	ldr	r3, [r0, #96]	; 0x60
80703a5c:	e3530000 	cmp	r3, #0
80703a60:	03a0101f 	moveq	r1, #31
80703a64:	13a0100f 	movne	r1, #15
80703a68:	e1520001 	cmp	r2, r1
80703a6c:	e1a00004 	mov	r0, r4
80703a70:	31a01002 	movcc	r1, r2
80703a74:	ebfffc66 	bl	80702c14 <ddr_vref_set>
80703a78:	e1a01005 	mov	r1, r5
80703a7c:	e1a00004 	mov	r0, r4
80703a80:	ebffff53 	bl	807037d4 <ddr_dataeye_deskew>
80703a84:	e5950100 	ldr	r0, [r5, #256]	; 0x100
80703a88:	e8bd8070 	pop	{r4, r5, r6, pc}

80703a8c <ddr_vref_find_best>:
80703a8c:	e92d4df3 	push	{r0, r1, r4, r5, r6, r7, r8, sl, fp, lr}
80703a90:	e1a0b003 	mov	fp, r3
80703a94:	e5903050 	ldr	r3, [r0, #80]	; 0x50
80703a98:	e1a08000 	mov	r8, r0
80703a9c:	e1a0a001 	mov	sl, r1
80703aa0:	e3530001 	cmp	r3, #1
80703aa4:	13a07032 	movne	r7, #50	; 0x32
80703aa8:	1a000003 	bne	80703abc <ddr_vref_find_best+0x30>
80703aac:	e5903060 	ldr	r3, [r0, #96]	; 0x60
80703ab0:	e3530000 	cmp	r3, #0
80703ab4:	03a0701f 	moveq	r7, #31
80703ab8:	13a0700f 	movne	r7, #15
80703abc:	e1570002 	cmp	r7, r2
80703ac0:	e3a06000 	mov	r6, #0
80703ac4:	e08b5002 	add	r5, fp, r2
80703ac8:	e1a03006 	mov	r3, r6
80703acc:	31a04007 	movcc	r4, r7
80703ad0:	21a04002 	movcs	r4, r2
80703ad4:	e1570005 	cmp	r7, r5
80703ad8:	3a00000a 	bcc	80703b08 <ddr_vref_find_best+0x7c>
80703adc:	e1a02005 	mov	r2, r5
80703ae0:	e1a0100a 	mov	r1, sl
80703ae4:	e1a00008 	mov	r0, r8
80703ae8:	e58d3004 	str	r3, [sp, #4]
80703aec:	ebffffd0 	bl	80703a34 <ddr_vref_get_win>
80703af0:	e59d3004 	ldr	r3, [sp, #4]
80703af4:	e1530000 	cmp	r3, r0
80703af8:	9a000005 	bls	80703b14 <ddr_vref_find_best+0x88>
80703afc:	e2866001 	add	r6, r6, #1
80703b00:	e3560003 	cmp	r6, #3
80703b04:	1a000005 	bne	80703b20 <ddr_vref_find_best+0x94>
80703b08:	e1a00004 	mov	r0, r4
80703b0c:	e28dd008 	add	sp, sp, #8
80703b10:	e8bd8df0 	pop	{r4, r5, r6, r7, r8, sl, fp, pc}
80703b14:	e1a03000 	mov	r3, r0
80703b18:	e1a04005 	mov	r4, r5
80703b1c:	e3a06000 	mov	r6, #0
80703b20:	e085500b 	add	r5, r5, fp
80703b24:	eaffffea 	b	80703ad4 <ddr_vref_find_best+0x48>

80703b28 <ddr_vref_cal>:
80703b28:	e92d41f0 	push	{r4, r5, r6, r7, r8, lr}
80703b2c:	e1a06001 	mov	r6, r1
80703b30:	e5903050 	ldr	r3, [r0, #80]	; 0x50
80703b34:	e1a04000 	mov	r4, r0
80703b38:	e5902054 	ldr	r2, [r0, #84]	; 0x54
80703b3c:	e3530001 	cmp	r3, #1
80703b40:	e5903040 	ldr	r3, [r0, #64]	; 0x40
80703b44:	05901060 	ldreq	r1, [r0, #96]	; 0x60
80703b48:	02833f9d 	addeq	r3, r3, #628	; 0x274
80703b4c:	12833e27 	addne	r3, r3, #624	; 0x270
80703b50:	00833501 	addeq	r3, r3, r1, lsl #10
80703b54:	17935382 	ldrne	r5, [r3, r2, lsl #7]
80703b58:	e1a01006 	mov	r1, r6
80703b5c:	07935382 	ldreq	r5, [r3, r2, lsl #7]
80703b60:	1205503f 	andne	r5, r5, #63	; 0x3f
80703b64:	0205501f 	andeq	r5, r5, #31
80703b68:	e2452003 	sub	r2, r5, #3
80703b6c:	ebffffb0 	bl	80703a34 <ddr_vref_get_win>
80703b70:	e2852003 	add	r2, r5, #3
80703b74:	e1a01006 	mov	r1, r6
80703b78:	e1a07000 	mov	r7, r0
80703b7c:	e1a00004 	mov	r0, r4
80703b80:	ebffffab 	bl	80703a34 <ddr_vref_get_win>
80703b84:	e1570000 	cmp	r7, r0
80703b88:	2a000008 	bcs	80703bb0 <ddr_vref_cal+0x88>
80703b8c:	e3a03001 	mov	r3, #1
80703b90:	e1a01006 	mov	r1, r6
80703b94:	e1a02005 	mov	r2, r5
80703b98:	e1a00004 	mov	r0, r4
80703b9c:	ebffffba 	bl	80703a8c <ddr_vref_find_best>
80703ba0:	e1a01000 	mov	r1, r0
80703ba4:	e1a00004 	mov	r0, r4
80703ba8:	e8bd41f0 	pop	{r4, r5, r6, r7, r8, lr}
80703bac:	eafffc18 	b	80702c14 <ddr_vref_set>
80703bb0:	9a000001 	bls	80703bbc <ddr_vref_cal+0x94>
80703bb4:	e3e03000 	mvn	r3, #0
80703bb8:	eafffff4 	b	80703b90 <ddr_vref_cal+0x68>
80703bbc:	e5943050 	ldr	r3, [r4, #80]	; 0x50
80703bc0:	e3530001 	cmp	r3, #1
80703bc4:	13a03032 	movne	r3, #50	; 0x32
80703bc8:	1a000003 	bne	80703bdc <ddr_vref_cal+0xb4>
80703bcc:	e5943060 	ldr	r3, [r4, #96]	; 0x60
80703bd0:	e3530000 	cmp	r3, #0
80703bd4:	03a0301f 	moveq	r3, #31
80703bd8:	13a0300f 	movne	r3, #15
80703bdc:	e15500a3 	cmp	r5, r3, lsr #1
80703be0:	2afffff3 	bcs	80703bb4 <ddr_vref_cal+0x8c>
80703be4:	eaffffe8 	b	80703b8c <ddr_vref_cal+0x64>

80703be8 <ddr_adjust_move_win.constprop.13>:
80703be8:	e92d4df3 	push	{r0, r1, r4, r5, r6, r7, r8, sl, fp, lr}
80703bec:	e1a0b001 	mov	fp, r1
80703bf0:	e5901050 	ldr	r1, [r0, #80]	; 0x50
80703bf4:	e1a0a002 	mov	sl, r2
80703bf8:	e1a04000 	mov	r4, r0
80703bfc:	e5902054 	ldr	r2, [r0, #84]	; 0x54
80703c00:	e3510002 	cmp	r1, #2
80703c04:	e5903040 	ldr	r3, [r0, #64]	; 0x40
80703c08:	0a000024 	beq	80703ca0 <ddr_adjust_move_win.constprop.13+0xb8>
80703c0c:	e3510001 	cmp	r1, #1
80703c10:	130081ff 	movwne	r8, #511	; 0x1ff
80703c14:	1a000022 	bne	80703ca4 <ddr_adjust_move_win.constprop.13+0xbc>
80703c18:	e2833f8b 	add	r3, r3, #556	; 0x22c
80703c1c:	e30081ff 	movw	r8, #511	; 0x1ff
80703c20:	e7936382 	ldr	r6, [r3, r2, lsl #7]
80703c24:	e7e86056 	ubfx	r6, r6, #0, #9
80703c28:	e1a05006 	mov	r5, r6
80703c2c:	e3a07000 	mov	r7, #0
80703c30:	e1580007 	cmp	r8, r7
80703c34:	3a000017 	bcc	80703c98 <ddr_adjust_move_win.constprop.13+0xb0>
80703c38:	e5943050 	ldr	r3, [r4, #80]	; 0x50
80703c3c:	e3530002 	cmp	r3, #2
80703c40:	1a00001d 	bne	80703cbc <ddr_adjust_move_win.constprop.13+0xd4>
80703c44:	e1a00004 	mov	r0, r4
80703c48:	e58d3004 	str	r3, [sp, #4]
80703c4c:	ebfffb3f 	bl	80702950 <ddr_adjust_get_average>
80703c50:	e59d3004 	ldr	r3, [sp, #4]
80703c54:	e3500037 	cmp	r0, #55	; 0x37
80703c58:	9a000002 	bls	80703c68 <ddr_adjust_move_win.constprop.13+0x80>
80703c5c:	e3500048 	cmp	r0, #72	; 0x48
80703c60:	83a03004 	movhi	r3, #4
80703c64:	93a03001 	movls	r3, #1
80703c68:	e3a02001 	mov	r2, #1
80703c6c:	e153000a 	cmp	r3, sl
80703c70:	13530001 	cmpne	r3, #1
80703c74:	0a000007 	beq	80703c98 <ddr_adjust_move_win.constprop.13+0xb0>
80703c78:	e5943050 	ldr	r3, [r4, #80]	; 0x50
80703c7c:	e3530001 	cmp	r3, #1
80703c80:	1a00001c 	bne	80703cf8 <ddr_adjust_move_win.constprop.13+0x110>
80703c84:	e35a0004 	cmp	sl, #4
80703c88:	00855002 	addeq	r5, r5, r2
80703c8c:	10455002 	subne	r5, r5, r2
80703c90:	e3550c02 	cmp	r5, #512	; 0x200
80703c94:	3a00001c 	bcc	80703d0c <ddr_adjust_move_win.constprop.13+0x124>
80703c98:	e28dd008 	add	sp, sp, #8
80703c9c:	e8bd8df0 	pop	{r4, r5, r6, r7, r8, sl, fp, pc}
80703ca0:	e3a0801f 	mov	r8, #31
80703ca4:	e5941060 	ldr	r1, [r4, #96]	; 0x60
80703ca8:	e2833f8d 	add	r3, r3, #564	; 0x234
80703cac:	e0833501 	add	r3, r3, r1, lsl #10
80703cb0:	e7936382 	ldr	r6, [r3, r2, lsl #7]
80703cb4:	e7e46456 	ubfx	r6, r6, #8, #5
80703cb8:	eaffffda 	b	80703c28 <ddr_adjust_move_win.constprop.13+0x40>
80703cbc:	e1a00004 	mov	r0, r4
80703cc0:	ebfffb22 	bl	80702950 <ddr_adjust_get_average>
80703cc4:	e3500040 	cmp	r0, #64	; 0x40
80703cc8:	9a000004 	bls	80703ce0 <ddr_adjust_move_win.constprop.13+0xf8>
80703ccc:	e3500048 	cmp	r0, #72	; 0x48
80703cd0:	e2402040 	sub	r2, r0, #64	; 0x40
80703cd4:	83a03004 	movhi	r3, #4
80703cd8:	93a03001 	movls	r3, #1
80703cdc:	eaffffe2 	b	80703c6c <ddr_adjust_move_win.constprop.13+0x84>
80703ce0:	12602040 	rsbne	r2, r0, #64	; 0x40
80703ce4:	03a02001 	moveq	r2, #1
80703ce8:	e3500037 	cmp	r0, #55	; 0x37
80703cec:	83a03001 	movhi	r3, #1
80703cf0:	93a03002 	movls	r3, #2
80703cf4:	eaffffdc 	b	80703c6c <ddr_adjust_move_win.constprop.13+0x84>
80703cf8:	e35a0004 	cmp	sl, #4
80703cfc:	00455002 	subeq	r5, r5, r2
80703d00:	10855002 	addne	r5, r5, r2
80703d04:	e355001f 	cmp	r5, #31
80703d08:	8affffe2 	bhi	80703c98 <ddr_adjust_move_win.constprop.13+0xb0>
80703d0c:	e1a01005 	mov	r1, r5
80703d10:	e1a00004 	mov	r0, r4
80703d14:	ebfffd6a 	bl	807032c4 <ddr_adjust_set_val>
80703d18:	e1a0100b 	mov	r1, fp
80703d1c:	e1a00004 	mov	r0, r4
80703d20:	ebfffeab 	bl	807037d4 <ddr_dataeye_deskew>
80703d24:	e3500000 	cmp	r0, #0
80703d28:	0a000007 	beq	80703d4c <ddr_adjust_move_win.constprop.13+0x164>
80703d2c:	e1a01006 	mov	r1, r6
80703d30:	e1a00004 	mov	r0, r4
80703d34:	ebfffd62 	bl	807032c4 <ddr_adjust_set_val>
80703d38:	e1a0100b 	mov	r1, fp
80703d3c:	e1a00004 	mov	r0, r4
80703d40:	e28dd008 	add	sp, sp, #8
80703d44:	e8bd4df0 	pop	{r4, r5, r6, r7, r8, sl, fp, lr}
80703d48:	eafffea1 	b	807037d4 <ddr_dataeye_deskew>
80703d4c:	e2877001 	add	r7, r7, #1
80703d50:	eaffffb6 	b	80703c30 <ddr_adjust_move_win.constprop.13+0x48>

80703d54 <ddr_adjust_dataeye>:
80703d54:	e92d41f0 	push	{r4, r5, r6, r7, r8, lr}
80703d58:	e5905048 	ldr	r5, [r0, #72]	; 0x48
80703d5c:	e2155201 	ands	r5, r5, #268435456	; 0x10000000
80703d60:	0a000014 	beq	80703db8 <ddr_adjust_dataeye+0x64>
80703d64:	e8bd81f0 	pop	{r4, r5, r6, r7, r8, pc}
80703d68:	e0852082 	add	r2, r5, r2, lsl #1
80703d6c:	e1a00004 	mov	r0, r4
80703d70:	e5842054 	str	r2, [r4, #84]	; 0x54
80703d74:	ebfffaf5 	bl	80702950 <ddr_adjust_get_average>
80703d78:	e3500037 	cmp	r0, #55	; 0x37
80703d7c:	93a02004 	movls	r2, #4
80703d80:	9a000014 	bls	80703dd8 <ddr_adjust_dataeye+0x84>
80703d84:	e3500048 	cmp	r0, #72	; 0x48
80703d88:	83a02002 	movhi	r2, #2
80703d8c:	8a000011 	bhi	80703dd8 <ddr_adjust_dataeye+0x84>
80703d90:	e2855001 	add	r5, r5, #1
80703d94:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
80703d98:	e5942064 	ldr	r2, [r4, #100]	; 0x64
80703d9c:	e0030398 	mul	r3, r8, r3
80703da0:	e0233297 	mla	r3, r7, r2, r3
80703da4:	e0843003 	add	r3, r4, r3
80703da8:	e5933018 	ldr	r3, [r3, #24]
80703dac:	e1550003 	cmp	r5, r3
80703db0:	3affffec 	bcc	80703d68 <ddr_adjust_dataeye+0x14>
80703db4:	e8bd81f0 	pop	{r4, r5, r6, r7, r8, pc}
80703db8:	e5903068 	ldr	r3, [r0, #104]	; 0x68
80703dbc:	e3530000 	cmp	r3, #0
80703dc0:	08bd81f0 	popeq	{r4, r5, r6, r7, r8, pc}
80703dc4:	e1a06001 	mov	r6, r1
80703dc8:	e1a04000 	mov	r4, r0
80703dcc:	e3a0700c 	mov	r7, #12
80703dd0:	e3a0803c 	mov	r8, #60	; 0x3c
80703dd4:	eaffffee 	b	80703d94 <ddr_adjust_dataeye+0x40>
80703dd8:	e1a01006 	mov	r1, r6
80703ddc:	e1a00004 	mov	r0, r4
80703de0:	ebffff80 	bl	80703be8 <ddr_adjust_move_win.constprop.13>
80703de4:	eaffffe9 	b	80703d90 <ddr_adjust_dataeye+0x3c>

80703de8 <ddr_dataeye_process>:
80703de8:	e92d4df0 	push	{r4, r5, r6, r7, r8, sl, fp, lr}
80703dec:	e3a06000 	mov	r6, #0
80703df0:	e1a04000 	mov	r4, r0
80703df4:	e1a07001 	mov	r7, r1
80703df8:	e1a05006 	mov	r5, r6
80703dfc:	e3a0800c 	mov	r8, #12
80703e00:	e3a0a03c 	mov	sl, #60	; 0x3c
80703e04:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
80703e08:	e5942064 	ldr	r2, [r4, #100]	; 0x64
80703e0c:	e003039a 	mul	r3, sl, r3
80703e10:	e0233298 	mla	r3, r8, r2, r3
80703e14:	e0843003 	add	r3, r4, r3
80703e18:	e5933018 	ldr	r3, [r3, #24]
80703e1c:	e1530006 	cmp	r3, r6
80703e20:	8a00000a 	bhi	80703e50 <ddr_dataeye_process+0x68>
80703e24:	e3550000 	cmp	r5, #0
80703e28:	13e05000 	mvnne	r5, #0
80703e2c:	1a000002 	bne	80703e3c <ddr_dataeye_process+0x54>
80703e30:	e1a01007 	mov	r1, r7
80703e34:	e1a00004 	mov	r0, r4
80703e38:	ebffffc5 	bl	80703d54 <ddr_adjust_dataeye>
80703e3c:	e1a00004 	mov	r0, r4
80703e40:	e1a01007 	mov	r1, r7
80703e44:	eb000305 	bl	80704a60 <ddr_result_data_save>
80703e48:	e1a00005 	mov	r0, r5
80703e4c:	e8bd8df0 	pop	{r4, r5, r6, r7, r8, sl, fp, pc}
80703e50:	e0862082 	add	r2, r6, r2, lsl #1
80703e54:	e1a01007 	mov	r1, r7
80703e58:	e5842054 	str	r2, [r4, #84]	; 0x54
80703e5c:	e1a00004 	mov	r0, r4
80703e60:	ebfffe5b 	bl	807037d4 <ddr_dataeye_deskew>
80703e64:	e2866001 	add	r6, r6, #1
80703e68:	e0855000 	add	r5, r5, r0
80703e6c:	eaffffe4 	b	80703e04 <ddr_dataeye_process+0x1c>

80703e70 <ddr_dataeye_training>:
80703e70:	e92d4030 	push	{r4, r5, lr}
80703e74:	e24ddf43 	sub	sp, sp, #268	; 0x10c
80703e78:	e1a04000 	mov	r4, r0
80703e7c:	e3a03002 	mov	r3, #2
80703e80:	e3a02f41 	mov	r2, #260	; 0x104
80703e84:	e5803050 	str	r3, [r0, #80]	; 0x50
80703e88:	e3a01000 	mov	r1, #0
80703e8c:	e28d0004 	add	r0, sp, #4
80703e90:	ebfffadb 	bl	80702a04 <ddrtr_memset>
80703e94:	e28d1004 	add	r1, sp, #4
80703e98:	e1a00004 	mov	r0, r4
80703e9c:	ebffffd1 	bl	80703de8 <ddr_dataeye_process>
80703ea0:	e3a03001 	mov	r3, #1
80703ea4:	e3a02f41 	mov	r2, #260	; 0x104
80703ea8:	e5843050 	str	r3, [r4, #80]	; 0x50
80703eac:	e3a01000 	mov	r1, #0
80703eb0:	e1a05000 	mov	r5, r0
80703eb4:	e28d0004 	add	r0, sp, #4
80703eb8:	ebfffad1 	bl	80702a04 <ddrtr_memset>
80703ebc:	e28d1004 	add	r1, sp, #4
80703ec0:	e1a00004 	mov	r0, r4
80703ec4:	ebffffc7 	bl	80703de8 <ddr_dataeye_process>
80703ec8:	e1953000 	orrs	r3, r5, r0
80703ecc:	13e00000 	mvnne	r0, #0
80703ed0:	03a00000 	moveq	r0, #0
80703ed4:	e28ddf43 	add	sp, sp, #268	; 0x10c
80703ed8:	e8bd8030 	pop	{r4, r5, pc}

80703edc <ddr_dataeye_training_func>:
80703edc:	e92d4030 	push	{r4, r5, lr}
80703ee0:	e24dd034 	sub	sp, sp, #52	; 0x34
80703ee4:	e5903048 	ldr	r3, [r0, #72]	; 0x48
80703ee8:	e3130801 	tst	r3, #65536	; 0x10000
80703eec:	13a05000 	movne	r5, #0
80703ef0:	1a000011 	bne	80703f3c <ddr_dataeye_training_func+0x60>
80703ef4:	e1a04000 	mov	r4, r0
80703ef8:	e3a02801 	mov	r2, #65536	; 0x10000
80703efc:	e1a0100d 	mov	r1, sp
80703f00:	ebfffc40 	bl	80703008 <ddr_training_save_reg>
80703f04:	e1a00004 	mov	r0, r4
80703f08:	ebfffc18 	bl	80702f70 <ddr_training_switch_axi>
80703f0c:	e3a01002 	mov	r1, #2
80703f10:	e1a00004 	mov	r0, r4
80703f14:	ebfffd4c 	bl	8070344c <ddr_ddrt_init>
80703f18:	e3a03001 	mov	r3, #1
80703f1c:	e1a00004 	mov	r0, r4
80703f20:	e5843068 	str	r3, [r4, #104]	; 0x68
80703f24:	e584306c 	str	r3, [r4, #108]	; 0x6c
80703f28:	ebffffd0 	bl	80703e70 <ddr_dataeye_training>
80703f2c:	e1a0100d 	mov	r1, sp
80703f30:	e1a05000 	mov	r5, r0
80703f34:	e1a00004 	mov	r0, r4
80703f38:	ebfffc6f 	bl	807030fc <ddr_training_restore_reg>
80703f3c:	e1a00005 	mov	r0, r5
80703f40:	e28dd034 	add	sp, sp, #52	; 0x34
80703f44:	e8bd8030 	pop	{r4, r5, pc}

80703f48 <ddr_hw_dataeye_read>:
80703f48:	e92d4070 	push	{r4, r5, r6, lr}
80703f4c:	e3a0203c 	mov	r2, #60	; 0x3c
80703f50:	e590305c 	ldr	r3, [r0, #92]	; 0x5c
80703f54:	e1a04000 	mov	r4, r0
80703f58:	e5905040 	ldr	r5, [r0, #64]	; 0x40
80703f5c:	e0230392 	mla	r3, r2, r3, r0
80703f60:	e5936010 	ldr	r6, [r3, #16]
80703f64:	ebfffd2c 	bl	8070341c <ddr_training_cfg_init>
80703f68:	e3a03000 	mov	r3, #0
80703f6c:	e2850f8b 	add	r0, r5, #556	; 0x22c
80703f70:	e285cf87 	add	ip, r5, #540	; 0x21c
80703f74:	e285ee22 	add	lr, r5, #544	; 0x220
80703f78:	e1a02003 	mov	r2, r3
80703f7c:	e1530006 	cmp	r3, r6
80703f80:	1a000005 	bne	80703f9c <ddr_hw_dataeye_read+0x54>
80703f84:	e1a00005 	mov	r0, r5
80703f88:	ebfffc12 	bl	80702fd8 <ddr_phy_cfg_update>
80703f8c:	e1a00004 	mov	r0, r4
80703f90:	e3a01c01 	mov	r1, #256	; 0x100
80703f94:	e8bd4070 	pop	{r4, r5, r6, lr}
80703f98:	eafffba5 	b	80702e34 <ddr_hw_training_process>
80703f9c:	e5941060 	ldr	r1, [r4, #96]	; 0x60
80703fa0:	e08c1501 	add	r1, ip, r1, lsl #10
80703fa4:	e7812383 	str	r2, [r1, r3, lsl #7]
80703fa8:	e5941060 	ldr	r1, [r4, #96]	; 0x60
80703fac:	e08e1501 	add	r1, lr, r1, lsl #10
80703fb0:	e7812383 	str	r2, [r1, r3, lsl #7]
80703fb4:	e2833001 	add	r3, r3, #1
80703fb8:	e4802080 	str	r2, [r0], #128	; 0x80
80703fbc:	eaffffee 	b	80703f7c <ddr_hw_dataeye_read+0x34>

80703fc0 <ddr_hw_training_ctl>:
80703fc0:	e92d4df0 	push	{r4, r5, r6, r7, r8, sl, fp, lr}
80703fc4:	e28db01c 	add	fp, sp, #28
80703fc8:	e24dd040 	sub	sp, sp, #64	; 0x40
80703fcc:	e5905040 	ldr	r5, [r0, #64]	; 0x40
80703fd0:	e5907074 	ldr	r7, [r0, #116]	; 0x74
80703fd4:	e5906048 	ldr	r6, [r0, #72]	; 0x48
80703fd8:	e59530c4 	ldr	r3, [r5, #196]	; 0xc4
80703fdc:	e3570000 	cmp	r7, #0
80703fe0:	13560000 	cmpne	r6, #0
80703fe4:	e50b3020 	str	r3, [fp, #-32]	; 0xffffffe0
80703fe8:	03a07000 	moveq	r7, #0
80703fec:	0a00005e 	beq	8070416c <ddr_hw_training_ctl+0x1ac>
80703ff0:	e590305c 	ldr	r3, [r0, #92]	; 0x5c
80703ff4:	e3a0803c 	mov	r8, #60	; 0x3c
80703ff8:	e1a04000 	mov	r4, r0
80703ffc:	e0230398 	mla	r3, r8, r3, r0
80704000:	e1a00005 	mov	r0, r5
80704004:	e5933010 	ldr	r3, [r3, #16]
80704008:	e50b3040 	str	r3, [fp, #-64]	; 0xffffffc0
8070400c:	ebfffbf1 	bl	80702fd8 <ddr_phy_cfg_update>
80704010:	e2061902 	and	r1, r6, #32768	; 0x8000
80704014:	e1a00004 	mov	r0, r4
80704018:	ebfffb85 	bl	80702e34 <ddr_hw_training_process>
8070401c:	e206100e 	and	r1, r6, #14
80704020:	e1a0a000 	mov	sl, r0
80704024:	e1a00004 	mov	r0, r4
80704028:	ebfffb81 	bl	80702e34 <ddr_hw_training_process>
8070402c:	e5943060 	ldr	r3, [r4, #96]	; 0x60
80704030:	e3530000 	cmp	r3, #0
80704034:	e08aa000 	add	sl, sl, r0
80704038:	0a00004e 	beq	80704178 <ddr_hw_training_ctl+0x1b8>
8070403c:	e3a0e000 	mov	lr, #0
80704040:	e51b3040 	ldr	r3, [fp, #-64]	; 0xffffffc0
80704044:	e15e0003 	cmp	lr, r3
80704048:	1a000055 	bne	807041a4 <ddr_hw_training_ctl+0x1e4>
8070404c:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
80704050:	e3a0203c 	mov	r2, #60	; 0x3c
80704054:	e2061b05 	and	r1, r6, #5120	; 0x1400
80704058:	e0234392 	mla	r3, r2, r3, r4
8070405c:	e5933004 	ldr	r3, [r3, #4]
80704060:	e3530006 	cmp	r3, #6
80704064:	1a0000af 	bne	80704328 <ddr_hw_training_ctl+0x368>
80704068:	e5958064 	ldr	r8, [r5, #100]	; 0x64
8070406c:	e1a00004 	mov	r0, r4
80704070:	e3c8320f 	bic	r3, r8, #-268435456	; 0xf0000000
80704074:	e5853064 	str	r3, [r5, #100]	; 0x64
80704078:	ebfffb6d 	bl	80702e34 <ddr_hw_training_process>
8070407c:	e5858064 	str	r8, [r5, #100]	; 0x64
80704080:	e5958048 	ldr	r8, [r5, #72]	; 0x48
80704084:	e3c83001 	bic	r3, r8, #1
80704088:	e5853048 	str	r3, [r5, #72]	; 0x48
8070408c:	e08a7000 	add	r7, sl, r0
80704090:	e206a602 	and	sl, r6, #2097152	; 0x200000
80704094:	e1a00004 	mov	r0, r4
80704098:	e1a0100a 	mov	r1, sl
8070409c:	ebfffb64 	bl	80702e34 <ddr_hw_training_process>
807040a0:	e3883001 	orr	r3, r8, #1
807040a4:	e1a0100a 	mov	r1, sl
807040a8:	e5853048 	str	r3, [r5, #72]	; 0x48
807040ac:	e0877000 	add	r7, r7, r0
807040b0:	e1a00004 	mov	r0, r4
807040b4:	ebfffb5e 	bl	80702e34 <ddr_hw_training_process>
807040b8:	e5858048 	str	r8, [r5, #72]	; 0x48
807040bc:	e2061b01 	and	r1, r6, #1024	; 0x400
807040c0:	e0877000 	add	r7, r7, r0
807040c4:	e1a00004 	mov	r0, r4
807040c8:	ebfffb59 	bl	80702e34 <ddr_hw_training_process>
807040cc:	e0877000 	add	r7, r7, r0
807040d0:	e2061b02 	and	r1, r6, #2048	; 0x800
807040d4:	e1a00004 	mov	r0, r4
807040d8:	ebfffb55 	bl	80702e34 <ddr_hw_training_process>
807040dc:	e2061501 	and	r1, r6, #4194304	; 0x400000
807040e0:	e2068701 	and	r8, r6, #262144	; 0x40000
807040e4:	e0877000 	add	r7, r7, r0
807040e8:	e1a00004 	mov	r0, r4
807040ec:	ebfffb50 	bl	80702e34 <ddr_hw_training_process>
807040f0:	e51b3020 	ldr	r3, [fp, #-32]	; 0xffffffe0
807040f4:	e1a01008 	mov	r1, r8
807040f8:	e3c33102 	bic	r3, r3, #-2147483648	; 0x80000000
807040fc:	e58530c4 	str	r3, [r5, #196]	; 0xc4
80704100:	e59530c0 	ldr	r3, [r5, #192]	; 0xc0
80704104:	e3833102 	orr	r3, r3, #-2147483648	; 0x80000000
80704108:	e58530c0 	str	r3, [r5, #192]	; 0xc0
8070410c:	e0877000 	add	r7, r7, r0
80704110:	e1a00004 	mov	r0, r4
80704114:	ebfffb46 	bl	80702e34 <ddr_hw_training_process>
80704118:	e1a01008 	mov	r1, r8
8070411c:	e0877000 	add	r7, r7, r0
80704120:	e1a00004 	mov	r0, r4
80704124:	ebfffb42 	bl	80702e34 <ddr_hw_training_process>
80704128:	e59530c0 	ldr	r3, [r5, #192]	; 0xc0
8070412c:	e1a01008 	mov	r1, r8
80704130:	e3c33102 	bic	r3, r3, #-2147483648	; 0x80000000
80704134:	e58530c0 	str	r3, [r5, #192]	; 0xc0
80704138:	e0877000 	add	r7, r7, r0
8070413c:	e1a00004 	mov	r0, r4
80704140:	ebfffb3b 	bl	80702e34 <ddr_hw_training_process>
80704144:	e51b3020 	ldr	r3, [fp, #-32]	; 0xffffffe0
80704148:	e59f11e8 	ldr	r1, [pc, #488]	; 80704338 <ddr_hw_training_ctl+0x378>
8070414c:	e58530c4 	str	r3, [r5, #196]	; 0xc4
80704150:	e0011006 	and	r1, r1, r6
80704154:	e0877000 	add	r7, r7, r0
80704158:	e1a00004 	mov	r0, r4
8070415c:	ebfffb34 	bl	80702e34 <ddr_hw_training_process>
80704160:	e0877000 	add	r7, r7, r0
80704164:	e1a00005 	mov	r0, r5
80704168:	ebfffb9a 	bl	80702fd8 <ddr_phy_cfg_update>
8070416c:	e1a00007 	mov	r0, r7
80704170:	e24bd01c 	sub	sp, fp, #28
80704174:	e8bd8df0 	pop	{r4, r5, r6, r7, r8, sl, fp, pc}
80704178:	e594205c 	ldr	r2, [r4, #92]	; 0x5c
8070417c:	e0284298 	mla	r8, r8, r2, r4
80704180:	e5942040 	ldr	r2, [r4, #64]	; 0x40
80704184:	e2822f8b 	add	r2, r2, #556	; 0x22c
80704188:	e5981010 	ldr	r1, [r8, #16]
8070418c:	e1510003 	cmp	r1, r3
80704190:	0affffa9 	beq	8070403c <ddr_hw_training_ctl+0x7c>
80704194:	e4920080 	ldr	r0, [r2], #128	; 0x80
80704198:	e7870103 	str	r0, [r7, r3, lsl #2]
8070419c:	e2833001 	add	r3, r3, #1
807041a0:	eafffff9 	b	8070418c <ddr_hw_training_ctl+0x1cc>
807041a4:	e5941060 	ldr	r1, [r4, #96]	; 0x60
807041a8:	e1a0238e 	lsl	r2, lr, #7
807041ac:	e594c040 	ldr	ip, [r4, #64]	; 0x40
807041b0:	e584e054 	str	lr, [r4, #84]	; 0x54
807041b4:	e08c1501 	add	r1, ip, r1, lsl #10
807041b8:	e28ccf8b 	add	ip, ip, #556	; 0x22c
807041bc:	e0811002 	add	r1, r1, r2
807041c0:	e50b2054 	str	r2, [fp, #-84]	; 0xffffffac
807041c4:	e50bc03c 	str	ip, [fp, #-60]	; 0xffffffc4
807041c8:	e591021c 	ldr	r0, [r1, #540]	; 0x21c
807041cc:	e5912220 	ldr	r2, [r1, #544]	; 0x220
807041d0:	e5917224 	ldr	r7, [r1, #548]	; 0x224
807041d4:	e79cc38e 	ldr	ip, [ip, lr, lsl #7]
807041d8:	e50bd044 	str	sp, [fp, #-68]	; 0xffffffbc
807041dc:	e24dd030 	sub	sp, sp, #48	; 0x30
807041e0:	e50b7024 	str	r7, [fp, #-36]	; 0xffffffdc
807041e4:	e7e67450 	ubfx	r7, r0, #8, #7
807041e8:	e50bc028 	str	ip, [fp, #-40]	; 0xffffffd8
807041ec:	e200c07f 	and	ip, r0, #127	; 0x7f
807041f0:	e50bc048 	str	ip, [fp, #-72]	; 0xffffffb8
807041f4:	e24d8004 	sub	r8, sp, #4
807041f8:	e58dc000 	str	ip, [sp]
807041fc:	e7e6c850 	ubfx	ip, r0, #16, #7
80704200:	e7e60c50 	ubfx	r0, r0, #24, #7
80704204:	e50b002c 	str	r0, [fp, #-44]	; 0xffffffd4
80704208:	e58d000c 	str	r0, [sp, #12]
8070420c:	e202007f 	and	r0, r2, #127	; 0x7f
80704210:	e50b004c 	str	r0, [fp, #-76]	; 0xffffffb4
80704214:	e28d3024 	add	r3, sp, #36	; 0x24
80704218:	e58d0010 	str	r0, [sp, #16]
8070421c:	e7e60452 	ubfx	r0, r2, #8, #7
80704220:	e50b0050 	str	r0, [fp, #-80]	; 0xffffffb0
80704224:	e58d0014 	str	r0, [sp, #20]
80704228:	e7e60852 	ubfx	r0, r2, #16, #7
8070422c:	e7e62c52 	ubfx	r2, r2, #24, #7
80704230:	e50b2030 	str	r2, [fp, #-48]	; 0xffffffd0
80704234:	e58d201c 	str	r2, [sp, #28]
80704238:	e51b2024 	ldr	r2, [fp, #-36]	; 0xffffffdc
8070423c:	e58d7004 	str	r7, [sp, #4]
80704240:	e202207f 	and	r2, r2, #127	; 0x7f
80704244:	e50b2034 	str	r2, [fp, #-52]	; 0xffffffcc
80704248:	e58d2020 	str	r2, [sp, #32]
8070424c:	e51b2028 	ldr	r2, [fp, #-40]	; 0xffffffd8
80704250:	e58dc008 	str	ip, [sp, #8]
80704254:	e7e82052 	ubfx	r2, r2, #0, #9
80704258:	e50b2038 	str	r2, [fp, #-56]	; 0xffffffc8
8070425c:	e58d2024 	str	r2, [sp, #36]	; 0x24
80704260:	e3e02000 	mvn	r2, #0
80704264:	e58d0018 	str	r0, [sp, #24]
80704268:	e50b3058 	str	r3, [fp, #-88]	; 0xffffffa8
8070426c:	e5b83004 	ldr	r3, [r8, #4]!
80704270:	e1520003 	cmp	r2, r3
80704274:	21a02003 	movcs	r2, r3
80704278:	e51b3058 	ldr	r3, [fp, #-88]	; 0xffffffa8
8070427c:	e1530008 	cmp	r3, r8
80704280:	1afffff9 	bne	8070426c <ddr_hw_training_ctl+0x2ac>
80704284:	e04cc002 	sub	ip, ip, r2
80704288:	e51b3048 	ldr	r3, [fp, #-72]	; 0xffffffb8
8070428c:	e0477002 	sub	r7, r7, r2
80704290:	e0400002 	sub	r0, r0, r2
80704294:	e1a0c80c 	lsl	ip, ip, #16
80704298:	e0433002 	sub	r3, r3, r2
8070429c:	e18c7407 	orr	r7, ip, r7, lsl #8
807042a0:	e1a00800 	lsl	r0, r0, #16
807042a4:	e1877003 	orr	r7, r7, r3
807042a8:	e51b302c 	ldr	r3, [fp, #-44]	; 0xffffffd4
807042ac:	e51bc034 	ldr	ip, [fp, #-52]	; 0xffffffcc
807042b0:	e28ee001 	add	lr, lr, #1
807042b4:	e0433002 	sub	r3, r3, r2
807042b8:	e51b8038 	ldr	r8, [fp, #-56]	; 0xffffffc8
807042bc:	e04cc002 	sub	ip, ip, r2
807042c0:	e1877c03 	orr	r7, r7, r3, lsl #24
807042c4:	e51b3050 	ldr	r3, [fp, #-80]	; 0xffffffb0
807042c8:	e581721c 	str	r7, [r1, #540]	; 0x21c
807042cc:	e0433002 	sub	r3, r3, r2
807042d0:	e1800403 	orr	r0, r0, r3, lsl #8
807042d4:	e51b304c 	ldr	r3, [fp, #-76]	; 0xffffffb4
807042d8:	e0433002 	sub	r3, r3, r2
807042dc:	e1800003 	orr	r0, r0, r3
807042e0:	e51b3030 	ldr	r3, [fp, #-48]	; 0xffffffd0
807042e4:	e0433002 	sub	r3, r3, r2
807042e8:	e0482002 	sub	r2, r8, r2
807042ec:	e1800c03 	orr	r0, r0, r3, lsl #24
807042f0:	e51b3024 	ldr	r3, [fp, #-36]	; 0xffffffdc
807042f4:	e5810220 	str	r0, [r1, #544]	; 0x220
807042f8:	e3c3307f 	bic	r3, r3, #127	; 0x7f
807042fc:	e183300c 	orr	r3, r3, ip
80704300:	e51bc028 	ldr	ip, [fp, #-40]	; 0xffffffd8
80704304:	e5813224 	str	r3, [r1, #548]	; 0x224
80704308:	e51b303c 	ldr	r3, [fp, #-60]	; 0xffffffc4
8070430c:	e3cccf7f 	bic	ip, ip, #508	; 0x1fc
80704310:	e51b1054 	ldr	r1, [fp, #-84]	; 0xffffffac
80704314:	e3ccc003 	bic	ip, ip, #3
80704318:	e18c2002 	orr	r2, ip, r2
8070431c:	e7832001 	str	r2, [r3, r1]
80704320:	e51bd044 	ldr	sp, [fp, #-68]	; 0xffffffbc
80704324:	eaffff45 	b	80704040 <ddr_hw_training_ctl+0x80>
80704328:	e1a00004 	mov	r0, r4
8070432c:	ebfffac0 	bl	80702e34 <ddr_hw_training_process>
80704330:	e08a7000 	add	r7, sl, r0
80704334:	eaffff65 	b	807040d0 <ddr_hw_training_ctl+0x110>
80704338:	100303f0 	.word	0x100303f0

8070433c <ddr_hw_training>:
8070433c:	e92d4df0 	push	{r4, r5, r6, r7, r8, sl, fp, lr}
80704340:	e1a04000 	mov	r4, r0
80704344:	e24dd078 	sub	sp, sp, #120	; 0x78
80704348:	e3a01001 	mov	r1, #1
8070434c:	e28d0018 	add	r0, sp, #24
80704350:	e1a06004 	mov	r6, r4
80704354:	eb0001e7 	bl	80704af8 <ddr_training_save_reg_custom>
80704358:	e3a07000 	mov	r7, #0
8070435c:	e58d7004 	str	r7, [sp, #4]
80704360:	e594303c 	ldr	r3, [r4, #60]	; 0x3c
80704364:	e1530007 	cmp	r3, r7
80704368:	8a000004 	bhi	80704380 <ddr_hw_training+0x44>
8070436c:	e28d0018 	add	r0, sp, #24
80704370:	eb0001eb 	bl	80704b24 <ddr_training_restore_reg_custom>
80704374:	e59d0004 	ldr	r0, [sp, #4]
80704378:	e28dd078 	add	sp, sp, #120	; 0x78
8070437c:	e8bd8df0 	pop	{r4, r5, r6, r7, r8, sl, fp, pc}
80704380:	e584705c 	str	r7, [r4, #92]	; 0x5c
80704384:	e3a02000 	mov	r2, #0
80704388:	e5963000 	ldr	r3, [r6]
8070438c:	e3a0500c 	mov	r5, #12
80704390:	e28da048 	add	sl, sp, #72	; 0x48
80704394:	e5843040 	str	r3, [r4, #64]	; 0x40
80704398:	e596800c 	ldr	r8, [r6, #12]
8070439c:	e584a074 	str	sl, [r4, #116]	; 0x74
807043a0:	e5963008 	ldr	r3, [r6, #8]
807043a4:	e1530002 	cmp	r3, r2
807043a8:	8a000026 	bhi	80704448 <ddr_hw_training+0x10c>
807043ac:	e3a0b03c 	mov	fp, #60	; 0x3c
807043b0:	e3a05000 	mov	r5, #0
807043b4:	e58d5000 	str	r5, [sp]
807043b8:	e1550008 	cmp	r5, r8
807043bc:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
807043c0:	1a00002a 	bne	80704470 <ddr_hw_training+0x134>
807043c4:	e3550002 	cmp	r5, #2
807043c8:	1a000010 	bne	80704410 <ddr_hw_training+0xd4>
807043cc:	e3a0203c 	mov	r2, #60	; 0x3c
807043d0:	e3a05000 	mov	r5, #0
807043d4:	e0234392 	mla	r3, r2, r3, r4
807043d8:	e28d8054 	add	r8, sp, #84	; 0x54
807043dc:	e1a0b005 	mov	fp, r5
807043e0:	e3a0a001 	mov	sl, #1
807043e4:	e5933010 	ldr	r3, [r3, #16]
807043e8:	e58d3008 	str	r3, [sp, #8]
807043ec:	e5943060 	ldr	r3, [r4, #96]	; 0x60
807043f0:	e58d300c 	str	r3, [sp, #12]
807043f4:	e59d3008 	ldr	r3, [sp, #8]
807043f8:	e1530005 	cmp	r3, r5
807043fc:	1a000048 	bne	80704524 <ddr_hw_training+0x1e8>
80704400:	e59d300c 	ldr	r3, [sp, #12]
80704404:	e5940040 	ldr	r0, [r4, #64]	; 0x40
80704408:	e5843060 	str	r3, [r4, #96]	; 0x60
8070440c:	ebfffaf1 	bl	80702fd8 <ddr_phy_cfg_update>
80704410:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
80704414:	e3a0503c 	mov	r5, #60	; 0x3c
80704418:	e3a02000 	mov	r2, #0
8070441c:	e0050395 	mul	r5, r5, r3
80704420:	e0848005 	add	r8, r4, r5
80704424:	e5983008 	ldr	r3, [r8, #8]
80704428:	e1530002 	cmp	r3, r2
8070442c:	8a00004f 	bhi	80704570 <ddr_hw_training+0x234>
80704430:	e89d000c 	ldm	sp, {r2, r3}
80704434:	e2877001 	add	r7, r7, #1
80704438:	e286603c 	add	r6, r6, #60	; 0x3c
8070443c:	e0833002 	add	r3, r3, r2
80704440:	e58d3004 	str	r3, [sp, #4]
80704444:	eaffffc5 	b	80704360 <ddr_hw_training+0x24>
80704448:	e0236295 	mla	r3, r5, r2, r6
8070444c:	e5930014 	ldr	r0, [r3, #20]
80704450:	e28d3010 	add	r3, sp, #16
80704454:	e5901108 	ldr	r1, [r0, #264]	; 0x108
80704458:	e7831102 	str	r1, [r3, r2, lsl #2]
8070445c:	e3c11eff 	bic	r1, r1, #4080	; 0xff0
80704460:	e3c1100f 	bic	r1, r1, #15
80704464:	ebfff9de 	bl	80702be4 <ddr_training_set_timing>
80704468:	e2822001 	add	r2, r2, #1
8070446c:	eaffffcb 	b	807043a0 <ddr_hw_training+0x64>
80704470:	e002039b 	mul	r2, fp, r3
80704474:	e2851005 	add	r1, r5, #5
80704478:	e5845060 	str	r5, [r4, #96]	; 0x60
8070447c:	e1a00004 	mov	r0, r4
80704480:	e0823181 	add	r3, r2, r1, lsl #3
80704484:	e5942040 	ldr	r2, [r4, #64]	; 0x40
80704488:	e0843003 	add	r3, r4, r3
8070448c:	e5933008 	ldr	r3, [r3, #8]
80704490:	e5843048 	str	r3, [r4, #72]	; 0x48
80704494:	e5923048 	ldr	r3, [r2, #72]	; 0x48
80704498:	e3c3300f 	bic	r3, r3, #15
8070449c:	e1833005 	orr	r3, r3, r5
807044a0:	e5823048 	str	r3, [r2, #72]	; 0x48
807044a4:	ebfffec5 	bl	80703fc0 <ddr_hw_training_ctl>
807044a8:	e59d3000 	ldr	r3, [sp]
807044ac:	e3580002 	cmp	r8, #2
807044b0:	e0833000 	add	r3, r3, r0
807044b4:	e58d3000 	str	r3, [sp]
807044b8:	1affffd4 	bne	80704410 <ddr_hw_training+0xd4>
807044bc:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
807044c0:	e08a1205 	add	r1, sl, r5, lsl #4
807044c4:	e281100c 	add	r1, r1, #12
807044c8:	e3a02000 	mov	r2, #0
807044cc:	e023439b 	mla	r3, fp, r3, r4
807044d0:	e593c010 	ldr	ip, [r3, #16]
807044d4:	e5943040 	ldr	r3, [r4, #64]	; 0x40
807044d8:	e2833f8b 	add	r3, r3, #556	; 0x22c
807044dc:	e1a00003 	mov	r0, r3
807044e0:	e15c0002 	cmp	ip, r2
807044e4:	1a000004 	bne	807044fc <ddr_hw_training+0x1c0>
807044e8:	e3550001 	cmp	r5, #1
807044ec:	13a01000 	movne	r1, #0
807044f0:	1a000008 	bne	80704518 <ddr_hw_training+0x1dc>
807044f4:	e2855001 	add	r5, r5, #1
807044f8:	eaffffae 	b	807043b8 <ddr_hw_training+0x7c>
807044fc:	e490e080 	ldr	lr, [r0], #128	; 0x80
80704500:	e2822001 	add	r2, r2, #1
80704504:	e5a1e004 	str	lr, [r1, #4]!
80704508:	eafffff4 	b	807044e0 <ddr_hw_training+0x1a4>
8070450c:	e79a0101 	ldr	r0, [sl, r1, lsl #2]
80704510:	e2811001 	add	r1, r1, #1
80704514:	e4830080 	str	r0, [r3], #128	; 0x80
80704518:	e1520001 	cmp	r2, r1
8070451c:	1afffffa 	bne	8070450c <ddr_hw_training+0x1d0>
80704520:	eafffff3 	b	807044f4 <ddr_hw_training+0x1b8>
80704524:	e5b8c004 	ldr	ip, [r8, #4]!
80704528:	e5943040 	ldr	r3, [r4, #64]	; 0x40
8070452c:	e7e8105c 	ubfx	r1, ip, #0, #9
80704530:	e5845054 	str	r5, [r4, #84]	; 0x54
80704534:	e5980010 	ldr	r0, [r8, #16]
80704538:	e7e82050 	ubfx	r2, r0, #0, #9
8070453c:	e1510002 	cmp	r1, r2
80704540:	82833f8b 	addhi	r3, r3, #556	; 0x22c
80704544:	92833f8b 	addls	r3, r3, #556	; 0x22c
80704548:	80411002 	subhi	r1, r1, r2
8070454c:	90421001 	subls	r1, r2, r1
80704550:	97830385 	strls	r0, [r3, r5, lsl #7]
80704554:	e1a00004 	mov	r0, r4
80704558:	8783c385 	strhi	ip, [r3, r5, lsl #7]
8070455c:	e2855001 	add	r5, r5, #1
80704560:	8584a060 	strhi	sl, [r4, #96]	; 0x60
80704564:	9584b060 	strls	fp, [r4, #96]	; 0x60
80704568:	ebfffb3a 	bl	80703258 <ddr_rdqs_sync_rank_rdq>
8070456c:	eaffffa0 	b	807043f4 <ddr_hw_training+0xb8>
80704570:	e3a0300c 	mov	r3, #12
80704574:	e28d1010 	add	r1, sp, #16
80704578:	e0235293 	mla	r3, r3, r2, r5
8070457c:	e7911102 	ldr	r1, [r1, r2, lsl #2]
80704580:	e0843003 	add	r3, r4, r3
80704584:	e5930014 	ldr	r0, [r3, #20]
80704588:	ebfff995 	bl	80702be4 <ddr_training_set_timing>
8070458c:	e2822001 	add	r2, r2, #1
80704590:	eaffffa3 	b	80704424 <ddr_hw_training+0xe8>

80704594 <ddr_vref_training>:
80704594:	e92d45f0 	push	{r4, r5, r6, r7, r8, sl, lr}
80704598:	e1a04000 	mov	r4, r0
8070459c:	e5903060 	ldr	r3, [r0, #96]	; 0x60
807045a0:	e24ddf57 	sub	sp, sp, #348	; 0x15c
807045a4:	e5902040 	ldr	r2, [r0, #64]	; 0x40
807045a8:	e28d5004 	add	r5, sp, #4
807045ac:	e590e064 	ldr	lr, [r0, #100]	; 0x64
807045b0:	e1a0c503 	lsl	ip, r3, #10
807045b4:	e2823f87 	add	r3, r2, #540	; 0x21c
807045b8:	e082100c 	add	r1, r2, ip
807045bc:	e083300c 	add	r3, r3, ip
807045c0:	e594c05c 	ldr	ip, [r4, #92]	; 0x5c
807045c4:	e3a0203c 	mov	r2, #60	; 0x3c
807045c8:	e1a0640e 	lsl	r6, lr, #8
807045cc:	e0860001 	add	r0, r6, r1
807045d0:	e2811e21 	add	r1, r1, #528	; 0x210
807045d4:	e00c0c92 	mul	ip, r2, ip
807045d8:	e3a0200c 	mov	r2, #12
807045dc:	e022ce92 	mla	r2, r2, lr, ip
807045e0:	e2800e21 	add	r0, r0, #528	; 0x210
807045e4:	e0833006 	add	r3, r3, r6
807045e8:	e0842002 	add	r2, r4, r2
807045ec:	e5922018 	ldr	r2, [r2, #24]
807045f0:	e082e08e 	add	lr, r2, lr, lsl #1
807045f4:	e1a02005 	mov	r2, r5
807045f8:	e081138e 	add	r1, r1, lr, lsl #7
807045fc:	e1510000 	cmp	r1, r0
80704600:	1a00002e 	bne	807046c0 <ddr_vref_training+0x12c>
80704604:	e3a02f41 	mov	r2, #260	; 0x104
80704608:	e3a01000 	mov	r1, #0
8070460c:	e28d0054 	add	r0, sp, #84	; 0x54
80704610:	ebfff8fb 	bl	80702a04 <ddrtr_memset>
80704614:	e5943050 	ldr	r3, [r4, #80]	; 0x50
80704618:	e3530001 	cmp	r3, #1
8070461c:	03a06000 	moveq	r6, #0
80704620:	03a0700c 	moveq	r7, #12
80704624:	03a0803c 	moveq	r8, #60	; 0x3c
80704628:	0a00003d 	beq	80704724 <ddr_vref_training+0x190>
8070462c:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
80704630:	e3a0203c 	mov	r2, #60	; 0x3c
80704634:	e5941060 	ldr	r1, [r4, #96]	; 0x60
80704638:	e0234392 	mla	r3, r2, r3, r4
8070463c:	e5932004 	ldr	r2, [r3, #4]
80704640:	e5943044 	ldr	r3, [r4, #68]	; 0x44
80704644:	e2833060 	add	r3, r3, #96	; 0x60
80704648:	e7937101 	ldr	r7, [r3, r1, lsl #2]
8070464c:	e2423006 	sub	r3, r2, #6
80704650:	e3d33004 	bics	r3, r3, #4
80704654:	1a00004b 	bne	80704788 <ddr_vref_training+0x1f4>
80704658:	e3520006 	cmp	r2, #6
8070465c:	e3a06000 	mov	r6, #0
80704660:	17e17557 	ubfxne	r7, r7, #10, #2
80704664:	e3a0800c 	mov	r8, #12
80704668:	03a07001 	moveq	r7, #1
8070466c:	e3a0a03c 	mov	sl, #60	; 0x3c
80704670:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
80704674:	e5942064 	ldr	r2, [r4, #100]	; 0x64
80704678:	e003039a 	mul	r3, sl, r3
8070467c:	e0233298 	mla	r3, r8, r2, r3
80704680:	e0843003 	add	r3, r4, r3
80704684:	e5933018 	ldr	r3, [r3, #24]
80704688:	e1530006 	cmp	r3, r6
8070468c:	9a00002c 	bls	80704744 <ddr_vref_training+0x1b0>
80704690:	e3570001 	cmp	r7, #1
80704694:	e0862082 	add	r2, r6, r2, lsl #1
80704698:	e5842054 	str	r2, [r4, #84]	; 0x54
8070469c:	1a000002 	bne	807046ac <ddr_vref_training+0x118>
807046a0:	e3c63002 	bic	r3, r6, #2
807046a4:	e3530001 	cmp	r3, #1
807046a8:	0a000002 	beq	807046b8 <ddr_vref_training+0x124>
807046ac:	e28d1054 	add	r1, sp, #84	; 0x54
807046b0:	e1a00004 	mov	r0, r4
807046b4:	ebfffd1b 	bl	80703b28 <ddr_vref_cal>
807046b8:	e2866001 	add	r6, r6, #1
807046bc:	eaffffeb 	b	80704670 <ddr_vref_training+0xdc>
807046c0:	e594c050 	ldr	ip, [r4, #80]	; 0x50
807046c4:	e2800080 	add	r0, r0, #128	; 0x80
807046c8:	e2822004 	add	r2, r2, #4
807046cc:	e2833080 	add	r3, r3, #128	; 0x80
807046d0:	e35c0002 	cmp	ip, #2
807046d4:	0510c080 	ldreq	ip, [r0, #-128]	; 0xffffff80
807046d8:	1513c080 	ldrne	ip, [r3, #-128]	; 0xffffff80
807046dc:	0502c004 	streq	ip, [r2, #-4]
807046e0:	0510c07c 	ldreq	ip, [r0, #-124]	; 0xffffff84
807046e4:	1502c004 	strne	ip, [r2, #-4]
807046e8:	1513c07c 	ldrne	ip, [r3, #-124]	; 0xffffff84
807046ec:	0582c00c 	streq	ip, [r2, #12]
807046f0:	0510c078 	ldreq	ip, [r0, #-120]	; 0xffffff88
807046f4:	1582c00c 	strne	ip, [r2, #12]
807046f8:	0582c03c 	streq	ip, [r2, #60]	; 0x3c
807046fc:	eaffffbe 	b	807045fc <ddr_vref_training+0x68>
80704700:	e0862082 	add	r2, r6, r2, lsl #1
80704704:	e5842054 	str	r2, [r4, #84]	; 0x54
80704708:	e3c22002 	bic	r2, r2, #2
8070470c:	e3520001 	cmp	r2, #1
80704710:	0a000002 	beq	80704720 <ddr_vref_training+0x18c>
80704714:	e28d1054 	add	r1, sp, #84	; 0x54
80704718:	e1a00004 	mov	r0, r4
8070471c:	ebfffd01 	bl	80703b28 <ddr_vref_cal>
80704720:	e2866001 	add	r6, r6, #1
80704724:	e594305c 	ldr	r3, [r4, #92]	; 0x5c
80704728:	e5942064 	ldr	r2, [r4, #100]	; 0x64
8070472c:	e0030398 	mul	r3, r8, r3
80704730:	e0233297 	mla	r3, r7, r2, r3
80704734:	e0843003 	add	r3, r4, r3
80704738:	e5933018 	ldr	r3, [r3, #24]
8070473c:	e1530006 	cmp	r3, r6
80704740:	8affffee 	bhi	80704700 <ddr_vref_training+0x16c>
80704744:	e5943060 	ldr	r3, [r4, #96]	; 0x60
80704748:	e3a01000 	mov	r1, #0
8070474c:	e5940040 	ldr	r0, [r4, #64]	; 0x40
80704750:	e3a0c00c 	mov	ip, #12
80704754:	e3a0e03c 	mov	lr, #60	; 0x3c
80704758:	e0800503 	add	r0, r0, r3, lsl #10
8070475c:	e594205c 	ldr	r2, [r4, #92]	; 0x5c
80704760:	e5943064 	ldr	r3, [r4, #100]	; 0x64
80704764:	e002029e 	mul	r2, lr, r2
80704768:	e022239c 	mla	r2, ip, r3, r2
8070476c:	e0842002 	add	r2, r4, r2
80704770:	e5922018 	ldr	r2, [r2, #24]
80704774:	e1510002 	cmp	r1, r2
80704778:	3a000005 	bcc	80704794 <ddr_vref_training+0x200>
8070477c:	e28d1054 	add	r1, sp, #84	; 0x54
80704780:	e1a00004 	mov	r0, r4
80704784:	eb0000b5 	bl	80704a60 <ddr_result_data_save>
80704788:	e3a00000 	mov	r0, #0
8070478c:	e28ddf57 	add	sp, sp, #348	; 0x15c
80704790:	e8bd85f0 	pop	{r4, r5, r6, r7, r8, sl, pc}
80704794:	e5942050 	ldr	r2, [r4, #80]	; 0x50
80704798:	e0813083 	add	r3, r1, r3, lsl #1
8070479c:	e2855004 	add	r5, r5, #4
807047a0:	e2811001 	add	r1, r1, #1
807047a4:	e3520002 	cmp	r2, #2
807047a8:	e1a03383 	lsl	r3, r3, #7
807047ac:	e5152004 	ldr	r2, [r5, #-4]
807047b0:	e0833000 	add	r3, r3, r0
807047b4:	05832210 	streq	r2, [r3, #528]	; 0x210
807047b8:	0595200c 	ldreq	r2, [r5, #12]
807047bc:	1583221c 	strne	r2, [r3, #540]	; 0x21c
807047c0:	1595200c 	ldrne	r2, [r5, #12]
807047c4:	05832214 	streq	r2, [r3, #532]	; 0x214
807047c8:	0595203c 	ldreq	r2, [r5, #60]	; 0x3c
807047cc:	15832220 	strne	r2, [r3, #544]	; 0x220
807047d0:	05832218 	streq	r2, [r3, #536]	; 0x218
807047d4:	eaffffe0 	b	8070475c <ddr_vref_training+0x1c8>

807047d8 <ddr_vref_training_func>:
807047d8:	e92d4030 	push	{r4, r5, lr}
807047dc:	e24dd034 	sub	sp, sp, #52	; 0x34
807047e0:	e1a04000 	mov	r4, r0
807047e4:	e3a02401 	mov	r2, #16777216	; 0x1000000
807047e8:	e1a0100d 	mov	r1, sp
807047ec:	ebfffa05 	bl	80703008 <ddr_training_save_reg>
807047f0:	e1a00004 	mov	r0, r4
807047f4:	ebfff9dd 	bl	80702f70 <ddr_training_switch_axi>
807047f8:	e3a01002 	mov	r1, #2
807047fc:	e1a00004 	mov	r0, r4
80704800:	ebfffb11 	bl	8070344c <ddr_ddrt_init>
80704804:	e5942048 	ldr	r2, [r4, #72]	; 0x48
80704808:	e3a03001 	mov	r3, #1
8070480c:	e584306c 	str	r3, [r4, #108]	; 0x6c
80704810:	e3120401 	tst	r2, #16777216	; 0x1000000
80704814:	0a000009 	beq	80704840 <ddr_vref_training_func+0x68>
80704818:	e3a05000 	mov	r5, #0
8070481c:	e5943048 	ldr	r3, [r4, #72]	; 0x48
80704820:	e3130402 	tst	r3, #33554432	; 0x2000000
80704824:	0a00000a 	beq	80704854 <ddr_vref_training_func+0x7c>
80704828:	e1a00004 	mov	r0, r4
8070482c:	e1a0100d 	mov	r1, sp
80704830:	ebfffa31 	bl	807030fc <ddr_training_restore_reg>
80704834:	e1a00005 	mov	r0, r5
80704838:	e28dd034 	add	sp, sp, #52	; 0x34
8070483c:	e8bd8030 	pop	{r4, r5, pc}
80704840:	e5843050 	str	r3, [r4, #80]	; 0x50
80704844:	e1a00004 	mov	r0, r4
80704848:	ebffff51 	bl	80704594 <ddr_vref_training>
8070484c:	e1a05000 	mov	r5, r0
80704850:	eafffff1 	b	8070481c <ddr_vref_training_func+0x44>
80704854:	e3a03002 	mov	r3, #2
80704858:	e1a00004 	mov	r0, r4
8070485c:	e5843050 	str	r3, [r4, #80]	; 0x50
80704860:	ebffff4b 	bl	80704594 <ddr_vref_training>
80704864:	e0855000 	add	r5, r5, r0
80704868:	eaffffee 	b	80704828 <ddr_vref_training_func+0x50>

8070486c <ddr_wl_func>:
8070486c:	e3a00000 	mov	r0, #0
80704870:	e12fff1e 	bx	lr

80704874 <ddr_gating_func>:
80704874:	e3a00000 	mov	r0, #0
80704878:	e12fff1e 	bx	lr

8070487c <ddr_ac_training_func>:
8070487c:	e3a00000 	mov	r0, #0
80704880:	e12fff1e 	bx	lr

80704884 <ddr_lpca_training_func>:
80704884:	e3a00000 	mov	r0, #0
80704888:	e12fff1e 	bx	lr

8070488c <ddr_dcc_training_func>:
8070488c:	e3a00000 	mov	r0, #0
80704890:	e12fff1e 	bx	lr

80704894 <ddr_pcode_training>:
80704894:	e3a00000 	mov	r0, #0
80704898:	e12fff1e 	bx	lr

8070489c <ddr_training_console_if>:
8070489c:	e3a00000 	mov	r0, #0
807048a0:	e12fff1e 	bx	lr

807048a4 <ddr_training_boot_func>:
807048a4:	e92d4030 	push	{r4, r5, lr}
807048a8:	e1a04000 	mov	r4, r0
807048ac:	e5900040 	ldr	r0, [r0, #64]	; 0x40
807048b0:	e24dd034 	sub	sp, sp, #52	; 0x34
807048b4:	e2800008 	add	r0, r0, #8
807048b8:	ebfff845 	bl	807029d4 <ddr_read>
807048bc:	e3100020 	tst	r0, #32
807048c0:	0a000004 	beq	807048d8 <ddr_training_boot_func+0x34>
807048c4:	e3e03000 	mvn	r3, #0
807048c8:	e5941040 	ldr	r1, [r4, #64]	; 0x40
807048cc:	e1a02003 	mov	r2, r3
807048d0:	e3a00002 	mov	r0, #2
807048d4:	ebfff93c 	bl	80702dcc <ddr_training_stat>
807048d8:	e1a00004 	mov	r0, r4
807048dc:	ebffffe8 	bl	80704884 <ddr_lpca_training_func>
807048e0:	e1a05000 	mov	r5, r0
807048e4:	e1a00004 	mov	r0, r4
807048e8:	ebffffdf 	bl	8070486c <ddr_wl_func>
807048ec:	e0855000 	add	r5, r5, r0
807048f0:	e1a00004 	mov	r0, r4
807048f4:	ebfffd78 	bl	80703edc <ddr_dataeye_training_func>
807048f8:	e0955000 	adds	r5, r5, r0
807048fc:	0a000013 	beq	80704950 <ddr_training_boot_func+0xac>
80704900:	e3a01601 	mov	r1, #1048576	; 0x100000
80704904:	e1a00004 	mov	r0, r4
80704908:	ebfff991 	bl	80702f54 <ddr_training_check_bypass>
8070490c:	e3500000 	cmp	r0, #0
80704910:	1a00000e 	bne	80704950 <ddr_training_boot_func+0xac>
80704914:	e3a02601 	mov	r2, #1048576	; 0x100000
80704918:	e1a0100d 	mov	r1, sp
8070491c:	e1a00004 	mov	r0, r4
80704920:	ebfff9b8 	bl	80703008 <ddr_training_save_reg>
80704924:	e1a00004 	mov	r0, r4
80704928:	ebfffd86 	bl	80703f48 <ddr_hw_dataeye_read>
8070492c:	e1a0100d 	mov	r1, sp
80704930:	e1a05000 	mov	r5, r0
80704934:	e1a00004 	mov	r0, r4
80704938:	ebfff9ef 	bl	807030fc <ddr_training_restore_reg>
8070493c:	e3a03001 	mov	r3, #1
80704940:	e1a00004 	mov	r0, r4
80704944:	e5843068 	str	r3, [r4, #104]	; 0x68
80704948:	ebfffd48 	bl	80703e70 <ddr_dataeye_training>
8070494c:	e0855000 	add	r5, r5, r0
80704950:	e1a00004 	mov	r0, r4
80704954:	ebffffc6 	bl	80704874 <ddr_gating_func>
80704958:	e0855000 	add	r5, r5, r0
8070495c:	e1a00004 	mov	r0, r4
80704960:	ebffff9c 	bl	807047d8 <ddr_vref_training_func>
80704964:	e0850000 	add	r0, r5, r0
80704968:	e28dd034 	add	sp, sp, #52	; 0x34
8070496c:	e8bd8030 	pop	{r4, r5, pc}

80704970 <ddr_sw_training_func>:
80704970:	e92d4030 	push	{r4, r5, lr}
80704974:	e24dd07c 	sub	sp, sp, #124	; 0x7c
80704978:	e59f0098 	ldr	r0, [pc, #152]	; 80704a18 <ddr_sw_training_func+0xa8>
8070497c:	ebfff814 	bl	807029d4 <ddr_read>
80704980:	e59f1090 	ldr	r1, [pc, #144]	; 80704a18 <ddr_sw_training_func+0xa8>
80704984:	e1a00820 	lsr	r0, r0, #16
80704988:	e1a00800 	lsl	r0, r0, #16
8070498c:	e3800f85 	orr	r0, r0, #532	; 0x214
80704990:	e3800002 	orr	r0, r0, #2
80704994:	ebfff810 	bl	807029dc <ddr_write>
80704998:	e59f007c 	ldr	r0, [pc, #124]	; 80704a1c <ddr_sw_training_func+0xac>
8070499c:	ebfff80c 	bl	807029d4 <ddr_read>
807049a0:	e3700001 	cmn	r0, #1
807049a4:	0a000012 	beq	807049f4 <ddr_sw_training_func+0x84>
807049a8:	eb000032 	bl	80704a78 <ddr_training_start>
807049ac:	e59f106c 	ldr	r1, [pc, #108]	; 80704a20 <ddr_sw_training_func+0xb0>
807049b0:	e3a00000 	mov	r0, #0
807049b4:	e3a05000 	mov	r5, #0
807049b8:	ebfff807 	bl	807029dc <ddr_write>
807049bc:	e1a0000d 	mov	r0, sp
807049c0:	ebfffa95 	bl	8070341c <ddr_training_cfg_init>
807049c4:	e1a0000d 	mov	r0, sp
807049c8:	e58d5070 	str	r5, [sp, #112]	; 0x70
807049cc:	ebfff857 	bl	80702b30 <ddr_training_all>
807049d0:	e1a04000 	mov	r4, r0
807049d4:	e1a0000d 	mov	r0, sp
807049d8:	ebffffab 	bl	8070488c <ddr_dcc_training_func>
807049dc:	e0944000 	adds	r4, r4, r0
807049e0:	1a000009 	bne	80704a0c <ddr_sw_training_func+0x9c>
807049e4:	eb000022 	bl	80704a74 <ddr_training_suc>
807049e8:	e1a00004 	mov	r0, r4
807049ec:	e28dd07c 	add	sp, sp, #124	; 0x7c
807049f0:	e8bd8030 	pop	{r4, r5, pc}
807049f4:	e59f0028 	ldr	r0, [pc, #40]	; 80704a24 <ddr_sw_training_func+0xb4>
807049f8:	ebfff7f5 	bl	807029d4 <ddr_read>
807049fc:	e3700001 	cmn	r0, #1
80704a00:	1affffe8 	bne	807049a8 <ddr_sw_training_func+0x38>
80704a04:	e3a04000 	mov	r4, #0
80704a08:	eafffff6 	b	807049e8 <ddr_sw_training_func+0x78>
80704a0c:	e1a00005 	mov	r0, r5
80704a10:	ebffffa1 	bl	8070489c <ddr_training_console_if>
80704a14:	eafffff3 	b	807049e8 <ddr_sw_training_func+0x78>
80704a18:	120200ac 	.word	0x120200ac
80704a1c:	120200a0 	.word	0x120200a0
80704a20:	1202009c 	.word	0x1202009c
80704a24:	120200a4 	.word	0x120200a4

80704a28 <ddr_pcode_training_func>:
80704a28:	e3a00000 	mov	r0, #0
80704a2c:	e12fff1e 	bx	lr

80704a30 <ddr_hw_training_func>:
80704a30:	e52de004 	push	{lr}		; (str lr, [sp, #-4]!)
80704a34:	e24dd07c 	sub	sp, sp, #124	; 0x7c
80704a38:	e1a0000d 	mov	r0, sp
80704a3c:	ebfffa76 	bl	8070341c <ddr_training_cfg_init>
80704a40:	e1a0000d 	mov	r0, sp
80704a44:	ebfffe3c 	bl	8070433c <ddr_hw_training>
80704a48:	e28dd07c 	add	sp, sp, #124	; 0x7c
80704a4c:	e49df004 	pop	{pc}		; (ldr pc, [sp], #4)

80704a50 <ddr_sw_training_if>:
80704a50:	eaffffc6 	b	80704970 <ddr_sw_training_func>

80704a54 <ddr_hw_training_if>:
80704a54:	eafffff5 	b	80704a30 <ddr_hw_training_func>

80704a58 <ddr_pcode_training_if>:
80704a58:	e3a00000 	mov	r0, #0
80704a5c:	e12fff1e 	bx	lr

80704a60 <ddr_result_data_save>:
80704a60:	e12fff1e 	bx	lr

80704a64 <ddr_lpca_data_save>:
80704a64:	e12fff1e 	bx	lr

80704a68 <ddr_ddrt_get_test_addr>:
80704a68:	e3a00102 	mov	r0, #-2147483648	; 0x80000000
80704a6c:	e12fff1e 	bx	lr

80704a70 <ddr_training_error>:
80704a70:	e12fff1e 	bx	lr

80704a74 <ddr_training_suc>:
80704a74:	e12fff1e 	bx	lr

80704a78 <ddr_training_start>:
80704a78:	e12fff1e 	bx	lr

80704a7c <ddr_cmd_prepare_copy>:
80704a7c:	e12fff1e 	bx	lr

80704a80 <ddr_cmd_site_save>:
80704a80:	e92d4010 	push	{r4, lr}
80704a84:	e59f402c 	ldr	r4, [pc, #44]	; 80704ab8 <ddr_cmd_site_save+0x38>
80704a88:	e1a00004 	mov	r0, r4
80704a8c:	ebfff7d0 	bl	807029d4 <ddr_read>
80704a90:	e1a01004 	mov	r1, r4
80704a94:	e3800002 	orr	r0, r0, #2
80704a98:	ebfff7cf 	bl	807029dc <ddr_write>
80704a9c:	e320f000 	nop	{0}
80704aa0:	e1a00004 	mov	r0, r4
80704aa4:	ebfff7ca 	bl	807029d4 <ddr_read>
80704aa8:	e1a01004 	mov	r1, r4
80704aac:	e8bd4010 	pop	{r4, lr}
80704ab0:	e3c00001 	bic	r0, r0, #1
80704ab4:	eafff7c8 	b	807029dc <ddr_write>
80704ab8:	12010198 	.word	0x12010198

80704abc <ddr_cmd_site_restore>:
80704abc:	e92d4010 	push	{r4, lr}
80704ac0:	e59f402c 	ldr	r4, [pc, #44]	; 80704af4 <ddr_cmd_site_restore+0x38>
80704ac4:	e1a00004 	mov	r0, r4
80704ac8:	ebfff7c1 	bl	807029d4 <ddr_read>
80704acc:	e1a01004 	mov	r1, r4
80704ad0:	e3800001 	orr	r0, r0, #1
80704ad4:	ebfff7c0 	bl	807029dc <ddr_write>
80704ad8:	e320f000 	nop	{0}
80704adc:	e1a00004 	mov	r0, r4
80704ae0:	ebfff7bb 	bl	807029d4 <ddr_read>
80704ae4:	e1a01004 	mov	r1, r4
80704ae8:	e8bd4010 	pop	{r4, lr}
80704aec:	e3c00002 	bic	r0, r0, #2
80704af0:	eafff7b9 	b	807029dc <ddr_write>
80704af4:	12010198 	.word	0x12010198

80704af8 <ddr_training_save_reg_custom>:
80704af8:	e92d4070 	push	{r4, r5, r6, lr}
80704afc:	e1a05000 	mov	r5, r0
80704b00:	e59f4018 	ldr	r4, [pc, #24]	; 80704b20 <ddr_training_save_reg_custom+0x28>
80704b04:	e1a00004 	mov	r0, r4
80704b08:	ebfff7b1 	bl	807029d4 <ddr_read>
80704b0c:	e1a01004 	mov	r1, r4
80704b10:	e585001c 	str	r0, [r5, #28]
80704b14:	e3c00102 	bic	r0, r0, #-2147483648	; 0x80000000
80704b18:	e8bd4070 	pop	{r4, r5, r6, lr}
80704b1c:	eafff7ae 	b	807029dc <ddr_write>
80704b20:	1206c0b0 	.word	0x1206c0b0

80704b24 <ddr_training_restore_reg_custom>:
80704b24:	e59f1004 	ldr	r1, [pc, #4]	; 80704b30 <ddr_training_restore_reg_custom+0xc>
80704b28:	e590001c 	ldr	r0, [r0, #28]
80704b2c:	eafff7aa 	b	807029dc <ddr_write>
80704b30:	1206c0b0 	.word	0x1206c0b0

80704b34 <uart_early_init>:
80704b34:	e59f3028 	ldr	r3, [pc, #40]	; 80704b64 <uart_base_addr_L0>
80704b38:	e3a02000 	mov	r2, #0
80704b3c:	e5832030 	str	r2, [r3, #48]	; 0x30
80704b40:	e282200d 	add	r2, r2, #13
80704b44:	e5832024 	str	r2, [r3, #36]	; 0x24
80704b48:	e3a02001 	mov	r2, #1
80704b4c:	e5832028 	str	r2, [r3, #40]	; 0x28
80704b50:	e3a02070 	mov	r2, #112	; 0x70
80704b54:	e583202c 	str	r2, [r3, #44]	; 0x2c
80704b58:	e59f2098 	ldr	r2, [pc, #152]	; 80704bf8 <uart_base_addr_L3+0x4>
80704b5c:	e5832030 	str	r2, [r3, #48]	; 0x30
80704b60:	e12fff1e 	bx	lr

80704b64 <uart_base_addr_L0>:
80704b64:	120a0000 	.word	0x120a0000

80704b68 <uart_early_puts>:
80704b68:	e59f1024 	ldr	r1, [pc, #36]	; 80704b94 <uart_base_addr_L1>
80704b6c:	ea000004 	b	80704b84 <next_char>

80704b70 <output>:
80704b70:	e5913018 	ldr	r3, [r1, #24]
80704b74:	e3130020 	tst	r3, #32
80704b78:	1afffffc 	bne	80704b70 <output>
80704b7c:	e5812000 	str	r2, [r1]
80704b80:	e2800001 	add	r0, r0, #1

80704b84 <next_char>:
80704b84:	e5d02000 	ldrb	r2, [r0]
80704b88:	e3520000 	cmp	r2, #0
80704b8c:	1afffff7 	bne	80704b70 <output>
80704b90:	e12fff1e 	bx	lr

80704b94 <uart_base_addr_L1>:
80704b94:	120a0000 	.word	0x120a0000

80704b98 <uart_early_put_hex>:
80704b98:	e59f1038 	ldr	r1, [pc, #56]	; 80704bd8 <uart_base_addr_L2>
80704b9c:	e3a0201c 	mov	r2, #28

80704ba0 <wait2>:
80704ba0:	e5913018 	ldr	r3, [r1, #24]
80704ba4:	e3130020 	tst	r3, #32
80704ba8:	1afffffc 	bne	80704ba0 <wait2>
80704bac:	e3a0300f 	mov	r3, #15
80704bb0:	e0033230 	and	r3, r3, r0, lsr r2
80704bb4:	e3530009 	cmp	r3, #9
80704bb8:	d2833030 	addle	r3, r3, #48	; 0x30
80704bbc:	c2833037 	addgt	r3, r3, #55	; 0x37
80704bc0:	e5813000 	str	r3, [r1]
80704bc4:	e3520000 	cmp	r2, #0
80704bc8:	0a000001 	beq	80704bd4 <exit2>
80704bcc:	e2422004 	sub	r2, r2, #4
80704bd0:	eafffff2 	b	80704ba0 <wait2>

80704bd4 <exit2>:
80704bd4:	e12fff1e 	bx	lr

80704bd8 <uart_base_addr_L2>:
80704bd8:	120a0000 	.word	0x120a0000

80704bdc <uart_early_putc>:
80704bdc:	e59f1010 	ldr	r1, [pc, #16]	; 80704bf4 <uart_base_addr_L3>

80704be0 <wait3>:
80704be0:	e5913018 	ldr	r3, [r1, #24]
80704be4:	e3130020 	tst	r3, #32
80704be8:	1afffffc 	bne	80704be0 <wait3>
80704bec:	e5810000 	str	r0, [r1]
80704bf0:	e12fff1e 	bx	lr

80704bf4 <uart_base_addr_L3>:
80704bf4:	120a0000 	.word	0x120a0000
80704bf8:	00000301 	.word	0x00000301

80704bfc <__div0>:
80704bfc:	ea00015b 	b	80705170 <hang>

80704c00 <emmc_boot_read>:
80704c00:	e3510a06 	cmp	r1, #24576	; 0x6000
80704c04:	e92d4070 	push	{r4, r5, r6, lr}
80704c08:	8a000010 	bhi	80704c50 <emmc_boot_read+0x50>
80704c0c:	e3c11003 	bic	r1, r1, #3
80704c10:	e59f313c 	ldr	r3, [pc, #316]	; 80704d54 <emmc_boot_read+0x154>
80704c14:	e0801001 	add	r1, r0, r1
80704c18:	e1510000 	cmp	r1, r0
80704c1c:	1a000008 	bne	80704c44 <emmc_boot_read+0x44>
80704c20:	e59f3130 	ldr	r3, [pc, #304]	; 80704d58 <emmc_boot_read+0x158>
80704c24:	e59f2130 	ldr	r2, [pc, #304]	; 80704d5c <emmc_boot_read+0x15c>
80704c28:	e583202c 	str	r2, [r3, #44]	; 0x2c
80704c2c:	e5932000 	ldr	r2, [r3]
80704c30:	e3822007 	orr	r2, r2, #7
80704c34:	e5832000 	str	r2, [r3]
80704c38:	e59f2120 	ldr	r2, [pc, #288]	; 80704d60 <emmc_boot_read+0x160>
80704c3c:	e5832044 	str	r2, [r3, #68]	; 0x44
80704c40:	e8bd8070 	pop	{r4, r5, r6, pc}
80704c44:	e4932004 	ldr	r2, [r3], #4
80704c48:	e4802004 	str	r2, [r0], #4
80704c4c:	eafffff1 	b	80704c18 <emmc_boot_read+0x18>
80704c50:	e59f30fc 	ldr	r3, [pc, #252]	; 80704d54 <emmc_boot_read+0x154>
80704c54:	e2402004 	sub	r2, r0, #4
80704c58:	e59fc104 	ldr	ip, [pc, #260]	; 80704d64 <emmc_boot_read+0x164>
80704c5c:	e493e004 	ldr	lr, [r3], #4
80704c60:	e153000c 	cmp	r3, ip
80704c64:	e5a2e004 	str	lr, [r2, #4]!
80704c68:	1afffffb 	bne	80704c5c <emmc_boot_read+0x5c>
80704c6c:	e2411a06 	sub	r1, r1, #24576	; 0x6000
80704c70:	e59f20e0 	ldr	r2, [pc, #224]	; 80704d58 <emmc_boot_read+0x158>
80704c74:	e59f50e4 	ldr	r5, [pc, #228]	; 80704d60 <emmc_boot_read+0x160>
80704c78:	e2800a06 	add	r0, r0, #24576	; 0x6000
80704c7c:	e1a01121 	lsr	r1, r1, #2
80704c80:	e30b4880 	movw	r4, #47232	; 0xb880
80704c84:	e3510000 	cmp	r1, #0
80704c88:	1a000019 	bne	80704cf4 <emmc_boot_read+0xf4>
80704c8c:	e59f30c8 	ldr	r3, [pc, #200]	; 80704d5c <emmc_boot_read+0x15c>
80704c90:	e59f00c0 	ldr	r0, [pc, #192]	; 80704d58 <emmc_boot_read+0x158>
80704c94:	e582302c 	str	r3, [r2, #44]	; 0x2c
80704c98:	e3a03ffa 	mov	r3, #1000	; 0x3e8
80704c9c:	e30023e9 	movw	r2, #1001	; 0x3e9
80704ca0:	e2522001 	subs	r2, r2, #1
80704ca4:	1a000026 	bne	80704d44 <emmc_boot_read+0x144>
80704ca8:	e590102c 	ldr	r1, [r0, #44]	; 0x2c
80704cac:	e2533001 	subs	r3, r3, #1
80704cb0:	13a02001 	movne	r2, #1
80704cb4:	03a02000 	moveq	r2, #0
80704cb8:	e0122fa1 	ands	r2, r2, r1, lsr #31
80704cbc:	1afffff6 	bne	80704c9c <emmc_boot_read+0x9c>
80704cc0:	e59f0090 	ldr	r0, [pc, #144]	; 80704d58 <emmc_boot_read+0x158>
80704cc4:	e3a02ffa 	mov	r2, #1000	; 0x3e8
80704cc8:	e30033e9 	movw	r3, #1001	; 0x3e9
80704ccc:	e2533001 	subs	r3, r3, #1
80704cd0:	1a00001d 	bne	80704d4c <emmc_boot_read+0x14c>
80704cd4:	e5903044 	ldr	r3, [r0, #68]	; 0x44
80704cd8:	e2522001 	subs	r2, r2, #1
80704cdc:	e2233008 	eor	r3, r3, #8
80704ce0:	13a01001 	movne	r1, #1
80704ce4:	03a01000 	moveq	r1, #0
80704ce8:	e01131a3 	ands	r3, r1, r3, lsr #3
80704cec:	1afffff5 	bne	80704cc8 <emmc_boot_read+0xc8>
80704cf0:	eaffffca 	b	80704c20 <emmc_boot_read+0x20>
80704cf4:	e5923048 	ldr	r3, [r2, #72]	; 0x48
80704cf8:	e592c044 	ldr	ip, [r2, #68]	; 0x44
80704cfc:	e11c0004 	tst	ip, r4
80704d00:	1affffc6 	bne	80704c20 <emmc_boot_read+0x20>
80704d04:	e31c0020 	tst	ip, #32
80704d08:	0afffffa 	beq	80704cf8 <emmc_boot_read+0xf8>
80704d0c:	e7ec38d3 	ubfx	r3, r3, #17, #13
80704d10:	e1a0e000 	mov	lr, r0
80704d14:	e1530001 	cmp	r3, r1
80704d18:	e5825044 	str	r5, [r2, #68]	; 0x44
80704d1c:	21a03001 	movcs	r3, r1
80704d20:	e1a0c003 	mov	ip, r3
80704d24:	e35c0000 	cmp	ip, #0
80704d28:	00800103 	addeq	r0, r0, r3, lsl #2
80704d2c:	00411003 	subeq	r1, r1, r3
80704d30:	0affffd3 	beq	80704c84 <emmc_boot_read+0x84>
80704d34:	e5926200 	ldr	r6, [r2, #512]	; 0x200
80704d38:	e24cc001 	sub	ip, ip, #1
80704d3c:	e48e6004 	str	r6, [lr], #4
80704d40:	eafffff7 	b	80704d24 <emmc_boot_read+0x124>
80704d44:	e320f000 	nop	{0}
80704d48:	eaffffd4 	b	80704ca0 <emmc_boot_read+0xa0>
80704d4c:	e320f000 	nop	{0}
80704d50:	eaffffdd 	b	80704ccc <emmc_boot_read+0xcc>
80704d54:	04010500 	.word	0x04010500
80704d58:	10100000 	.word	0x10100000
80704d5c:	84004000 	.word	0x84004000
80704d60:	0001affe 	.word	0x0001affe
80704d64:	04016500 	.word	0x04016500

80704d68 <hw_dec_sop_eop_first_set>:
80704d68:	e59f2034 	ldr	r2, [pc, #52]	; 80704da4 <hw_dec_sop_eop_first_set+0x3c>
80704d6c:	e3500001 	cmp	r0, #1
80704d70:	e59f3030 	ldr	r3, [pc, #48]	; 80704da8 <hw_dec_sop_eop_first_set+0x40>
80704d74:	13a01001 	movne	r1, #1
80704d78:	15821000 	strne	r1, [r2]
80704d7c:	13a02000 	movne	r2, #0
80704d80:	05830000 	streq	r0, [r3]
80704d84:	15832000 	strne	r2, [r3]
80704d88:	e59f301c 	ldr	r3, [pc, #28]	; 80704dac <hw_dec_sop_eop_first_set+0x44>
80704d8c:	05820000 	streq	r0, [r2]
80704d90:	e3a02000 	mov	r2, #0
80704d94:	e5832000 	str	r2, [r3]
80704d98:	e59f3010 	ldr	r3, [pc, #16]	; 80704db0 <hw_dec_sop_eop_first_set+0x48>
80704d9c:	e5830000 	str	r0, [r3]
80704da0:	e12fff1e 	bx	lr
80704da4:	8074b868 	.word	0x8074b868
80704da8:	8074b864 	.word	0x8074b864
80704dac:	8074b860 	.word	0x8074b860
80704db0:	8074b85c 	.word	0x8074b85c

80704db4 <hw_dec_intr_proc>:
80704db4:	e59f30a0 	ldr	r3, [pc, #160]	; 80704e5c <hw_dec_intr_proc+0xa8>
80704db8:	e5931124 	ldr	r1, [r3, #292]	; 0x124
80704dbc:	f57ff05f 	dmb	sy
80704dc0:	e3110002 	tst	r1, #2
80704dc4:	0a00000c 	beq	80704dfc <hw_dec_intr_proc+0x48>
80704dc8:	e59f2090 	ldr	r2, [pc, #144]	; 80704e60 <hw_dec_intr_proc+0xac>
80704dcc:	e5920080 	ldr	r0, [r2, #128]	; 0x80
80704dd0:	f57ff05f 	dmb	sy
80704dd4:	e3500000 	cmp	r0, #0
80704dd8:	aa000002 	bge	80704de8 <hw_dec_intr_proc+0x34>
80704ddc:	f57ff05f 	dmb	sy
80704de0:	e3a00001 	mov	r0, #1
80704de4:	e5820090 	str	r0, [r2, #144]	; 0x90
80704de8:	e5932130 	ldr	r2, [r3, #304]	; 0x130
80704dec:	f57ff05f 	dmb	sy
80704df0:	e3822002 	orr	r2, r2, #2
80704df4:	f57ff05f 	dmb	sy
80704df8:	e5832130 	str	r2, [r3, #304]	; 0x130
80704dfc:	e3110001 	tst	r1, #1
80704e00:	0a000013 	beq	80704e54 <hw_dec_intr_proc+0xa0>
80704e04:	e59f2054 	ldr	r2, [pc, #84]	; 80704e60 <hw_dec_intr_proc+0xac>
80704e08:	e5923084 	ldr	r3, [r2, #132]	; 0x84
80704e0c:	f57ff05f 	dmb	sy
80704e10:	e3530000 	cmp	r3, #0
80704e14:	a3a00000 	movge	r0, #0
80704e18:	aa000006 	bge	80704e38 <hw_dec_intr_proc+0x84>
80704e1c:	e6ef3073 	uxtb	r3, r3
80704e20:	e3530000 	cmp	r3, #0
80704e24:	03a00000 	moveq	r0, #0
80704e28:	13e00001 	mvnne	r0, #1
80704e2c:	f57ff05f 	dmb	sy
80704e30:	e3a03001 	mov	r3, #1
80704e34:	e5823094 	str	r3, [r2, #148]	; 0x94
80704e38:	e59f201c 	ldr	r2, [pc, #28]	; 80704e5c <hw_dec_intr_proc+0xa8>
80704e3c:	e5923130 	ldr	r3, [r2, #304]	; 0x130
80704e40:	f57ff05f 	dmb	sy
80704e44:	e3833001 	orr	r3, r3, #1
80704e48:	f57ff05f 	dmb	sy
80704e4c:	e5823130 	str	r3, [r2, #304]	; 0x130
80704e50:	e12fff1e 	bx	lr
80704e54:	e3e00000 	mvn	r0, #0
80704e58:	e12fff1e 	bx	lr
80704e5c:	11200000 	.word	0x11200000
80704e60:	11202000 	.word	0x11202000

80704e64 <hw_dec_start>:
80704e64:	e59fc0a0 	ldr	ip, [pc, #160]	; 80704f0c <hw_dec_start+0xa8>
80704e68:	e92d4010 	push	{r4, lr}
80704e6c:	e59ce000 	ldr	lr, [ip]
80704e70:	e59d4010 	ldr	r4, [sp, #16]
80704e74:	e35e0000 	cmp	lr, #0
80704e78:	e1a0e00c 	mov	lr, ip
80704e7c:	e59fc08c 	ldr	ip, [pc, #140]	; 80704f10 <hw_dec_start+0xac>
80704e80:	0a000005 	beq	80704e9c <hw_dec_start+0x38>
80704e84:	e3540000 	cmp	r4, #0
80704e88:	1a000016 	bne	80704ee8 <hw_dec_start+0x84>
80704e8c:	f57ff05f 	dmb	sy
80704e90:	e58c1020 	str	r1, [ip, #32]
80704e94:	f57ff05f 	dmb	sy
80704e98:	e58c3024 	str	r3, [ip, #36]	; 0x24
80704e9c:	f57ff05f 	dmb	sy
80704ea0:	e59f306c 	ldr	r3, [pc, #108]	; 80704f14 <hw_dec_start+0xb0>
80704ea4:	e3540000 	cmp	r4, #0
80704ea8:	e58c0040 	str	r0, [ip, #64]	; 0x40
80704eac:	e59e1000 	ldr	r1, [lr]
80704eb0:	e5933000 	ldr	r3, [r3]
80704eb4:	e1a03e83 	lsl	r3, r3, #29
80704eb8:	e1833e01 	orr	r3, r3, r1, lsl #28
80704ebc:	e1832002 	orr	r2, r3, r2
80704ec0:	03a03102 	moveq	r3, #-2147483648	; 0x80000000
80704ec4:	13a03000 	movne	r3, #0
80704ec8:	e1832002 	orr	r2, r3, r2
80704ecc:	f57ff05f 	dmb	sy
80704ed0:	e58c2044 	str	r2, [ip, #68]	; 0x44
80704ed4:	f57ff05f 	dmb	sy
80704ed8:	e59f3038 	ldr	r3, [pc, #56]	; 80704f18 <hw_dec_start+0xb4>
80704edc:	e59d2008 	ldr	r2, [sp, #8]
80704ee0:	e5832000 	str	r2, [r3]
80704ee4:	e8bd8010 	pop	{r4, pc}
80704ee8:	f57ff05f 	dmb	sy
80704eec:	e2833eff 	add	r3, r3, #4080	; 0xff0
80704ef0:	e58c1028 	str	r1, [ip, #40]	; 0x28
80704ef4:	e283300f 	add	r3, r3, #15
80704ef8:	e1a03623 	lsr	r3, r3, #12
80704efc:	e1a03103 	lsl	r3, r3, #2
80704f00:	f57ff05f 	dmb	sy
80704f04:	e58c302c 	str	r3, [ip, #44]	; 0x2c
80704f08:	eaffffe3 	b	80704e9c <hw_dec_start+0x38>
80704f0c:	8074b868 	.word	0x8074b868
80704f10:	11202000 	.word	0x11202000
80704f14:	8074b864 	.word	0x8074b864
80704f18:	11204000 	.word	0x11204000

80704f1c <hw_dec_wait_finish>:
80704f1c:	e92d4073 	push	{r0, r1, r4, r5, r6, lr}
80704f20:	e3a06000 	mov	r6, #0
80704f24:	e59f4058 	ldr	r4, [pc, #88]	; 80704f84 <hw_dec_wait_finish+0x68>
80704f28:	e3a01000 	mov	r1, #0
80704f2c:	e3a00056 	mov	r0, #86	; 0x56
80704f30:	ebffff9f 	bl	80704db4 <hw_dec_intr_proc>
80704f34:	e2544001 	subs	r4, r4, #1
80704f38:	e1a05000 	mov	r5, r0
80704f3c:	1a000004 	bne	80704f54 <hw_dec_wait_finish+0x38>
80704f40:	e59f0040 	ldr	r0, [pc, #64]	; 80704f88 <hw_dec_wait_finish+0x6c>
80704f44:	ebffff07 	bl	80704b68 <uart_early_puts>
80704f48:	e1a00005 	mov	r0, r5
80704f4c:	e28dd008 	add	sp, sp, #8
80704f50:	e8bd8070 	pop	{r4, r5, r6, pc}
80704f54:	e58d6004 	str	r6, [sp, #4]
80704f58:	e59d3004 	ldr	r3, [sp, #4]
80704f5c:	e3530063 	cmp	r3, #99	; 0x63
80704f60:	9a000002 	bls	80704f70 <hw_dec_wait_finish+0x54>
80704f64:	e3750001 	cmn	r5, #1
80704f68:	0affffee 	beq	80704f28 <hw_dec_wait_finish+0xc>
80704f6c:	eafffff5 	b	80704f48 <hw_dec_wait_finish+0x2c>
80704f70:	e320f000 	nop	{0}
80704f74:	e59d3004 	ldr	r3, [sp, #4]
80704f78:	e2833001 	add	r3, r3, #1
80704f7c:	e58d3004 	str	r3, [sp, #4]
80704f80:	eafffff4 	b	80704f58 <hw_dec_wait_finish+0x3c>
80704f84:	001e8481 	.word	0x001e8481
80704f88:	8074b824 	.word	0x8074b824

80704f8c <hw_dec_decompress>:
80704f8c:	e92d40f0 	push	{r4, r5, r6, r7, lr}
80704f90:	e1a06000 	mov	r6, r0
80704f94:	e24dd014 	sub	sp, sp, #20
80704f98:	e3a00001 	mov	r0, #1
80704f9c:	e1a04001 	mov	r4, r1
80704fa0:	e1a05002 	mov	r5, r2
80704fa4:	e1a07003 	mov	r7, r3
80704fa8:	ebffff6e 	bl	80704d68 <hw_dec_sop_eop_first_set>
80704fac:	e59f304c 	ldr	r3, [pc, #76]	; 80705000 <hw_dec_decompress+0x74>
80704fb0:	e1a01006 	mov	r1, r6
80704fb4:	e1a00005 	mov	r0, r5
80704fb8:	e3a02001 	mov	r2, #1
80704fbc:	e5933000 	ldr	r3, [r3]
80704fc0:	e58d3008 	str	r3, [sp, #8]
80704fc4:	e3a03000 	mov	r3, #0
80704fc8:	e88d000c 	stm	sp, {r2, r3}
80704fcc:	e1a02007 	mov	r2, r7
80704fd0:	e5943000 	ldr	r3, [r4]
80704fd4:	ebffffa2 	bl	80704e64 <hw_dec_start>
80704fd8:	ebffffcf 	bl	80704f1c <hw_dec_wait_finish>
80704fdc:	e59f3020 	ldr	r3, [pc, #32]	; 80705004 <hw_dec_decompress+0x78>
80704fe0:	e5933088 	ldr	r3, [r3, #136]	; 0x88
80704fe4:	f57ff05f 	dmb	sy
80704fe8:	e2900000 	adds	r0, r0, #0
80704fec:	e5843000 	str	r3, [r4]
80704ff0:	13a00001 	movne	r0, #1
80704ff4:	e2600000 	rsb	r0, r0, #0
80704ff8:	e28dd014 	add	sp, sp, #20
80704ffc:	e8bd80f0 	pop	{r4, r5, r6, r7, pc}
80705000:	8074b86c 	.word	0x8074b86c
80705004:	11202000 	.word	0x11202000

80705008 <hw_dec_init>:
80705008:	e59f2054 	ldr	r2, [pc, #84]	; 80705064 <hw_dec_init+0x5c>
8070500c:	e5923190 	ldr	r3, [r2, #400]	; 0x190
80705010:	f57ff05f 	dmb	sy
80705014:	e3833007 	orr	r3, r3, #7
80705018:	f57ff05f 	dmb	sy
8070501c:	e5823190 	str	r3, [r2, #400]	; 0x190
80705020:	f57ff05f 	dmb	sy
80705024:	e59f303c 	ldr	r3, [pc, #60]	; 80705068 <hw_dec_init+0x60>
80705028:	e3a01000 	mov	r1, #0
8070502c:	e5831108 	str	r1, [r3, #264]	; 0x108
80705030:	f57ff05f 	dmb	sy
80705034:	e3a02003 	mov	r2, #3
80705038:	e583210c 	str	r2, [r3, #268]	; 0x10c
8070503c:	f57ff05f 	dmb	sy
80705040:	e5831110 	str	r1, [r3, #272]	; 0x110
80705044:	f57ff05f 	dmb	sy
80705048:	e5832114 	str	r2, [r3, #276]	; 0x114
8070504c:	f57ff05f 	dmb	sy
80705050:	e5832128 	str	r2, [r3, #296]	; 0x128
80705054:	f57ff05f 	dmb	sy
80705058:	e3a02001 	mov	r2, #1
8070505c:	e5832100 	str	r2, [r3, #256]	; 0x100
80705060:	e12fff1e 	bx	lr
80705064:	12010000 	.word	0x12010000
80705068:	11200000 	.word	0x11200000

8070506c <hw_dec_uinit>:
8070506c:	f57ff05f 	dmb	sy
80705070:	e59f1034 	ldr	r1, [pc, #52]	; 807050ac <hw_dec_uinit+0x40>
80705074:	e3a02000 	mov	r2, #0
80705078:	e1a03002 	mov	r3, r2
8070507c:	e7c03012 	bfi	r3, r2, #0, #1
80705080:	e7c13092 	bfi	r3, r2, #1, #1
80705084:	e5812100 	str	r2, [r1, #256]	; 0x100
80705088:	f57ff05f 	dmb	sy
8070508c:	e59f201c 	ldr	r2, [pc, #28]	; 807050b0 <hw_dec_uinit+0x44>
80705090:	e5813128 	str	r3, [r1, #296]	; 0x128
80705094:	e5923190 	ldr	r3, [r2, #400]	; 0x190
80705098:	f57ff05f 	dmb	sy
8070509c:	e3c33007 	bic	r3, r3, #7
807050a0:	f57ff05f 	dmb	sy
807050a4:	e5823190 	str	r3, [r2, #400]	; 0x190
807050a8:	e12fff1e 	bx	lr
807050ac:	11200000 	.word	0x11200000
807050b0:	12010000 	.word	0x12010000

807050b4 <start_armboot>:
807050b4:	e92d4030 	push	{r4, r5, lr}
807050b8:	e24dd014 	sub	sp, sp, #20
807050bc:	ebfffe9c 	bl	80704b34 <uart_early_init>
807050c0:	e59f008c 	ldr	r0, [pc, #140]	; 80705154 <start_armboot+0xa0>
807050c4:	ebfffea7 	bl	80704b68 <uart_early_puts>
807050c8:	e59f3088 	ldr	r3, [pc, #136]	; 80705158 <start_armboot+0xa4>
807050cc:	e3a00000 	mov	r0, #0
807050d0:	e59f4084 	ldr	r4, [pc, #132]	; 8070515c <start_armboot+0xa8>
807050d4:	e5830000 	str	r0, [r3]
807050d8:	ebffffca 	bl	80705008 <hw_dec_init>
807050dc:	e59f307c 	ldr	r3, [pc, #124]	; 80705160 <start_armboot+0xac>
807050e0:	e28d100c 	add	r1, sp, #12
807050e4:	e5532004 	ldrb	r2, [r3, #-4]
807050e8:	e5cd200c 	strb	r2, [sp, #12]
807050ec:	e5532003 	ldrb	r2, [r3, #-3]
807050f0:	e5cd200d 	strb	r2, [sp, #13]
807050f4:	e5532002 	ldrb	r2, [r3, #-2]
807050f8:	e5cd200e 	strb	r2, [sp, #14]
807050fc:	e5532001 	ldrb	r2, [r3, #-1]
80705100:	e5cd200f 	strb	r2, [sp, #15]
80705104:	e59f2058 	ldr	r2, [pc, #88]	; 80705164 <start_armboot+0xb0>
80705108:	e0433002 	sub	r3, r3, r2
8070510c:	e58d0000 	str	r0, [sp]
80705110:	e1a00004 	mov	r0, r4
80705114:	ebffff9c 	bl	80704f8c <hw_dec_decompress>
80705118:	e2505000 	subs	r5, r0, #0
8070511c:	1a000009 	bne	80705148 <start_armboot+0x94>
80705120:	e59f0040 	ldr	r0, [pc, #64]	; 80705168 <start_armboot+0xb4>
80705124:	ebfffe8f 	bl	80704b68 <uart_early_puts>
80705128:	ebffffcf 	bl	8070506c <hw_dec_uinit>
8070512c:	ee075f15 	mcr	15, 0, r5, cr7, cr5, {0}
80705130:	ee075fd5 	mcr	15, 0, r5, cr7, cr5, {6}
80705134:	f57ff04f 	dsb	sy
80705138:	f57ff06f 	isb	sy
8070513c:	e12fff34 	blx	r4
80705140:	e28dd014 	add	sp, sp, #20
80705144:	e8bd8030 	pop	{r4, r5, pc}
80705148:	e59f001c 	ldr	r0, [pc, #28]	; 8070516c <start_armboot+0xb8>
8070514c:	ebfffe85 	bl	80704b68 <uart_early_puts>
80705150:	eafffffe 	b	80705150 <start_armboot+0x9c>
80705154:	8074b843 	.word	0x8074b843
80705158:	8074b86c 	.word	0x8074b86c
8070515c:	80800000 	.word	0x80800000
80705160:	8074b78e 	.word	0x8074b78e
80705164:	807052e0 	.word	0x807052e0
80705168:	8074b851 	.word	0x8074b851
8070516c:	8074b855 	.word	0x8074b855

80705170 <hang>:
80705170:	e92d4010 	push	{r4, lr}
80705174:	e59f0004 	ldr	r0, [pc, #4]	; 80705180 <hang+0x10>
80705178:	ebfffe7a 	bl	80704b68 <uart_early_puts>
8070517c:	eafffffe 	b	8070517c <hang+0xc>
80705180:	8074b7fa 	.word	0x8074b7fa

80705184 <do_bad_sync>:
80705184:	e92d4010 	push	{r4, lr}
80705188:	e59f0014 	ldr	r0, [pc, #20]	; 807051a4 <do_bad_sync+0x20>
8070518c:	ebfffe75 	bl	80704b68 <uart_early_puts>
80705190:	e59f0010 	ldr	r0, [pc, #16]	; 807051a8 <do_bad_sync+0x24>
80705194:	ebfffe73 	bl	80704b68 <uart_early_puts>
80705198:	e3a00000 	mov	r0, #0
8070519c:	e8bd4010 	pop	{r4, lr}
807051a0:	ea000047 	b	807052c4 <reset_cpu>
807051a4:	8074b7d5 	.word	0x8074b7d5
807051a8:	8074b7e6 	.word	0x8074b7e6

807051ac <do_sync>:
807051ac:	e92d4010 	push	{r4, lr}
807051b0:	e59f0014 	ldr	r0, [pc, #20]	; 807051cc <do_sync+0x20>
807051b4:	ebfffe6b 	bl	80704b68 <uart_early_puts>
807051b8:	e59f0010 	ldr	r0, [pc, #16]	; 807051d0 <do_sync+0x24>
807051bc:	ebfffe69 	bl	80704b68 <uart_early_puts>
807051c0:	e3a00000 	mov	r0, #0
807051c4:	e8bd4010 	pop	{r4, lr}
807051c8:	ea00003d 	b	807052c4 <reset_cpu>
807051cc:	8074b7d9 	.word	0x8074b7d9
807051d0:	8074b7e6 	.word	0x8074b7e6

807051d4 <do_bad_error>:
807051d4:	e92d4010 	push	{r4, lr}
807051d8:	e59f0014 	ldr	r0, [pc, #20]	; 807051f4 <do_bad_error+0x20>
807051dc:	ebfffe61 	bl	80704b68 <uart_early_puts>
807051e0:	e59f0010 	ldr	r0, [pc, #16]	; 807051f8 <do_bad_error+0x24>
807051e4:	ebfffe5f 	bl	80704b68 <uart_early_puts>
807051e8:	e3a00000 	mov	r0, #0
807051ec:	e8bd4010 	pop	{r4, lr}
807051f0:	ea000033 	b	807052c4 <reset_cpu>
807051f4:	8074b794 	.word	0x8074b794
807051f8:	8074b7e6 	.word	0x8074b7e6

807051fc <do_error>:
807051fc:	e92d4010 	push	{r4, lr}
80705200:	e59f0014 	ldr	r0, [pc, #20]	; 8070521c <do_error+0x20>
80705204:	ebfffe57 	bl	80704b68 <uart_early_puts>
80705208:	e59f0010 	ldr	r0, [pc, #16]	; 80705220 <do_error+0x24>
8070520c:	ebfffe55 	bl	80704b68 <uart_early_puts>
80705210:	e3a00000 	mov	r0, #0
80705214:	e8bd4010 	pop	{r4, lr}
80705218:	ea000029 	b	807052c4 <reset_cpu>
8070521c:	8074b798 	.word	0x8074b798
80705220:	8074b7e6 	.word	0x8074b7e6

80705224 <do_bad_fiq>:
80705224:	e92d4010 	push	{r4, lr}
80705228:	e59f0014 	ldr	r0, [pc, #20]	; 80705244 <do_bad_fiq+0x20>
8070522c:	ebfffe4d 	bl	80704b68 <uart_early_puts>
80705230:	e59f0010 	ldr	r0, [pc, #16]	; 80705248 <do_bad_fiq+0x24>
80705234:	ebfffe4b 	bl	80704b68 <uart_early_puts>
80705238:	e3a00000 	mov	r0, #0
8070523c:	e8bd4010 	pop	{r4, lr}
80705240:	ea00001f 	b	807052c4 <reset_cpu>
80705244:	8074b7a0 	.word	0x8074b7a0
80705248:	8074b7e6 	.word	0x8074b7e6

8070524c <do_bad_irq>:
8070524c:	e92d4010 	push	{r4, lr}
80705250:	e59f0014 	ldr	r0, [pc, #20]	; 8070526c <do_bad_irq+0x20>
80705254:	ebfffe43 	bl	80704b68 <uart_early_puts>
80705258:	e59f0010 	ldr	r0, [pc, #16]	; 80705270 <do_bad_irq+0x24>
8070525c:	ebfffe41 	bl	80704b68 <uart_early_puts>
80705260:	e3a00000 	mov	r0, #0
80705264:	e8bd4010 	pop	{r4, lr}
80705268:	ea000015 	b	807052c4 <reset_cpu>
8070526c:	8074b7bd 	.word	0x8074b7bd
80705270:	8074b7e6 	.word	0x8074b7e6

80705274 <do_fiq>:
80705274:	e92d4010 	push	{r4, lr}
80705278:	e59f0014 	ldr	r0, [pc, #20]	; 80705294 <do_fiq+0x20>
8070527c:	ebfffe39 	bl	80704b68 <uart_early_puts>
80705280:	e59f0010 	ldr	r0, [pc, #16]	; 80705298 <do_fiq+0x24>
80705284:	ebfffe37 	bl	80704b68 <uart_early_puts>
80705288:	e3a00000 	mov	r0, #0
8070528c:	e8bd4010 	pop	{r4, lr}
80705290:	ea00000b 	b	807052c4 <reset_cpu>
80705294:	8074b7a4 	.word	0x8074b7a4
80705298:	8074b7e6 	.word	0x8074b7e6

8070529c <do_irq>:
8070529c:	e92d4010 	push	{r4, lr}
807052a0:	e59f0014 	ldr	r0, [pc, #20]	; 807052bc <do_irq+0x20>
807052a4:	ebfffe2f 	bl	80704b68 <uart_early_puts>
807052a8:	e59f0010 	ldr	r0, [pc, #16]	; 807052c0 <do_irq+0x24>
807052ac:	ebfffe2d 	bl	80704b68 <uart_early_puts>
807052b0:	e3a00000 	mov	r0, #0
807052b4:	e8bd4010 	pop	{r4, lr}
807052b8:	ea000001 	b	807052c4 <reset_cpu>
807052bc:	8074b7c1 	.word	0x8074b7c1
807052c0:	8074b7e6 	.word	0x8074b7e6

807052c4 <reset_cpu>:
807052c4:	e59f100c 	ldr	r1, [pc, #12]	; 807052d8 <rstctl>
807052c8:	e3a03002 	mov	r3, #2
807052cc:	e5813000 	str	r3, [r1]
807052d0:	e1a00000 	nop			; (mov r0, r0)

807052d4 <_loop_forever>:
807052d4:	eafffffe 	b	807052d4 <_loop_forever>

807052d8 <rstctl>:
807052d8:	12020004 	.word	0x12020004
