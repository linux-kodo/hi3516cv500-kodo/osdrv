1.开源源码包见附件，结构如下所示
|── readme.txt
|── build.sh
|---- CASignTool
|-----|-------bin
|-------------|----CASignTool_m32
|-------------|----CASignTool_m64
|-------------openssl
|-------------|----m32
|-------------|----m64
|-------------secure
|-------------|----m32
|-------------|----m64
|-------------src
|-------------|----Linux_CA_Boot_Sign_A.c
|-------------Makefile
|------libCASign
|------|------bin
|------|------|----bin_CASignTool
|------|------opensrc
|------|------openssl-1.0.2n
|------|------src
|------|------|-----adapter.c
|------|------|-----commonmethod.c
|------|------|-----commonmethod.h
|------|------|-----encrypt.c
|------|------|-----encrypt.h
|------|------|-----libCASign.c
|------|------|-----libCASign.h
|------|------|-----priv_signTool.h
|------|------|-----sha2.c
|------|------|-----sha2.h
|------|------|-----signHi3519V101.c
|------|------|-----signHi3519V101.h
|------|------|-----signHi3556AV100.c
|------|------|-----signHi3556AV100.h
|------|------|-----type.h
|------|------|-----verifyboot.c
|------|------|-----verifyboot.h
|------|------Makefile

说明：
CASignTool 为生成可执行文件的源文件，生成可执行文件的路径在./CASignTool/bin, CASignTool_m32 和 CASignTool_m64 分别为32位和64位的执行文件
libCASign 为生成的静态库源文件，生成的静态库路径在./bin/bin_CASignTool, libCASign_m32.a 和 libCASign_m64.a 分别为32位和64位的静态库
签名安全os的相关代码在signHi3519V101.c和signHi3556AV100.c这两个文件中，可依照芯片类型对相应的代码进行修改。

2. 编译
2.1. 编译64位
source build.sh m64

2.2. 编译32位
source build.sh m32



