
#ifndef SIGNHI3519V101
#define SIGNHI3519V101

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <stdbool.h>
#include "type.h"
#include "openssl/adapter.h"
#include "priv_signTool.h"
#include <strings.h>

typedef struct signHi3519V101Param
{
    char pRSAPubKeyPath[PATH_MAX];
    char pRSAPriKeyPath[PATH_MAX]; 
    char pAESKeyPath[PATH_MAX];
    char pDrrInitPath[PATH_MAX]; 
    char pUBootPath[PATH_MAX];
    char pOutputFilePath[PATH_MAX];
    unsigned int RSAIOType;    
    unsigned int no_enc;
}SignHi3519V101Param;

int ParseSignHi3519V101Param(char **argv, int argc, SignHi3519V101Param *pSignHi3519V101Params);
bool SignCryptoHi3519V101(SignHi3519V101Param *pParams);
bool ProcessKeyArea(char *pRSAPubKeyPath, char *pAESKeyPath, char *pOutputFilePath, RSAIOType keyIOtype, int no_enc_flag);
bool ProcessSignArea(char *pRSAPriKeyPath, char *pBinaryPath, char *pOutputFilePath, RSAIOType keytype);

#endif // SIGNHI3519V101
