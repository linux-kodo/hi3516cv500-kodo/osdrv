#ifndef __LIBCASIGN
#define __LIBCASIGN

#ifdef __cplusplus
extern "C" {
#endif

#include "commonmethod.h"
//#include "encrypt.h"
#include "signHi3519V101.h"
#include "signHi3556AV100.h"

typedef enum{
    BranchSignBootImage,
    BranchMergeBootImage,
    BranchSignNonBootImageCommon,
    BranchSignNonBootImageSpecial,
    BranchCryptoImage,
    BranchSignNonBootImageMA,
    BranchSignNonBootImageSpecialDPT,
    BranchVerifyBootImage,
    BranchCreateRSA,
    BranchSignHi3519V101,
    BranchSignNonBootImageMUStep1,
    BranchSignNonBootImageMUStep2,
    BranchPrintMasks,
    BranchEncBoot,
    BranchSignTA,
    BranchTransKey,
    BranchMergeImage,
    BranchCreateSM,
    BranchSignHSL,
    BranchSignHi3556AV100,
    BranchSignCASImage,
}Branchs;

int CreateRSAKeyWithDefaultE(char* pDestPath);
int CreateRSAKey(char *pEValue, char *destPath);

int SignNonBOOTImage_Crypto_cfg(const char* strConfigFile, CryptoParam* pCryptoParams);

int SignHi3519V101(SignHi3519V101Param *pSignHi3519V101Params);

void SetBootChipsetType(char* pType);

#ifdef __cplusplus
}
#endif

#endif // __LIBCASIGN
