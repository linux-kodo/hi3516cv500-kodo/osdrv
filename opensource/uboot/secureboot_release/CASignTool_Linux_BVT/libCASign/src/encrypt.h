#ifndef __ENCRYPT_H__
#define __ENCRYPT_H__

#include "type.h"
#include "openssl/adapter.h"
#include "priv_signTool.h"

typedef enum tagAlgorithm
{
    AES = 0x0,
    TDES,
    SHA1_ALG,
    SHA256_ALG,
    RSA_ALG,
    RSA_VERIFY,
    SM2_SIGN,
    SM4,    
}ALGORITHM_EN;

typedef enum tagMode
{
    CBC = 0x0,
    ECB,
    CTR,
}MODE_EN;

typedef enum tagCrypt
{
    ENCRYPT_OP = 0x0,
    DECRYPT_OP,
}CRYPT_EN;

#define BUFFER_SIZE (64)
#define MAX_IMAGE_COUNT (64)
#define MAX_BOOT_LEN 4

typedef struct crypto_config_parameter_st
{
    unsigned char External_private_key_file[PATH_MAX];
    unsigned char Algorithm[BUFFER_SIZE];
    unsigned char Mode[BUFFER_SIZE];
    unsigned char OperationType[BUFFER_SIZE];
    unsigned char Key[BUFFER_SIZE * 2 + 1];
    unsigned char IV[BUFFER_SIZE * 2 + 1];
    unsigned char ImageNumber[BUFFER_SIZE];
    unsigned char InputFileDir[PATH_MAX];
    unsigned char OutputFileDir[PATH_MAX];
    unsigned char ifEncryptNonBoot[MAX_BOOT_LEN];
}CRYPTO_CONFIG_PARAMETER_ST;

int Sign_NonBOOTImage_Crypto(const char* strConfigFile, CryptoParam* pCryptoParams);

int GetCryptoOperation(char *strCryptMode, CRYPT_EN *p);
int PaserCryptoConfigParameter(const char *strConfigFile, CRYPTO_CONFIG_PARAMETER_ST *pConfigPara);

int CryptoSHA1_ALG(char *pDestPath, char *pSrcPath);
int CryptoSHA256_ALG(char *pDestPath, char *pSrcPath);
int CryptoSM2VERIFY_ALG(char *SMKeyfilepath, char *pSrcFilePath, char *signatureFilePath);
int CryptoSM2SIGN_ALG(char *SMKeyfilepath, char *pDestPath, char *pSrcPath);
int CryptoRSA_ALG(char *rsakeyfilepath, char *pDestPath, char *pSrcPath);
int CryptoAES_TDES(char *pDestPath, char *pSrcPath, unsigned int inputType, unsigned int outputType, CRYPTO_CONFIG_PARAMETER_ST* pstConfigPara);
BOOL getAlgorithm(const char *algorithm, ALGORITHM_EN *encAlgorithm, int *alignSize);
BOOL getCryptoParam(CRYPTO_CONFIG_PARAMETER_ST* stConfigPara, MODE_EN *stMode, CRYPT_EN *stOPeration, unsigned char *ucKey, unsigned char *ucIV);
BOOL setBlockLength( ALGORITHM_EN stALGTHM, UINT *ucBlockLength);
BOOL readInputFile(UINT inputType, UCHAR **ptxtBuf, UINT *uwFileLength, char *pSrcPath, UCHAR **pbinBuf, FILE **fpInFile);
void crypto(ALGORITHM_EN stALGTHM, CRYPT_EN stOPeration, MODE_EN stMode, UCHAR *ucKey, 
                UCHAR* ucTmpIV, UCHAR *pbinBuf, UCHAR *pbinOutputBuf, unsigned long uwDataLength);
BOOL writeOutputFile(UINT outputType, FILE **fpOutFile,  char *pDestPath, UCHAR *pbinOutputBuf, UCHAR **ptxtOutputBuf, int uwFileLength);
    
bool ParseTxtRSAKeyFile(const char *strKeyFilePath, rsa_context *rsa, RSAKeyType keyType);

int DecodePEMKey(FILE *pKeyFile, rsa_context *rsa, RSAKeyType RSAType);
int DecodeBINKey(FILE *pRSAPubKeyFile, rsa_context rsa, RSAKeyType KeyType);


int ParseBinRSAKey(unsigned char* PublicKeyDecodeBinBuf, rsa_context *rsa, 
                                   size_t InputLength, RSAKeyType KeyType);
int WriteTxtRSAKey(rsa_context rsa, int radix, RSAKeyType keyType, FILE * fOut, const char* strFileName);

int ParseTxtRSAKey(rsa_context rsa, int radix, RSAKeyType keyType, FILE * fin, const char* strFileName);

#endif
