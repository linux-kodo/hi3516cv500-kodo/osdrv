#include "signHi3556AV100.h"
#include "libCASign.h"
const unsigned int magic = 0x4253424d;

unsigned char g_FlashProtectingKey[32] =
    {0x50, 0xDB, 0x86, 0xC5, 0x92, 0xC5, 0x2F, 0x0C, 0x43, 0x6C, 0xCA, 0x6F, 0x2F, 0xFE, 0xCA, 0xF5, 
     0x4A, 0x96, 0xAE, 0x01, 0x3F, 0xC6, 0x0E, 0x20, 0x5E, 0x9D, 0xA4, 0xC9, 0xD5, 0xAD, 0x9B, 0x99};

#define HI_PRINT printf
#define HI_PRINT_HEX(name, str, len) \
{\
    HI_U32 _i = 0;\
    HI_U32 _val; \
    HI_U8 *_str = (HI_U8*)str;\
    if (_str != HI_NULL) \
    {\
        HI_PRINT("[%s]:\n", name);\
        for ( _i = 0 ; _i < (len); _i+=4 )\
        {\
            _val  = *((_str)+_i+3);\
            _val |= *((_str)+_i+2) << 8;\
            _val |= *((_str)+_i+1) << 16;\
            _val |= *((_str)+_i+0) << 24;\
            if( (_i % 16 == 0) && (_i != 0)) HI_PRINT("\n");\
            HI_PRINT("%x ", _val);\
        }\
        HI_PRINT("\n");\
    }\
}


//19 cfgFile RSAPubKeyFilePath RSAPriKeyFilePath DrrInitFilePath UBootFilePath OutputFilePath
int ParseSignParam(char **argv, int argc, SignParam *signParams)
{
    if ((NULL == argv)||(argc <= 0))
    {
        return -1;
    }

    unsigned int ArgvLength;
    unsigned int ParamLength;

    if(0 != ParseHi3556AV100ConfigFile(argv[2], signParams))
    {
        printf("Failed to parse config file!\n");
        return -1;
    }
    
    ArgvLength = strlen(argv[3]);
    ParamLength = sizeof(signParams->pRSAPubKeyPath);
    STRNCPY_S(signParams->pRSAPubKeyPath, ParamLength, argv[3], ArgvLength, "parsing RSAPubKeyFilePath", return -1;);

    ArgvLength = strlen(argv[4]);
    ParamLength = sizeof(signParams->pRSAPriKeyPath);
    STRNCPY_S(signParams->pRSAPriKeyPath, ParamLength, argv[4], ArgvLength, "parsing RSAPriKeyFilePath", return -1;);

    ArgvLength = strlen(argv[5]);
    ParamLength = sizeof(signParams->pDrrInitPath);
    STRNCPY_S(signParams->pDrrInitPath, ParamLength, argv[5], ArgvLength, "parsing DrrInitFilePath", return -1;);

    ArgvLength =  strlen(argv[6]);
    ParamLength = sizeof(signParams->pUBootPath);
    STRNCPY_S(signParams->pUBootPath, ParamLength, argv[6], ArgvLength, "parsing UBootFilePath", return -1;);

    ArgvLength = strlen(argv[7]);
    ParamLength = sizeof(signParams->pOutputFilePath);
    STRNCPY_S(signParams->pOutputFilePath, ParamLength, argv[7], ArgvLength, "parsing OutputFilePath", return -1;);  
    return 0;   
}

//Hi3556AV100
int ParseHi3556AV100ConfigFile(char *strConfigFile, SignParam *signParams)
{
    if(NULL == strConfigFile || NULL == signParams)
    {
        printf("Null input params!\n");
        return -1;
    }
    FILE *fpConfigFile = NULL;
    char tmp[PATH_MAX] = {0};
    //it might start with '0x' or '0X'
    //it should contain the '\0' in the end, otherwise, the error will be found in strncpy_s
    char txtAESKey[AES_ENCRYPT_KEY_SIZE*2*2 + 2 + 1] = {0};  //3516CV500 may have 64 characters
    char txtIV[AES_ENCRYPT_KEY_SIZE*2 + 2 + 1] = {0};
    char type[2] = {0};
    char chipType[32];
    
    int binaryKeyLength = (Hi3556AV100 == g_enChipsetType) ? 16 : 32;

    if(!OpenMyFile(&fpConfigFile, strConfigFile, "r+"))
    {
        printf("Failed to open config file!\n");
        return -1;
    }
    do
    {
        MEMSET_S(tmp, sizeof(tmp), 0, sizeof(tmp), "parsing config file", SAFE_CLOSE(fpConfigFile); return -1;);
        if(NULL == fgets(tmp, PATH_MAX, fpConfigFile))
        {
           if(ferror(fpConfigFile))
           {
               printf("Failed to get content of file.\n");
               SAFE_CLOSE(fpConfigFile);
               return -1;
           }
        }
        if(!legalInput(tmp, sizeof(tmp)))
        {
            SAFE_CLOSE(fpConfigFile);
            return -1;
        }
        
        ParseFile(tmp, "KEY", txtAESKey, sizeof(txtAESKey));
        ParseFile(tmp, "IV", txtIV, sizeof(txtIV));
        ParseFile(tmp, "Type", type, sizeof(type));
        ParseFile(tmp, "ChipType", chipType, sizeof(chipType));
    }while(!feof(fpConfigFile));
    SAFE_CLOSE(fpConfigFile);

    if(strlen(chipType) > 0)
    {
        SetBootChipsetType(chipType);
    }
    else
    {
        g_enChipsetType = Hi3556AV100;
    }

    if(*txtAESKey != '\0')
    {
        if(!TxtKeyToBinKey(txtAESKey, signParams->binaryKey, binaryKeyLength))
        {
            printf("Failed to transform txt key to binary key!\n");
            return -1;
        }
    }
    else
    {
        signParams->no_enc = 1;
    }
    
    if(*txtIV != '\0')
    {
        if(!AESTxtKeyToBinaryKey(txtIV, signParams->binaryIV))
        {
            printf("Failed to transform txt iv to binary iv!\n");
            return -1;
        }
    }
    signParams->RSAIOType = strtoul(type, NULL, 0);
    return 0;
}

int SignHi3556AV100(SignParam *signParam)
{
    if(NULL == signParam)
    {
        printf("Null input!\n");
        return -1;
    }
    FILE *fpFinalBootFile = NULL;
    unsigned char *totalBuf = NULL;
    long length = 0;
    int ret = -1;

    //bool encFlag = (Hi3516CV500 == g_enChipsetType) ? false : true; 
   
    //hi3516cv500 series chip:do not encrypt ddr_init!
    bool encFlag = false;

    if(!OpenMyFile(&fpFinalBootFile, signParam->pOutputFilePath, "wb+"))
    {
        printf("Failed to open or create final boot file %s.\n", signParam->pOutputFilePath);
        goto ScopeRelease;
    }
    SAFE_CLOSE(fpFinalBootFile);
      //process KeyArea
    if(!ProcessHi3556AV100KeyArea(signParam))
    {
        printf("Failed to process key area!\n");
        goto ScopeRelease;
    }

    char binaryIVstep1[16] = {0};
    MEMCPY_S(binaryIVstep1, sizeof(binaryIVstep1), signParam->binaryIV, sizeof(binaryIVstep1), "copying iv", goto ScopeRelease;);

    if(!ProcessHi3556AV100SignArea(signParam->pRSAPriKeyPath, signParam->pDrrInitPath, signParam, binaryIVstep1, encFlag))
    {
        printf("Failed to process ddr_init area!\n");
        goto ScopeRelease;
    }

    MEMCPY_S(binaryIVstep1, sizeof(binaryIVstep1), signParam->binaryIV, sizeof(binaryIVstep1), "copying iv", goto ScopeRelease;);
    
    long curOffset = GetSizeFromPath(signParam->pOutputFilePath); 
    //without 4 byte magic, without 4byte sumlength, with uboot.bin length, 
    //curOffset is without uboot.bin length
    if (curOffset <= 0) 
    {
        goto ScopeRelease;
    }
    else if (curOffset > MAX_HEAD_DDRINIT_SIZE) 
    {
        printf("curOffset: %ld\n", curOffset);
        printf("Sum of key size and ddrInit size is greater than %d.\n", MAX_HEAD_DDRINIT_SIZE);
        goto ScopeRelease;
    }

    if(!ProcessHi3556AV100SignArea(signParam->pRSAPriKeyPath, signParam->pUBootPath, signParam, binaryIVstep1, true))
    {
        printf("Failed to process uboot area!\n");
        goto ScopeRelease;
    }

    length = GetSizeFromPath(signParam->pOutputFilePath);
    SAFE_MALLOC(unsigned char *, totalBuf, (unsigned int)length, "", return -1;);

    if(!OpenMyFile(&fpFinalBootFile, signParam->pOutputFilePath, "rb+wb"))
    {
        printf("Failed to open or create final boot file %s.\n", signParam->pOutputFilePath);
        goto ScopeRelease;
    }
    
    if(1 != fread(totalBuf, length, 1, fpFinalBootFile)) 
    {
        printf("Failed to read buffer!\n");
        goto ScopeRelease;
    }
    if(0 != fseek(fpFinalBootFile, 0, SEEK_SET)) 
    {
        printf("Failed to seek file!\n");
        goto ScopeRelease;
    }

    //write magic
    if(1 != fwrite(&magic, sizeof(unsigned int), 1, fpFinalBootFile))
    {  
         printf("Failed to write file!\n");
         goto ScopeRelease;
    }

    //write sum length
    length = length + 8;
    if(1 != fwrite(&length, sizeof(unsigned int), 1, fpFinalBootFile))
    {
         printf("Failed to write file!\n");
         goto ScopeRelease;
    }
    
    if(1 != fwrite(totalBuf, length - 8, 1, fpFinalBootFile))
    {
         printf("Failed to write file!\n");
         goto ScopeRelease;
    }
    
    ret = 0;
ScopeRelease:  
    
    SAFE_CLOSE(fpFinalBootFile);
    SAFE_FREE(totalBuf);
    return ret;
}

bool ProcessHi3556AV100KeyArea(SignParam *signParam)
{
    if(NULL == signParam)
    {
        printf("Null input!\n");
        return -1;
    }
    FILE *pRSAPubKeyFile = NULL;
    rsa_context rsa = NULL;
    unsigned char *aRSAPubKeyData = NULL;
    FILE *pOutputFile = NULL;
    unsigned char *aOutputDataBuf = NULL;

    bool ret = false;
    if(!OpenMyFile(&pRSAPubKeyFile, signParam->pRSAPubKeyPath, "r+")) 
    {
        printf("Failed to open key file %s!\n", signParam->pRSAPubKeyPath);
        goto ScopeRelease;
    }
    
    rsa_init(&rsa, RSA_PKCS_V15, 0, NULL, NULL);   

    if (TypePEM == signParam->RSAIOType)
    {
        //Base64decode
        if(-1 == DecodePEMKey(pRSAPubKeyFile, &rsa, TypePublic))
        {
            printf("%s!\n", PrintMsg[ErrorFailedToParsePEMFile]);
            goto ScopeRelease;
        }
        
    }
    else if(TypeBIN == signParam->RSAIOType)
    {
        if(-1 == DecodeBINKey(pRSAPubKeyFile, rsa, TypePublic))
        {
            printf("%s!\n", PrintMsg[ErrorFailedToParseBinFile]);
            goto ScopeRelease;
        }
    }
    else 
    {
        if(-1 == ParseTxtRSAKey(rsa, HEX_RADIX, TypePublic, pRSAPubKeyFile, signParam->pRSAPubKeyPath))
        {
            goto ScopeRelease;
        }
    }

    int rsaLen = BN_num_bytes(rsa->n);
    int keyBinSize = rsaLen * 2;

    if ( keyBinSize <= 0)
    {
        printf("The length of rsaLen is less or equal to zero!\n");
        goto ScopeRelease;
    }

    SAFE_MALLOC(unsigned char*, aRSAPubKeyData, keyBinSize, "processing key area", goto ScopeRelease;);
    MEMSET_S(aRSAPubKeyData, keyBinSize, 0, keyBinSize, "processing key area", goto ScopeRelease;);

    if(0 != MpiWriteBuf(rsa, rsaLen, aRSAPubKeyData))
    {
        printf("Failed to write Buf.\n");
        goto ScopeRelease;
    }

    // output
    int outputBufSize = RSA_N_LEN + RSA_E1_LEN + keyBinSize;
    SAFE_MALLOC(unsigned char*, aOutputDataBuf, outputBufSize, "", goto ScopeRelease;);
    MEMSET_S(aOutputDataBuf, outputBufSize, 0, outputBufSize, "processing key area", goto ScopeRelease;);

    ((unsigned int*)aOutputDataBuf)[0] = rsaLen;
    ((unsigned int*)aOutputDataBuf)[1] = rsaLen;
    MEMCPY_S(aOutputDataBuf + 8, outputBufSize - 8, aRSAPubKeyData, keyBinSize, "processing key area", goto ScopeRelease;);

    if(!OpenMyFile(&pOutputFile, signParam->pOutputFilePath, "ab+"))
    {
         printf("Failed to open file %s!\n", signParam->pOutputFilePath);
         goto ScopeRelease;
    }

    if(1 != fwrite(aOutputDataBuf, outputBufSize, 1, pOutputFile)) 
    {
        printf("Failed to write file %s!\n", signParam->pOutputFilePath);
        goto ScopeRelease;
    }
    //write iv
    if(1 != fwrite(signParam->binaryIV, AES_ENCRYPT_KEY_SIZE, 1, pOutputFile))
    {
        printf("Failed to write file %s!\n", signParam->pOutputFilePath);
        goto ScopeRelease;
    }
    ret = true;
ScopeRelease:
    SAFE_CLOSE(pRSAPubKeyFile);
    SAFE_FREE(aRSAPubKeyData);
    SAFE_CLOSE(pOutputFile);
    SAFE_FREE(aOutputDataBuf);
    rsa_free(&rsa);
    return ret;
}

bool ProcessHi3556AV100SignArea(char *pRSAPriKeyPath, char *pBinaryPath, SignParam *signParam, char *binaryIV, bool encFlag)
{
    BOOL bTempRet = (NULL == pRSAPriKeyPath || NULL == pBinaryPath || NULL == signParam);
    if (bTempRet)
    {
        return false;
    }

    bool ret = true;
    FILE *pInputFile = NULL;
    long fileLength = 0;
    unsigned int alignedFileLength = 0;
    unsigned char *pInputFileData = NULL;
    FILE *fileRsaKey = NULL;
    FILE *pOutputFile = NULL;
    unsigned char *rsaBuf = NULL;
    rsa_context rsa = NULL;
    unsigned char hash[32] = {0};
    char absRSAPrivKeyPath[PATH_MAX] = {0};
    unsigned char *pTotalBuf = NULL;
    unsigned char *pCryptoBuf = NULL;
    unsigned int totalLength = 0;

    // *******************************read src file data
    if(!OpenMyFile(&pInputFile, pBinaryPath, "rb")) 
    {
        printf("Failed to open file %s!\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }

    fileLength = GetSizeFromPointer(pInputFile);
    if(fileLength <= 0) 
    {
        printf("Failed to get size of file %s.\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }

    //16Bytes aligned, aes-cbc
    alignedFileLength = (fileLength + 16 - 1) / 16 * 16; 

    pInputFileData = (unsigned char*)malloc(alignedFileLength);
    if(NULL == pInputFileData) 
    {
        printf("Failed to malloc memory for file %s!\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }
    MEMSET_S(pInputFileData, alignedFileLength, 0, alignedFileLength, "", ret = false; goto ScopeRelease;);

    if(1 != fread(pInputFileData, fileLength, 1, pInputFile))
    {
        printf("Failed to read file %s!\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }
    
    // *******************************read rsa key
    if(!OpenMyFile(&fileRsaKey, pRSAPriKeyPath, "r"))
    {
        printf("Failed to open file %s!\n", pRSAPriKeyPath);
        ret = false;
        goto ScopeRelease;
    }
    
    rsa_init(&rsa, RSA_PKCS_V15, 0, NULL, NULL);

    if(TypePEM == signParam->RSAIOType)
    {
        if(-1 == DecodePEMKey(fileRsaKey, &rsa, TypePrivate))
        {
            printf("%s (private key)!\n", PrintMsg[ErrorFailedToParsePEMFile]);
            goto ScopeRelease;
        }
    }
    else if(TypeBIN == signParam->RSAIOType)
    {
        if(-1 == DecodeBINKey(fileRsaKey, rsa, TypePrivate))
        {
            printf("%s (private key)!\n", PrintMsg[ErrorFailedToParseBinFile]);
            goto ScopeRelease;
        }
    }
    else
    {
        if(-1 == ParseTxtRSAKey(rsa, HEX_RADIX, TypePrivate, fileRsaKey, pRSAPriKeyPath))
        {
            ret = false;
            goto ScopeRelease;
        }
    }
    // sign
    unsigned int rsaLen = BN_num_bytes(rsa->n);   //The length of signature depends on the length of n
    rsaBuf = (unsigned char*)malloc(rsaLen);
    if(NULL == rsaBuf)
    {
        printf("Failed to malloc %u memory for rsa Buffer\n", rsaLen);
        ret = false;
        goto ScopeRelease;
    }

    sha2(pInputFileData, alignedFileLength, hash, 0);
    
    if(rsa_pkcs_oaep_sign(&rsa, rsaLen, hash, rsaBuf) != 0)
    {
        printf("Failed to make the sign!\n");
        ret = false;
        goto ScopeRelease;
    }

    
    totalLength = rsaLen + alignedFileLength;
    pTotalBuf = (unsigned char *) malloc (totalLength);
    if(NULL == pTotalBuf)
    {
        printf("Failed to malloc!\n");
        ret = false; 
        goto ScopeRelease;
    }
    
    pCryptoBuf = (unsigned char *) malloc (totalLength);
    if(NULL == pCryptoBuf)
    {
        printf("Failed to malloc!\n");
        ret = false; 
        goto ScopeRelease;
    }
    
    //SAFE_MALLOC(unsigned char *, pTotalBuf, totalLength, "", ret = false; goto ScopeRelease;);
    //SAFE_MALLOC(unsigned char *, pCryptoBuf, totalLength, "", ret = false; goto ScopeRelease;);
    MEMSET_S(pTotalBuf, totalLength, 0, totalLength, "", ret = false; goto ScopeRelease;);
    MEMSET_S(pCryptoBuf, totalLength, 0, totalLength, "", ret = false; goto ScopeRelease;);
    MEMCPY_S(pTotalBuf, totalLength, pInputFileData, alignedFileLength, "", ret = false; goto ScopeRelease;);
    MEMCPY_S(pTotalBuf + alignedFileLength, rsaLen, rsaBuf, rsaLen, "", ret = false; goto ScopeRelease;);

    int keyBitLength = (Hi3516CV500 == g_enChipsetType) ?  2 * KEY_BIT_LEN : KEY_BIT_LEN;
    int KeyByteLength =  (Hi3516CV500 == g_enChipsetType) ? 32:16;

    int i = 0;
    if(0 == signParam->no_enc && encFlag)
    {
        // aes ecb decrypt
        aes_context ctx;
        char binaryKey[32] = {0};
        aes_setkey_dec(&ctx, signParam->binaryKey, keyBitLength);

        for (i = 0; i < KeyByteLength / 16; i++)
        {
           aes_crypt_ecb(&ctx, AES_DECRYPT, g_FlashProtectingKey + 16 * i, binaryKey + 16 * i);
        }

        //aes cbc encrypt

        aes_setkey_enc(&ctx, binaryKey, keyBitLength); //128 or 258   
        aes_crypt_cbc(&ctx, AES_ENCRYPT, totalLength, binaryIV, pTotalBuf, pCryptoBuf);

        MEMCPY_S(pTotalBuf, totalLength, pCryptoBuf, totalLength, "", ret = false; goto ScopeRelease;);
    }

    // write to file
    if(!OpenMyFile(&pOutputFile, signParam->pOutputFilePath, "ab"))
    {
        printf("Failed to open file %s!\n", signParam->pOutputFilePath);
        ret = false;
        goto ScopeRelease;
    }
    bTempRet = (1 != fwrite(&alignedFileLength, sizeof(unsigned int), 1, pOutputFile))
        || (1 != fwrite(pTotalBuf, totalLength, 1, pOutputFile));
            
    if (bTempRet)
    {
        printf("Failed to write file %s!\n", signParam->pOutputFilePath);
        ret = false;
        goto ScopeRelease;
    }
    
ScopeRelease:
    rsa_free(&rsa);
    SAFE_FREE(pInputFileData);
    SAFE_CLOSE(pInputFile);
    SAFE_FREE(rsaBuf);
    SAFE_FREE(pTotalBuf);
    SAFE_FREE(pCryptoBuf);
    SAFE_CLOSE(pOutputFile);
    SAFE_CLOSE(fileRsaKey);   
    return ret;   
}

