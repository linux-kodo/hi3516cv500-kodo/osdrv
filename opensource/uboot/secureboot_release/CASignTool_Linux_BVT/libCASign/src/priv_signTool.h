#ifndef _PRIV_SIGNTOOL_H
#define _PRIV_SIGNTOOL_H

//#include <windows.h>
//#include <securec.h>
#include "type.h"
#include "priv_signTool.h"

#include  <limits.h>

#define FILE_PATH_LENGTH (300)
#define HASH_BUFFER_LEN (16)
#define HEX_LEN (10)
#define CHIPNAME_LEN (16)
#define WORD_LEN (0x20)
#define FILES_NUMBER (32)
#define HEX_RADIX (0x10)
#define BOOT_NOT_ENCRYPT_FLAG_IN_PARAM (0x3C7896E1)
#define HASH_LEN (0x20)
#define MSID_LEN (0x20)
#define VERSIONID_LEM (0x20)
#define MASK_LEN (0x20)

#define MAXTYPELENGTH  12
#define PARAMNUMFORPRIV 8
#define PARAMNUMFORPUB 2
#define MAX_INT_LEN (4)

#define EXT_PARAM_AREA_FLAG (0x83855248)

#define OTHER_ERROR (254)
#define OUTPUT_DIR_NOT_EXIST (3)
#define INPUT_FILE_NOT_EXIST (2)
#define INPUT_DIR_NOT_EXIST (1)

#define KEY_BIT_LEN (128)
#define DEFAULT_SM_ID_LEN (16)

#define CR_ASCII 0x0d   // \r
#define LF_ASCII 0x0a   // \n

#define RSA_N_LEN (4)
#define RSA_E1_LEN (4)
#define KEY_LEN 16

#define KEYRIGHTSFLAG 0x3c78962d

#define KEYRIGHTSOFFSET 0x210 //the same to hsl and key area


#define MAX_HEAD_DDRINIT_SIZE 0x8000
typedef enum rsaType
{
    TypeTXT = 0,
    TypePEM,
    TypeBIN,
}RSAIOType;

typedef enum encMethod
{
    TypeAES_CBC = 0,
    TypeSM4,
}ENCMethod;

typedef enum sigMethod
{
    TypeSHA256 = 0,
    TypeSM2,
}SIGMethod;

#ifndef SAFE_CLOSE
#define SAFE_CLOSE(pFile) \
do \
{ \
    if (pFile) \
    { \
        fclose(pFile); \
        pFile = NULL; \
    } \
} while (0);
#endif // SAFE_DELETE


#ifndef SAFE_DELETE
#define SAFE_DELETE(pObject) \
do \
{ \
    if (pObject) \
    { \
        delete(pObject); \
        pObject = NULL; \
    } \
} while (0);
#endif // SAFE_DELETE

#ifndef SAFE_FREE
#define SAFE_FREE(pMemory) \
do \
{ \
    if (pMemory) \
    { \
        free(pMemory); \
        pMemory = NULL; \
    } \
} while (0);
#endif // SAFE_FREE

#ifndef SAFE_MALLOC
#define SAFE_MALLOC(type, buffer, bufflen, hint, operation) \
do \
{\
    buffer = (type) malloc(bufflen); \
    if(NULL == buffer) \
    { \
        printf("Failed to malloc %u memory at %s!\n", bufflen, hint); \
        {operation}; \
    } \
} while (0);
#endif

#ifndef MEMCPY_S
#define MEMCPY_S(dest, destMax, src, count, hint, operation) \
do \
{ \
    if(0 != memcpy_s(dest, destMax, src, count)) \
    { \
        printf("Failed at memcpy at %s!\n", hint); \
        {operation}\
    } \
} while (0);
#endif

#ifndef MEMSET_S
#define MEMSET_S(dest, destMax, c, count, hint, operation) \
do \
{ \
    if(0 != memset_s(dest, destMax, c, count)) \
    { \
        printf("Failed at memset at %s!\n", hint); \
        {operation} \
    } \
} while (0);
#endif

#ifndef STRNCPY_S
#define STRNCPY_S(strDest, destMax, strSrc, count, hint, operation) \
do \
{ \
    if(0 != strncpy_s(strDest, destMax, strSrc, count)) \
    { \
        printf("Failed at strncpy at %s!\n", hint); \
        {operation} \
    } \
} while (0);
#endif

#ifndef STRNCAT_S
#define STRNCAT_S(strDest, destMax, strSrc, count, hint, operation) \
do \
{ \
    if(0 != strncat_s(strDest, destMax, strSrc, count)) \
    { \
        printf("Failed to strncat at %s!\n", hint); \
        {operation} \
    } \
} while (0);
#endif

#ifndef SNPRINTF_S
#define SNPRINTF_S(strDest, destMax, count, format, param, hint, operation) \
do \
{ \
    if(-1 == snprintf_s(strDest, destMax, count, format, param)) \
    { \
        printf("Failed to snprintf at %s!\n", hint); \
        {operation} \
    } \
} while(0);
#endif

#ifndef SPRINTF_S
#define SPRINTF_S(strDest, destMax, format, param, hint, operation) \
do \
{ \
    if(-1 == sprintf_s(strDest, destMax, format, param)) \
    { \
        printf("Failed to sprintf at %s!\n", hint); \
        {operation} \
    } \
} while (0); 
#endif

#ifndef SSCANF_S
#define SSCANF_S(buffer, format, param, hint, operation) \
do \
{ \
    if(strlen(buffer) > 0)\
    { \
        if(-1 == sscanf_s(buffer, format, param)) \
        { \
            printf("Failed to scanf at %s!\n", hint); \
            {operation} \
        } \
    } \
} while (0);
#endif
#define COMPARE_LEN 4

typedef enum rsaKeyType
{
    TypePrivate = 1,
    TypePublic, 
}RSAKeyType;

#define TYPETXT "TypeTXT"
#define TYPEPEM "TypePEM"
#define TYPEBIN "TypePKCSBIN"
#define BEGINPUBLICKEY "-----BEGIN PUBLIC KEY-----"
#define ENDPUBLICKEY "-----END PUBLIC KEY-----"
#define BEGINPRIVATEKLEY "-----BEGIN RSA PRIVATE KEY-----"
#define ENDPRIVATEKLEY "-----END RSA PRIVATE KEY-----"


typedef struct commonParams{    
    char keyFilePath[PATH_MAX];
    char srcFilePath[PATH_MAX];
    char destFilePath[PATH_MAX];
    char HashMode[WORD_LEN];
    char sectionSize[HEX_LEN];  
    char aIfYaffs[WORD_LEN];
}CommonParam;

typedef struct myImageList
{
    unsigned char ImageName[PATH_MAX];
    unsigned char strOffset[HEX_LEN+1];
    unsigned char SignName[PATH_MAX];
}MyImageList;

typedef struct specialParams{   
    char keyFilePath[PATH_MAX];
    char srcFilePath[PATH_MAX];
    char srcDirPath[PATH_MAX];
    char destFilePath[PATH_MAX];
    char destDirPath[PATH_MAX];
    char mode[WORD_LEN];
    char ImageNumber[HEX_LEN];
    MyImageList ImageList[FILES_NUMBER]; //HERE
    char osVersion[HEX_LEN + 1];
}SpecialParam;

typedef struct cryptoParams
{
    char srcFilePath[PATH_MAX];
    char destFilePath[PATH_MAX];
    char keyFilePath[PATH_MAX];
    char signatureFilePath[PATH_MAX];
    unsigned int inputType; //1,txt  2,bin
    unsigned int outputType;//1,txt  2,bin
}CryptoParam;

typedef struct
{
    char keysDirPath[PATH_MAX];  //TODO:PATH_MAX
    char cfgDirPath[PATH_MAX]; // boot cfg file
    char srcDirPath[PATH_MAX];
    char destDirPath[PATH_MAX];
    char keyFilePath[PATH_MAX];
    char srcFilePath[PATH_MAX];
    char destFilePath[PATH_MAX];
    char privateDataFilePath[PATH_MAX];
    char softwareVersion[HEX_LEN + 1];
    char chipName[CHIPNAME_LEN+1];
    char SWPKFile[PATH_MAX];
}SignPaths;

typedef struct signbootpaths{
    char keysDirPath[PATH_MAX];
    char cfgDirPath[PATH_MAX]; // boot cfg file
    char srcDirPath[PATH_MAX];
    char destDirPath[PATH_MAX];
    char ifParamOnly[WORD_LEN];
    //char blpkFilePath[PATH_MAX];
    char auxCodeHashFile[PATH_MAX]; 
    char strBLPKFilePath[PATH_MAX];
    char strEncBLPKFilePath[PATH_MAX];
    char strAuxHashFile[PATH_MAX];
    char strRootPrivateKey[PATH_MAX];
    char strExternalPrivateKey[PATH_MAX];
    char strExternalPublicKey[PATH_MAX];
    char strBootFilePath[PATH_MAX];
    char strHSLHashCodeFile[PATH_MAX];
    char strHSLExtPrivKeyPath[PATH_MAX];
    char strHSLExtPubKeyPath[PATH_MAX];
    char strBootConfigFilePath[PATH_MAX];
}SignBootPaths;

typedef struct signHSLPaths{
    char keysDirPath[PATH_MAX];
    char srcDirPath[PATH_MAX];
    char destDirPath[PATH_MAX];
    char HSLKeyAreaFile[PATH_MAX];
    char HSLCodeAreaFile[PATH_MAX];
    char HSLKeyAreaSigFile[PATH_MAX];
    char HSLCodeAreaSigFile[PATH_MAX];
    char strHSLExtPrivKeyPath[PATH_MAX];
    char strHSLExtPubKeyPath[PATH_MAX];
    char strRootPrivKeyPath[PATH_MAX];
    char strHSLFilePath[PATH_MAX];
}SignHSLPaths;

typedef struct
{
    char strKeyAreaPath[PATH_MAX];
    char strKeyAreaSignPath[PATH_MAX];
    char strParamAreaPath[PATH_MAX];
    char strParamAreaSignPath[PATH_MAX];
    char strAuxAreaPath[PATH_MAX];
    char strAuxAreaSignPath[PATH_MAX];
    char strBootAreaPath[PATH_MAX];
    char strBootAreaSignPath[PATH_MAX];
    char strFinalBootPath[PATH_MAX];
    char strUncheckedAreaPath[PATH_MAX];
    char strUncheckedAreaForHisiPath[PATH_MAX];
    char strHSLAreaPath[PATH_MAX];
    char strASCAreaPath[PATH_MAX];
    char strMultiParamAreaPath[PATH_MAX];
}MergeBootPaths;

typedef struct 
{
    char strHSLKeyAreaPath[PATH_MAX];
    char strHSLKeyAreaSignPath[PATH_MAX];
    char strHSLCodeAreaPath[PATH_MAX];
    char strHSLCodeAreaSignPath[PATH_MAX];
    char strHSLAreaPath[PATH_MAX];
}MergeHSLPaths;

typedef struct{
    char bootFilePath[PATH_MAX];
    char blpkFilePath[PATH_MAX];
}MaskParam;

typedef struct{
    char bootFilePath[PATH_MAX];
    char destFilePath[PATH_MAX];
    char blpkFilePath[PATH_MAX];
    char encblpkFilePath[PATH_MAX];
}EncBlpkParam;

typedef struct{
    char pemFilePath[PATH_MAX];
    unsigned int outputType;
    char destFilePath[PATH_MAX];
}TransPEMParam;

typedef struct{
    char privateKeyFilePath[PATH_MAX];
    char publicKeyFilePath[PATH_MAX];
    char destFilePath[PATH_MAX];
    char srcFilePath[PATH_MAX];
    char privateDataPath[PATH_MAX];
    char encPKPpath[PATH_MAX];
    char PKPath[PATH_MAX];
}TAParams;

typedef struct{
    char inputDirPath[PATH_MAX];
    char outputFilePath[PATH_MAX];
    char ImageNumber[HEX_LEN];
    MyImageList ImageList[FILES_NUMBER]; //HERE
}MergeImageParam;

typedef struct{
    char keyDirPath[PATH_MAX];
    char outputFilePath[PATH_MAX];
    char codeFilePath[PATH_MAX];
}SignCASImageParam;

typedef enum tagChipsetType
{
    Hi3110EV300,
    Hi3798MV100,
    Hi3716MV310,
    //Hi3716CV200_IRDETO,
    Hi3796MV100,
    Hi3796MV200,
    Hi3110EV500,
    Hi3716MV320,
    Hi3716MV410,
    Hi3716MV420,
    //Hi3798CV200_A,
    Hi3798CV200,
    Hi3716MV330,
    Hi3751V620,
    Hi3798MV200,
    Hi3716DV110,
    Hi3716DV100,
    Hi3798MV310,
    Hi3716MV450,
    Hi3751V810,
    Hi3751V811,
    Hi3556AV100,
    Hi3516CV500,    
    Hi3716MV430,
}HI_CHIPSET_TYPE_EN;

typedef enum tagSigVersion
{
    HI_SIG_VERSION_V100,
    HI_SIG_VERSION_V200,
    HI_SIG_VERSION_V300,
    HI_SIG_VERSION_V400,
    HI_SIG_VERSION_BUTT,
}HI_SIG_VERSION_EN;


//HI_CHIPSET_TYPE_EN g_enChipsetType;
//unsigned long g_ulHeadAreaLength;


//#define CFGFILE _T("\\config.ini")

/************************************************************************/
/* memory map on board                                                  */
/************************************************************************/
#define SECTION_P_ROM       0              /*onchip rom and ram*/
#define SECTION_P_CA        0x10000000     /*ca reg*/
#define SECTION_P_SYS       0x10100000     /*sys ctrl reg and otp reg*/
#define SECTION_P_PERI      0x10200000     /*peri ctrl reg*/
#define SECTION_P_CSSYS     0x10300000     /*cssys reg*/
#define SECTION_P_DNR       0x10400000     /*dnr reg*/
#define SECTION_P_L2        0x16800000     /*l2 cache reg*/
#define SECTION_P_NAND      0x24000000     /*nand flash*/
#define SECTION_P_SPI       0x26000000     /*spi flash*/
#define SECTION_P_NOR       0x34000000     /*nor flash*/
#define SECTION_P_NANDC     0x60000000     /*nandc and cipher reg*/
#define SECTION_P_MMC       0x60100000     /*mmc reg*/
#define SECTION_P_DDR_1     0x80000000     /*DDR*/
#define SECTION_P_DDR_2     0x80100000     /*DDR*/
#define SECTION_P_DDR_3     0x80200000     /*DDR*/
#define SECTION_P_DDR_4     0x80300000     /*DDR*/
#define SECTION_P_DDR_5     0x80400000     /*DDR*/
#define SECTION_P_DDR_6     0x80500000     /*DDR*/
#define SECTION_P_DDR_7     0x80600000     /*DDR*/

#define SECTION_V_ROM       0              /*onchip rom and ram*/
#define SECTION_V_CA        0x100000       /*ca reg*/
#define SECTION_V_SYS       0x200000       /*sys ctrl reg and otp reg*/
#define SECTION_V_PERI      0x300000       /*peri ctrl reg*/
#define SECTION_V_CSSYS     0x400000       /*cssys reg*/
#define SECTION_V_DNR       0x500000       /*dnr reg*/
#define SECTION_V_L2        0x600000       /*l2 cache reg*/
#define SECTION_V_NAND      0x700000       /*nand flash*/
#define SECTION_V_SPI       0x800000       /*spi flash*/
#define SECTION_V_NOR       0x900000       /*nor flash*/
#define SECTION_V_NANDC     0xA00000       /*nandc and cipher reg*/
#define SECTION_V_MMC       0xB00000       /*mmc reg*/
#define SECTION_V_DDR_1     0xC00000       /*DDR*/
#define SECTION_V_DDR_2     0xD00000       /*DDR*/
#define SECTION_V_DDR_3     0xE00000       /*DDR*/
#define SECTION_V_DDR_4     0xF00000       /*DDR*/
#define SECTION_V_DDR_5    0x1000000       /*DDR*/
#define SECTION_V_DDR_6    0x1100000       /*DDR*/
#define SECTION_V_DDR_7    0x1200000       /*DDR*/

/************************************************************************/
/* param area struct on flash, little endian                            */
/************************************************************************/
#define KEY_AREA_OFFSET        0x0
#define PARA_AREA_OFFSET    0x304
#define BOOT_AREA_OFFSET_MV200        0x800

#define BOOT_AREA_OFFSET_MV300  0x1800

#define RSA_2048_LEN         0x100
#define KEY_AREA_LEN        PARA_AREA_OFFSET
#define PARA_AREA_LEN    (g_ulHeadAreaLength - PARA_AREA_OFFSET)

#define RSA_SIGNATURE_LEN   0x100

#define CFG_TYPE_INIT       1
#define CFG_TYPE_WAKEUP     2
#define CFG_TYPE_NULL       0xff

#define CFG_CMD_END         0xf
#define CFG_CMD_REG         0
#define CFG_CMD_DELAY       1
#define CFG_CMD_WAIT        2
#define CFG_CMD_REG_MSK     3
#define CFG_CMD_WAIT_MSK    4

#define CFG_BIT_8           0x0
#define CFG_BIT_16          0x1
#define CFG_BIT_32          0x2

#define BOOT_AREA_NUM       2

typedef struct {
    UINT32   check_area_off[BOOT_AREA_NUM];
    UINT32   check_area_len[BOOT_AREA_NUM];
    UINT32   boot_area_len;
    UINT32   clear_flag    :1;    /*0:encrypted, 1: clear*/
    UINT32   alg           :1;    /*0:AES, 1: TDES*/
    UINT32   mode          :1;    /*0:CBC, 1: ECB*/
    UINT32   reserved      :29;
}boot_area_info;

typedef union { 
    struct
    {
        UINT32     ecc_type    :3;
        UINT32     block_size  :2;
        UINT32     page_size   :2;
        UINT32     bus_with    :1;
        UINT32     addr_num    :1;
        UINT32     disable     :1;
        UINT32     reserved    :22;
    }bits;
    UINT32 all;
}nand_param;

typedef struct {
    UCHAR  type;
    UCHAR  reserved;
    UCHAR  len[2];
    UCHAR  reg_base[4];
}cfg_param_head;


#define RSV_PARAM_LEN (32 - sizeof(boot_area_info) - sizeof (nand_param))
#define CFG_PARAM_LEN (PARA_AREA_LEN - RSA_2048_LEN - 32)

#define MAX_CFG_PARAM_LEN (BOOT_AREA_OFFSET_MV300 - PARA_AREA_OFFSET - RSA_2048_LEN - 32)
/*parameter area*/
typedef struct {    
    boot_area_info boot_area;  //PARA_AREA_OFFSET
    nand_param nand;
    UCHAR    reserved[RSV_PARAM_LEN];
    UCHAR    cfg_param[MAX_CFG_PARAM_LEN];
}param_area;


#define UNCHECKED_AREA_LEN  0x800

#endif
