#ifndef TYPE_H
#define TYPE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define BOOL    bool
#ifndef TRUE
#define TRUE    true
#define FALSE   false
#endif
#define ULONGLONG unsigned long long
#define UCHAR   unsigned char
#define UINT    unsigned int
#define UINT32  unsigned int
//#define CString char

#endif
