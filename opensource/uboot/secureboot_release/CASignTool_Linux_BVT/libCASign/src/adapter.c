/******************************************************************************

Copyright (C), 2004-2020, Hisilicon Tech. Co., Ltd.
******************************************************************************
File Name     : adapter.c
Version       : Initial
Author        : Hisilicon hisecurity team
Created       : 2014-03-01
Last Modified :
Description   : Hisilicon CA API definition
Function List :
History       :
******************************************************************************/
#include "openssl/adapter.h"
#include "commonmethod.h"
#include "string.h"

int write_rsa_file( const char *p, const BIGNUM *X, FILE *fout )
{
    int ret = 0;
    unsigned char numRsaKey[512] = {0};
    int len = 0;
    int i = 0;

    len = BN_bn2bin(X, numRsaKey);
    ret = fwrite(p, 1, strlen(p), fout);
    if (ret <= 0)
    {
        return ret;
    }

    for (i = 0; i < len; i++)
    {
        ret |= fprintf(fout, "%02x", numRsaKey[i]);
    }
    ret |= fwrite("\r\n", 1, 2, fout);
    if (ret <= 0)
    {
        return ret;
    }

    return 0;
}

/*
 * Convert an ASCII character to digit value
 */
int mpi_get_digit( unsigned char *d, int radix, char c )
{
    *d = 255;

    if( c >= 0x30 && c <= 0x39 ) *d = c - 0x30;
    if( c >= 0x41 && c <= 0x46 ) *d = c - 0x37;
    if( c >= 0x61 && c <= 0x66 ) *d = c - 0x57;

    if( *d >= (unsigned long) radix )
        return( -1 );

    return( 0 );
}

int get_rsa_from_file( BIGNUM **rsa, FILE *fin )
{
    char strRsaKey[2048] = {0};
    unsigned char numRsaKey[2048] = {0};
    int index = 0;
    int i = 0;
    int slen = 0;  //Total length of the whole string
    char *p;
    unsigned char d = 0;
    int dataLen = 0; //Length of the valid data

    if (NULL == fgets( strRsaKey, sizeof( strRsaKey ) - 1, fin))
    {
        return -1;
    }

    slen = strlen(strRsaKey);
    if (slen < 2) 
    {
        return -1;
    }
    if('\n' == strRsaKey[slen - 1]) 
    { 
        slen--; 
        strRsaKey[slen] = '\0'; 
    }
    if('\r' == strRsaKey[slen - 1]) 
    { 
        slen--; 
        strRsaKey[slen] = '\0'; 
    }

    p = strRsaKey + 2;
    i = 0;
    index = 0;
    dataLen = slen;
    while( p <= (strRsaKey + slen) )
    {
        if( mpi_get_digit( &d, 16, *p ) != 0 )
        {
            p++;
            dataLen--;
            continue;
        }
        else
        {
            //Jump "0x" or "0X"
            if(((p[0] == '0') && (p[1] == 'x'))
                    || ((p[0] == '0') && (p[1] == 'X')))
            {
                //p++;
                p += 2;
                dataLen -= 2;
                continue;
            }
        }

        numRsaKey[index] = numRsaKey[index] << (4 * (i % 2)) | d;

        if(1 == (dataLen %2))
        {
            dataLen = 0;
            index++;
        }
        else if (1 == (i++ % 2))
        {
            index++;
        }
        p++;
    }

    *rsa = BN_bin2bn(numRsaKey, index, *rsa);

    return 0;
}

void* havege_rand(void)
{
    return NULL;
}

void* havege_init(havege_state *state)
{
    state = NULL;
    return NULL;
}

void rsa_init( rsa_context *ctx,
               int padding,
               int hash_id,
               void *gen,
               void *genlen)
{
    *ctx = RSA_new();
}

void rsa_free( rsa_context *ctx )
{
    return RSA_free(*ctx);
}

void sha1( const unsigned char *input, int ilen, unsigned char output[20] )
{
    SHA1(input, ilen, output);
}

//TODOD:  param check   SHA256's return
void sha2( const unsigned char *input, int ilen,
           unsigned char output[32], int is224 )
{
    SHA256(input, ilen, output);
}

int mpi_read_file( BIGNUM **X, int radix, FILE *fin )
{
    return get_rsa_from_file(X, fin);
}

int mpi_write_file( const char *p, BIGNUM **X, int radix, FILE *fout )
{
    return write_rsa_file( p , *X , fout );
}

int mpi_write_binary( BIGNUM **X, unsigned char *buf, int buflen )
{
    int length = 0;
    unsigned char numRsa[512] = {0};
    
    length = BN_bn2bin(*X, numRsa);

    MEMCPY_S(buf + buflen - length, length, numRsa, length, "", return -1;);
    return 0;
}

int mpi_read_binary( BIGNUM **X, const unsigned char *buf, int buflen )
{
    *X = BN_bin2bn(buf, buflen, *X);
    return 0;
}

int aes_setkey_enc( aes_context *ctx, const unsigned char *key, int keysize )
{
    return AES_set_encrypt_key(key, keysize, ctx);
}

int aes_crypt_cbc( aes_context *ctx,
                   int mode,
                   int length,
                   unsigned char iv[16],
const unsigned char *input,
unsigned char *output )
{
    AES_cbc_encrypt(input, output, length, ctx, iv, mode);

    return 0;
}

int aes_crypt_ecb( aes_context *ctx,
                   int mode,
                   const unsigned char input[16],
unsigned char output[16])
{
    AES_ecb_encrypt(input, output, ctx, mode);

    return 0;
}

int aes_setkey_dec( aes_context *ctx, const unsigned char *key, int keysize )
{
    return AES_set_decrypt_key(key, keysize, ctx);
}

int g_desMode = DES_ENCRYPT;

void des3_set2key_enc( des3_context *ctx, const unsigned char key[16] )
{
    g_desMode = DES_ENCRYPT;
    DES_set_key_unchecked((const_DES_cblock *)&key[0], &ctx->des_ks);
    DES_set_key_unchecked((const_DES_cblock *)&key[8], &ctx->des_ks2);
}

void des3_set2key_dec( des3_context *ctx, const unsigned char key[16] )
{
    g_desMode = DES_DECRYPT;
    DES_set_key_unchecked((const_DES_cblock *)&key[0], &ctx->des_ks);
    DES_set_key_unchecked((const_DES_cblock *)&key[8], &ctx->des_ks2);
}

int des3_crypt_cbc( des3_context *ctx,
                    int mode,
                    int length,
                    unsigned char iv[8],
const unsigned char *input,
unsigned char *output )
{
    DES_ede2_cbc_encrypt(input, output, length, &ctx->des_ks, &ctx->des_ks2, (DES_cblock *)iv, mode);

    return 0;
}

int des3_crypt_ecb( des3_context *ctx,
                    const unsigned char input[8],
unsigned char output[8] )
{
    DES_ecb2_encrypt((const_DES_cblock *)input, (DES_cblock *)output, &ctx->des_ks, &ctx->des_ks2, g_desMode);

    return 0;
}

int sha2_file( const char *path, unsigned char output[32], int is224 )
{
    FILE *fp = NULL;
    unsigned char buf[512] = {0};
    SHA256_CTX ctx;
    int number = 0;
    
    if(!OpenMyFile(&fp, path, "rb"))
    {
        printf("Failed to open source file!");
        return -1;
    }

    SHA256_Init(&ctx);

    while( ( number = fread( buf, 1, sizeof( buf ), fp ) ) > 0 )
    {
        SHA256_Update(&ctx, buf, number);
    }

    SHA256_Final(output, &ctx);
    
    fclose(fp);
    
    return 0;
}

int rsa_pkcs_oaep_sign(rsa_context *ctx, 
                            unsigned int rsaLen, 
                            const unsigned char *hash, 
                            unsigned char *sig)
{
    const EVP_MD *md = EVP_sha256();
    if(0 == (RSA_padding_add_PKCS1_PSS(*ctx, sig, hash, md, -1)))
    {
        printf("Failed to make the sign!\n");
        return -1;
    }

    //RSA_encrypt
    if(RSA_private_encrypt(rsaLen, sig, sig, *ctx, RSA_NO_PADDING) != rsaLen)
    {
        printf("Failed to make the sign!\n");
        return -1;
    }

    return 0;
}

int rsa_pkcs1_sign( rsa_context *ctx,
                    int mode,
                    int hash_id,
                    int hashlen,
                    const unsigned char *hash,
                    unsigned char *sig )
{
    if (hash_id == SIG_RSA_SHA256)
    {
        if (RSA_private_encrypt(hashlen, hash, sig, *ctx, RSA_PKCS1_SHA2_PADDING) != BN_num_bytes((*ctx)->n))
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
    return -1;
}

int rsa_pkcs1_verify( rsa_context *ctx,
                      int mode,
                      int hash_id,
                      int hashlen,
                      unsigned char *hash,
                      unsigned char *sig )
{
    unsigned char hash_result[256] = {0};

    if (hash_id == SIG_RSA_SHA256)
    {
        if (32 == RSA_public_decrypt(RSA_2048_LEN, sig, hash_result, *ctx, RSA_PKCS1_SHA2_PADDING))
        {
            if (!memcmp(hash_result, hash, hashlen))
            {
                return 0;
            }
        }
    }

    return -1;
}

static int genrsa_cb(int p, int n, BN_GENCB *cb)
{
    char c='*';

    if (p == 0) c='.';
    if (p == 1) c='+';
    if (p == 2) c='*';
    if (p == 3) c='\n';
    BIO_write((BIO *)(cb->arg),&c,1);
    (void)BIO_flush((BIO *)(cb->arg));

    return 1;
}

int rsa_gen_key( rsa_context *ctx,
                 int nbits, int exponent )
{
    BIGNUM *bn = BN_new();
    BN_GENCB cb;

    if(!bn)
    {
        return -1;
    }
    BN_GENCB_set(&cb, genrsa_cb, NULL);

    if(!BN_set_word(bn, exponent) || !RSA_generate_key_ex(*ctx, nbits, bn, &cb))
    {
        return -1;
    }

    BN_free(bn);
    
    return 0;
}

int aes_crypt_ctr( aes_context *ctx,
                    int length,
                    unsigned char iv[16],
                    const unsigned char *input,
                    unsigned char *output )
{
    unsigned char iv_out[16] = {0};
    unsigned int num = 0;

    AES_ctr128_encrypt(input, output, length, ctx, iv, iv_out, &num);    
    return 0;
}





