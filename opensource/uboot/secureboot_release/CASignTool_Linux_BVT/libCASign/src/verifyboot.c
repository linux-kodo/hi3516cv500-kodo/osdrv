#include "verifyboot.h"
//#include "encrypt.h"

int authDataSign(rsa_context *rsa, unsigned char* BlockStartAddr, int  Blocklength, unsigned char* sign)
{
    int ret = 0;

    if(TypeSHA256 == g_sigMethod)
    {
        unsigned char hash[HASH_LEN] = {0};
        sha2(BlockStartAddr, Blocklength, hash,0);
        ret = rsa_pkcs1_verify( rsa, RSA_PUBLIC, SIG_RSA_SHA256, HASH_LEN, hash, sign);
    }
    return ret;
}


