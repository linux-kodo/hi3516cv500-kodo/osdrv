#ifndef SIGNHI3556AV100
#define SIGNHI3556AV100

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <stdbool.h>
#include "type.h"
#include "openssl/adapter.h"
#include "priv_signTool.h"
#include <strings.h>
#include "commonmethod.h"


typedef struct signParam
{
    char pRSAPubKeyPath[PATH_MAX];
    char pRSAPriKeyPath[PATH_MAX]; 
    char pDrrInitPath[PATH_MAX]; 
    char pUBootPath[PATH_MAX];
    char pOutputFilePath[PATH_MAX];
    unsigned int RSAIOType;  //TypeTXT...    
    unsigned int no_enc;  
    char binaryKey[AES_ENCRYPT_KEY_SIZE*2];
    char binaryIV[AES_ENCRYPT_KEY_SIZE];
}SignParam;

//Hi3556AV100

int ParseHi3556AV100ConfigFile(char *strConfigFile, SignParam *signParams);

int ParseSignParam(char **argv, int argc, SignParam *signParams);

int SignHi3556AV100(SignParam *signParam);

bool ProcessHi3556AV100KeyArea(SignParam *signParam);

bool ProcessHi3556AV100SignArea(char *pRSAPriKeyPath, char *pBinaryPath, SignParam *signParam, char *binaryIV, bool encFlag);


#endif
