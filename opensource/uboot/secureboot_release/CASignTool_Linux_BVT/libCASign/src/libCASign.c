#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include "../../opensrc/openssl/adapter.h"
#include "priv_signTool.h"
#include "sha2.h"
#include "libCASign.h"
#include "signHi3519V101.h"

#ifdef __cplusplus
}
#endif


//boot
#define KeyArea                 "KeyArea.bin"
#define KeyAreaSign             "KeyAreaSign.bin"
#define ParamArea               "ParamArea.bin"
#define ParamAreaSign           "ParamAreaSign.bin"
#define BootArea                "BootArea.bin"
#define BootAreaSign            "BootAreaSign.bin"
#define FinalBoot               "FinalBoot.bin"
#define UncheckedArea           "UncheckedArea.bin"
#define POSTFIX_LEN 5

void SetBootChipsetType(char* pType)
{
    if (NULL == pType)
    {
        return;
    }
    else if(!strcasecmp("Hi3556AV100", pType))
    {
        SetChipsetType(Hi3556AV100);
    }
    else if(!strcasecmp("Hi3516CV500", pType))
    {
        SetChipsetType(Hi3516CV500);
    }
}

int CreateRSAKeyWithDefaultE(char* pDestPath)
{
    return ((NULL == pDestPath) ? -1 : CreateRSAKey("0x10001", pDestPath));
}

int CreateRSAKey(char* pEValue, char* destPath)
{
    bool bTempRet = (NULL == pEValue || NULL == destPath);
    if (bTempRet)
    {
        return -1;
    }

    if (!isValidNumber(pEValue))
    {
        printf("Invalid eValue %s.\n", pEValue);
        return -1;
    }

    int eValue = strtoul(pEValue, NULL, 16);

    if (!isPrimeNumber(eValue))
    {
        printf("eValue %d is not a prime number that greater than 2.\n", eValue);
        return -1;
    }

    int ret;
    havege_state hs;
    int offset = 0;
    //KEY_SIZE: 2048
    unsigned char test_rsa[KEY_SIZE] = {0};
    unsigned char test_hash[32] = {0};
    UINT nRsaE = eValue;
    int i = 0;
    unsigned int crc_value = 0;

    FILE* fpubtxt  = NULL;
    FILE* fpubbin  = NULL;
    FILE* fpubcrc  = NULL;
    FILE* fpubheader  = NULL;
    FILE* fpriv = NULL;
    rsa_context rsa = NULL;
    int result = -1;

    char strRsaPubTxtFile[PATH_MAX] = {0};
    char strRsaPubBinFile[PATH_MAX] = {0};
    char strRsaPubHeaderFile[PATH_MAX] = {0};
    char strRsaPrivFile[PATH_MAX] = {0};
    char strRsaPubCrcFile[PATH_MAX] = {0};
    
    mkdir_p(destPath);

    bTempRet = (!MergePathName(destPath, "rsa_pub.txt", strRsaPubTxtFile, PATH_MAX))
        || (!MergePathName(destPath, "rsa_pub.bin", strRsaPubBinFile, PATH_MAX))
        || (!MergePathName(destPath, "rsa_pub.h", strRsaPubHeaderFile, PATH_MAX))
        || (!MergePathName(destPath, "rsa_priv.txt", strRsaPrivFile, PATH_MAX))
        || (!MergePathName(destPath, "rsa_pub_crc.bin", strRsaPubCrcFile, PATH_MAX));


    if (bTempRet)
    {
        goto exit;
    }

    if (nRsaE >= 0xffffffff)
    {
        printf("Invalid RSA E value!\n");
        goto exit;
    }

    havege_init( &hs );

    /*Gen rsa key and verify if it is valid*/
    rsa_init( &rsa, RSA_PKCS_V15, 0, (void*)havege_rand, &hs );
    printf("The RSA key is being created, please wait......\n");
    for (offset = 0 ; offset < RSA_2048_LEN; offset++)
    {
        if (( ret = rsa_gen_key( &rsa, KEY_SIZE, nRsaE )) != 0 )
        {
            printf("Gen key failed!\n");
            goto exit;
        }
        
        if(0 != mpi_write_binary(&rsa->n, test_rsa, RSA_2048_LEN))
        {
            printf("Write binary failed!\n");
            goto exit;
        }

        if (0 == isSignatureValid(test_rsa, 64, test_hash))
        {
            break;
        }
    }

    if(!OpenMyFile(&fpubtxt, strRsaPubTxtFile, "wb+"))
    {
         printf("Open public file %s failed!\n", strRsaPubTxtFile);
         goto exit;
    }

    if (WriteTxtRSAKey(rsa, HEX_RADIX, TypePublic, fpubtxt, strRsaPubTxtFile) != 0)
    {
        printf("Write public file failed!\n");
        goto exit;
    }

    /*Write rsa pub bin file and crc file*/
    if(!OpenMyFile(&fpubbin, strRsaPubBinFile, "wb+"))
    {
        printf("Open public file failed!\n");
        goto exit;
    }

    if(!OpenMyFile(&fpubcrc, strRsaPubCrcFile, "wb+"))
    {
        printf("Open crc file failed!\n");
        goto exit;
    }

    if(0 != mpi_write_binary( &rsa->n, test_rsa, RSA_2048_LEN ))
    {
        printf("Failed to transform N!\n");
        goto exit;
    }
    
    if (1 != fwrite(test_rsa, RSA_2048_LEN, 1, fpubbin))
    {
        printf("Failed to write file %s\n", strRsaPubBinFile);
        goto exit;
    }

    if (1 != fwrite(test_rsa, RSA_2048_LEN, 1, fpubcrc))
    {
        printf("Failed to write file %s\n", strRsaPubCrcFile);
        goto exit;
    }

    crc_value = crc32(0, test_rsa, RSA_2048_LEN);

    MEMSET_S(test_rsa, RSA_2048_LEN, 0, RSA_2048_LEN, "", goto exit;);

    if (1 != fwrite(test_rsa, 252, 1, fpubbin))
    {
        printf("Failed to write file %s\n", strRsaPubBinFile);
        goto exit;
    }

    if (1 != fwrite(test_rsa, 252, 1, fpubcrc))
    {
        printf("Failed to write file %s\n", strRsaPubCrcFile);
        goto exit;
    }

    crc_value = crc32(crc_value, test_rsa, 252);

    if (0 != mpi_write_binary(&rsa->e, test_rsa, 4))
    {
        printf("Failed to transform E!\n");
        goto exit;
    }
    if (1 != fwrite(test_rsa, 4, 1, fpubbin))
    {
        printf("Failed to write file %s\n", strRsaPubBinFile);
        goto exit;
    }

    if (1 != fwrite(test_rsa, 4, 1, fpubcrc))
    {
        printf("Failed to write file %s\n", strRsaPubCrcFile);
        goto exit;
    }
    crc_value = crc32(crc_value, test_rsa, 4);

    MEMSET_S(test_rsa, 4, 0, 4, "", goto exit;);
    test_rsa[0] = (unsigned char)(crc_value >> 24);
    test_rsa[1] = (unsigned char)(crc_value >> 16);
    test_rsa[2] = (unsigned char)(crc_value >> 8);
    test_rsa[3] = (unsigned char)(crc_value >> 0);

    if (1 != fwrite(test_rsa, 4, 1, fpubcrc))
    {
        printf("Failed to write file %s\n", strRsaPubCrcFile);
        goto exit;
    }

    /*Write rsa pub header file*/
    if(!OpenMyFile(&fpubheader, strRsaPubHeaderFile, "wb+"))
    {
        printf("Open public file failed!\n");
        goto exit;
    }
    
    if(0 != mpi_write_binary( &rsa->n, test_rsa, RSA_2048_LEN ))
    {
        printf("Failed to transform N!\n");
        goto exit;
    }
    fprintf(fpubheader, "unsigned char rsa_pub_N[256] = {\r\n");

    for (i = 0; i < RSA_2048_LEN; i++)
    {
        fprintf(fpubheader, "0x%02x, ", test_rsa[i]);

        if (((i + 1) % 16) == 0)
        {
            fprintf(fpubheader, "\r\n");
        }
    }

    fprintf(fpubheader, "};\r\n");

    fprintf(fpubheader, "unsigned char rsa_pub_E[256] = {\r\n");

    MEMSET_S(test_rsa, RSA_2048_LEN, 0, RSA_2048_LEN, "", goto exit;);

    for (i = 0; i < 252; i++)
    {
        fprintf(fpubheader, "0x00, ");

        if (((i + 1) % 16) == 0)
        {
            fprintf(fpubheader, "\r\n");
        }
    }

    if(0 != mpi_write_binary( &rsa->e, test_rsa, 4 ))
    {
        printf("Failed to transform E!\n");
        goto exit;
    }

    for (i = 0; i < 3; i++)
    {
        fprintf(fpubheader, "0x%02x, ", test_rsa[i]);
    }

    fprintf(fpubheader, "0x%02x\r\n", test_rsa[i]);

    fprintf(fpubheader, "};\r\n");

    if(!OpenMyFile(&fpriv, strRsaPrivFile, "wb+"))
    {
        printf("Open private file %s failed!\n", strRsaPrivFile);
        goto exit;
    }
    ret = WriteTxtRSAKey(rsa, HEX_RADIX, TypePrivate, fpriv, strRsaPrivFile);

    if (ret != 0)
    {
        printf("Write private file failed!\n");
        goto exit;
    }

    result = 0;
    printf("All RSA files have been created successfully!\n");

exit:
    SAFE_CLOSE(fpubtxt);
    SAFE_CLOSE(fpubcrc);
    SAFE_CLOSE(fpubbin);
    SAFE_CLOSE(fpubheader);
    SAFE_CLOSE(fpriv);
    rsa_free(&rsa);
    return (result);
}

int SignNonBOOTImage_Crypto_cfg(const char* strConfigFile, CryptoParam* pCryptoParams)
{
    if (NULL == strConfigFile)
    {
        return -1;
    }

    if (0 == Sign_NonBOOTImage_Crypto(strConfigFile, pCryptoParams))
    {
        return 0;
    }
    else
    {
        return -1;
    }

    return 0;
}


int SignHi3519V101(SignHi3519V101Param *pSignHi3519V101Params)
{
    if (NULL == pSignHi3519V101Params)
    {
        return -1;
    }

     if (SignCryptoHi3519V101(pSignHi3519V101Params))
    {
        printf("Succeed.\n");
        return 0;
    }
    return -1;  
}

