#include "sha2.h"
#include <string.h>
#include <stdio.h>

/**
 * \brief          SHA-256 context structure
 */
typedef struct
{
    unsigned long total[2];     /*!< number of bytes processed  */
    unsigned long state[8];     /*!< intermediate digest state  */
    unsigned char buffer[64];   /*!< data block being processed */

    unsigned char ipad[64];     /*!< HMAC: inner padding        */
    unsigned char opad[64];     /*!< HMAC: outer padding        */
    int is224;                  /*!< 0 => SHA-256, else SHA-224 */
}
sha2_context;

/*
 * 32-bit integer manipulation macros (big endian)
 */

static void GET_ULONG_BE(unsigned long * n, unsigned char * b, unsigned char i)
{
    *n = ( (unsigned long) (b)[(i)    ] << 24 )
            | ( (unsigned long) (b)[(i) + 1] << 16 )
            | ( (unsigned long) (b)[(i) + 2] <<  8 )
            | ( (unsigned long) (b)[(i) + 3]       );

}

static void PUT_ULONG_BE(unsigned long n, unsigned char * b, unsigned char i)
{                                                       
    (b)[(i)    ] = (unsigned char) ( (n) >> 24 );
    (b)[(i) + 1] = (unsigned char) ( (n) >> 16 );
    (b)[(i) + 2] = (unsigned char) ( (n) >>  8 );
    (b)[(i) + 3] = (unsigned char) ( (n)       );
}
/*
 * SHA-256 context setup
 */
static void sha2_starts( sha2_context *ctx, int is224 )
//void sha2_starts( sha2_context *ctx )
{
    ctx->total[0] = 0;
    ctx->total[1] = 0;

    if( is224 == 0 )
    {
        /* SHA-256 */
        ctx->state[0] = 0x6A09E667;
        ctx->state[1] = 0xBB67AE85;
        ctx->state[2] = 0x3C6EF372;
        ctx->state[3] = 0xA54FF53A;
        ctx->state[4] = 0x510E527F;
        ctx->state[5] = 0x9B05688C;
        ctx->state[6] = 0x1F83D9AB;
        ctx->state[7] = 0x5BE0CD19;
    }
    //The code in zero won't be reached
    ctx->is224 = is224;
}

static int sha2_process( sha2_context *ctx, unsigned char data[64] )
{
    unsigned long temp1, temp2, W[64], i;
    unsigned long H[8];
    unsigned long const K[64]= {
        0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5,
        0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
        0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
        0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
        0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC,
        0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
        0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7,
        0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
        0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
        0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
        0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3,
        0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
        0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5,
        0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
        0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
        0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2
    };

#define  SHR(x,n) ((x & 0xFFFFFFFF) >> n)
#define ROTR(x,n) (SHR(x,n) | (x << (32 - n)))

#define S0(x) (ROTR(x, 7) ^ ROTR(x,18) ^  SHR(x, 3))
#define S1(x) (ROTR(x,17) ^ ROTR(x,19) ^  SHR(x,10))

#define S2(x) (ROTR(x, 2) ^ ROTR(x,13) ^ ROTR(x,22))
#define S3(x) (ROTR(x, 6) ^ ROTR(x,11) ^ ROTR(x,25))
    
#define F0(x,y,z) ((x & y) | (z & (x | y)))
#define F1(x,y,z) (z ^ (x & (y ^ z)))

#define R(t)                                    \
    (                                               \
    W[t] = S1(W[t -  2]) + W[t -  7] +          \
    S0(W[t - 15]) + W[t - 16]            \
    )

#define P(a,b,c,d,e,f,g,h,x,K)                  \
    {                                               \
    temp1 = h + S3(e) + F1(e,f,g) + K + x;      \
    temp2 = S2(a) + F0(a,b,c);                  \
    h = g;g = f;f = e;e = d + temp1;            \
    d = c;c = b;b = a;a = temp1+temp2;          \
}

    /*prepare the message schedule*/
    for (i = 0; i < 16; ++i)
    {
        GET_ULONG_BE( &W[ i], data,  i * 4 );
    }
    for (; i < 64; ++i)
    {
        R(i);
    }
    
    for (i = 0; i < 8; ++i)
    {
        H[i] = ctx->state[i];
    }

    for (i = 0; i < 64; ++i)
    {
        P(H[0],H[1],H[2],H[3],H[4],H[5],H[6],H[7],W[i],K[i]);
    }

    for (i = 0; i < 8; ++i)
    {
        ctx->state[i] += H[i];
    }
    
    return ((H[7] % 2) == 0);
}
/*
 * SHA-256 process buffer
 */
static int sha2_update( sha2_context *ctx, unsigned char *input, int ilen )
{
    int fill;
    unsigned long left;
    int ret = 0;

    if( ilen <= 0 )
    {
        return -1;
    }
    left = ctx->total[0] & 0x3F;
    fill = 64 - left;

    ctx->total[0] += ilen;
    ctx->total[0] &= 0xFFFFFFFF;

    if( ctx->total[0] < (unsigned long) ilen )
        ctx->total[1]++;

    if( left && ilen >= fill )
    {
        MEMCPY_S((void *) (ctx->buffer + left), sizeof(ctx->buffer) - left, (void *) input, fill, "updating", return -1;);

        ret = sha2_process( ctx, ctx->buffer );
        if(ret != 0)
        {
            return ret;
        }

        input += fill;
        ilen  -= fill;
        left = 0;
    }

    while( ilen >= 64 )
    {
        ret = sha2_process( ctx, input );
        input += 64;
        ilen  -= 64;
    }

    if( ilen > 0 )
    {
        MEMCPY_S((void *) (ctx->buffer + left), sizeof(ctx->buffer) - left, (void *) input, ilen, "updating", return -1;);
    }

    return ret;
}

/*
static const unsigned char sha2_padding[64] =
{
 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
*/
/*
 * SHA-256 final digest
 */
static void sha2_finish( sha2_context *ctx, unsigned char output[32] )
{
    int i;
    unsigned long last, padn;
    unsigned long high, low;
    unsigned char msglen[8] = {0};
    unsigned char sha2_padding[64] = {0};

	MEMSET_S(sha2_padding, sizeof(sha2_padding), 0, sizeof(sha2_padding), "", return;);
    sha2_padding[0] = 0x80;

    high = ( ctx->total[0] >> 29 )
            | ( ctx->total[1] <<  3 );
    low  = ( ctx->total[0] <<  3 );

    PUT_ULONG_BE( high, msglen, 0 );
    PUT_ULONG_BE( low,  msglen, 4 );

    last = ctx->total[0] & 0x3F;
    padn = ( last < 56 ) ? ( 56 - last ) : ( 120 - last );

    sha2_update( ctx, (unsigned char *) sha2_padding, padn );
    sha2_update( ctx, msglen, 8 );

    for (i = 0; i < 8; ++i)
    {
        PUT_ULONG_BE( ctx->state[i], output,  i * 4 );
    }

    /*
    PUT_ULONG_BE( ctx->state[0], output,  0 );
    PUT_ULONG_BE( ctx->state[1], output,  4 );
    PUT_ULONG_BE( ctx->state[2], output,  8 );
    PUT_ULONG_BE( ctx->state[3], output, 12 );
    PUT_ULONG_BE( ctx->state[4], output, 16 );
    PUT_ULONG_BE( ctx->state[5], output, 20 );
    PUT_ULONG_BE( ctx->state[6], output, 24 );

    if( ctx->is224 == 0 )
        PUT_ULONG_BE( ctx->state[7], output, 28 );*/
}

/*
 * output = SHA-256( input buffer )
 */
/*void sha2( const unsigned char *input, int ilen,
           unsigned char output[32], int is224 )*/
static int sha2( unsigned char *input, int ilen, unsigned char output[32] )
{
    int ret = 0;

    sha2_context ctx;

    sha2_starts( &ctx, 0);
    ret = sha2_update( &ctx, input, ilen );
    sha2_finish( &ctx, output );
    MEMSET_S( &ctx, sizeof(sha2_context), 0, sizeof(sha2_context), "", return ret;);
    
    return ret;
}

int isSignatureValid( unsigned char *input, int ilen, unsigned char output[32] )
{
    return sha2(input, ilen, output);
}
