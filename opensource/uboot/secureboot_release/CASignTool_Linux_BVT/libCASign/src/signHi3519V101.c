
#include "signHi3519V101.h"
#include "commonmethod.h"


#define NO_ENC_FILE "no_enc"


//20480

int ParseSignHi3519V101Param(char **argv, int argc, SignHi3519V101Param *pSignHi3519V101Params)
{
    if ((NULL == argv) || (argc <= 0))
    {
        return -1;
    }

    unsigned int ArgvLength;
    unsigned int ParamLength;
    
    ArgvLength = strlen(argv[2]);
    ParamLength = sizeof(pSignHi3519V101Params->pRSAPubKeyPath);
     
    STRNCPY_S(pSignHi3519V101Params->pRSAPubKeyPath, ParamLength, argv[2], ArgvLength, "parsing RSAPubKeyFilePath", return -1;);

    ArgvLength = strlen(argv[3]);
    ParamLength = sizeof(pSignHi3519V101Params->pRSAPriKeyPath);
    STRNCPY_S(pSignHi3519V101Params->pRSAPriKeyPath, ParamLength, argv[3], ArgvLength, "parsing RSAPriKeyFilePath", return -1;);

    ArgvLength = strlen(argv[4]);
    ParamLength = sizeof(pSignHi3519V101Params->pAESKeyPath);
    STRNCPY_S(pSignHi3519V101Params->pAESKeyPath, ParamLength, argv[4], ArgvLength, "parsing AESKeyFilePath", return -1;);

    ArgvLength = strlen(argv[5]);
    ParamLength = sizeof(pSignHi3519V101Params->pDrrInitPath);
    STRNCPY_S(pSignHi3519V101Params->pDrrInitPath, ParamLength, argv[5], ArgvLength, "parsing DrrInitFilePath", return -1;);

    ArgvLength =  strlen(argv[6]);
    ParamLength = sizeof(pSignHi3519V101Params->pUBootPath);
    STRNCPY_S(pSignHi3519V101Params->pUBootPath, ParamLength, argv[6], ArgvLength, "parsing UBootFilePath", return -1;);

    ArgvLength = strlen(argv[7]);
    ParamLength = sizeof(pSignHi3519V101Params->pOutputFilePath);
    STRNCPY_S(pSignHi3519V101Params->pOutputFilePath, ParamLength, argv[7], ArgvLength, "parsing OutputFilePath", return -1;);

    if(10 == argc)
    {
        if(0 == strncasecmp(argv[9], NO_ENC_FILE, strlen(NO_ENC_FILE)))
        {
            pSignHi3519V101Params->no_enc = 1;
        }
        else
        {
            pSignHi3519V101Params->no_enc = 0;
        }
        if (0 == strncasecmp(argv[8], TYPETXT, strlen(TYPETXT)))
        {
            pSignHi3519V101Params->RSAIOType = TypeTXT;
        }
        else if (0 == strncasecmp(argv[8], TYPEPEM, strlen(TYPEPEM)))
        {
            pSignHi3519V101Params->RSAIOType = TypePEM;
        }
        else if (0 == strncasecmp(argv[8], TYPEBIN, strlen(TYPEBIN)))
        {
            pSignHi3519V101Params->RSAIOType = TypeBIN;
        }
        else
        {
            printf("Error is \"%s\" or \"NO_ENC spelled error\"!\n", PrintMsg[ErrorUnKnownSignHi3519V101RSAType]);
            return -1;
        }
    }
    else if (9 == argc)
    {
        if (0 == strncasecmp(argv[8], TYPETXT, strlen(TYPETXT)))
        {
            pSignHi3519V101Params->RSAIOType = TypeTXT;
        }
        else if (0 == strncasecmp(argv[8], TYPEPEM, strlen(TYPEPEM)))
        {
            pSignHi3519V101Params->RSAIOType = TypePEM;
        }
        else if (0 == strncasecmp(argv[8], TYPEBIN, strlen(TYPEBIN)))
        {
            pSignHi3519V101Params->RSAIOType = TypeBIN;
        }
        else if (0 ==strncasecmp(argv[8], NO_ENC_FILE, strlen(NO_ENC_FILE)))
        {
            pSignHi3519V101Params->no_enc = 1;
        }
        else
        {
            printf("Error is \"%s\" or \"NO_ENC spelled error\"!\n", PrintMsg[ErrorUnKnownSignHi3519V101RSAType]);
            return -1;
        }
    }
    else if (8 == argc)
    {
        pSignHi3519V101Params->RSAIOType = TypeTXT;
        pSignHi3519V101Params->no_enc = 0;
    }
    else
    {
        printf("Error is %s\n", PrintMsg[ErrorWrongArgsNum]);
        return -1;
    }   
    return 0;   
}

bool ProcessKeyArea(char *pRSAPubKeyPath, char *pAESKeyPath, char *pOutputFilePath, RSAIOType keyIOtype, int no_enc_flag)
{
    BOOL bTempRet = (NULL == pRSAPubKeyPath || NULL == pAESKeyPath || NULL == pOutputFilePath);
    if (bTempRet)
    {
        return false;
    }

    FILE *pRSAPubKeyFile = NULL;
    rsa_context rsa = NULL;
    unsigned char *aRSAPubKeyData = NULL;
    unsigned char *aEncryptedPubKeyData = NULL;
    FILE *pAESKeyFile = NULL;
    FILE *pOutputFile = NULL;
    unsigned char *aOutputDataBuf = NULL;

    bool ret = false;

    if(!OpenMyFile(&pRSAPubKeyFile, pRSAPubKeyPath, "r+")) 
    {
        printf("Failed to open key file %s!\n", pRSAPubKeyPath);
        goto ScopeRelease;
    }

    // read rsa public key
    rsa_init(&rsa, RSA_PKCS_V15, 0, NULL, NULL);   
    //judge type
    if (TypePEM == keyIOtype)
    {
        //Base64decode
        if(-1 == DecodePEMKey(pRSAPubKeyFile, &rsa, TypePublic))
        {
            printf("%s!\n", PrintMsg[ErrorFailedToParsePEMFile]);
            goto ScopeRelease;
        }
        
    }
    else if(TypeBIN == keyIOtype)
    {
        if(-1 == DecodeBINKey(pRSAPubKeyFile, rsa, TypePublic))
        {
            printf("%s!\n", PrintMsg[ErrorFailedToParseBinFile]);
            goto ScopeRelease;
        }
    }
    else 
    {
        if(-1 == ParseTxtRSAKey(rsa, HEX_RADIX, TypePublic, pRSAPubKeyFile, pRSAPubKeyPath))
        {
            goto ScopeRelease;
        }
    }
    
    int rsaLen = BN_num_bytes(rsa->n);
    int keyBinSize = rsaLen * 2;

    if ( keyBinSize <= 0)
    {
        printf("The length of rsaLen is less or equal to zero!\n");
        goto ScopeRelease;
    }

    aRSAPubKeyData = (unsigned char*)malloc(keyBinSize);
    if(NULL == aRSAPubKeyData) 
    {
        printf("Failed to malloc aRSAPubKeyData.\n");
        goto ScopeRelease;
    }
    
    MEMSET_S(aRSAPubKeyData, keyBinSize, 0, keyBinSize, "processing key area", goto ScopeRelease;);

    aEncryptedPubKeyData = (unsigned char*)malloc(keyBinSize);

    if(NULL == aEncryptedPubKeyData) 
    {
        printf("Failed to malloc aEncryptedPubKeyData.\n");
        goto ScopeRelease;
    }

    if(0 != MpiWriteBuf(rsa, rsaLen, aRSAPubKeyData))
    {
        printf("Failed to write Buf.\n");
        goto ScopeRelease;
    }

    // read aes key
    unsigned char aTxtAESKey[AES_ENCRYPT_KEY_SIZE * 2 + 1] = {0};
    unsigned char aAESKeyData[AES_ENCRYPT_KEY_SIZE] = {0};
    
    // output
    int outputBufSize = RSA_N_LEN + RSA_E1_LEN + keyBinSize;

    aOutputDataBuf = (unsigned char*)malloc(outputBufSize);
    if(NULL == aOutputDataBuf) 
    {
        goto ScopeRelease;
    }
    MEMSET_S(aOutputDataBuf, outputBufSize, 0, outputBufSize, "processing key area", goto ScopeRelease;);

    ((unsigned int*)aOutputDataBuf)[0] = rsaLen;
    ((unsigned int*)aOutputDataBuf)[1] = rsaLen;
    if(1 == no_enc_flag)
    {
        MEMCPY_S(aOutputDataBuf + 8, outputBufSize - 8, aRSAPubKeyData, keyBinSize, "processing key area", goto ScopeRelease;);
    }
    else
    {   
        if(!OpenMyFile(&pAESKeyFile, pAESKeyPath, "r"))
        {
            printf("Failed to open key file %s!\n", pAESKeyPath);
            goto ScopeRelease;
        }
        if(1 != fread(aTxtAESKey, AES_ENCRYPT_KEY_SIZE * 2, 1, pAESKeyFile)) 
        {
            printf("Failed to read key file %s!\n", pAESKeyPath);
            goto ScopeRelease;
        }

        if(!AESTxtKeyToBinaryKey(aTxtAESKey, aAESKeyData)) 
        {
            goto ScopeRelease;
        }

        // aes ecb encrypt
        aes_context ctx;

        aes_setkey_enc(&ctx, aAESKeyData, KEY_BIT_LEN); // 128

        // cbc encrypt
        unsigned char aesIV[16] = {0};
        aes_crypt_cbc(&ctx, AES_ENCRYPT, keyBinSize, aesIV, aRSAPubKeyData, aEncryptedPubKeyData);

        MEMCPY_S(aOutputDataBuf + 8, outputBufSize - 8, aEncryptedPubKeyData, keyBinSize, "processing key area", goto ScopeRelease;);
    }
        
    if(!OpenMyFile(&pOutputFile, pOutputFilePath, "ab"))
    {
        printf("Failed to open file %s!\n", pOutputFilePath);
        goto ScopeRelease;
    }

    if(1 != fwrite(aOutputDataBuf, outputBufSize, 1, pOutputFile)) 
    {
        printf("Failed to write file %s!\n", pOutputFilePath);
        goto ScopeRelease;
    }
    ret = true;

ScopeRelease:
    SAFE_FREE(aRSAPubKeyData);
    SAFE_FREE(aOutputDataBuf);
    SAFE_FREE(aEncryptedPubKeyData);
    SAFE_CLOSE(pOutputFile);
    SAFE_CLOSE(pAESKeyFile);
    SAFE_CLOSE(pRSAPubKeyFile);
    rsa_free(&rsa);
    return ret;
}

bool ProcessSignArea(char *pRSAPriKeyPath, char *pBinaryPath, char *pOutputFilePath, RSAIOType keytype)
{
    BOOL bTempRet = (NULL == pRSAPriKeyPath || NULL == pBinaryPath || NULL == pOutputFilePath);
    if (bTempRet)
    {
        return false;
    }

    bool ret = true;
    FILE *pInputFile = NULL;
    long fileLength = 0;
    unsigned char *pInputFileData = NULL;
    FILE *fileRsaKey = NULL;
    FILE *pOutputFile = NULL;
    unsigned char *rsaBuf = NULL;
    rsa_context rsa = NULL;
    unsigned char hash[32] = {0};
    char absRSAPrivKeyPath[PATH_MAX] = {0};

    // *******************************read src file data*********//
    if(!OpenMyFile(&pInputFile, pBinaryPath, "rb")) 
    {
        printf("Failed to open file %s!\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }
    if(0 != fseek(pInputFile, 0, SEEK_END)) 
    {
        printf("Failed to seek file %s.\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }
    fileLength = ftell(pInputFile);
    if(fileLength <= 0) 
    {
        printf("Failed to get size of file %s.\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }
    if(0 != fseek(pInputFile, 0, SEEK_SET)) 
    {
        printf("Failed to seek file %s.\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }

    pInputFileData = (unsigned char*)malloc(fileLength);
    if(NULL == pInputFileData) 
    {
        printf("Failed to malloc memory for file %s!\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }

    if(1 != fread(pInputFileData, fileLength, 1, pInputFile))
    {
        printf("Failed to read file %s!\n", pBinaryPath);
        ret = false;
        goto ScopeRelease;
    }

    // *******************************read rsa key
    if(!OpenMyFile(&fileRsaKey, pRSAPriKeyPath, "r"))
    {
        printf("Failed to open file %s!\n", pRSAPriKeyPath);
        ret = false;
        goto ScopeRelease;
    }
    
    rsa_init(&rsa, RSA_PKCS_V15, 0, NULL, NULL);

    if(TypePEM == keytype)
    {
        if(-1 == DecodePEMKey(fileRsaKey, &rsa, TypePrivate))
        {
            printf("%s (private key)!\n", PrintMsg[ErrorFailedToParsePEMFile]);
            goto ScopeRelease;
        }
    }
    else if(TypeBIN == keytype)
    {
        if(-1 == DecodeBINKey(fileRsaKey, rsa, TypePrivate))
        {
            printf("%s (private key)!\n", PrintMsg[ErrorFailedToParseBinFile]);
            goto ScopeRelease;
        }
    }
    else
    {
        if(-1 == ParseTxtRSAKey(rsa, HEX_RADIX, TypePrivate, fileRsaKey, pRSAPriKeyPath))
        {
            ret = false;
            goto ScopeRelease;
        }
    }

    // sign
    unsigned int rsaLen = BN_num_bytes(rsa->n);   //The length of signature depends on the length of n
    rsaBuf = (unsigned char*)malloc(rsaLen);
    if(NULL == rsaBuf)
    {
        printf("Failed to malloc %u memory for rsa Buffer\n", rsaLen);
        ret = false;
        goto ScopeRelease;
    }

    sha2(pInputFileData, fileLength, hash, 0);
    
    if(rsa_pkcs_oaep_sign(&rsa, rsaLen, hash, rsaBuf) != 0)
    {
        printf("Failed to make the sign!\n");
        ret = false;
        goto ScopeRelease;
    }
    
    // write to file
    if(!OpenMyFile(&pOutputFile, pOutputFilePath, "ab"))
    {
        printf("Failed to open file %s!\n", pOutputFilePath);
        ret = false;
        goto ScopeRelease;
    }
    bTempRet = (1 != fwrite(&fileLength, sizeof(unsigned int), 1, pOutputFile) 
        || (1 != fwrite(pInputFileData, fileLength, 1, pOutputFile)) 
        || (1 != fwrite(rsaBuf, rsaLen, 1, pOutputFile)));
            
    if (bTempRet)
    {
        printf("Failed to write file %s!\n", pOutputFilePath);
        ret = false;
        goto ScopeRelease;
    }
    
ScopeRelease:
    rsa_free(&rsa);
    SAFE_CLOSE(pInputFile);
    SAFE_FREE(pInputFileData);
    SAFE_FREE(rsaBuf);
    SAFE_CLOSE(pOutputFile);
    SAFE_CLOSE(fileRsaKey);
    return ret;
}

bool SignCryptoHi3519V101(SignHi3519V101Param *pParams)
{
    if (NULL == pParams)
    {
        return false;
    }
    
    FILE *pFinalBootFile = NULL;
    
    if(!OpenMyFile(&pFinalBootFile, pParams->pOutputFilePath, "wb+"))
    {
        printf("Failed to open or create final boot file %s.\n", pParams->pOutputFilePath);
        return false;
    }
    SAFE_CLOSE(pFinalBootFile)
    
    if(!ProcessKeyArea(pParams->pRSAPubKeyPath, pParams->pAESKeyPath, pParams->pOutputFilePath, pParams->RSAIOType, pParams->no_enc)) 
    {
        return false;
    }
    /////////no_enc
    if(!ProcessSignArea(pParams->pRSAPriKeyPath, pParams->pDrrInitPath, pParams->pOutputFilePath, pParams->RSAIOType)) 
    {
        return false;
    }
    
    long fileSize = GetSizeFromPath(pParams->pOutputFilePath);
    if (fileSize <= 0) 
    {
        return false;
    }
    else if (fileSize > MAX_HEAD_DDRINIT_SIZE) 
    {
        printf("Sum of key size and ddrInit size is greater than %d.\n", MAX_HEAD_DDRINIT_SIZE);
        return false;
    }

    if(!ProcessSignArea(pParams->pRSAPriKeyPath, pParams->pUBootPath, pParams->pOutputFilePath, pParams->RSAIOType)) 
    {
        return false;
    }
    return true;
}
