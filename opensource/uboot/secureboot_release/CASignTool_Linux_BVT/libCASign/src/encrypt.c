#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include "commonmethod.h"
#include "encrypt.h"
#define SM_LEY_LEN PATH_MAX
#define BINARY_SM_KEY_LEN 32

static unsigned char g_ID[16] =
    {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38};


int PaserCryptoConfigParameter(const char *strConfigFile, CRYPTO_CONFIG_PARAMETER_ST *pConfigPara)
{
    FILE *fp = NULL;
    char tmp[PATH_MAX] = {0};

    if(!(OpenMyFile(&fp, strConfigFile, "r")))
    {
        printf("Failed at proccess config file\n");
        return -1;
    }

    do
    {
        MEMSET_S(tmp, sizeof(tmp), 0, sizeof(tmp), "parsing config file", SAFE_CLOSE(fp); return -1;);
        if(NULL == fgets(tmp, PATH_MAX, fp))
        { 
            if(ferror(fp))
            {
               printf("Failed to get content of file.\n");
               SAFE_CLOSE(fp);
               return -1;
           }            
        }
        SpaceStrTrim(tmp);
        ParseFile(tmp, "External_private_key_file", pConfigPara->External_private_key_file, sizeof(pConfigPara->External_private_key_file));
        ParseFile(tmp, "Algorithm", pConfigPara->Algorithm, sizeof(pConfigPara->Algorithm));
        ParseFile(tmp, "Mode", pConfigPara->Mode, sizeof(pConfigPara->Mode));
        ParseFile(tmp, "OperationType", pConfigPara->OperationType, sizeof(pConfigPara->OperationType));
        ParseFile(tmp, "Key", pConfigPara->Key, sizeof(pConfigPara->Key));
        ParseFile(tmp, "IV", pConfigPara->IV, sizeof(pConfigPara->IV));

    }while(!feof(fp));

    SAFE_CLOSE(fp);

    return 0;
}

static int GetAlgtheMode(char *strAlgMode, MODE_EN *p)
{
    int ret = 0;
    if (!strcmp(strAlgMode, "CBC"))
    {
        *p = CBC;
    }
    else if (!strcmp(strAlgMode, "ECB"))
    {
        *p = ECB;
    }
    else if (!strcmp(strAlgMode, "CTR"))
    {
        *p = CTR;
    }
    else
    {
        ret = -1;
    }

    return ret;
}

int GetCryptoOperation(char *strCryptMode, CRYPT_EN *p)
{
    int ret = 0;
    if (!strcmp(strCryptMode, "Decrypt"))
    {
        *p = DECRYPT_OP;
    }
    else if (!strcmp(strCryptMode, "Encrypt"))
    {
        *p = ENCRYPT_OP;
    }
    else
    {
        ret = -1;
    }

    return ret;
}

static int GetAlgthem(char *strAlg, ALGORITHM_EN *p)
{
    int ret = 0;
    if (!strcmp(strAlg, "AES"))
    {
        *p = AES;
    }
    else if (!strcmp(strAlg, "TDES"))
    {
        *p = TDES;
    }
    else if (!strcmp(strAlg, "SHA1"))
    {
        *p = SHA1_ALG;
    }
    else if (!strcmp(strAlg, "SHA256"))
    {
        *p = SHA256_ALG;
    }
    else if (!strcmp(strAlg, "RSA"))
    {
        *p = RSA_ALG;
    }
    else if(!strcmp(strAlg, "RSA_VERIFY"))
    {
        *p = RSA_VERIFY;
    }  
    else
    {
        ret = -1;
    }

    return ret;
}

bool legalCryptoInputInput(CryptoParam* pCryptoParams) 
{
    bool tmpRet = true;
    if('\0' == *pCryptoParams->srcFilePath) 
    {
        printf("Please set your input filepath in format (-R srcFilePath).\n");
        tmpRet = tmpRet&&false;
    }

    if('\0' == *pCryptoParams->destFilePath) 
    {
        printf("Please set your output filepath in format (-R destFilePath).\n");
        tmpRet = tmpRet&&false;
    }
    tmpRet = (TypeTXT == pCryptoParams->inputType) || (TypeBIN == pCryptoParams->inputType);
    if(!tmpRet) 
    {
        printf("Please set your output filepath in format (-it t/b), t means txt, b means binary.\n");
    }
    
    tmpRet = (TypeTXT == pCryptoParams->outputType) || (TypeBIN == pCryptoParams->outputType);
    if(!tmpRet) 
    {
        printf("Please set your output filepath in format (-it t/b), t means txt, b means binary.\n");
    }
    return tmpRet;
    
}

int Sign_NonBOOTImage_Crypto(const char* strConfigFile, CryptoParam* pCryptoParams)
{
    int offset, i, index, fileNubmer = 0;
    int ret = 0;
    BOOL bTempRet;
    CRYPTO_CONFIG_PARAMETER_ST *pstConfigParam = (CRYPTO_CONFIG_PARAMETER_ST *) malloc(sizeof(CRYPTO_CONFIG_PARAMETER_ST));
    if (NULL == pstConfigParam)
    {
        printf("Failed to malloc for CRYPTO_CONFIG_PARAMETER_ST!\n");
        goto ScopeRelease;
    }

    ALGORITHM_EN stALGTHM;

    MEMSET_S(pstConfigParam, sizeof(CRYPTO_CONFIG_PARAMETER_ST), '\0', sizeof(CRYPTO_CONFIG_PARAMETER_ST), "initializing struct", goto ScopeRelease;);
    ret = PaserCryptoConfigParameter(strConfigFile, pstConfigParam);
    if (ret != 0)
    {
        printf("parse %s failed!\n", strConfigFile);
        goto ScopeRelease;
    }

    ret = GetAlgthem(pstConfigParam->Algorithm, &stALGTHM);
    if (ret !=  0)
    {
        printf("The parameter Algorithm is not correct. Please check it.\n");
        goto ScopeRelease;
    }

    bTempRet = (AES == stALGTHM|| TDES == stALGTHM || SM4 == stALGTHM);
   
    if (SHA1_ALG == stALGTHM)
    {
        ret = CryptoSHA1_ALG(pCryptoParams->destFilePath, pCryptoParams->srcFilePath);
    }
    else if (SHA256_ALG == stALGTHM)
    {
        ret = CryptoSHA256_ALG(pCryptoParams->destFilePath, pCryptoParams->srcFilePath);
    }
    else if (RSA_ALG == stALGTHM)
    {
        ret = CryptoRSA_ALG(pCryptoParams->keyFilePath, pCryptoParams->destFilePath, pCryptoParams->srcFilePath);
    }
    else if (RSA_VERIFY == stALGTHM)
    {
        ret = CryptoRSA_VERIFY(pCryptoParams->keyFilePath, pCryptoParams->srcFilePath, pCryptoParams->signatureFilePath);
    }
    else if (bTempRet)
    {
        if(!legalCryptoInputInput(pCryptoParams)) 
        {
            goto ScopeRelease;
        }
        ret = CryptoAES_TDES(pCryptoParams->destFilePath, pCryptoParams->srcFilePath, pCryptoParams->inputType, pCryptoParams->outputType, pstConfigParam);
    }
ScopeRelease:
    SAFE_FREE(pstConfigParam)
    return ret;
}

int CryptoSHA1_ALG(char *pDestPath, char *pSrcPath)
{
    BOOL bTempRet = (NULL == pSrcPath || NULL == pDestPath);
    if (bTempRet)
    {
        return -1;
    }

    int uwFileLength;
    unsigned char hash[32] = {0};
    int i;
    unsigned char ucTmpIV[16] = {0};

    FILE *fpInFile = NULL;
    unsigned char *pBuf = NULL;
    FILE *fpOutFile = NULL;
    int ret = -1;
    
    if(!OpenMyFile(&fpInFile, pSrcPath, "rb+"))
    {
        printf("Open file %s failed!\n", pSrcPath);
        goto ScopeRelease;
    }

    uwFileLength = GetMyFileSize(pSrcPath);
    if( uwFileLength <= 0) 
    {
        printf("Failed to clac the length.\n");
        goto ScopeRelease;
    }
    
    pBuf = (char *)malloc((unsigned int)uwFileLength);
    if (pBuf == NULL)
    {
        printf("Failed to malloc memory!!\n");
        goto ScopeRelease;
    }
    if(1 != fread(pBuf, (unsigned int)uwFileLength, 1, fpInFile)) 
    {
        printf("Failed to read file %s\n", pSrcPath);
        goto ScopeRelease;
    } 

    sha1(pBuf, (unsigned int)uwFileLength, hash); 
    if(!OpenMyFile(&fpOutFile, pDestPath, "wb"))
    {
        printf("Open the file %s failed\n", pDestPath);
        goto ScopeRelease;
    }

    for (i = 0; i < 20; i++)
    {
        SPRINTF_S((char*)ucTmpIV, sizeof(ucTmpIV), "0x%02x,", hash[i], "calculating hash", goto ScopeRelease;);
        if(1 != fwrite(ucTmpIV, 5, 1, fpOutFile)) 
        {
            printf("Failed to write file %s\n", pDestPath);
            goto ScopeRelease;
        }
    }
    printf("Succeed!\n");

    ret = 0;
ScopeRelease:
    SAFE_CLOSE(fpOutFile);
    SAFE_CLOSE(fpInFile);
    SAFE_FREE(pBuf);
    return ret;
}

int CryptoSHA256_ALG(char *pDestPath, char *pSrcPath)
{
    BOOL bTempRet = (NULL == pSrcPath || NULL == pDestPath);
    if (bTempRet)
    {
        return -1;
    }

    int uwFileLength;
    unsigned char hash[32] = {0};
    int i;
    unsigned char ucTmpIV[16] = {0};

    FILE *fpInFile = NULL;
    unsigned char *pBuf = NULL;
    FILE *fpOutFile = NULL;

    int ret = -1;
    
    if(!(OpenMyFile(&fpInFile, pSrcPath, "rb+")))
    {
        goto ScopeRelease;
    }
    uwFileLength = GetSizeFromPointer(fpInFile);
    if(uwFileLength <= 0)
    {
        goto ScopeRelease;
    }
    pBuf = (char *)malloc((unsigned int)uwFileLength);
    if (pBuf == NULL)
    {
        printf("Failed to malloc memory!!\n");
        goto ScopeRelease;
    }
    if(1 != fread(pBuf, (unsigned int)uwFileLength, 1, fpInFile)) 
    {
        printf("Failed to read file %s\n", pSrcPath);
        goto ScopeRelease;
    } 

    sha2(pBuf, (unsigned int)uwFileLength, hash, 0); 

    if(!OpenMyFile(&fpOutFile, pDestPath, "wb"))
    {
         printf("Open the file %s failed\n", pDestPath);
         goto ScopeRelease;
    }

    for (i = 0; i < 32; i++)
    {
        if(-1 == sprintf_s((char*)ucTmpIV, sizeof(ucTmpIV), "0x%02x%s", hash[i], ((i + 1) % 16) ? "," : "\n"))
        {
            printf("Failed to sprintf at hash!\n");
            goto ScopeRelease;
        }
        if(1 != fwrite(ucTmpIV, 5, 1, fpOutFile)) 
        {
            printf("Failed to write file %s\n", pDestPath);
            goto ScopeRelease;
        }
    }

    ret = 0;
    printf("Succeed!\n");
ScopeRelease:
    SAFE_CLOSE(fpOutFile)
    SAFE_CLOSE(fpInFile)
    SAFE_FREE(pBuf)
    return ret;
}

int CryptoRSA_VERIFY(char *RSAKeyFilePath, char *pSrcFilePath, char *signatureFilePath)
{
    BOOL bTempRet = (NULL == RSAKeyFilePath || NULL == pSrcFilePath || NULL == signatureFilePath);
    if(bTempRet)
    {
        printf("NULL input!\n");
        return -1;
    }

    int ret = -1;
    FILE *fpSrcFile = NULL;
    FILE *fpSignatureFile = NULL;
    UCHAR *fileBuffer = NULL;
    UCHAR signature[SIGNATURE_LEN] = {0};

    rsa_context rsa = NULL;

    if(!ParseTxtRSAKeyFile(RSAKeyFilePath, &rsa, TypePublic))
    {
        printf("Failed to read key file %s\n", RSAKeyFilePath);
        goto ScopeRelease;
    }

    //read source file to buffer
    if(!OpenMyFile(&fpSrcFile, pSrcFilePath, "rb+"))
    {
        printf("Failed to open src File!\n");
        goto ScopeRelease;
    }
    long fileSize = GetSizeFromPointer(fpSrcFile);
    if(fileSize <= 0)
    {
        printf("Failed to get file size from file pointer!\n");
        goto ScopeRelease;
    }
    fileBuffer = (UCHAR *) malloc(fileSize);
    if(NULL == fileBuffer)
    {
        printf("Failed to malloc for file buffer!\n");
        goto ScopeRelease;
    }
    
    if(1 != fread(fileBuffer, fileSize, 1, fpSrcFile)) 
    {
        printf("Failed to read from src file!\n");
        goto ScopeRelease;
    }
    //read signature
    if(!OpenMyFile(&fpSignatureFile, signatureFilePath, "rb+"))
    {
        printf("Failed to read from signature file!\n");
        goto ScopeRelease;
    }
    if(1 != fread(signature, SIGNATURE_LEN, 1, fpSignatureFile)) 
    {
        printf("Failed to read from src file!\n");
        goto ScopeRelease;
    }

    if(0 == authDataSign(&rsa, fileBuffer, fileSize, signature))
    {
        printf("RSA verify succeessfully!\n");
    }
    else
    {
        printf("RSA verify Fiailed!\n");
        goto ScopeRelease;
    }
    ret = 0;
ScopeRelease:
    SAFE_CLOSE(fpSrcFile);
    SAFE_CLOSE(fpSignatureFile);      
    SAFE_FREE(fileBuffer);
    return ret;
}

int CryptoRSA_ALG(char *rsakeyfilepath, char *pDestPath, char *pSrcPath)
{
    BOOL bTempRet = (NULL == pSrcPath || NULL == pDestPath);
    if (bTempRet)
    {
        return -1;
    }

    unsigned char rsaBuf[SIGNATURE_LEN] = {0};
    int uwFileLength;
    unsigned char hash[32] = {0};
    int ret;

    rsa_context rsa = NULL;
    FILE *fpInFile = NULL;
    FILE *fpOutFile = NULL;
    unsigned char *pBuf = NULL;

    int result = -1;

    if(!ParseTxtRSAKeyFile(rsakeyfilepath, &rsa, TypePrivate))
    {
        goto ScopeRelease;
    }

    if(!(OpenMyFile(&fpInFile, pSrcPath, "rb+")))
    {
        goto ScopeRelease;
    }

    uwFileLength = GetSizeFromPointer(fpInFile);

    if(uwFileLength <= 0) 
    {
        goto ScopeRelease;
    }
    pBuf = (char *)malloc((unsigned int)uwFileLength);
    if (pBuf == NULL)
    {
        printf("Failed to malloc memory!\n");
        goto ScopeRelease;
    }
    if(1 != fread(pBuf, (unsigned int)uwFileLength, 1, fpInFile)) 
    {
        printf("Failed to read file %s!\n", pSrcPath);        
        goto ScopeRelease;
    } 

    sha2(pBuf, (unsigned int)uwFileLength, hash, 0); 

    if((ret = rsa_pkcs1_sign(&rsa, RSA_PRIVATE, SIG_RSA_SHA256, 32, hash, rsaBuf)) != 0)
    {
        printf("Failed to sign the file %s!\n", pSrcPath);
        goto ScopeRelease;
    }

    if(!OpenMyFile(&fpOutFile, pDestPath, "wb"))
    {
        printf("Open the file %s failed\n", pDestPath);
        goto ScopeRelease;
    }

    if(1 != fwrite(rsaBuf, SIGNATURE_LEN, 1, fpOutFile)) 
    {
        printf("Failed to write file %s\n", pDestPath);
        goto ScopeRelease;
    } 
    printf("RSA sign successfully!\n");

    result = 0;
ScopeRelease:
    SAFE_CLOSE(fpOutFile);
    SAFE_CLOSE(fpInFile);
    SAFE_FREE(pBuf);
    rsa_free(&rsa);

    return result;
}

int CryptoAES_TDES(char *pDestPath, char *pSrcPath, unsigned int inputType, unsigned int outputType, CRYPTO_CONFIG_PARAMETER_ST* pstConfigPara)
{
    BOOL bTempRet = (NULL == pSrcPath || NULL == pDestPath || NULL == pstConfigPara);
    if (bTempRet)
    {
        return -1;
    }

    int i;

    ALGORITHM_EN stALGTHM;
    int result = GetAlgthem(pstConfigPara->Algorithm, &stALGTHM);
    if (result !=  0)
    {
        printf("The parameter Algorithm is not correct. Please check it.\n");
        return -1;
    }   

    MODE_EN stMode;
    CRYPT_EN stOPeration;

    unsigned char ucKey[16] = {0}; //for AES, it is 16; for TDES, 8
    unsigned char ucIV[16] = {0};

    int offset = 0;
    if(!getCryptoParam(pstConfigPara, &stMode, &stOPeration, ucKey, ucIV))
    {
        return -1;
    } 

    //ALGORITHM_EN stALGTHM;
    unsigned int ucBlockLength;

    FILE* fpInFile = NULL;
    int uwFileLength;
    
    unsigned char *pbinBuf = NULL;
    unsigned char *ptxtBuf = NULL;
    FILE* fpOutFile = NULL;
    unsigned long uwDataLength;
    unsigned char *pbinOutputBuf = NULL;
    unsigned char *ptxtOutputBuf = NULL;
    unsigned char ucTmpIV[16] = {0};

    int ret = 0;

    if(!setBlockLength(stALGTHM, &ucBlockLength))
    {
        return -1;
    }    
    
    if(!readInputFile(inputType, &ptxtBuf, &uwFileLength, pSrcPath, &pbinBuf, &fpInFile))
    {
        ret = -1;
        goto ScopeRelease;
    }
    
    if(uwFileLength <= 0)
    {
        printf("You can not malloc a memory with size less than or equal to 0\n");
        ret = -1;
        goto ScopeRelease;
    }
    if(SM4 != stALGTHM)
    {
        uwDataLength = (uwFileLength / ucBlockLength) * ucBlockLength;
    }
    else
    {
        uwDataLength = uwFileLength;
    }
    pbinOutputBuf = (unsigned char *)malloc((unsigned int)uwFileLength);

    if (NULL == pbinOutputBuf)
    {
        printf("Failed to malloc memory!!\n");
        ret = -1;
        goto ScopeRelease;
    }
    
    MEMSET_S(pbinOutputBuf, (unsigned int)uwFileLength, 0, (unsigned int)uwFileLength, "crypto", ret = -1; goto ScopeRelease;);
    MEMCPY_S(pbinOutputBuf, uwFileLength, pbinBuf, (unsigned int)uwFileLength, "crypto", ret = -1; goto ScopeRelease;);
    MEMCPY_S(ucTmpIV, sizeof(ucTmpIV), ucIV, 16, "crypto", ret = -1; goto ScopeRelease;);

    crypto(stALGTHM, stOPeration, stMode, ucKey, ucTmpIV, pbinBuf, pbinOutputBuf, uwDataLength);

    if(!writeOutputFile(outputType, &fpOutFile, pDestPath, pbinOutputBuf, &ptxtOutputBuf, uwFileLength))
    {
        ret = -1;
    }
    if(-1 == ret)
    {
        printf("Crypto failed!\n");
    }
    else
    {
        printf("Crypto successfully!\n");
    }
ScopeRelease:
    SAFE_FREE(pbinOutputBuf)
    SAFE_FREE(ptxtOutputBuf)
    SAFE_FREE(pbinBuf)
    SAFE_FREE(ptxtBuf)
    SAFE_CLOSE(fpOutFile)
    SAFE_CLOSE(fpInFile)
    return ret;
}

BOOL writeOutputFile(UINT outputType, FILE **fpOutFile, char *pDestPath, UCHAR *pbinOutputBuf, UCHAR **ptxtOutputBuf, int uwFileLength)
{    
    if(TypeTXT == outputType) 
    {
        int length = 0;
        if(!OpenMyFile(fpOutFile, pDestPath, "wb"))
        {
            printf("Create file %s failed!\n", pDestPath);
            return FALSE;
        }
        length = uwFileLength*2;
        if(length <= 0)
        {
            printf("Can't malloc with size zero!\n");
            return FALSE;
        }
        *ptxtOutputBuf = (unsigned char *)malloc((unsigned int)length);
                
        if(NULL == *ptxtOutputBuf) 
        {
            printf("Failed to malloc memory!!\n");
            return FALSE;
        }
        MEMSET_S(*ptxtOutputBuf, (unsigned int)length, 0, (unsigned int)length, "", return FALSE;);
        
        if(!BinaryToTxt(pbinOutputBuf, *ptxtOutputBuf, (unsigned int)uwFileLength, (unsigned int)length))
        {
            return FALSE;
        }
            
        if(1 != fwrite(*ptxtOutputBuf, (unsigned int)length, 1, *fpOutFile)) 
        {
            printf("Failed to write file %s\n", pDestPath);
            return FALSE;
        }
    }
    else
    {
        if(!OpenMyFile(fpOutFile, pDestPath, "wb"))
        {
            printf("Open the file %s failed\n", pDestPath);
            return FALSE;
        }
        if(1 != fwrite(pbinOutputBuf, (unsigned int)uwFileLength, 1, *fpOutFile)) 
        {
            printf("Failed to write file %s\n", pDestPath);
            return FALSE;
        }
    }
    return TRUE;
}

void crypto(ALGORITHM_EN stALGTHM, CRYPT_EN stOPeration, MODE_EN stMode, UCHAR *ucKey, 
                    UCHAR* ucTmpIV, UCHAR *pbinBuf, UCHAR *pbinOutputBuf, unsigned long uwDataLength)
{
    int i;
    aes_context ctx;
    des3_context des3ctx;    
    switch(stALGTHM)
    {
    case AES:
        if (ENCRYPT_OP == stOPeration)
        {
            aes_setkey_enc(&ctx, ucKey, KEY_BIT_LEN);

            if (CBC == stMode)
            {
                aes_crypt_cbc(&ctx, AES_ENCRYPT, uwDataLength, ucTmpIV, pbinBuf, pbinOutputBuf);
            }
            else if (ECB == stMode)
            {
                for (i = 0; i < uwDataLength / 16; i++)
                {
                    aes_crypt_ecb(&ctx, AES_ENCRYPT, pbinBuf + 16 * i, pbinOutputBuf + 16 * i);
                }
            }
            else if(CTR == stMode)
            {
                 aes_crypt_ctr(&ctx, (int)uwDataLength, ucTmpIV, (unsigned char*)pbinBuf, (unsigned char*)pbinOutputBuf);
            }
        }
        else if (DECRYPT_OP == stOPeration)
        {
            if (CBC == stMode)
            {
                aes_setkey_dec(&ctx, ucKey, KEY_BIT_LEN);
                aes_crypt_cbc(&ctx, AES_DECRYPT, uwDataLength, ucTmpIV, pbinBuf, pbinOutputBuf);
            }
            else if (ECB == stMode)
            {
                aes_setkey_dec(&ctx, ucKey, KEY_BIT_LEN);
                for (i = 0; i < uwDataLength / 16; i++)
                {                   
                    aes_crypt_ecb(&ctx, AES_DECRYPT, pbinBuf + 16 * i, pbinOutputBuf + 16 *  i);
                }
            }
            else if (CTR == stMode)
            {
                aes_setkey_enc(&ctx, ucKey, KEY_BIT_LEN);
                aes_crypt_ctr(&ctx, (int)uwDataLength, ucTmpIV, (unsigned char*)pbinBuf, (unsigned char*)pbinOutputBuf);
            }
        }
        break;
    case TDES:
        if (ENCRYPT_OP == stOPeration)
        {
            des3_set2key_enc(&des3ctx, ucKey);
    
            if(CBC == stMode)
            {
                des3_crypt_cbc(&des3ctx, DES_ENCRYPT, uwDataLength, ucTmpIV, pbinBuf, pbinOutputBuf);
            }
            else if(ECB == stMode)
            {
                for(i = 0; i < uwDataLength / 8; i++)
                {
                    des3_crypt_ecb(&des3ctx, pbinBuf + 8 * i, pbinOutputBuf + 8 * i);
                }
            }
        }
        else if (DECRYPT_OP == stOPeration)
        {
            des3_set2key_dec(&des3ctx, ucKey);
            if(CBC == stMode)
            {
                des3_crypt_cbc(&des3ctx, DES_DECRYPT, uwDataLength, ucTmpIV, pbinBuf, pbinOutputBuf);
            }
            else if(ECB == stMode)
            {
                for(i = 0; i < uwDataLength / 8; i++)
                {
                    des3_crypt_ecb(&des3ctx, pbinBuf+ 8 * i, pbinOutputBuf + 8 * i);
                }
            }
        }
        break;
    }
}


BOOL readInputFile(UINT inputType, UCHAR **ptxtBuf, UINT *uwFileLength, char *pSrcPath, UCHAR **pbinBuf, FILE **fpInFile)
{
    int i;
    int srcFileLength = 0;
    if(!OpenMyFile(fpInFile, pSrcPath, "rb+"))
    {
        return FALSE;
    }
    srcFileLength = GetSizeFromPointer(*fpInFile);
    if ( srcFileLength<= 0) 
    {
        return FALSE;
    }
    *uwFileLength = srcFileLength;
    if(*uwFileLength <= 0)
    {
        printf("Can't malloc with size zero!\n");
        return FALSE;
    }
    if(TypeTXT == inputType) 
    {
        *ptxtBuf = (UCHAR *)malloc((UINT)*uwFileLength);
        if (NULL == *ptxtBuf)
        {
            printf("Failed to malloc memory!!\n");
            return FALSE;
        }
        if(1 == (UINT)*uwFileLength % 2)
        {
            printf("Invalid length of input file %s!\n", pSrcPath);
            return FALSE;
        }
        if(1 != fread(*ptxtBuf, (UINT)*uwFileLength, 1, *fpInFile)) 
        {
             printf("Failed to read file %s\n", pSrcPath);
             return FALSE;
        }
        *uwFileLength = (UINT)*uwFileLength/2;
        *pbinBuf = (UCHAR *)malloc((UINT)*uwFileLength);
        if (NULL == *pbinBuf)
        {
            printf("Failed to malloc memory!!\n");
            return FALSE;
        }
        for (i = 0; i < (UINT)*uwFileLength; i++)
        {
            (*pbinBuf)[i] = Char2Hex(&((*ptxtBuf)[i * 2]));
        }
    }
    else 
    {
        *pbinBuf = (UCHAR *)malloc((UINT)*uwFileLength);
        if ( NULL == *pbinBuf)
        {
            printf("Failed to malloc memory!!\n");
            return FALSE;
        }
        if(1 != fread(*pbinBuf, (UINT)*uwFileLength, 1, *fpInFile)) 
        {
            printf("Failed to read file %s\n", pSrcPath);
            return FALSE;
        }
    }
    return TRUE;
}

BOOL setBlockLength( ALGORITHM_EN stALGTHM, UINT *ucBlockLength)
{
    if (stALGTHM == AES)
    {
       *ucBlockLength = 16;
    }
    else if (stALGTHM == TDES)
    {
       *ucBlockLength = 8;
    }
    else if(stALGTHM != SM4)
    {
        printf("Unknown Algorithm!\n");
        return FALSE;
    }
    return TRUE;
}


BOOL getCryptoParam(CRYPTO_CONFIG_PARAMETER_ST* stConfigPara, MODE_EN *stMode, CRYPT_EN *stOPeration, unsigned char *ucKey, unsigned char *ucIV)
{
    UINT uwKeylength = 16;
    int result = GetAlgtheMode(stConfigPara->Mode, stMode);
    int offset = 0;
    int i;
    BOOL bTempRet;
    if (result !=  0)
    {
        printf("The parameter Mode is not correct. Please check it.\n");
        return FALSE;
    }
    result = GetCryptoOperation(stConfigPara->OperationType, stOPeration);
    if (result !=  0)
    {
        printf("The parameter OperationType is not correct. Please check it.\n");
        return FALSE;
    }
    // key
    if(!IsValidHex(stConfigPara->Key)) 
    {
        printf("Key should be HEX\n");
        return FALSE;
    }
    bTempRet= (strncmp(stConfigPara->Key, "0x", 2) == 0) || (strncmp(stConfigPara->Key, "0X", 2) == 0);
    
    offset = bTempRet ? 2 : 0;

    if (strlen(stConfigPara->Key + offset) != 32 )
    {
        printf("The length of Key must be 32, please check it!\n");
        return FALSE;
    }
    
    for (i = 0; i < uwKeylength; i++)
    {
        ucKey[i] = Char2Hex(&(stConfigPara->Key[i * 2 + offset]));
    }
    
    if(CBC == *stMode) 
    {
        // IV
        bTempRet = (strncmp(stConfigPara->IV, "0x", 2) == 0) || (strncmp(stConfigPara->IV, "0X", 2) == 0);
        offset = bTempRet ? 2 : 0;
        if (strlen(stConfigPara->IV + offset) != 32)
        {
            printf("The length of IV must be 32, please check it!\n");
            return FALSE;
        }
        if(!IsValidHex(stConfigPara->IV)) 
        {
            printf("IV should be HEX\n");
            return FALSE;
        }
        for (i = 0; i < uwKeylength; i++)
        {
            ucIV[i] = Char2Hex(&(stConfigPara->IV[i * 2 + offset]));
        }
    }
    return TRUE;
}


bool getAlgorithm(const char *algorithm, ALGORITHM_EN *encAlgorithm, int *alignSize)
{
    if(0 == strncmp(algorithm, "AES", sizeof("AES")))
    {
        *encAlgorithm = AES;
        *alignSize = 16;
    }
    else if(0 == strncmp(algorithm, "TDES", sizeof("TDES")))
    {
        *encAlgorithm = TDES;
        *alignSize = 8; 
    }
    else
    {
        return false;
    }
    return true;
}

int DecodePEMKey(FILE *pKeyFile, rsa_context *rsa, RSAKeyType RSAType)
{
    unsigned char tmp[PATH_MAX] = {0};
    unsigned char *publicKeyBuf = NULL;
    unsigned char *publicKeyDecodeOutput = NULL;

    unsigned char *tmpPublicKeyBuf = NULL;
    
    size_t outputLength = 0;
    
    int ret = -1;
    long fileSize = 0;
    unsigned int leftSize = 0;
    unsigned int tmpLength = 0;
    unsigned int publicKeyBufOffset = 0;
    unsigned int offset = 0;
    int i;
    
    fileSize = GetSizeFromPointer(pKeyFile);
    if(fileSize < 0)
    {
        printf("%s!\n", PrintMsg[ErrorWrongFileSize]);
        return ret;
    }
        
    tmpPublicKeyBuf = (UCHAR*) malloc(fileSize);
    if (NULL == tmpPublicKeyBuf)
    {
        printf("%s public key temp buffer!\n", PrintMsg[ErrorFailedToMalloc]);
        return ret;
    }

    MEMSET_S(tmpPublicKeyBuf, fileSize, 0, fileSize, "decoding pem key", goto ScopeRelease;);

    do
    {
        if (NULL == fgets(tmp, PATH_MAX, pKeyFile))  
        {
            if(ferror(pKeyFile))
            {
                printf("Failed to get content of file.\n");
                goto ScopeRelease;
            }
        }
        tmpLength = strlen(tmp);
                
        if (tmpLength > 0)
        {   
            if (TypePrivate == RSAType)
            {
                if (0 == strncmp(tmp, BEGINPRIVATEKLEY, strlen(BEGINPRIVATEKLEY)))   //sizeof may not fit here
                {
                    leftSize += tmpLength;
                    continue;
                }
                if (0 == strncmp(tmp, ENDPRIVATEKLEY, strlen(ENDPRIVATEKLEY)))
                {
                    break;
                }
            }
            else if (TypePublic == RSAType)
            {
                if (0 == strncmp(tmp, BEGINPUBLICKEY, strlen(BEGINPUBLICKEY)))
                {
                    leftSize += tmpLength;
                    continue;
                }
                if (0 == strncmp(tmp, ENDPUBLICKEY, strlen(ENDPUBLICKEY)))
                {
                    break;
                }
            }
            
            if (tmpLength > 2)   
            {
                if ((LF_ASCII == tmp[tmpLength-1]) && (CR_ASCII == tmp[tmpLength-2]))
                {
                    offset = 2;
                    leftSize += 2;
                }
                else if ((LF_ASCII == tmp[tmpLength-1]) && (CR_ASCII != tmp[tmpLength-2]))
                {
                    offset = 1;
                    leftSize += 1;
                }
                else
                {
                    printf("%s\n", PrintMsg[ErrorWrongPEMFormat]);
                    goto ScopeRelease;
                }
            }
            else if (2 == tmpLength)
            {
                if((LF_ASCII == tmp[1]) && (CR_ASCII != tmp[0]))
                {
                    offset = 1;
                    leftSize += 1;
                }
                else
                {
                    printf("%s\n", PrintMsg[ErrorWrongPEMFormat]);
                    goto ScopeRelease;
                }
            }
            else
            {
                printf("%s\n", PrintMsg[ErrorWrongPEMFormat]);
                goto ScopeRelease;
            }

            MEMCPY_S(tmpPublicKeyBuf + publicKeyBufOffset, fileSize - publicKeyBufOffset, tmp, tmpLength - offset, "decoding pem key", goto ScopeRelease;);
        
            publicKeyBufOffset += (tmpLength - offset);
        
        }
        tmpLength = 0;  
        offset = 0;
    }while(!feof(pKeyFile));  

    if(0 == publicKeyBufOffset)
    {
        printf("The size of real key buffer should not be zero!\n");
        goto ScopeRelease;
    }
    publicKeyBuf = (UCHAR *)malloc(publicKeyBufOffset);
    if (NULL == publicKeyBuf)
    {
        printf("%s public key buffer!\n", PrintMsg[ErrorFailedToMalloc]);
        goto ScopeRelease;
    }
    MEMSET_S(publicKeyBuf, publicKeyBufOffset, 0, publicKeyBufOffset, "decoding pem key", goto ScopeRelease;);
    MEMCPY_S(publicKeyBuf, publicKeyBufOffset, tmpPublicKeyBuf, publicKeyBufOffset, "decoding pem key", goto ScopeRelease;);
    
    Base64Decode(publicKeyBuf, publicKeyBufOffset, &publicKeyDecodeOutput, &outputLength);
    if (0 == outputLength)
    { 
        printf("%s!\n", PrintMsg[ErrorZeroMallocSize]);
        goto ScopeRelease;
    }

    if (-1 == ParseBinRSAKey(publicKeyDecodeOutput, rsa, outputLength, RSAType))
    {
        goto ScopeRelease;
    }
    
    ret = 0;
ScopeRelease:
    SAFE_FREE(publicKeyBuf)
    SAFE_FREE(tmpPublicKeyBuf)
    SAFE_FREE(publicKeyDecodeOutput)
    return ret;
}

int DecodeBINKey(FILE *pRSAPubKeyFile, rsa_context rsa, RSAKeyType KeyType)
{
    long fileSize = 0;
    int ret = -1;
    unsigned char *TmpPublicKeyBinBuf = NULL;

    if(NULL == pRSAPubKeyFile)
    {
        printf("Null input parameter!\n");
        return 0;
    }
    
    fileSize = GetSizeFromPointer(pRSAPubKeyFile);
    if (fileSize <= 0)
    {
        printf("%s!\n", PrintMsg[ErrorWrongFileSize]);
        return ret;
    }
    TmpPublicKeyBinBuf = (UCHAR *)malloc(fileSize);
    if (NULL == TmpPublicKeyBinBuf)
    {
        printf("%s public key binary buffer!\n", PrintMsg[ErrorFailedToMalloc]);
        return ret;
    }
    if (1 != fread(TmpPublicKeyBinBuf, fileSize, 1, pRSAPubKeyFile))
    {
        printf("%s RSA public key file!\n", PrintMsg[ErrorFailedToRead]);
        SAFE_FREE(TmpPublicKeyBinBuf)
        return ret;
    }
    
    if (-1 == ParseBinRSAKey(TmpPublicKeyBinBuf, &rsa, fileSize, KeyType))
    {
        SAFE_FREE(TmpPublicKeyBinBuf)
        return ret;
    }   
    SAFE_FREE(TmpPublicKeyBinBuf)
    return 0;
}


int ParseBinRSAKey(unsigned char* PublicKeyDecodeBinBuf, rsa_context *rsa, 
                                   size_t InputLength, RSAKeyType KeyType)
{
    if (NULL == PublicKeyDecodeBinBuf)
    {
        printf("%s!\n", PrintMsg[ErrorNullInputMsg]);
        return -1;
    }
    UCHAR *Modulus = NULL;
    UCHAR *PublicExponent = NULL;
    short *ShortValue;  //2Bytes
    unsigned char CharValueh;
    unsigned char CharValuel;
    int LengthOfN;
    int LengthOfE;
    unsigned int LengthOfValue = 0;
    int index = 0;
    int i = 0;
    int ret = -1;
    UCHAR *pRSAKey[8] = {0};
    for(i = 0; i < 8; i++)
    {
        pRSAKey[i] = (UCHAR *) malloc(2048);
        if(NULL == pRSAKey[i])
        {
            printf("Failed to malloc at %d!\n", i+1);
            goto ScopeRelease;
        }
    }    
    int numOfRSAKeyLen[8]= {0};
    
    int max = (TypePrivate == KeyType) ? PARAMNUMFORPRIV : PARAMNUMFORPUB;
    i=0;
    while ((index < InputLength) && (i < max))
    {
        if((0x30 == PublicKeyDecodeBinBuf[index])||(0x03 == PublicKeyDecodeBinBuf[index]))
        {
            index++;
            if (0x82 == PublicKeyDecodeBinBuf[index])
            {
                index++;
                CharValueh = PublicKeyDecodeBinBuf[index];  //high bit of sum length
                index++;
                CharValuel = PublicKeyDecodeBinBuf[index];  // low bit of sum length
                LengthOfValue = (CharValueh)*0x100 + CharValuel;   // sum length
            }
            else if (0x81 == PublicKeyDecodeBinBuf[index])
            {
                index++;
                LengthOfValue = PublicKeyDecodeBinBuf[index];
            }
            else
            {
                LengthOfValue = PublicKeyDecodeBinBuf[index];
            }
            index++;
        }
        else if (0x00 == PublicKeyDecodeBinBuf[index])
        {
            index++;
        } 
        else if (0x02 == PublicKeyDecodeBinBuf[index])
        {
            index++;
            if (0x82 == PublicKeyDecodeBinBuf[index])
            {
                index++;
                CharValueh = PublicKeyDecodeBinBuf[index];
                index++;
                CharValuel = PublicKeyDecodeBinBuf[index];
                index++;
                LengthOfValue = CharValueh*0x100 + CharValuel;  //get low 8 bit
            }
            else if (0x81 == PublicKeyDecodeBinBuf[index])
            {
                index++;
                LengthOfValue = PublicKeyDecodeBinBuf[index];
                index++;
            }
            else
            {
                LengthOfValue = PublicKeyDecodeBinBuf[index];
                index++;
            }
            if((0 == i)&&(LengthOfValue < 0x100))
            {
                index += LengthOfValue;
                continue;
            }
            //out of range might exist!
            MEMCPY_S(pRSAKey[i], 2048, PublicKeyDecodeBinBuf+index, LengthOfValue, "parsing binary RSA key", goto ScopeRelease;);

            index += LengthOfValue;

            numOfRSAKeyLen[i] = LengthOfValue;

            i++;
            
        }
        else
        {
             index += LengthOfValue;
        }
    
    }
   
    (*rsa)->n = BN_bin2bn(pRSAKey[0], numOfRSAKeyLen[0], (*rsa)->n);
    (*rsa)->e = BN_bin2bn(pRSAKey[1], numOfRSAKeyLen[1], (*rsa)->e);
    if (TypePrivate == KeyType)
    {
        (*rsa)->d = BN_bin2bn(pRSAKey[2], numOfRSAKeyLen[2], (*rsa)->d);
        (*rsa)->p = BN_bin2bn(pRSAKey[3], numOfRSAKeyLen[3], (*rsa)->p);
        (*rsa)->q = BN_bin2bn(pRSAKey[4], numOfRSAKeyLen[4], (*rsa)->q);
        (*rsa)->dmp1 = BN_bin2bn(pRSAKey[5], numOfRSAKeyLen[5], (*rsa)->dmp1);
        (*rsa)->dmq1 = BN_bin2bn(pRSAKey[6], numOfRSAKeyLen[6], (*rsa)->dmq1);
        (*rsa)->iqmp = BN_bin2bn(pRSAKey[7], numOfRSAKeyLen[7], (*rsa)->iqmp); 
    }
    ret = 0;
ScopeRelease:
    for(i = 0; i < 8; i++)
    {
        SAFE_FREE(pRSAKey[i])
    }
    return ret;
}

int WriteTxtRSAKey(rsa_context rsa, int radix, RSAKeyType keyType, FILE * fOut, const char* strFileName)
{
    if(0 != mpi_write_file("N = ", &(rsa)->n, radix, fOut))
    {
        printf("Failed to write N value to %s!\n", strFileName);
        return -1;
    }
    if(0 != mpi_write_file("E = ", &(rsa)->e, radix, fOut))
    {
        printf("Failed to write E value to %s!\n", strFileName);
        return -1;
    }
    if(TypePrivate == keyType)
    {
        if(0 != mpi_write_file("D = ", &(rsa)->d, radix, fOut))
        {
            printf("Failed to write D value to %s!\n", strFileName);
            return -1;
        }
        if(0 != mpi_write_file("P = ", &(rsa)->p, radix, fOut))
        {
            printf("Failed to write P value to %s!\n", strFileName);
            return -1;
        }
        if(0 != mpi_write_file("Q = ", &(rsa)->q, radix, fOut))
        {
            printf("Failed to write Q value to %s!\n", strFileName);
            return -1;
        }
        if(0 != mpi_write_file("DP = ", &(rsa)->dmp1, radix, fOut))
        {
            printf("Failed to write DP value to %s!\n", strFileName);
            return -1;
        }
        if(0 != mpi_write_file("DQ = ", &(rsa)->dmq1, radix, fOut))
        {
            printf("Failed to write DQ value to %s!\n", strFileName);
            return -1;
        }
        if(0 != mpi_write_file("QP = ", &(rsa)->iqmp, radix, fOut ))
        {
            printf("Failed to write DP value to %s!\n", strFileName);
            return -1;
        }
    }
    return 0;
}

int ParseTxtRSAKey(rsa_context rsa, int radix, RSAKeyType keyType, FILE * fin, const char* strFileName)
{
    if(0 != mpi_read_file(&rsa->n, radix, fin)) 
    {  
        printf("Failed to read rsa n value from %s!\n", strFileName);
        return -1;
    }
 
    if(0 != mpi_read_file(&rsa->e, radix, fin)) 
    {  
        printf("Failed to read rsa e value from %s!\n", strFileName);
        return -1;
    }
    
    if(TypePrivate == keyType)
    {
        if(0 != mpi_read_file(&rsa->d , radix, fin))
        {
            printf("Failed to read rsa d value from %s!\n", strFileName);
            return -1;   
        }
        
        if(0 != mpi_read_file(&rsa->p , radix, fin))
        {
            printf("Failed to read rsa p value from %s!\n", strFileName);
            return -1;
        }
        if(0 != mpi_read_file(&rsa->q, radix, fin))
        {
            printf("Failed to read rsa q value from %s!\n", strFileName);
            return -1;
        }
        if(0 != mpi_read_file(&rsa->dmp1, radix, fin))
        {
            printf("Failed to read rsa dmp1 value from %s!\n", strFileName);
            return -1;
        }
        if(0 != mpi_read_file(&rsa->dmq1, radix, fin))
        {
            printf("Failed to read rsa dmq1 value from %s!\n", strFileName);
            return -1;
        }
        if(0 != mpi_read_file(&rsa->iqmp, radix, fin))
        {
            printf("Failed to read rsa iqmp value from %s!\n", strFileName);
            return -1;
        }
    }
    return 0;
}

bool ParseTxtRSAKeyFile(const char *strKeyFilePath, rsa_context *rsa, RSAKeyType keyType)
{
    bool ret                      = false;
    FILE *fpRsaKey                = NULL; 
    
    if(!OpenMyFile(&fpRsaKey, strKeyFilePath, "r+"))
    {
        printf("Failed to open RSA key file.\n");
        goto ScopeRelease;
    }

    rsa_init(rsa, RSA_PKCS_V15, 0, NULL, NULL);

    if(-1 == ParseTxtRSAKey(*rsa, HEX_RADIX, keyType, fpRsaKey, strKeyFilePath))
    {
        goto ScopeRelease;
    }
    ret = true;
ScopeRelease:
    SAFE_CLOSE(fpRsaKey);
    return ret;     
}

int ParseTxtSMItemKeyFile(unsigned int *sm, FILE *fp)
{
    char strSMKey[SM_LEY_LEN + 1] = {0};
    int slen = 0;
    char *p;
    if(NULL == fgets(strSMKey, SM_LEY_LEN, fp))
    {
        printf("Failed to get content of file.\n");
        return -1;
    }
    
    SpaceStrTrim(strSMKey);
    
    slen = strlen(strSMKey);
    if(slen < 2)
    { 
        return -1;
    }
    if('\n' == strSMKey[slen - 1]) 
    { 
        slen--; 
        strSMKey[slen] = '\0'; 
    }
    if('\r' == strSMKey[slen - 1]) 
    { 
        slen--; 
        strSMKey[slen] = '\0'; 
    }
    p = strSMKey + 2;
    int binaryLen = (slen-2)/2;
    if(binaryLen != BINARY_SM_KEY_LEN)
    {
        printf("Wrong sm key format, each length should be with eight bytes!\n");
        return -1;
    }
    char binaryKey[BINARY_SM_KEY_LEN] = {0};
    if(!TxtKeyToBinKey(p, binaryKey, binaryLen))
    {
        printf("Failed to transform sm key to binary!\n");
        return -1;
    }

    MEMCPY_S(sm, BINARY_SM_KEY_LEN, binaryKey, binaryLen, "parsing SM item", return -1;);
    return 0;
}

