#ifndef COMMONMETHOD_H
#define COMMONMETHOD_H

#ifdef __cplusplus
extern "C" {
#endif

//#include <securec.h>
#include <stdbool.h>
#include <stdlib.h>
#include "priv_signTool.h"
#include <limits.h>   //new added
#include <fcntl.h>

#ifdef __cplusplus
extern "C" {
#endif
#include "../../opensrc/openssl/adapter.h"
#ifdef __cplusplus
}
#endif


#define SRC_FILE_NUM (32)
#define SRC_NUM (10)
#define SIGNATURE_LEN (0x100)
#define BOOT_MAX_LEN (0x100000)
#define AES_ENCRYPT_KEY_SIZE (16)

static char* PrintMsg[] =
                 { 
                   "Process successfully",
                   "UnKnown RSA Type",
                   "Wrong args num",
                   "The file size is less than zero",
                   "Failed to malloc memory for ",
                   "Null input params",
                   "Can't malloc a memory with size zero",
                   "The output length should be twice as input length",
                   "The tag is not a universal tag, please check you input file",
                   "The real filesize is not matched with the value in the tag",
                   "The RSA length type is wrong",
                   "The place should be an INTEGR tag, the file may not be a proper file",
                   "The length of Modulus is wrong",
                   "Failed to parse PEM key file",
                   "Failed to read",
                   "Failed to parse binary file",
                   "Wrong PEM file format"
                 };
typedef enum returnType
{
    ProcessOK = 0,
    ErrorUnKnownSignHi3519V101RSAType,
    ErrorWrongArgsNum,
    ErrorWrongFileSize,
    ErrorFailedToMalloc,
    ErrorNullInputMsg,
    ErrorZeroMallocSize,
    ErrorOutputLengthShouldBeTwiceAsInputLength,
    ErrorNullUniversalTag,
    ErrorFileSizeNotMatched,
    ErrorRSALengthTypeFile,
    ErrorWrongINTEGERTag,
    ErrorInvalidModulusLength,
    ErrorFailedToParsePEMFile,
    ErrorFailedToRead,
    ErrorFailedToParseBinFile,
    ErrorWrongPEMFormat,
}ReturnType;

extern HI_CHIPSET_TYPE_EN g_enChipsetType;
extern unsigned long g_ulHeadAreaLength;
extern ENCMethod g_encMethod;
extern SIGMethod g_sigMethod;

static void GenCrcTable();
unsigned long DVB_CRC32(unsigned char *buf, int length);
unsigned int crc32 (unsigned int crc, const unsigned char* buf, unsigned int len);

void SpaceStrTrim(char*pStr);
void ParseFile(char *pText, const char *pTitle, char *pOutput, unsigned int outputBufLen);
int GetDirName(char *tmp, char *dir);
int GetValueofTitle(const char *pConfigFile, char* pTitle, char *pOutput, unsigned int outputBufLen);
bool AddEncryptFileContent(const char* pDestFilePath, char* pSrcFilePath, bool bEncrypt, char* pEncryptKey);
bool AddEncryptCheckedAreaContent_V400(const char *pDestFilePath, char *pSrcFilePath, bool bEncrypt, 
    char* pEncryptKey, unsigned int offset, unsigned int tailLength, bool bBLPK);
void SetChipsetType(HI_CHIPSET_TYPE_EN enChipsetType);
HI_SIG_VERSION_EN GetSigVersion(HI_CHIPSET_TYPE_EN enChipsetType);
bool AESTxtKeyToBinaryKey(char *pTxtKey, char *pBinaryKey);
bool TxtKeyToBinKey(char *pTxtKey, char *pBinKey, unsigned int binLen);
bool MergePathName(char *pPath, char *pName, char *pOutput, unsigned int outputBufLen);
bool SplitPathName(char *pFilePathName, char *pOutDirPath, int outDirPathLen, char *pOutFileName, int outFileNameLen);

void GlobalKeyEncryptBLPK(char *pBinaryGlobalKey, char *pBinaryBLPK, char *encryptedBLPK);
void GlobalKeyDecryptBLPK(char *pBinaryGlobalKey, char *pBinaryBLPK, char *pDecryptedBLPK);
bool isPrimeNumber(unsigned int number);
bool IsValidHex(char* pText);
bool isValidWord(char *pMsidVersion);
bool isValidNumber(char *pText);
void mkdir_p(char* pPath);
long GetSizeFromPath(char* pFilePath);
long GetSizeFromPointer(FILE* pFile);
unsigned int Char2Hex(char *pDataBuf);
bool BinaryToTxt(const unsigned char *pBinaryData, unsigned char *pTxtData, unsigned long binaryLen, unsigned long txtLen);
int GetMyFileSize(const char *path);
bool isValidMSIDandVersion(char *pMsid, char *pVersion);
bool isValidPosDec(char *pText);
bool OpenMyFile(FILE **fp, const char *srcFilePath, char *mode);
size_t calcDecodeLength(char* b64input, int inLen);  
int Base64Decode(UCHAR* b64message, int inLen, UCHAR** buffer, size_t* length);
bool legalInput(char *inputStr, int inputLength);
bool ChangeEndian(unsigned char *pData, unsigned int len);
void ChangeEndian_uint(unsigned int *ref);
int ParsePKFile(char* keyFile, char *binaryKey, unsigned int keyLen);
BOOL SetTruth(char *strIfEncryptBoot, char *param);

int MpiWriteBuf(rsa_context rsa, int rsaLen, unsigned char *buf);

#ifdef __cplusplus
}
#endif

#endif // COMMONMETHOD_H
