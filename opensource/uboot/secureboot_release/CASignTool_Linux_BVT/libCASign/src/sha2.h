#ifndef SHA2_H
#define SHA2_H
#include "priv_signTool.h"

int isSignatureValid( unsigned char *input, int ilen, unsigned char output[32]);

#endif // SHA2_H

