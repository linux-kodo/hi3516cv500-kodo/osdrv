#include <ctype.h>
#include <math.h>
#include <unistd.h>

#include "commonmethod.h"


HI_CHIPSET_TYPE_EN g_enChipsetType = Hi3798CV200;
ENCMethod g_encMethod = TypeAES_CBC;
SIGMethod g_sigMethod = TypeSHA256;

const char * base64char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";


unsigned long g_ulHeadAreaLength = 0;


static unsigned long  _CrcTable[256];          /*Calculate CRC32*/
static short _First = 1;                       /*Init _CrcTable[256] Flag, Only init once time*/
#define     POLYNOMIAL 0x04c11db7L
/*
 * CRC-32 algorithm
 */
static unsigned crc_table[256] = 
{
    0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L,
    0x706af48fL, 0xe963a535L, 0x9e6495a3L, 0x0edb8832L, 0x79dcb8a4L,
    0xe0d5e91eL, 0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L,
    0x90bf1d91L, 0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
    0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L, 0x136c9856L,
    0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL, 0x63066cd9L,
    0xfa0f3d63L, 0x8d080df5L, 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L,
    0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
    0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L,
    0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L, 0x26d930acL, 0x51de003aL,
    0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L,
    0xb8bda50fL, 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
    0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL, 0x76dc4190L,
    0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL,
    0x9fbfe4a5L, 0xe8b8d433L, 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL,
    0xe10e9818L, 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
    0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL, 0x6c0695edL,
    0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L, 0x65b0d9c6L, 0x12b7e950L,
    0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L,
    0xfbd44c65L, 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
    0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL, 0x4369e96aL,
    0x346ed9fcL, 0xad678846L, 0xda60b8d0L, 0x44042d73L, 0x33031de5L,
    0xaa0a4c5fL, 0xdd0d7cc9L, 0x5005713cL, 0x270241aaL, 0xbe0b1010L,
    0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
    0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L,
    0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL, 0xedb88320L, 0x9abfb3b6L,
    0x03b6e20cL, 0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L,
    0x73dc1683L, 0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
    0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L, 0xf00f9344L,
    0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL, 0x806567cbL,
    0x196c3671L, 0x6e6b06e7L, 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL,
    0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
    0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L,
    0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL, 0xd80d2bdaL, 0xaf0a1b4cL,
    0x36034af6L, 0x41047a60L, 0xdf60efc3L, 0xa867df55L, 0x316e8eefL,
    0x4669be79L, 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
    0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL, 0xc5ba3bbeL,
    0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L,
    0x2cd99e8bL, 0x5bdeae1dL, 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL,
    0x026d930aL, 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
    0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L, 0x92d28e9bL,
    0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L, 0x86d3d2d4L, 0xf1d4e242L,
    0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L,
    0x18b74777L, 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
    0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L, 0xa00ae278L,
    0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L, 0xa7672661L, 0xd06016f7L,
    0x4969474dL, 0x3e6e77dbL, 0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L,
    0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
    0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L,
    0xcdd70693L, 0x54de5729L, 0x23d967bfL, 0xb3667a2eL, 0xc4614ab8L,
    0x5d681b02L, 0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL,
    0x2d02ef8dL
};

#define DO1(buf) crc = crc_table[((int)crc ^ (*buf++)) & 0xff] ^ (crc >> 8);
#define DO2(buf) DO1(buf); DO1(buf);
#define DO4(buf) DO2(buf); DO2(buf);
#define DO8(buf) DO4(buf); DO4(buf);

static void GenCrcTable()
{
    unsigned int i, j;
    unsigned long crc_accum;
    for( i = 0;  i < 256;  i++ )
    {
        crc_accum =  ( i << 24 );
        for( j = 0;  j < 8;  j++ )
        {
            if( crc_accum & 0x80000000L )
            {
                crc_accum = ( crc_accum << 1 ) ^ POLYNOMIAL;
            }
            else
            {
                crc_accum = ( crc_accum << 1 );
            }
        }
        _CrcTable[i] = crc_accum;
    }
    return;
}


unsigned long DVB_CRC32(unsigned char *buf, int length)
{
    unsigned long crc = 0xffffffff;

    if(_First)
    {
        GenCrcTable();
        _First = 0;
    }
    while( length-- )
    {
        crc = (crc << 8) ^ _CrcTable[((crc >> 24) ^ *buf++) & 0xff];
    }

    return crc;
}

unsigned int crc32 (unsigned int crc, const unsigned char* buf, unsigned int len)
{
    crc = crc ^ 0xffffffffL;
    while (len >= 8)
    {
        DO8(buf);
        len -= 8;
    }

    if (len)
    {
        do
        {
            DO1(buf);
        } while (--len);
    }

    return crc ^ 0xffffffffL;
}


void SpaceStrTrim(char *pStr)
{
    char *pTmp = pStr;

    //while((*pStr != '\0')&&(*pStr != '\r')&&(*pStr != '\n'))
    while((*pStr != '\0'))
    {
        if(*pStr != ' ')
        {
            *pTmp++ = *pStr;
        }

        ++pStr;
    }

    *pTmp = '\0';
}


// read ini param, if cannot found the title, then not change the pOutput.
void ParseFile(char *pText, const char *pTitle, char *pOutput, unsigned int outputBufLen)
{
    BOOL bTempRet, bTempRet1;
    bTempRet = (NULL == pText || NULL == pTitle || NULL == pOutput || '\0' == pTitle[0]) || ('#' == pText[0]);
    if (bTempRet)
    {
        return;
    }

    int textLen = strlen(pText);
    int offset = 0;
    if(1 == textLen)
    {
        return;
    }
    bTempRet = (LF_ASCII == pText[textLen - 1] && CR_ASCII == pText[textLen - 2]);
    bTempRet1 = (LF_ASCII == pText[textLen -1] && CR_ASCII != pText[textLen - 2]);
    if (bTempRet) 
    {
        offset = 2;
    }
    else if (bTempRet1) 
    {
        offset = 1;
    }
    
    if('#' == pText[0]) 
    {
        return;
    }
    int titleLength = strlen(pTitle);
    char *ptr = NULL;
    bTempRet = (0 == strncmp(pText, pTitle, titleLength) && '=' == *(pText + titleLength));
 
    if (bTempRet)
    {
        ptr = pText + (titleLength + 1);
        int outputLen = strlen(ptr) - offset;
        STRNCPY_S(pOutput, outputBufLen, ptr, outputLen,  "parsing config file", return;); 
    }
}

int GetValueofTitle(const char *pConfigFile, char* pTitle, char *pOutput, unsigned int outputBufLen)
{
    BOOL bTempRet = (NULL == pConfigFile || NULL == pTitle || NULL == pOutput);
    if (bTempRet) 
    {
        return -1;
    }

    FILE *fp = NULL;
    char tmp[PATH_MAX] = {0};
    if(!(OpenMyFile(&fp, pConfigFile, "r")))
    {
        return -1;
    }

    do
    {
        MEMSET_S(tmp, sizeof(tmp), 0, sizeof(tmp), "parsing config file", SAFE_CLOSE(fp); return -1;);
        if(NULL == fgets(tmp, PATH_MAX, fp))
        {
            if(ferror(fp))
            {
                 printf("Failed to get content of file.\n");
                 SAFE_CLOSE(fp);
                 return -1;
            }
        }
        SpaceStrTrim(tmp);
        ParseFile(tmp, pTitle, pOutput, outputBufLen);  
        
    }while(!feof(fp));
    SAFE_CLOSE(fp);
    return 0;
}

bool AddEncryptFileContent(const char *pDestFilePath, char *pSrcFilePath, bool bEncrypt, char* pEncryptKey)
{
    BOOL bTempRet =  (NULL == pDestFilePath || NULL == pSrcFilePath);
    if (bTempRet)
    {
        return false;
    }
    bool ret = true;
    FILE* fpSrc = NULL;
    FILE* fpDest = NULL;
    long fileSize = 0;
    char* pData = NULL;

    // get src file size
    if(!OpenMyFile(&fpSrc, pSrcFilePath, "rb+")) 
    {
        ret = false;
        goto ScopeRelease;
    }
    fileSize = GetSizeFromPointer(fpSrc);
    if(fileSize < 0)
    {
        printf("Failed malloc, the size should be larger than 0.\n");
        goto ScopeRelease;
    }
    else if (0 == fileSize) 
    {
        //printf("The multi param area is empty!\n");
        goto ScopeRelease;
    }

    pData = (char*)malloc(fileSize);
    
    if(NULL == pData) 
    {
        printf("failed to malloc memory\n");
        ret = false;
        goto ScopeRelease;
    }
    MEMSET_S(pData, fileSize, 0, fileSize, "merging area", ret = false;  goto ScopeRelease;);

    if(1 != fread(pData, fileSize, 1, fpSrc))
    {
        printf("failed to read %s.\n", pSrcFilePath);
        ret = false;
        goto ScopeRelease;
    }

    if(bEncrypt) 
    {
        if(NULL == pEncryptKey) 
        {
            printf("EncryptKey is NULL.\n");
            ret = false;
            goto ScopeRelease;
        }
        aes_context ctx;
        unsigned char iv[16] = {0};
        unsigned char blpk[AES_ENCRYPT_KEY_SIZE] = {0};
        MEMCPY_S(blpk, AES_ENCRYPT_KEY_SIZE, pEncryptKey, AES_ENCRYPT_KEY_SIZE, "merging area", ret = false; goto ScopeRelease;);
        aes_setkey_enc(&ctx, blpk, KEY_BIT_LEN);
        aes_crypt_cbc(&ctx, AES_ENCRYPT, fileSize, iv, (UCHAR*)pData, (UCHAR*)pData);
    }
    
    if(!OpenMyFile(&fpDest, pDestFilePath, "ab"))
    {
        printf("Failed to open dest file %s\n", pDestFilePath);
        ret = false;
        goto ScopeRelease;
    }
        
    if(1 != fwrite(pData, fileSize, 1, fpDest)) 
    {
        printf("failed to write %s.\n", pDestFilePath);
        ret = false;
        goto ScopeRelease;
    }

ScopeRelease:
    SAFE_CLOSE(fpSrc);
    SAFE_FREE(pData);
    SAFE_CLOSE(fpDest);
    return ret;
}

bool AddEncryptCheckedAreaContent_V400(const char *pDestFilePath, char *pSrcFilePath, bool bEncrypt, 
    char* pEncryptKey, unsigned int offset, unsigned int tailLength, bool bBLPK)
{
    BOOL bTempRet = (NULL == pDestFilePath || NULL == pSrcFilePath) ;
    if(bTempRet) 
    {
        return false;
    }

    bool ret = true;
    FILE* fpSrc = NULL;
    FILE* fpDest = NULL;
    long fileSize = 0;
    long checkedAreaLength = 0;
    long checkedAreaLengthWithoutTail = 0;
    UCHAR* pData = NULL;
    UCHAR* pCryptoData = NULL;
    UCHAR* pTailData = NULL;
    int alignSize = 16;

    // get total src file size
    if(Hi3798MV310 == g_enChipsetType)
    {
        offset = 0;
    }
    
    if(!OpenMyFile(&fpSrc, pSrcFilePath, "rb"))
    {
        ret = false;
        goto ScopeRelease;
    }
    if(0 != fseek(fpSrc, 0, SEEK_END)) 
    {
        printf("failed to seek file %s.\n", pSrcFilePath);
        ret = false;
        goto ScopeRelease;
    }
    fileSize = ftell(fpSrc);
    if(fileSize <= 0) 
    {
        printf("Failed to get size of file %s.\n", pSrcFilePath);
        ret = false;
        goto ScopeRelease;
    }
    if(0 != fseek(fpSrc, 0, SEEK_SET)) 
    {
        printf("failed to seek file %s.\n", pSrcFilePath);
        ret = false;
        goto ScopeRelease;
    }
    checkedAreaLength = fileSize- offset;
    checkedAreaLengthWithoutTail = checkedAreaLength - tailLength;

    if(checkedAreaLength <= 0)
    {
        printf("Failed malloc, the size should be larger than 0.\n");
        goto ScopeRelease;
    }

    SAFE_MALLOC(char*, pData, (unsigned int)checkedAreaLength, "merging content", ret = false; goto ScopeRelease;);
    MEMSET_S(pData, checkedAreaLength, 0, (int)checkedAreaLength, "merging content", ret = false; goto ScopeRelease;);

    if(0 != fseek(fpSrc, offset, SEEK_SET)) 
    {
        printf("failed to seek file %s.\n", pSrcFilePath);
        ret = false;
        goto ScopeRelease;
    }
    
    if(1 != fread(pData, checkedAreaLength, 1, fpSrc)) 
    {
        printf("failed to read %s.\n", pSrcFilePath);
        ret = false;
        goto ScopeRelease;
    }

    // encrypt
    if(NULL != pEncryptKey && bEncrypt && bBLPK)  
    {
        unsigned char iv[16] = {0};
        unsigned char blpk[AES_ENCRYPT_KEY_SIZE] = {0};

        MEMCPY_S(blpk, AES_ENCRYPT_KEY_SIZE, pEncryptKey, AES_ENCRYPT_KEY_SIZE, "adding content", ret = false; goto ScopeRelease;);
                
        if(TypeAES_CBC == g_encMethod)
        {
            aes_context ctx;
            aes_setkey_enc(&ctx, blpk, KEY_BIT_LEN);
            aes_crypt_cbc(&ctx, AES_ENCRYPT, checkedAreaLength, iv, (UCHAR*)pData, (UCHAR*)pData);
        }
    }
    
    if(!OpenMyFile(&fpDest, pDestFilePath, "ab")) 
    {
        printf("Open the file %s failed\n", pDestFilePath);
        ret = false;
        goto ScopeRelease;
    }
    
    if(1 != fwrite(pData, checkedAreaLength, 1, fpDest)) 
    {
        printf("failed to write %s.\n", pDestFilePath);
        ret = false;
        goto ScopeRelease;
    }    
    
ScopeRelease:
    SAFE_CLOSE(fpSrc);
    SAFE_FREE(pData);
    SAFE_CLOSE(fpDest);
    SAFE_FREE(pTailData);
    SAFE_FREE(pCryptoData);
    return ret;
}

void SetChipsetType(HI_CHIPSET_TYPE_EN enChipsetType)
{
    g_enChipsetType = enChipsetType;
    switch(g_enChipsetType)
    {
    case Hi3798CV200:
    case Hi3798MV100:
    case Hi3716MV310:
    case Hi3796MV100:
    case Hi3716DV100:
    case Hi3110EV500:
    case Hi3716MV320:
    case Hi3716MV410:
    case Hi3716MV420:
    case Hi3716MV330:
        g_ulHeadAreaLength = BOOT_AREA_OFFSET_MV300;
        break;
    default:
        break;
    }
}

HI_SIG_VERSION_EN GetSigVersion(HI_CHIPSET_TYPE_EN enChipsetType)
{
    BOOL bTempRet2, bTempRet3;
    
    bTempRet2 = ( 
            Hi3798MV100 == enChipsetType || Hi3716MV310 == enChipsetType || 
            Hi3796MV100 == enChipsetType || Hi3716MV320 == enChipsetType || 
            Hi3110EV500 == enChipsetType || Hi3716MV410 == enChipsetType || 
            Hi3716MV330 == enChipsetType || Hi3716DV100 == enChipsetType);

    bTempRet3 = (
        Hi3798CV200 == enChipsetType || Hi3751V620 == enChipsetType || 
        Hi3798MV200 == enChipsetType || Hi3716DV110 == enChipsetType || 
        Hi3751V810 == enChipsetType || Hi3751V811 == enChipsetType);

    if (bTempRet2 || (Hi3716MV420 == enChipsetType))
    {
        return HI_SIG_VERSION_V200;
    }
    else if (bTempRet3)
    {
        return HI_SIG_VERSION_V300;
    }
    else if (Hi3796MV200 == enChipsetType || Hi3798MV310 == enChipsetType || 
        Hi3716MV450 == enChipsetType || Hi3716MV430 == enChipsetType) 
    {
        return HI_SIG_VERSION_V400;
    }
    else 
    {
        return HI_SIG_VERSION_BUTT;
    }
}

bool AESTxtKeyToBinaryKey(char *pTxtKey, char *pBinaryKey)
{
    BOOL bTempRet;
    char ucTmpBuf[3] = {0};
    unsigned int u32Value;
    unsigned int i;
    
    bTempRet = (NULL == pTxtKey || NULL == pBinaryKey);
    if(bTempRet)
    {
        printf("input or output key is NULL.\n");
        return false;
    }

    if(0 == strncasecmp(pTxtKey, "0x", 2)) 
    {
        pTxtKey += 2;
    }

    if(AES_ENCRYPT_KEY_SIZE * 2 != strlen(pTxtKey)) 
    {
        printf("%s Key's length should be %d.\n.", pTxtKey, AES_ENCRYPT_KEY_SIZE * 2);
        return false;
    }

    if(!IsValidHex(pTxtKey)) 
    {
        printf("%s Key should be HEX.\n.", pTxtKey);
        return false;
    }
   
    MEMSET_S(ucTmpBuf, sizeof(ucTmpBuf), 0x0, sizeof(ucTmpBuf), "transforming format", return false;);
    for(i = 0; i < AES_ENCRYPT_KEY_SIZE; i++)
    {
        ucTmpBuf[0] = *(pTxtKey + i * 2);
        ucTmpBuf[1] = *(pTxtKey + i * 2 + 1);
        u32Value = strtoul((char*)ucTmpBuf, NULL, 16);
        pBinaryKey[i] = u32Value;
    }

    return true;
}

bool TxtKeyToBinKey(char *pTxtKey, char *pBinKey, unsigned int binLen)
{
    BOOL bTempRet = (NULL == pTxtKey || NULL == pBinKey);
    if (bTempRet) 
    {
        printf("input or output key is NULL.\n");
        return false;
    }

    if(0 == strncasecmp(pTxtKey, "0x", 2)) 
    {
        pTxtKey += 2;
    }

    unsigned int txtLen = strlen(pTxtKey);
    if(txtLen & 1) 
    {
        printf("Length of %s should be a multiple of 2.\n.", pTxtKey);
        return false;
    }
    if(binLen * 2 < txtLen) 
    {
        printf("%s is too long to be processed.\n.", pTxtKey);
        return false;
    }

    char tmpBuf[3] = {0};
    unsigned int index;
    unsigned int realBinLen = txtLen >> 1;
    for(index = 0; index < realBinLen; index++) 
    {
        MEMCPY_S(tmpBuf, 3, pTxtKey + index * 2, 2, "transforming txt to bin", return false;);
        pBinKey[index] = strtoul(tmpBuf, NULL, 16);
    }

    return true;
}

bool MergePathName(char *pPath, char *pName, char *pOutput, unsigned int outputBufLen)
{
    BOOL bTempRet = (NULL == pPath || NULL == pName || NULL == pOutput);
    if (bTempRet)
    {
        printf("The input buffer param should not be null!\n");
        return false;
    }
    
    unsigned int pathLength = strlen(pPath);
    unsigned int nameLength = strlen(pName);
    char pOutputBuffer[PATH_MAX] = {0};
    
    STRNCPY_S(pOutputBuffer, outputBufLen, pPath, pathLength, "merging path name", return false;);
    
    if(0 != pathLength && '/' != pPath[pathLength - 1]) 
    {
        STRNCAT_S(pOutputBuffer, outputBufLen, "/", strlen("/"), "merging path name", return false;);
    }
    STRNCAT_S(pOutputBuffer, outputBufLen, pName, nameLength, "merging path name", return false;);
    STRNCPY_S(pOutput, outputBufLen, pOutputBuffer, outputBufLen, "merging path name", return false;);
    return true;
}

void GlobalKeyEncryptBLPK(char *pBinaryGlobalKey, char *pBinaryBLPK, char *pEncryptedBLPK)
{
    if(g_encMethod == TypeAES_CBC)
    {
        des3_context des3ctx;
        unsigned int i = 0;

        des3_set2key_enc(&des3ctx, (unsigned char*)pBinaryGlobalKey);
        for(i = 0; i < 2; i++)
        {
            des3_crypt_ecb(&des3ctx, (unsigned char*)pBinaryBLPK + 8 * i, (unsigned char*)pEncryptedBLPK + 8 * i);
        }
    }
}

void GlobalKeyDecryptBLPK(char *pBinaryGlobalKey, char *pBinaryBLPK, char *pDecryptedBLPK)
{
    if(g_encMethod == TypeAES_CBC)
    {
        des3_context des3ctx;
        unsigned int i = 0;

        des3_set2key_dec(&des3ctx, pBinaryGlobalKey);
        for(i = 0; i < 2; i++)
        {
            des3_crypt_ecb(&des3ctx, pBinaryBLPK + 8 * i, pDecryptedBLPK + 8 * i);
        }
    }
}

bool isPrimeNumber(unsigned int number)
{
    if(number < 3) {
        return false;
    }
    else if(3 == number) {
        return true;
    }
    else {
        unsigned int index = 0;
        unsigned int judgeIndexMax = sqrt(number);
        for(index = 2; index <= judgeIndexMax; ++index) {
            if(number % index == 0) {
                return false;
            }
        }
    }

    return true;
}

bool isDec(char *pText){
    if(NULL == pText) {
        return false;
    }
    int len = strlen(pText);
    int index = 0;
    for(index = 0; index < len; index++){
        if(!((pText[index]<='9')&&(pText[index]>='0'))){
            return false;
        }
    }
    return true;
}

bool IsValidHex(char *pText)
{   
    int len = 0;
    int index = 0;
    if(NULL == pText) 
    {   
        return false;
    }

    if(0 == strncasecmp(pText, "0x", 2)) 
    { 
        pText += 2;
    } 

    len = strlen(pText);
   
    for(index = 0; index < len; ++index) 
    {
        if(pText[index] < '0' || (pText[index] > '9' && pText[index] < 'A') 
            || (pText[index] > 'F' && pText[index] < 'a') || pText[index] > 'f') 
        {
            return false;
        }
    }

    return true;
}

bool isValidWord(char *pMsidVersion)
{
    if(NULL == pMsidVersion) 
    {
        return false;
    }

    if(0 == strncasecmp(pMsidVersion, "0x", 2)) 
    {    
        pMsidVersion += 2;
    }

    if(strlen(pMsidVersion) > 8) 
    {
        return false;
    }

    if(!IsValidHex(pMsidVersion))
    {
        return false;
    }

    return true;
}

bool isValidMSIDandVersion(char *pMsid, char *pVersion)
{
    bool nRet = true;
    bool nMsidRet = true;
    bool nVersionMask = true;
    if (!isValidWord(pMsid))
    {
        printf("MSID should be HEX and not over 4 bytes(8 characters).\n");
        nMsidRet = false;
    }

    if (!isValidWord(pVersion))
    {
        printf("Version should be HEX and not over 4 bytes(8 characters).\n");
        nVersionMask = false;
    }
    nRet = nMsidRet&&nVersionMask;
    return nRet;
}

bool isValidNumber(char *pText)
{
    if(NULL == pText) 
    {
        return false;
    }
    BOOL bTempRet;

    if(0 == strncasecmp(pText, "0x", 2)) { // HEX
        pText += 2;

        int len = strlen(pText);
        int index = 0;
        for(index = 0; index < len; ++index) {
            bTempRet = (pText[index] < '0' || (pText[index] > '9' && pText[index] < 'A') || (pText[index] > 'F' && pText[index] < 'a') || pText[index] > 'f');
            if (bTempRet){
                return false;
            }
        }
    }
    else { //DEC (or others?
        int len = strlen(pText);
        int index = 0;
        for(index = 0; index < len; ++index) {
            bTempRet = (pText[index] < '0' || pText[index] > '9');
            if (bTempRet){
                return false;
            }
        }
    }

    return true;
}

bool isValidPosDec(char *pText) 
{
    if(NULL == pText)
    {
        return false;
    }
    if('-' == pText[0])
    {
        return false;
    }
    BOOL bTempRet;
    int len = strlen(pText);
    int index = 0;
    for(index = 0; index < len; ++index) 
    {
        bTempRet = (pText[index] < '0' || pText[index] > '9');
        if (bTempRet)
        {
            return false;
        }
    }
    return true;
}

bool SplitPathName(char *pFilePathName, char *pOutDirPath, int outDirPathLen, char *pOutFileName, int outFileNameLen)
{
    BOOL nTempRet = (NULL == pFilePathName || NULL == pOutDirPath || NULL == pOutFileName);
    if (nTempRet) 
    {
        return false;
    }

    int len = strlen(pFilePathName);
    int index;
    for (index = len - 1; index >= 0; --index) 
    {
        if('/' == pFilePathName[index]) 
        {
            break;
        }
    }
    nTempRet = (-1 == index || index == len - 1);
    if (nTempRet)
    {
        return false;
    }

    int dirLen = index + 1;
    STRNCPY_S(pOutDirPath, outDirPathLen, pFilePathName, dirLen, "splitting path name", return false;);

    int fileLen = len - 1 - index;
    STRNCPY_S(pOutFileName, outFileNameLen, pFilePathName + index + 1, fileLen, "splitting path name", return false;);

    return true;
}

void mkdir_p(char *pPath)
{
    if (NULL == pPath) 
    {
        return;
    }

    size_t len = 0;
    size_t len1 = 0;
    int ret = 0;
    char absPath[PATH_MAX] = {0};

    if(absPath != realpath(pPath, absPath))
    {
        if(ENOENT == errno)
        {
            STRNCPY_S(absPath, PATH_MAX, pPath, strlen(pPath), "making directory", return;);
        }
        else
        {
            printf("Failed to transform the path, or the file path does not exist: %s\n", pPath);
            return;
        }
    }

    len = strlen(absPath);
    if(0 == len) 
    {
        printf("Path is empty.\n");
        return;
    }
    len1 = len + 2;
    if(len1 > PATH_MAX)
    {
        printf("Failed to calculate the length.\n");
        return;
    }
    
    char *pTempPath = (char*)malloc(len1);
    if(NULL == pTempPath) 
    {  
        return;
    }
    
    MEMSET_S(pTempPath, len1, 0, len1, "making directory", SAFE_FREE(pTempPath); return;);
    STRNCPY_S(pTempPath, len1, absPath, len, "making directory", SAFE_FREE(pTempPath); return;);

    if('/' != pTempPath[len - 1]) 
    {
        pTempPath[len] = '/';
    }

    len = strlen(pTempPath);
    int i = 0;
    for(i = 0; i < len; ++i) 
    {
        if('/' == pTempPath[i])
        {
            char temp = pTempPath[i + 1];
            pTempPath[i + 1] = '\0';

            ret = mkdir(pTempPath, 0777);
            if(0 != ret) 
            {
                //printf("The directory %s has already existed.\n", pTempPath);
            }
            else 
            {
                printf("mkdir directory %s\n", pTempPath);
            }
            pTempPath[i + 1] = temp;
        }
    }
    SAFE_FREE(pTempPath)
}

long GetSizeFromPath(char *pFilePath)
{
    if(NULL == pFilePath)
    {
        return -1;
    }

    FILE *pFile = NULL;
    if(!OpenMyFile(&pFile, pFilePath, "rb")) 
    {
        return -1;
    }

    long fileSize = GetSizeFromPointer(pFile);
    SAFE_CLOSE(pFile)

    return fileSize;
}

long GetSizeFromPointer(FILE *pFile)
{
    if(NULL == pFile) 
    {
        return -1;
    }

    long curOffset = ftell(pFile);
    if(-1 == curOffset) 
    {
        return -1;
    }

    if(0 != fseek(pFile, 0, SEEK_END)) 
    {
        return -1;
    }

    long fileSize = ftell(pFile);

    if(0 != fseek(pFile, curOffset, SEEK_SET)) 
    {
        return -1;
    }

    return fileSize;
}

unsigned int Char2Hex(char *pDataBuf)
{
    unsigned char u8TmpBuf[16] = {0};
    unsigned int u32Value;
    MEMSET_S(u8TmpBuf, sizeof(u8TmpBuf), 0, sizeof(u8TmpBuf), "transforming format", return 0;);
    u8TmpBuf[0] = pDataBuf[0];
    u8TmpBuf[1] = pDataBuf[1];
    u32Value = strtoul((const char *)u8TmpBuf, 0, 16);
    return u32Value;
}

bool BinaryToTxt(const unsigned char *pBinaryData, unsigned char *pTxtData, unsigned long binaryLen, unsigned long txtLen)
{
    BOOL bTempRet = (NULL == pBinaryData || NULL == pTxtData);
    if (bTempRet)
    {
        printf("input or output NULL.\n");
        return false;
    }
    
    if(binaryLen *2 > txtLen)
    {
        printf("The txtbuf should have more than %lu Bytes space.\n", binaryLen *2);
        return false;
    }

    int i;
    for(i = 0; i < binaryLen; i++)
    {
        char c[3] = {0};        
        SPRINTF_S(c, sizeof(c), "%02X", pBinaryData[i], "transforming hex to char", ;);
        pTxtData[2*i] = c[0];
        pTxtData[2*i+1] = c[1];
    }
    
    return true;
}

int GetMyFileSize(const char *path) 
{ 
    int fileSize = -1;  
    FILE *fp;

    if(!OpenMyFile(&fp, path, "rb+"))
    {
        printf("Failed to open file at path: %s!\n", path);
        return fileSize;
    }
    if(-1 == fseek(fp, 0, SEEK_END))
    { 
        printf("Failed to seek file: %s!\n", path);
        SAFE_CLOSE(fp)
        return fileSize;
    }
    fileSize = (int)ftell(fp);
    if(0 == fileSize)
    {
        printf("The file %s is empty!\n", path);
    }
    SAFE_CLOSE(fp)
    return fileSize;
}

bool OpenMyFile(FILE **fp, const char *srcFilePath, char *mode)
{
    char absFilePath[PATH_MAX] = {0};

    if(absFilePath != realpath(srcFilePath, absFilePath))
    {
        if(ENOENT == errno)  
        {
            STRNCPY_S(absFilePath, PATH_MAX, srcFilePath, PATH_MAX, "opening file", return false;);
        }
        else
        {
            printf("Failed to transform the path %s!\n", srcFilePath);
            return false;
        }
    }

    if (NULL == (*fp = fopen(absFilePath, mode)))
    {
        printf("Open the file %s failed, the error code is %d!\n", srcFilePath, errno);
        return false;
    }   
    return true;
}

size_t calcDecodeLength(char* b64input, int inLen)              //Calculates the length of a decoded string
{ 
    //size_t len = strlen(b64input);
    size_t padding = 0;

    if (b64input[inLen-1] == '=' && b64input[inLen-2] == '=')  //last two chars are =
    {
        padding = 2;
    }
    else if (b64input[inLen-1] == '=')                       //last char is =
    {
        padding = 1;
    }
    return (inLen*3)/4 - padding;
}

int Base64Decode(UCHAR* b64message, int inLen, UCHAR** buffer, size_t* length) //Decodes a base64 encoded string
{ 
    BIO *bio, *b64;
    int decodeLen = calcDecodeLength(b64message, inLen);
    int mallocSize = decodeLen + 1;
    if(mallocSize <= 0)
    {
        printf("You can't malloc with size less or equal to zero!\n");
        return -1;
    }
    *buffer = (unsigned char*)malloc(mallocSize);
    if(NULL == *buffer)
    { 
        printf("Failed to malloc!\n");
        return -1;
    }
    (*buffer)[decodeLen] = '\0';

    bio = BIO_new_mem_buf(b64message, -1);
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Do not use newlines to flush buffer
    *length = BIO_read(bio, *buffer, strlen(b64message));
    BIO_free_all(bio);

    return (0); //success    
}

bool legalInput(char *inputStr, int inputLength)
{
    int i = 0;
    int sumEqual = 0;
    if('#' == *inputStr)
    {
        return true;
    }
    for(i = 0; i< inputLength; i++)
    {
        if('=' == inputStr[i])
        {
            sumEqual++;
        }
    }
    if(sumEqual > 1)
    {
        printf("One line should have no more than one '=', please check your config file!\n");
        return false;
    }
    return true;
}


bool ChangeEndian(unsigned char *pData, unsigned int len)
{
    if(NULL == pData || len % 4) 
    {
        return false;
    }
    
    unsigned block = len >> 2;
    unsigned int *pBlock = (unsigned int*)pData;
    unsigned int i = 0;
    for(i = 0; i < block; ++i) 
    {
        ChangeEndian_uint(&pBlock[i]);
    }

    return true;
}

void ChangeEndian_uint(unsigned int *ref)
{
    unsigned int bit31_24 = ((*ref) & 0x000000FF) << 24;
    unsigned int bit23_16 = ((*ref) & 0x0000FF00) << 8;
    unsigned int bit15_8 = ((*ref) & 0x00FF0000) >> 8;
    unsigned int bit7_0 = (*ref) >> 24;

    *ref = bit31_24 | bit23_16 | bit15_8 | bit7_0;
}

int ParsePKFile(char* keyFile, char *binaryKey, unsigned int keyLen)
{ 
    FILE *fKeyFile = NULL;
    int ret = -1;
    char strTemp[COMPARE_LEN+1] = {0};
    char strTxtEncBLPK[AES_ENCRYPT_KEY_SIZE*2+1] = {0};
    if(keyLen != AES_ENCRYPT_KEY_SIZE)
    {
        printf("Illegal key file length!\n");
        return -1;
    }
    if(!OpenMyFile(&fKeyFile, keyFile, "rb+"))
    {
        printf("Failed to open blpk file!\n");
        ret = INPUT_FILE_NOT_EXIST;
        goto ScopeRelease;
    }
    long comparedLength = strlen(keyFile) - COMPARE_LEN;

    if(comparedLength < 0)
    {
        printf("The file path is invalid, Please set the path of blpk with suffix (.txt) or (.bin)!\n");
        goto ScopeRelease;
    }

    MEMCPY_S(strTemp, COMPARE_LEN + 1, keyFile + comparedLength, COMPARE_LEN, "parsing PK", goto ScopeRelease;);

    if(0 == strncmp(strTemp, ".txt", COMPARE_LEN))
    {
        if(1 != fread(strTxtEncBLPK, AES_ENCRYPT_KEY_SIZE*2, 1, fKeyFile))
        {
            printf("Failed to read blpk file: %s.\n", keyFile);
            goto ScopeRelease;
        }
        
        if(!IsValidHex(strTxtEncBLPK))
        {
            printf("Invalid Key!\n");
            goto ScopeRelease;
        }
        if(!AESTxtKeyToBinaryKey(strTxtEncBLPK, binaryKey))
        {
            printf("Failed to transform blpk!\n");
            goto ScopeRelease;
        }
    }
    else if(0 == strncmp(strTemp, ".bin", COMPARE_LEN))
    {
        if(1 != fread(binaryKey, AES_ENCRYPT_KEY_SIZE, 1, fKeyFile))
        {
            printf("Failed to read PK file!\n");
            goto ScopeRelease;
        }
    }
    else
    {
        printf("Please set the path of PK with suffix (.txt) or (.bin)\n");
        goto ScopeRelease;
    }
    ret = 0;
ScopeRelease:
    SAFE_CLOSE(fKeyFile);
    return ret;
    
}

BOOL SetTruth(char *strIfEncryptBoot, char *param)
{
    if (!strcasecmp(strIfEncryptBoot, "YES"))
    {
        return true;                   
    }
    else if (!strcasecmp(strIfEncryptBoot, "NO"))
    {
        return false;
    }
    else if(*strIfEncryptBoot != '\0')
    {
        printf("Unknown %s param(Please set NO or YES).\n", param);
        return false;
    }
    return false;
}

int MpiWriteBuf(rsa_context rsa, int rsaLen, unsigned char *buf)
{
    int ret = 0;
    ret = mpi_write_binary(&rsa->n, buf, rsaLen);
    if(0 != ret)
    {
        printf("Failed to write N into buffer!\n");
        return ret;
    }
    ret = mpi_write_binary(&rsa->e, buf + rsaLen, rsaLen); 
    if(0 != ret)
    {
        printf("Failed to write E into buffer!\n");
        return ret;
    }
    return ret;
}

