#ifndef __MADPTER_H
#define __MADPTER_H
#include "hi_type.h"
#include "sm4.h"
#include "hi_unf_cipher.h"
#include "hi_drv_cipher.h"
#include "bignum.h"


//#include "rm_sm4.h"


#define HASH_BLOCK_SIZE           (64)

#define SM3_BLOCK_SIZE 64

typedef struct
{
    unsigned int u32Hash[8];
    unsigned char u8LastBlock[SM3_BLOCK_SIZE];
    unsigned int u8LastBlockSize;
    unsigned int u32TotalDataLen;
}HASH_INFO_S;

#define FUNC_SM3_RAND 5

#define SM3_DIGEST_LENGTH 32

#ifdef __cplusplus
extern "C" {
#endif

#define SM3_BASE_ADDR 0x10150000
#define SM3_BUSY_FLAG 0x1

int sm4_crypt_ecb(const unsigned char *data_in, unsigned char *data_out, int data_len, const unsigned char *key, int bit, unsigned int decrypt);
int sm4_crypt_cbc(const unsigned char *data_in, unsigned char *data_out, int data_len, const unsigned char *key, int bit, unsigned int decrypt, unsigned char *iv);

#ifdef __cplusplus
}
#endif

#endif

