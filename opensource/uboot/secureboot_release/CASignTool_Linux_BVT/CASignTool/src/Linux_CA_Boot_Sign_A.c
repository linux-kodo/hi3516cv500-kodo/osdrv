#include <stdio.h>
#include <stdlib.h>
//#include <dlfcn.h>
#include <unistd.h>
#include <errno.h>
#include "priv_signTool.h"
#include "libCASign.h"

#define BASE_CMD_NUM 3
#define BASE_CMD_NUM_15 8
#define CREATE_KEY_FLAG "8"
#define VERSION_NUM "5.0.17"
#define COPYRIGHT_MESSAGE "Copyright (c) Hisilicon Technologies Co., Ltd. 2014 - 2017. All rights reserved."

static int ParseCryptoParam(char *pLParam, char *pRParam, CryptoParam *pCryptoParams)
{
    BOOL bTempRet = (NULL == pLParam || NULL == pRParam || NULL == pCryptoParams);

    if (bTempRet)
    {
        return -1;
    }

    int pathLen = strlen(pRParam);
    if (0 == strcmp(pLParam, "-R"))
    {
        STRNCPY_S(pCryptoParams->srcFilePath, sizeof(pCryptoParams->srcFilePath), pRParam, pathLen, "parsing crypto param -R", return -1;);
    }
    else if (0 == strcmp(pLParam, "-O")) 
    {
        STRNCPY_S(pCryptoParams->destFilePath, sizeof(pCryptoParams->destFilePath), pRParam, pathLen, "parsing crypto param -O", return -1;);
    }
    else if (0 == strcmp(pLParam, "-S"))
    {
        STRNCPY_S(pCryptoParams->signatureFilePath, sizeof(pCryptoParams->signatureFilePath), pRParam, pathLen, "parsing crypto param -S", return -1;);
    }
    else if (0 == strcmp(pLParam, "-K")) 
    {
        STRNCPY_S(pCryptoParams->keyFilePath, sizeof(pCryptoParams->keyFilePath), pRParam, pathLen, "parsing crypto param -K", return -1;);
    }
    else if (0 == strcmp(pLParam, "-it")) 
    {
        if(0 == strcmp(pRParam, "t")) 
        {
            pCryptoParams->inputType = TypeTXT;
        }
        else if(0 == strcmp(pRParam, "b")) 
        {
            pCryptoParams->inputType = TypeBIN;
        }
        else 
        {
            printf("Please type in proper input file type, t or b.\n");
            return -1;
        }
    }
    else if (0 == strcmp(pLParam, "-ot")) 
    {
        if(0 == strcmp(pRParam, "t")) 
        {
            pCryptoParams->outputType = TypeTXT;
        }
        else if(0 == strcmp(pRParam, "b"))
        {
            pCryptoParams->outputType = TypeBIN;
        }
        else 
        {
            printf("Please type in proper output file type, t or b.\n");
            return -1;
        }
    }
    return 0;
}

static void hint(char **argv)
{
    printf("Usage: %s -v \n", argv[0]);

    printf("Usage: %s 4 crypto_config.cfg [-it t/b -ot t/b] [-K KeyFilePath] -R srcFilePath -O destFilePath\n", argv[0]);
    printf("Usage: %s 8 eValue destKeyDirPath or %s 8 destKeyDirPath(Default eValue: 3)\n", argv[0], argv[0]);
    printf("Usage: %s 9 RSAPubKeyFilePath RSAPriKeyFilePath AESKeyFilePath DrrInitFilePath UBootFilePath OutputFilePath [TypePEM/TypePKCSBin/TypeTXT/no_enc]\n", argv[0]);
    printf("    or %s 9 RSAPubKeyFilePath RSAPriKeyFilePath AESKeyFilePath DrrInitFilePath UBootFilePath OutputFilePath [TypePEM/TypePKCSBin/TypeTXT] [no_enc](If the type is not set, typeTXT by default, enc by dafault)\n", argv[0]);
    printf("Usage: %s 19 cfgFile RSAPubKeyFilePath RSAPriKeyFilePath DrrInitFilePath UBootFilePath OutputFilePath\n", argv[0]);

}

static void MainPrintVersion(char **argv, int argc)
{

    bool tempRet = (2 == argc && 0 == strcmp(argv[1], "-v"));
    if(tempRet)
    {
        printf("CASignTool version %s. %s\n", VERSION_NUM, COPYRIGHT_MESSAGE);
        return;
    }
    hint(argv);
}

static int MainSignNonBootImageCrypto(char **argv, int argc)
{
    int ret = -1;
    if((argc - BASE_CMD_NUM) %2)
    {
        hint(argv);
        return ret;
    }
    CryptoParam* pCryptoParams = (CryptoParam *)malloc(sizeof(CryptoParam));
    if(NULL == pCryptoParams)
    {
        printf("Failed to malloc for CryptoParam struct. \n");
        return ret;
    }
    
    MEMSET_S(pCryptoParams, sizeof(CryptoParam), 0, sizeof(CryptoParam), "initializing", goto ScopeRelease;);
    int index = 0;
    for(index = 0; index < (argc - BASE_CMD_NUM); index += 2)
    {
        if(0 != ParseCryptoParam(argv[BASE_CMD_NUM + index], argv[BASE_CMD_NUM + index + 1], pCryptoParams))
        {
            goto ScopeRelease;
        }
    }
    ret = (*SignNonBOOTImage_Crypto_cfg)(argv[2], pCryptoParams);
ScopeRelease:
    SAFE_FREE(pCryptoParams)
    return ret;
}

static int MainCreateRSA(char **argv, int argc)
{
    int ret = -1;
    if(4 == argc) 
    {
        ret = (*CreateRSAKey)(argv[2], argv[3]);
    }
    else if(3 == argc) 
    {
        ret = (*CreateRSAKeyWithDefaultE)(argv[2]);
    }
    else 
    {
        hint(argv);
        ret = -1;
    }
    return ret;
}

static int mainSignHi3519V101(char **argv, int argc)
{
    int ret = -1;
    
    SignHi3519V101Param signHi3519V101Params;
    MEMSET_S(&signHi3519V101Params, sizeof(signHi3519V101Params), 0,sizeof(signHi3519V101Params), "initializing", return ret;);

    ret = ParseSignHi3519V101Param(argv, argc, &signHi3519V101Params);
    if (-1 == ret)
    {
        return ret;
    }
    ret = (*SignHi3519V101)(&signHi3519V101Params);
    return ret;
}

static int mainSignHi3556AV100(char **argv, int argc)
{
    int ret = -1;
    SignParam signParam;
    MEMSET_S(&signParam, sizeof(signParam), 0, sizeof(signParam), "initializing", return ret;);
//19 cfgFile RSAPubKeyFilePath RSAPriKeyFilePath DrrInitFilePath UBootFilePath OutputFilePath
    ret = ParseSignParam(argv, argc, &signParam);
    if(-1 == ret)
    {
        return ret;
    }
    if(0 == SignHi3556AV100(&signParam))
    {
       printf("Succeed!\n");
       return 0;
    }
    
    return -1;
}

static int mainBranchB(int nArgv, char **argv, int argc)
{
    int ret = -1;
    if(BranchCryptoImage == nArgv) 
    {
        ret = MainSignNonBootImageCrypto(argv, argc);  
    }
    else if(BranchCreateRSA == nArgv) 
    {
        ret = MainCreateRSA(argv, argc);
    }
    else if(BranchSignHi3519V101 == nArgv) 
    {
        ret = mainSignHi3519V101(argv, argc);   
    }
    else if(BranchSignHi3556AV100 == nArgv)
    {
        ret = mainSignHi3556AV100(argv, argc);
    }
    return ret;
}

int main(int argc, char **argv)
{
    if(argc < BASE_CMD_NUM)
    {
        MainPrintVersion(argv, argc);
        return 0;
    }

    int ret = -1;
    int nArgv = atoi(argv[1]);
    ret = mainBranchB(nArgv, argv, argc);
    return ret;
}


