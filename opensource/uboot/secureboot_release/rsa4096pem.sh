#! /bin/sh

[ -n "$1" ] || { 
	echo ""; 
	echo "usage:"; 
	echo "     rsa4096pem.sh <uboot bin> <secure uboot bin>"; 
	echo "     e.g." 
	echo "     rsa4096pem.sh u-boot.bin u-boot-secure.bin"; 
	echo ""; 
	exit 1; 
}

[ -n "$2" ] || {
	echo ""; 
	echo "usage:"; 
	echo "     rsa4096pem.sh <uboot bin> <secure uboot bin>"; 
	echo "     e.g." 
	echo "     rsa4096pem.sh u-boot.bin u-boot-secure.bin"; 
	echo ""; 
	exit 1; 
}

if [ -f $1 ]; then
	echo "get reginfo.bin from $1..."
else
	echo "no $1!!!"
	exit
fi

echo "======================================================================================"
echo "|                                     rsa4096pem                                      |"
echo "======================================================================================"
# generate rsa pub key and private key
if [ -f rsa4096pem/rsa_pub_4096_sha256.txt ]; then
	echo "use old rsa4096pem/rsa_pub.bin rsa4096pem/rsa_pub_4096_sha256.txt"
	./CASignTool 19 secure_boot.cfg rsa4096pem/rsa_pub_4096.pem rsa4096pem/rsa_priv_4096.pem ./ddr_init_reg_info.bin ./$1 ./$2
	./HASH rsa4096pem/rsa_pub_4096_sha256.txt
else
	echo "gen new rsa4096pem/rsa_pub.bin rsa4096pem/rsa_pub_4096_sha256.txt"
	openssl genrsa -out rsa4096pem/rsa_priv_4096.pem  4096
	openssl rsa -in rsa4096pem/rsa_priv_4096.pem -pubout -out rsa4096pem/rsa_pub_4096.pem

	./CASignTool 19 secure_boot.cfg rsa4096pem/rsa_pub_4096.pem rsa4096pem/rsa_priv_4096.pem ./ddr_init_reg_info.bin ./$1 ./$2

	dd if=./$2 of=rsa4096pem/rsa_pub_4096.bin bs=1 skip=16 count=1024
	./CASignTool 4 sha256.cfg -R rsa4096pem/rsa_pub_4096.bin -O rsa4096pem/rsa_pub_4096_sha256.txt
	./HASH rsa4096pem/rsa_pub_4096_sha256.txt
	./HASH rsa4096pem/rsa_pub_4096_sha256.txt > rsa4096pem/rsa4096_pem_hash_val.txt
fi
