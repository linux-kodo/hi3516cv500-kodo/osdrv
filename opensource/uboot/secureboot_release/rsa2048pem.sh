#! /bin/sh

[ -n "$1" ] || { 
	echo ""; 
	echo "usage:"; 
	echo "     rsa2048pem.sh <uboot bin> <secure uboot bin>"; 
	echo "     e.g." 
	echo "     rsa2048pem.sh u-boot.bin u-boot-secure.bin"; 
	echo ""; 
	exit 1; 
}

[ -n "$2" ] || {
	echo ""; 
	echo "usage:"; 
	echo "     rsa2048pem.sh <uboot bin> <secure uboot bin>"; 
	echo "     e.g." 
	echo "     rsa2048pem.sh u-boot.bin u-boot-secure.bin"; 
	echo ""; 
	exit 1; 
}

if [ -f $1 ]; then
	echo "get reginfo.bin from $1..."
else
	echo "no $1!!!"
	exit
fi

echo "======================================================================================"
echo "|                                     rsa2048pem                                      |"
echo "======================================================================================"
# generate rsa pub key and private key
if [ -f rsa2048pem/rsa_pub_2048_sha256.txt ]; then
	echo "use old rsa2048pem/rsa_pub.bin rsa2048pem/rsa_pub_2048_sha256.txt"
	./CASignTool 19 secure_boot.cfg rsa2048pem/rsa_pub_2048.pem rsa2048pem/rsa_priv_2048.pem ./ddr_init_reg_info.bin ./$1 ./$2
	./HASH rsa2048pem/rsa_pub_2048_sha256.txt
else
	echo "gen new rsa2048pem/rsa_pub.bin rsa2048pem/rsa_pub_2048_sha256.txt"
	openssl genrsa -out rsa2048pem/rsa_priv_2048.pem  2048
	openssl rsa -in rsa2048pem/rsa_priv_2048.pem -pubout -out rsa2048pem/rsa_pub_2048.pem

	./CASignTool 19 secure_boot.cfg rsa2048pem/rsa_pub_2048.pem rsa2048pem/rsa_priv_2048.pem ./ddr_init_reg_info.bin ./$1 ./$2

	dd if=./$2 of=rsa2048pem/rsa_pub_2048.bin bs=1 skip=16 count=512
	./CASignTool 4 sha256.cfg -R rsa2048pem/rsa_pub_2048.bin -O rsa2048pem/rsa_pub_2048_sha256.txt
	./HASH rsa2048pem/rsa_pub_2048_sha256.txt
	./HASH rsa2048pem/rsa_pub_2048_sha256.txt > rsa2048pem/rsa2048_pem_hash_val.txt
fi
