Welcome to use the hiregbin command line tool!

This tool is mainly used for the Hi35** chip to generate a regbin file according to an Excel file. 
(Only Excel files in the version 2007 or above are supported. Other Excel files in the versions below 2007 must be converted to the version 2007 or above first.)
The Windows and Linux operating systems are supported.

1. Function description of generating a regbin file

The steps are follows:

(1) In the command line, go to the directory where the hiregbin is located.


(2) Enter the following command:./hiregbin [excelFile] <outputFile>
excelFile and outputFile are mandatory parameters and specified by the user.
Example:
./hiregbin ./Hi35**.xlsm ./reg.bin

(3) Wait for the regbin file to be created successfully.
