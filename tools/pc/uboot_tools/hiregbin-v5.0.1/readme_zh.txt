欢迎使用hiregbin命令行工具!

此工具功能主要是：Hi35**芯片根据excel文件生成regbin文件。
(只支持2007版本及以上版本的xlsm格式文件，低于2007版本的格式文件需要转换成2007版本及以上版本的xlsm格式文件。)
支持Windows与Linux操作系统。

1.生成regbin文件功能说明

步骤如下：

(1)在命令行下，进入hiregbin所在目录；

(2)输入命令如下：./hiregbin [excelFile] [outputFile]
excelFile、outputFile为必须参数，由用户指定。
示例：
./hiregbin ./Hi35**.xlsm ./reg.bin

(3)等待制作完成。